/* global ShiftApp, window, document */
import React from 'react';

import Modal from './modal';
import LoginForm from './login-form';
import RegisterForm from './register-form';
import ShiftRegisterForm from './shift-widgets/shift-register-form';

class LoginRequired extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      session: null,
      show: false,
      form: 'login',
      redirect: props.to || window.location.pathname,
    };
  }

  componentDidMount() {
    this.session = ShiftApp.session.subscribe(data => {
      if (data.Authenticated === false) {
        if (ShiftApp.config.useCustomLoginScreen) {
          document.location = ShiftApp.config.logoutRedirect;
          return false;
        }
        return this.setState({ show: true });
      }
      return false;
    });
  }

  componentWillUnmount() {
    this.session.dispose();
  }

  toggleForm = () => {
    this.state.form === 'login' ? this.setState({ form: 'register' }) : this.setState({ form: 'login' });
  }

  hide = () => this.setState({ show: false });

  render() {
    const show = !window.localStorage.getItem('SessionToken') ||
      window.localStorage.getItem('SessionToken') === 'undefined' ||
      this.state.show;

    return (
      <span>
        {show &&
          <Modal noCloseOnClickOrKey opacity="0.93" close={this.hide} width="450px">
            {this.state.form === 'login' 
              ? <LoginForm hideCancelBtn hideCloseLink toggleForm={this.toggleForm} to={this.state.redirect} {...this.props} />
              : <ShiftRegisterForm toggleForm={this.toggleForm} {...this.props} />
            }
          </Modal>}
      </span>
    );
  }
}

export default LoginRequired;
