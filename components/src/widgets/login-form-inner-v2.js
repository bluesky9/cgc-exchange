/* global ShiftApp, document, localStorage, APConfig */
import React from 'react';
import InputNoLabel from '../misc/inputNoLabel';

class LoginFormInnerV2 extends React.Component {
  constructor() {
    super();

    this.state = {
      authyRequired: false,
      googleRequired: false,
      TwoFARequired: false,
      smsRequired: false,
      passwordReset: false,
      sessionToken: '',
      server: localStorage.getItem('tradingServer') || APConfig.API_V2_URL,
      showCustomServerInput: false,
      inputsFilled: { email: '', password: '' },
    };
  }

  getCookie = name => {
    const cookies = document.cookie.split(';');
    let found = null;

    for (let i = 0, len = cookies.length; i < len; i++) {
      const cookie = cookies[i].split('=');

      if (name === cookie[0].trim()) found = cookie[1];
    }
    return found;
  };


  createCookie = (name, value, days) => {
    let expires = '';

    if (days) {
      const date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = `; expires=${date.toUTCString()}`;
    }
    document.cookie = `${name}=${value}${expires}; path=/`;
  };

  signIn = (e) => {
    e.preventDefault();

    this.props.setBanner({
      information: ShiftApp.translation('COMMON.PLEASE_WAIT') || 'Please wait',
      error: '',
    });

    if (!this.refs.email.value()) {
      return this.props.setBanner({
        information: '',
        error: ShiftApp.config.useEmailAsUsername ?
          ShiftApp.translation('SIGNIN_MODAL.REQUEST_EMAIL') || 'Enter a User email' :
          ShiftApp.translation('SIGNIN_MODAL.REQUEST_USERNAME') || 'Enter a User name',
      });
    }
    if (!this.refs.password.value()) {
      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNIN_MODAL.REQUEST_PASSWORD') || 'Enter a Password',
      });
    }

    localStorage.setItem('isRefresh', 'false');

    if (this.state.googleRequired) {
      // send the 2FA code
      const data = { Code: this.refs.authCode.value() };

      ShiftApp.auth2FA.subscribe((res) => {
        if (res.Authenticated) {
          if (this.props && this.props.to) {
            document.location = this.props.to;
          } else if (this.props.redirect && ShiftApp.config.loginRedirect) {
            document.location = ShiftApp.config.loginRedirect;
          }
          ShiftApp.config.siteName !== 'bitcoindirect.net' && this.props.close(); // eslint-disable-line no-unused-expressions
          return null;
        }
        localStorage.setItem('SessionToken', '');
        const self = this;
        return setTimeout(() => {
          self.props.setBanner({
            information: '',
            error: ShiftApp.translation('SIGNIN_MODAL.INVALID_AUTH_CODE') || 'Invalid Auth Code. Please Try Again.',
          });
        }, 1500);
      });
      return ShiftApp.authenticate2FA(data);
    }

    // this is authenticate level 1
    const data = { UserName: this.refs.email.value(), Password: this.refs.password.value() };

    const code = this.refs.authCode && this.refs.authCode.value();
    const twoFaToken = this.getCookie(`${APConfig.TwoFACookie}.${data.UserName}`);

    if (ShiftApp.config.sendOmsIdInLogin) data.OMSId = ShiftApp.oms.value;
    if (code) data.code = code;
    if (!code && twoFaToken) data.twoFaTokens = twoFaToken;

    ShiftApp.webAuthenticateSubject
      .filter((res) => res.hasOwnProperty('Authenticated')) // eslint-disable-line no-prototype-builtins
      .subscribe((res) => {
        if (res.Requires2FA) {
          if (res.AuthType) {
            this.setState({
              TwoFARequired: true,
              AuthType: res.AuthType,
            });
            if (res.AuthType === 'Google') {
              this.props.setBanner && this.props.setBanner({ // eslint-disable-line no-unused-expressions
                information: ShiftApp.translation('SIGNIN_MODAL.2FA_INFO') || 'Please enter 2FA token',
                error: '',
              });
              this.setState({
                googleRequired: true,
                sessionToken: res.SessionToken,
              });
            }
          }
          if (this.props.setBanner) {
            this.props.setBanner({
              information: ShiftApp.translation('SIGNIN_MODAL.2FA_REQUIRED') || 'Two factor authentication required',
              error: '',
            });
          }
          return;
        }

        if (res.Authenticated) {
          if (res.twoFaToken) {
            this.createCookie(`${APConfig.TwoFACookie}.${data.UserName}`, res.twoFaToken);
          }
          if (res.SessionToken) localStorage.setItem('SessionToken', res.SessionToken);
          if (this.props && this.props.to) {
            document.location = this.props.to;
          } else if (this.props.redirect && ShiftApp.config.loginRedirect) {
            document.location = ShiftApp.config.loginRedirect;
          }
          if (ShiftApp.config.siteName !== 'bitcoindirect.net') this.props.close();
        } else {
          this.props.setBanner && this.props.setBanner({ // eslint-disable-line no-unused-expressions
            information: '',
            error: ShiftApp.translation('SIGNIN_MODAL.ERROR_MSG') || res.errormsg || '',
          });
          localStorage.setItem('SessionToken', '');
        }
      });

    return ShiftApp.WebAuthenticate(data);
  };

  resetPassword = () => {
    if (!this.refs.email.value()) {
      return this.props.setBanner({
        information: '',
        error: ShiftApp.config.useEmailAsUsername ?
          ShiftApp.translation('SIGNIN_MODAL.REQUEST_EMAIL') || 'Enter a User email' :
          ShiftApp.translation('SIGNIN_MODAL.REQUEST_USERNAME') || 'Enter a User name',
      });
    }

    ShiftApp.resetPass.subscribe((res) => {
      if (res.result === true || res.result === 'true') {
        this.props.setBanner({
          information: ShiftApp.translation('SIGNIN_MODAL.PASSWORD_SENT') || 'Check Your Email For Reset Password Link.',
          error: '',
        });
        this.setState({ TwoFARequired: false });
      } else if (res.result === false || res.result === 'false') {
        this.props.setBanner({
          information: '',
          error: ShiftApp.translation('RESET_PASSWORD.ERROR') || res.detail,
        });
      } else if ((res.result === false || res.result === 'false') && res.detail === 'Waiting for 2FA.') {
        this.props.setBanner({ information: ShiftApp.translation('RESET_PASSWORD.2FA') || 'Enter your 2fa code.' });
      }
    });

    return ShiftApp.resetPassword({ UserName: this.refs.email.value() });
  };

  changeServer = (e) => {
    this.setState({ server: e.target.value });
    document.wsConnection.close();
    document.wsConnection = document.APAPI.Connect(e.target.value);
    localStorage.setItem('tradingServer', e.target.value);
  };

  colorInIcons = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    const inputsFilled = this.state.inputsFilled;

    inputsFilled[name] = value;
    this.setState({ inputsFilled });
  };

  render() {
    const servers = APConfig.serversList.concat(['Custom server']);

    return (
      <form id="loginForm" onSubmit={this.signIn}>
        <span>
          {' '}
          {APConfig.useServerSelect && (
            <div className="form-group">
              <label htmlFor="serverSelect"> Select server </label>{' '}
              <div>
                <select
                  id="serverSelect"
                  className="form-control"
                  style={{
                    width: '100%',
                  }}
                  onChange={e => {
                    if (e.target.value === 'Custom server') {
                      return this.setState({
                        showCustomServerInput: true,
                        server: e.target.value,
                      });
                    }
                    this.setState({
                      showCustomServerInput: false,
                    });
                    return this.changeServer(e);
                  }}
                  defaultValue={localStorage.getItem('tradingServer') || servers[0]}
                >
                  {servers.map(server => (
                    <option value={server} key={server}>
                      {' '}
                      {server}{' '}
                    </option>
                  ))}{' '}
                </select>{' '}
              </div>{' '}
            </div>
          )}
          {this.state.showCustomServerInput && (
            <div className="form-group">
              <label htmlFor="customServer"> Enter server address </label>{' '}
              <div>
                <input
                  id="customServer"
                  className="form-control"
                  style={{ width: '100%' }}
                  onBlur={this.changeServer}
                />{' '}
              </div>{' '}
            </div>
          )}{' '}
          {ShiftApp.config.useEmailAsUsername ? (
            <span className="input input--custom">
              <InputNoLabel
                placeholder={
                  ShiftApp.translation('SIGNIN_MODAL.EMAIL_PLACEHOLDER') || 'Email'
                }
                className="input-field"
                ref="email"
                name="email"
                colorChange={this.colorInIcons}
              />{' '}
              <i
                className={
                  `fa fa-envelope ${this.state.inputsFilled.email && 'completed'}`
                }
                aria-hidden="true"
              >
                {' '}
              </i>{' '}
            </span>
          ) : (
            <span className="input input--custom">
              <InputNoLabel
                placeholder={
                  ShiftApp.translation('SIGNIN_MODAL.USERNAME') || 'Username'
                }
                className="input-field"
                ref="email"
                name="email"
                colorChange={this.colorInIcons}
              />{' '}
              <i
                className={
                  `fa fa-user ${this.state.inputsFilled.email && 'completed'}`
                }
                aria-hidden="true"
              >
                {' '}
              </i>{' '}
            </span>
          )}
          <span className="input input--custom">
            <InputNoLabel
              placeholder={
                ShiftApp.translation('SIGNIN_MODAL.PASSWORD_PLACEHOLDER') || 'Password'
              }
              type="password"
              className="input-field"
              ref="password"
              name="password"
              colorChange={this.colorInIcons}
            />{' '}
            <i
              className={
                `fa fa-key ${this.state.inputsFilled.password && 'completed'}`
              }
              aria-hidden="true"
            >
              {' '}
            </i>{' '}
          </span>
          {(this.state.authyRequired ||
            this.state.googleRequired ||
            this.state.smsRequired ||
            this.state.TwoFARequired) && (
              <span className="input input--custom">
                <InputNoLabel
                  placeholder={
                    ShiftApp.translation('SIGNIN_MODAL.AUTH_QUES') ||
                    'Your 2FA Verification Code'
                  }
                  type="string"
                  className="input-field"
                  ref="authCode"
                  name="authCode"
                  colorChange={this.colorInIcons}
                />{' '}
                <i className="fa fa-check" aria-hidden="true">
                  {' '}
                </i>{' '}
              </span>
            )}
          <div className="clearfix">
            <div
              className={
                ShiftApp.config.apexSite
                  ? 'text-center row around-xs'
                  : 'text-center'
              }
            >
              <button
                type="submit"
                className="btn btn-lg submit underline"
              >
                {ShiftApp.translation('BUTTONS.TEXT_SIGNIN') || 'Sign In'}{' '}
              </button>{' '}
              <span className="reset-pw-label">{ShiftApp.translation('SIGNIN_MODAL.RESET_PW_LABEL') ||
                  ''}
                <a
                  className="reset-password-button"
                  onClick={this.resetPassword}
                  style={{ cursor: 'pointer' }}
                >
                
                  <span className="reset-pw-link">{ShiftApp.translation('PASSWORD_MODAL.TITLE_TEXT') ||
                    'Reset Password'}{' '}</span>
                </a>
              </span>{' '}
              {this.props.toggleForm 
                && ShiftApp.config.loginForm
                && ShiftApp.config.loginForm.showSignUpLink 
                &&
                <a 
                  href="#" 
                  className="signup-link"
                  onClick={this.props.toggleForm}>
                  {ShiftApp.translation("SIGNIN_MODAL.REGISTER_INSTEAD") || "Don't have an account? Register"}
                </a>
              }
            </div>{' '}
          </div>{' '}
        </span>{' '}
        {false && this.loadScript()}{' '}
      </form>
    );
  }
}

LoginFormInnerV2.defaultProps = {
  setBanner: () => {},
  close: () => {},
  to: '',
  redirect: false,
};

LoginFormInnerV2.propTypes = {
  setBanner: React.PropTypes.func,
  close: React.PropTypes.func,
  to: React.PropTypes.string,
  redirect: React.PropTypes.bool,
};

export default LoginFormInnerV2;
