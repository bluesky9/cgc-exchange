/* global ShiftApp, $ */
import Rx from 'rx-lite';

ShiftApp.accountBalances.debounce(() => Rx.Observable.interval(100)).subscribe((res) => {
  const growlerOpts = ShiftApp.config.growlerDefaultOptions;

  if (res.OrderId) {
    const orderId = res.OrderId || '';
    const cancelReason = res.CancelReason || '';

    if (res.OrderState === 'Working') {
      $.bootstrapGrowl(
        ShiftApp.translation('ALERTS.ORDER_ACCEPTED', {orderId}) || `Order ${orderId} was accepted`,
        {...growlerOpts, type: 'success'}
      );
    }
    if (res.OrderState === 'Canceled') {
      $.bootstrapGrowl(
        ShiftApp.translation('ALERTS.ORDER_CANCELLED', {orderId, cancelReason}) || `Order ${orderId} was cancelled ${cancelReason}`,
        {...growlerOpts, type: 'danger'}
      );
    }
    if (res.OrderState === 'Rejected') {
      $.bootstrapGrowl(
        ShiftApp.translation('ALERTS.ORDER_REJECTED', {orderId}) || `Order ${orderId} was rejected`,
        {...growlerOpts, type: 'danger'}
      );
    }
  }
});

ShiftApp.sendorder.debounce(() => Rx.Observable.interval(100)).subscribe((res) => {
  const growlerOpts = {...ShiftApp.config.growlerDefaultOptions, type: 'info'};
  const orderId = res.OrderId || '';

  if (res.status === 'Pending') {
    $.bootstrapGrowl(
      ShiftApp.translation('ALERTS.ORDER_PENDING', {orderId}) || `Order ${orderId} is pending`,
      {...growlerOpts, type: 'info'}
    );
  }
  if (res.status === 'Rejected') {
    $.bootstrapGrowl(
      ShiftApp.translation('ALERTS.ORDER_REJECTED', {orderId}) || `Order ${orderId} was rejected`,
      {...growlerOpts, type: 'danger'});
  }
  if (res.status === 'Accepted') {
    $.bootstrapGrowl(
      ShiftApp.translation('ALERTS.ORDER_PLACED', {orderId}) || `Order ${orderId} was placed`,
      {...growlerOpts, type: 'info'}
    );
  }
});

ShiftApp.rejectedOrders.debounce(() => Rx.Observable.interval(100)).subscribe((res) => {
  const growlerOpts = {...ShiftApp.config.growlerDefaultOptions, type: 'danger'};

  if (Object.keys(res).length && res.RejectReason) $.bootstrapGrowl(res.RejectReason, growlerOpts);
});

ShiftApp.requestfunds.subscribe((data) => {
  if (data.length === 0) return;
  const growlerOpts = ShiftApp.config.growlerDefaultOptions;

  if (data && data.result) {
    $.bootstrapGrowl(
      ShiftApp.translation('ALERTS.REQUEST_SUCCESSFUL') || 'Request successful',
      {...growlerOpts, type: 'info'}
    );
  } else if (data && !data.result) {
    $.bootstrapGrowl(
      ShiftApp.translation('ALERTS.REQUEST_UNSUCCESSFUL', {errormsg: data.errormsg}) || `Request unsuccessful: ${data.errormsg}`,
      {...growlerOpts, type: 'danger'}
    );
  }
});
