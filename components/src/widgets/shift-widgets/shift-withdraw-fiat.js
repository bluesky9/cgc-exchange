/* global $, ShiftApp, document, localStorage */
import React from 'react';
import uuidV4 from 'uuid/v4';

import WidgetBase from '../base';
import Modal from '../modal';
import InputLabeled from '../../misc/inputLabeled';
import SelectLabeled from '../../misc/selectLabeled';
import TextareaLabeled from '../../misc/textareaLabeled';
import ProcessingButton from '../../misc/processingButton';
import TwoFACodeInput from '../twoFACodeInput';
import Validate from '../../misc/form/validators';
import ShiftInputLabeled from '../../misc/shift-fields/shift-input-labeled';

import { parseNumberToLocale, getDecimalPrecision } from '../helper';

const cities = ShiftApp.config.cities;
const provinces = ShiftApp.config.provinces;
const fiatWithdrawAccounts = ShiftApp.config.fiatWithdrawAccounts;

class ShiftWithdrawFIAT extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showTemplateTypeSelect: false,
      showTemplate: false,
      showDefaultForm: false,
      templateTypes: [],
      templatesArray: [],
      template: {},
      editedTemplate: {},
      data: {},
      error: '',
      success: '',
      twoFA: {},
      twoFACancel: false,
      processing: false,
      bankProvince: '',
      billingData: {},
      productId: ShiftApp.products.value.find(
        product => product.Product === props.Product
      ).ProductId,
      accountId:
        ShiftApp.selectedAccount.value || ShiftApp.userAccounts.value[0],
      validNumber: false
    };
  }

  componentDidMount() {
    const { accountId, productId } = this.state;
    const data = {
      accountId,
      productId
    };

    this.selectedAccount = ShiftApp.selectedAccount.subscribe(account =>
      this.setState({ accountId: account })
    );

    this.withdrawTemplateTypes = ShiftApp.withdrawTemplateTypes.subscribe(
      res => {
        if (Object.keys(res).length) {
          if (res.result) {
            if (res.TemplateTypes.length > 1) {
              return this.setState({
                showTemplateTypeSelect: true,
                selectedTemplate: res.TemplateTypes[0],
                templateTypes: res.TemplateTypes
              });
            }
            if (res.TemplateTypes.length === 0) {
              return this.setState({ showDefaultForm: true });
            }

            const templateData = {
              ...data,
              templateType: res.TemplateTypes[0]
            };
            this.setState({ selectedTemplate: res.TemplateTypes[0] });
            return ShiftApp.getWithdrawTemplate(templateData);
          }
          return this.setState({ showDefaultForm: true });
        }
        return false;
      }
    );

    if (ShiftApp.config.useCoinsPHWithdrawTemplate) {
      const template = { ID: null, Email: null, PhoneNumber: null };
      return this.setState({
        templatesArray: template,
        template: template,
        editedTemplate: template,
        showTemplate: true,
        showTemplateTypeSelect: false,
        TemplateType: 'CoinsPH'
      });
    }

    this.withdrawTemplate = ShiftApp.withdrawTemplate.subscribe(res => {
      if (res.result) {
        if (
          !res.Template ||
          res.Template === '[]' ||
          !ShiftApp.config.useCoinsPHWithdrawTemplate
        ) {
          return this.setState({ showDefaultForm: true });
        }
        const response = JSON.parse(res.Template);

        if (Array.isArray(response)) {
          return this.setState({
            templatesArray: response.length > 1 ? response : [],
            template: response.length > 1 ? {} : response[0],
            editedTemplate: response.length > 1 ? {} : response[0],
            showTemplate: true,
            showTemplateTypeSelect: false
          });
        }

        if (response.isSuccessful) {
          return this.setState({
            template: response.Template,
            editedTemplate: response.Template,
            showTemplate: true,
            showTemplateTypeSelect: false
          });
        }
        return this.setState({
          template: response,
          editedTemplate: response,
          showTemplate: true,
          showTemplateTypeSelect: false
        });
      }
      return false;
    });

    this.submitWithdraw = ShiftApp.submitWithdraw.subscribe(res => {
      if (Object.keys(res).length) {
        if (res.requireGoogle2FA || res.requireAuthy2FA || res.requireSMS2FA) {
          return this.setState({
            data: res,
            twoFA: res,
            processing: false
          });
        }

        if (!res.result) {
          const defaultResponse = `There was an error: ${res.errormsg}, ${
            res.detail
          }`;
          let error = '';

          if (res.errorcode) {
            if (res.errorcode === 67) {
              error =
                ShiftApp.translation('ERRORS.REQUEST_FAILED') ||
                defaultResponse;
            }

            if (res.errorcode === 100) {
              if (res.detail) {
                if (res.detail === 'Insufficient Balance') {
                  error =
                    ShiftApp.translation('ERRORS.NOT_ENOUGH_FUNDS') ||
                    defaultResponse;
                }
                if (
                  res.detail === 'Invalid ProductId' ||
                  res.detail === 'Withdraw Amount must be greater than zero' ||
                  res.detail ===
                    'OMSId, AccountId and ProductId are Required' ||
                  res.detail ===
                    'Withdraw Amount must be greater than zero and less than 9223372036.854775807'
                ) {
                  error = defaultResponse;
                }
              }
            } else if (res.errorcode === 102) {
              error = 'Invalid data entered';
            } else if (res.errormsg.indexOf('invalid amount') > -1) {
              error =
                ShiftApp.translation('ERRORS.INVALID_AMOUNT') ||
                defaultResponse;
            } else if (res.errormsg.indexOf('Not Enough funds') > -1) {
              const response = res.errormsg.split(':');

              error =
                ShiftApp.translation('ERRORS.NOT_ENOUGH_FUNDS') +
                  response[1] || defaultResponse;
            } else if (res.errormsg.indexOf('Bank Account Mismatch') > -1) {
              error =
                ShiftApp.translation('ERRORS.BANK_MISMATCH') ||
                defaultResponse;
            } else if (
              res.errormsg.indexOf('Not allowed. Verification level failed.') >
              -1
            ) {
              const response = res.errormsg.split(':');

              error =
                ShiftApp.translation('ERRORS.VERIFY_FAILED1') +
                  response[1] +
                  ShiftApp.translation('ERRORS.VERIFY_FAILED2') +
                  response[3] || defaultResponse;
            } else {
              error = `${res.errormsg}: ${res.detail}`;
            }
          } else {
            // this is a generic error with no code
            error = `There was a problem with your request: ${res.detail}`;
          }

          $.bootstrapGrowl(error, {
            type: 'danger',
            allow_dismiss: true,
            align: ShiftApp.config.growlwerPosition,
            delay: ShiftApp.config.growlwerDelay,
            offset: { from: 'top', amount: 30 },
            left: '60%'
          });

          return this.setState({
            data: res,
            error,
            processing: false
          });
        }
        const successTemp =
          ShiftApp.translation('WITHDRAW.FIAT_TEMP_CONFIRM') ||
          'Request Received';
        const successTemp2 =
          ShiftApp.translation('WITHDRAW.FIAT_TEMP_CONFIRM2') ||
          'Please Check your email';

        res.errormsg = '';
        $.bootstrapGrowl(successTemp, {
          type: 'success',
          allow_dismiss: true,
          align: ShiftApp.config.growlwerPosition,
          delay: ShiftApp.config.growlwerDelay,
          offset: { from: 'top', amount: 30 },
          left: '60%'
        });
        if (ShiftApp.config.confirmWithEMail) {
          $.bootstrapGrowl(successTemp2, {
            type: 'info',
            allow_dismiss: true,
            align: ShiftApp.config.growlwerPosition,
            delay: ShiftApp.config.growlwerDelay,
            offset: { from: 'top', amount: 30 },
            left: '60%'
          });
        }

        return this.setState({
          data: res,
          error: '',
          processing: false,
          showDefaultForm: false,
          showTemplate: false
        });
      }

      return ShiftApp.getWithdrawTickets({
        OMSId: ShiftApp.oms.value,
        AccountId: ShiftApp.selectedAccount.value
      });
    });

    if (ShiftApp.config.useWithdrawFees) {
      this.withdrawFee = ShiftApp.withdrawFee.subscribe(fee =>
        this.setState({ fee })
      );
    }

    this.products = ShiftApp.products
      .filter(data => data.length)
      .subscribe(products => {
        const decimalPlaces = {};
        products.forEach(product => {
          decimalPlaces[product.Product] = product.DecimalPlaces;
        });
        this.setState({ decimalPlaces });
      });

    ShiftApp.getWithdrawTemplateTypes(data);
  }

  componentWillUnmount() {
    /* eslint-disable no-unused-expressions */
    ShiftApp.withdrawTemplateTypes.onNext([]);
    ShiftApp.withdrawTemplate.onNext([]);
    ShiftApp.submitWithdraw.onNext([]);
    this.selectedAccount.dispose();
    this.auth2FAuthentication && this.auth2FAuthentication.dispose();
    this.submitWithdraw && this.submitWithdraw.dispose();
    this.withdrawTemplateTypes && this.withdrawTemplateTypes.dispose();
    this.withdrawTemplate && this.withdrawTemplate.dispose();
    this.withdrawFee && this.withdrawFee.dispose();
    this.products.dispose();
    /* eslint-enable no-unused-expressions */
  }

  onChangeTemplateField = e => {
    const template = { ...this.state.editedTemplate };

    template[e.target.name] = e.target.value;
    this.setState({ editedTemplate: template });
  };

  checkValidWithdrawalAmount = () => {
    if (
      ShiftApp.config.withdrawFiat.minimumWithdrawalAmount &&
      parseInt(this.state.amountString) <
        ShiftApp.config.withdrawFiat.minimumWithdrawalAmount
    ) {
      this.setState({
        error:
          ShiftApp.translation('WITHDRAW.ERRORS.MINIMUM_WITHDRAWAL_AMOUNT') ||
          'Withdrawl amount is too small, please make a withdrawal greater than $100.'
      });
    }
  };

  getWithdrawTemplate = () => {
    const payload = {
      accountId: this.state.accountId,
      productId: this.state.productId,
      templateType: this.state.selectedTemplate
    };

    ShiftApp.getWithdrawTemplate(payload);
  };

  selectWithdrawTemplate = e => {
    this.setState({ selectedTemplate: e.target.value });
  };

  changeBilling = () => {
    const data = {};

    Object.keys(this.refs).forEach(key => {
      if (
        this.refs[key].type === 'checkbox' ||
        this.refs[key].type === 'radio'
      ) {
        data[key] = this.refs[key].checked;
      } else {
        data[key] = this.refs[key].value();
      }
    });

    return this.setState({
      billingData: data,
      success: '',
      error: ''
    });
  };

  closeModal = () => {
    this.setState({
      twoFA: {},
      twoFACancel: true
    });
  };

  changeAmount = e => {
    this.setState({ error: '' });
    const amount = parseNumberToLocale(e.target.value);
    const decimals = getDecimalPrecision(amount);
    const decimalsAllowed = this.state.decimalPlaces[this.props.Product];
    const msgDecimals = `Only ${decimalsAllowed} decimal places are allowed for ${
      this.props.Product
    }.`;

    if (decimals > decimalsAllowed) {
      this.setState({
        amount,
        amountString: e.target.value,
        error: msgDecimals,
        validNumber: false
      });
    } else if (!isNaN(amount)) {
      this.setState({
        amount,
        amountString: e.target.value,
        validNumber: true
      });

      if (ShiftApp.config.useWithdrawFees && amount) {
        ShiftApp.getWithdrawFee({
          OMSId: ShiftApp.oms.value,
          ProductId: this.state.productId,
          AccountId: this.state.accountId,
          Amount: amount
        });
      }
    }
    return null;
  };

  withdraw = e => {
    e.preventDefault();
    const { balance } = this.props;
    const templateForm = {
      ...this.state.template,
      ...this.state.editedTemplate
    };
    const templateType = 'FIAT';

    this.setState({ processing: true });

    if (this.state.amount > balance) {
      return this.setState({
        error: 'Insufficient Funds',
        success: '',
        processing: false
      });
    }

    const data = {
      OMSId: ShiftApp.oms.value,
      productId: this.state.productId,
      accountId: this.state.accountId,
      amount: +this.state.amount,
      TemplateType:
        this.state.TemplateType ||
        this.state.template.TemplateType ||
        this.state.selectedTemplate ||
        templateType
    };

    if (this.state.showDefaultForm) {
      // NZD withdrawals must include Fullname
      if (this.props.Product === 'NZD' && !this.refs.fullName.value()) {
        const error =
          ShiftApp.translation('WITHDRAW.FULLNAME_REQUIRED') ||
          'Please enter your full name';
        $.bootstrapGrowl(error, {
          type: 'danger',
          allow_dismiss: true,
          align: ShiftApp.config.growlwerPosition,
          delay: ShiftApp.config.growlwerDelay,
          offset: { from: 'top', amount: 30 },
          left: '60%'
        });
        return this.setState({
          error:
            ShiftApp.translation('WITHDRAW.FULLNAME_REQUIRED') ||
            'Please enter your full name',
          processing: false
        });
      }
      templateForm.language =
        localStorage && localStorage.lang
          ? localStorage.lang
          : ShiftApp.config.defaultLanguage;

      if (this.refs.bankAddress) {
        templateForm.bankAddress = this.refs.bankAddress.value();
      }
      if (this.refs.comment) {
        templateForm.comment = this.refs.comment.value();
      }
      if (this.refs.fullName) {
        templateForm.fullName = this.refs.fullName.value();
      }
      if (this.refs.bankAccountName) {
        templateForm.bankAccountName = this.refs.bankAccountName.value();
      }

      templateForm.bankAccountNumber = this.refs.bankAccountNumber.value();

      if (this.refs.swiftCode)
        templateForm.swiftCode = this.refs.swiftCode.value();
      if (this.refs.sortCode)
        templateForm.sortCode = this.refs.sortCode.value();

      if (ShiftApp.config.withdrawFiat.synapseWithdraw) {
        templateForm.routingNumber = this.refs.routingNumber.value();
      }

      // Save the billing data
      const billingData = {};
      /* eslint-disable quotes */
      /* eslint-disable prefer-template */
      billingData.billingInfo =
        '{"bankAccountNumber":"' + this.refs.bankAccountNumber.value() + '"';
      if (templateForm.bankCity)
        billingData.billingInfo +=
          ',"bankCity":"' + templateForm.bankCity + '"';
      if (templateForm.bankProvince)
        billingData.billingInfo +=
          ',"bankProvince":"' + templateForm.bankProvince + '"';
      if (this.props.instrument === 'USD')
        billingData.billingInfo +=
          ',"swiftCode":"' + this.refs.swiftCode.value() + '"';

      // if (this.refs.bankAccountName.value) {
      //   billingData.billingInfo +=
      //     ',"bankAccountName":"' + this.refs.bankAccountName.value() + '"';
      // }

      // if (this.refs.fullName.value) {
      //   billingData.billingInfo +=
      //     ',"fullName":"' + this.refs.fullName.value() + '"';
      // }
      billingData.billingInfo +=
        ',"bankAddress":"' + templateForm.userAddress + '"';
      billingData.billingInfo += '}';
      /* eslint-enable quotes */
      /* eslint-enable prefer-template */
    }

    const newUserRequestBody = {
      logins: [{ email: ShiftApp.getUser.value.Email }],
      extra: {
        supp_id: ShiftApp.userData.value.UserId,
        cip_tag: 1,
        is_business: false
      }
    };

    if (ShiftApp.config.withdrawFiat.synapseWithdraw) {
      fetch('https://synapse.shiftcrypto.com/api/users/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(newUserRequestBody)
      })
        .then(response => response.json())
        .then(user => {
          fetch('https://synapse.shiftcrypto.com/api/transactions/withdraw', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
              user_id: ShiftApp.userData.value.UserId,
              amount: {
                amount: data.amount,
                currency: 'USD'
              },
              account_num: templateForm.bankAccountNumber,
              routing_num: templateForm.routingNumber
            })
          })
            .then(response => response.text())
            .then(transaction => {
              if (transaction) {
                data.templateForm = JSON.stringify(templateForm);
                ShiftApp.withdraw(data);
              }
            });
        })
        .catch(error => console.error(error));
    } else {
      data.templateForm = JSON.stringify(templateForm);
      return ShiftApp.withdraw(data);
    }
  };

  do2FAVerification = code => {
    const data = {};

    if (this.state.twoFA.requireGoogle2FA) {
      data.val2FaGoogleCode = code;
    }
    if (this.state.twoFA.requireAuthy2FA) {
      data.val2FaOAuthyCode = code;
    }
    if (this.state.twoFA.requireSMS2FA) {
      data.val2FaSMSCode = code;
    }

    data.val2FaRequestCode = this.state.twoFA.val2FaRequestCode;

    ShiftApp.do2FAVerification(data, res => {
      if (res.rejectReason) {
        if (
          res.rejectReason.indexOf(
            'Withdraw is Pending for Operator Approval'
          ) > -1
        ) {
          const successTemp = res.rejectReason;
          res.isAccepted = true;
          res.rejectReason = '';
          return this.setState({
            twoFA: {},
            data: res,
            success: successTemp,
            processing: false
          });
        }
        return this.setState({
          twoFA: {},
          data: res,
          error: res.rejectReason,
          processing: false
        });
      }
      return this.setState({
        twoFA: {},
        data: res,
        processing: false
      });
    });
  };

  selectTemplateArrayOption = e => {
    const { value } = e.target;
    const template = this.state.templatesArray.find(
      temp => temp.nickname === value
    );

    if (!value) return this.setState({ template: {} });
    return this.setState({ template });
  };

  render() {
    const headerTitle = `${ShiftApp.translation('WITHDRAW.WITHDRAW') ||
      'Withdraw'} ${this.props.Product ? `(${this.props.Product})` : ''}`;
    const provinceList = (provinces || []).map((item, i) => (
      <option value={item.code} key={i}>
        {item.name}
      </option>
    )); // eslint-disable-line react/no-array-index-key
    const accountTypes = (fiatWithdrawAccounts || []).map((item, i) => (
      <option value={item.type} key={i}>
        {item.type}
      </option>
    ));
    let province;
    const cityList = (cities || []).map((item, i) => {
      if (province === item.province) {
        return (
          <option value={item.code} className="show" key={i}>
            {item.name}
          </option>
        ); // eslint-disable-line react/no-array-index-key
      }
      return null;
    });

    if (this.state.showTemplateTypeSelect) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          <div className="pad">
            <h3>
              {ShiftApp.translation('WITHDRAW.SELECT_TEMPLATE') ||
                'Select a withdraw template'}
            </h3>
            <select
              className="form-control"
              onChange={this.selectWithdrawTemplate}
              value={this.state.selectedTemplate}
            >
              {this.state.templateTypes.map(type => <option>{type}</option>)}
            </select>
          </div>
          <div className="pad">
            <div className="clearfix">
              <div className="pull-right">
                <button
                  className="btn btn-action btn-inverse"
                  onClick={this.props.close}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                </button>
                <button
                  className="btn btn-action"
                  onClick={this.getWithdrawTemplate}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_NEXT') || 'Next'}
                </button>
              </div>
            </div>
          </div>
        </WidgetBase>
      );
    }

    if (this.state.showTemplate) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          <div className="pad">
            <InputLabeled
              placeholder={
                ShiftApp.translation('WITHDRAW.AMOUNT') || 'Amount'
              }
              onChange={this.changeAmount}
              value={this.state.amountString}
              type="text"
              ref="amount"
            />

            {this.state.templatesArray.length ? (
              <SelectLabeled
                placeholder="Select account"
                onChange={this.selectTemplateArrayOption}
              >
                <option value="">Select account</option>
                {this.state.templatesArray.map(template => (
                  <option value={template.nickname}>{template.nickname}</option>
                ))}
              </SelectLabeled>
            ) : null}

            {Object.keys(this.state.template)
              .filter(key => key !== 'TemplateType')
              .map((field, index) => {
                if (this.state.template[field]) {
                  return (
                    <div key={uuidV4()} style={{ marginBottom: '1rem' }}>
                      <p
                        style={{
                          fontSize: '0.9rem',
                          fontWeight: 'bold',
                          marginBottom: '0.5rem'
                        }}
                      >
                        {field.replace(/([a-z])([A-Z])/g, '$1 $2')}
                      </p>
                      <p style={{ marginLeft: '1rem' }}>
                        {this.state.template[field]}
                      </p>
                    </div>
                  );
                }
                return (
                  <InputLabeled
                    key={index} // eslint-disable-line react/no-array-index-key
                    name={field}
                    label={field.replace(/([a-z])([A-Z])/g, '$1 $2')}
                    placeholder={field.replace(/([a-z])([A-Z])/g, '$1 $2')}
                    onChange={this.onChangeTemplateField}
                    value={this.state.editedTemplate[field]}
                    wrapperClass={
                      'withdrawfiat-' + field.toLowerCase() + '-input'
                    }
                  />
                );
              })}

            {ShiftApp.config.useWithdrawFees && this.state.fee ? (
              <p>
                {ShiftApp.translation('WITHDRAW.FEE_MESSAGE') ||
                  "You'll be charged a fee of"}{' '}
                {this.state.fee}
                {this.props.Product}
              </p>
            ) : null}

            <div className="clearfix">
              <div className="pull-right">
                <button
                  className="btn btn-action btn-inverse"
                  onClick={this.props.close}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                </button>
                <ProcessingButton
                  className="btn btn-action"
                  onClick={this.withdraw}
                  processing={this.state.processing}
                  disabled={this.state.processing || !this.state.validNumber}
                >
                  {ShiftApp.translation('WITHDRAW.PROCESS_WITHDRAW') ||
                    'Process Withdraw'}
                </ProcessingButton>
              </div>
            </div>
          </div>
        </WidgetBase>
      );
    }

    if (this.state.showDefaultForm) {
      if (ShiftApp.config.withdrawFiat.synapseWithdraw) {
        return (
          // wrap all content in widget base
          <WidgetBase
            {...this.props}
            headerTitle={headerTitle}
            error={this.state.error}
            success={this.state.success}
          >
            <div className="pad">
              <form onSubmit={this.withdraw}>
                <p>
                  {ShiftApp.translation('WITHDRAW.TITLE_TEXT') ||
                    'Withdraw Form'}
                </p>
                <div>
                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.AMOUNT') ||
                      'Amount - Required'
                    }
                    onChange={this.changeAmount}
                    onBlur={this.checkValidWithdrawalAmount}
                    value={this.state.amountString}
                    type="text"
                    ref="amount"
                    required="yes"
                  />

                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.BANK_NAME') ||
                      'Bank Name'
                    }
                    ref="bankAccountName"
                    value={this.state.billingData.bankAccountName}
                    onChange={this.changeBilling}
                  />

                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.BANK_ADDRESS') ||
                      'Bank Address'
                    }
                    ref="bankAddress"
                    value={this.state.billingData.bankAddress}
                    onChange={this.changeBilling}
                  />

                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.ACCOUNT_ID') ||
                      'Bank Account #'
                    }
                    ref="bankAccountNumber"
                    value={this.state.billingData.bankAccountNumber}
                    onChange={this.changeBilling}
                  />

                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.ROUTING_NUMBER') ||
                      'Routing #'
                    }
                    ref="routingNumber"
                    value={this.state.billingData.routingNumber}
                    onChange={this.changeBilling}
                  />

                  <div>
                    <p>
                      {ShiftApp.translation('WITHDRAW.COMMENT') ||
                        'The comment field is optional. Please use it for special instructions.'}
                    </p>

                    <TextareaLabeled
                      rows="6"
                      placeholder={
                        ShiftApp.translation('WITHDRAW.COMMENT_LABEL') ||
                        'Comment'
                      }
                      ref="comment"
                      wrapperClass="withdraw-comment-input"
                    />
                  </div>
                </div>

                {ShiftApp.config.useWithdrawFees && this.state.fee ? (
                  <p>
                    {ShiftApp.translation('WITHDRAW.FEE_MESSAGE') ||
                      "You'll be charged a fee of"}{' '}
                    {this.state.fee}
                    {this.props.Product}
                  </p>
                ) : null}

                <div className="clearfix">
                  <div
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <button
                      className="btn btn-action btn-inverse"
                      onClick={this.props.close}
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                    </button>{' '}
                    <ProcessingButton
                      className="btn btn-action"
                      onClick={this.withdraw}
                      processing={this.state.processing}
                      disabled={
                        this.state.processing || !this.state.validNumber
                      }
                      type="submit"
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') ||
                        'Submit'}
                    </ProcessingButton>
                  </div>
                </div>
              </form>
            </div>

            {(this.state.twoFA.requireGoogle2FA ||
              this.state.twoFA.requireAuthy2FA ||
              this.state.twoFA.requireSMS2FA) && (
              <Modal close={this.closeModal}>
                <TwoFACodeInput
                  {...this.state.twoFA}
                  submit={this.do2FAVerification}
                />
              </Modal>
            )}
          </WidgetBase>
        );
      } else {
        return (
          // wrap all content in widget base
          <WidgetBase
            {...this.props}
            headerTitle={headerTitle}
            error={this.state.error}
            success={this.state.success}
          >
            <div className="pad">
              <form onSubmit={this.withdraw}>
                {this.props.Product === 'NZD' ? (
                  ''
                ) : (
                  <p>
                    {ShiftApp.translation('WITHDRAW.TITLE_TEXT') ||
                      'Withdraw Form'}
                  </p>
                )}
                <div>
                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('WITHDRAW.AMOUNT') ||
                      'Amount - Required'
                    }
                    onChange={this.changeAmount}
                    onBlur={this.checkValidWithdrawalAmount}
                    value={this.state.amountString}
                    type="text"
                    ref="amount"
                    required="yes"
                  />
                  {ShiftApp.config.homeCountry !== 'Brazil' && (
                    <InputLabeled
                      placeholder={
                        ShiftApp.translation('WITHDRAW.BANK_NAME') ||
                        'Bank Name'
                      }
                      ref="bankAccountName"
                      value={this.state.billingData.bankAccountName}
                      onChange={this.changeBilling}
                    />
                  )}

                  {ShiftApp.config.homeCountry === 'Brazil' && (
                    <div className="row">
                      <div className="col-xs-12">
                        <InputLabeled
                          placeholder={
                            ShiftApp.translation('WITHDRAW.BANK_NUMBER') ||
                            'Bank Number'
                          }
                          ref="bankNumber"
                          value={this.state.billingData.bankNumber}
                          onChange={this.changeBilling}
                          required="yes"
                        />
                      </div>
                      <div className="col-xs-6">
                        <InputLabeled
                          placeholder={
                            ShiftApp.translation('WITHDRAW.BRANCH_NUMBER') ||
                            'Branch Number'
                          }
                          ref="branchNumber"
                          value={this.state.billingData.branchNumber}
                          onChange={this.changeBilling}
                          required="yes"
                        />
                      </div>
                      <div className="col-xs-6">
                        <InputLabeled
                          placeholder={
                            ShiftApp.translation('WITHDRAW.CHECK_DIGIT_1') ||
                            'Check Digit'
                          }
                          ref="checkDigit1"
                          value={this.state.billingData.checkDigit1}
                          onChange={this.changeBilling}
                        />
                      </div>
                      <div className="col-xs-6">
                        <InputLabeled
                          placeholder={
                            ShiftApp.translation('WITHDRAW.ACCOUNT_ID') ||
                            'Account #'
                          }
                          ref="bankAccountNumber"
                          value={this.state.billingData.bankAccountNumber}
                          onChange={this.changeBilling}
                          required="yes"
                        />
                      </div>
                      <div className="col-xs-6">
                        <InputLabeled
                          placeholder={
                            ShiftApp.translation('WITHDRAW.CHECK_DIGIT_2') ||
                            'Check Digit'
                          }
                          ref="checkDigit2"
                          value={this.state.billingData.checkDigit2}
                          onChange={this.changeBilling}
                        />
                      </div>
                      <div className="col-xs-12">
                        <SelectLabeled
                          className="form-control"
                          ref="typeOfAcct"
                          onChange={this.changeBilling}
                          placeholder={
                            ShiftApp.translation(
                              'WITHDRAW.TYPE_OF_ACCOUNT'
                            ) || 'Type of Account'
                          }
                          value={this.state.billingData.typeOfAcct}
                        >
                          {accountTypes}
                        </SelectLabeled>
                      </div>
                    </div>
                  )}

                  {ShiftApp.config.homeCountry !== 'Brazil' && (
                    <InputLabeled
                      placeholder={
                        ShiftApp.translation('WITHDRAW.BANK_ADDRESS') ||
                        'Bank Address'
                      }
                      ref="bankAddress"
                      value={this.state.billingData.bankAddress}
                      onChange={this.changeBilling}
                    />
                  )}

                  {ShiftApp.config.homeCountry !== 'Brazil' && (
                    <InputLabeled
                      placeholder={
                        ShiftApp.translation('WITHDRAW.ACCOUNT_ID') ||
                        'Bank Account #'
                      }
                      ref="bankAccountNumber"
                      value={this.state.billingData.bankAccountNumber}
                      onChange={this.changeBilling}
                    />
                  )}
                  {/* Swift Code displayed for all Fiat Currency except NZD */}
                  {this.props.Product !== 'NZD' &&
                    ShiftApp.config.homeCountry !== 'Brazil' && (
                      <InputLabeled
                        placeholder={
                          ShiftApp.translation('WITHDRAW.SWIFT') ||
                          'Swift Code'
                        }
                        ref="swiftCode"
                        value={this.state.billingData.swiftCode}
                        onChange={this.changeBilling}
                      />
                    )}
                  {ShiftApp.config.homeCountry !== 'Brazil' && (
                    <div>
                      <p>
                        {ShiftApp.translation('WITHDRAW.COMMENT') ||
                          'The comment field is optional. Please use it for special instructions.'}
                      </p>

                      <TextareaLabeled
                        rows="6"
                        placeholder={
                          ShiftApp.translation('WITHDRAW.COMMENT_LABEL') ||
                          'Comment'
                        }
                        ref="comment"
                        wrapperClass="withdraw-comment-input"
                      />
                    </div>
                  )}
                </div>

                {ShiftApp.config.useWithdrawFees && this.state.fee ? (
                  <p>
                    {ShiftApp.translation('WITHDRAW.FEE_MESSAGE') ||
                      "You'll be charged a fee of"}{' '}
                    {this.state.fee}
                    {this.props.Product}
                  </p>
                ) : null}

                <div className="clearfix">
                  <div
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <button
                      className="btn btn-action btn-inverse"
                      onClick={this.props.close}
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                    </button>{' '}
                    <ProcessingButton
                      className="btn btn-action"
                      onClick={this.withdraw}
                      processing={this.state.processing}
                      disabled={
                        this.state.processing || !this.state.validNumber
                      }
                      type="submit"
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') ||
                        'Submit'}
                    </ProcessingButton>
                  </div>
                </div>
              </form>
            </div>

            {(this.state.twoFA.requireGoogle2FA ||
              this.state.twoFA.requireAuthy2FA ||
              this.state.twoFA.requireSMS2FA) && (
              <Modal close={this.closeModal}>
                <TwoFACodeInput
                  {...this.state.twoFA}
                  submit={this.do2FAVerification}
                />
              </Modal>
            )}
          </WidgetBase>
        );
      }
    }

    if (this.state.data.result) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          {!this.state.twoFACancel ? (
            <div className="pad">
              <h3 className="text-center">
                {ShiftApp.translation('WITHDRAW.FIAT_CONFIRM') ||
                  'Request Received'}
              </h3>
              <div className=" clearfix">
                <div className="pull-right">
                  <button className="btn btn-action" onClick={this.props.close}>
                    {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <div className="pad">
              <h3 className="text-center">
                {ShiftApp.translation('WITHDRAW.CANCEL') ||
                  'Withdraw request cancelled.'}
              </h3>
              <div className=" clearfix">
                <div className="pull-right">
                  <button className="btn btn-action" onClick={this.props.close}>
                    {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                  </button>
                </div>
              </div>
            </div>
          )}
        </WidgetBase>
      );
    }

    return null;
  }
}

ShiftWithdrawFIAT.defaultProps = {
  Product: '',
  instrument: '',
  close: () => {},
  balance: null
};

ShiftWithdrawFIAT.propTypes = {
  Product: React.PropTypes.string,
  instrument: React.PropTypes.string,
  close: React.PropTypes.func,
  balance: React.PropTypes.number
};

export default ShiftWithdrawFIAT;
