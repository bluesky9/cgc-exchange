/* global ShiftApp, $, window, localStorage */
import React from 'react';
import uuidV4 from 'uuid/v4';
import Rx from 'rx-lite';
import WidgetBase from '../base';
import Modal from '../modal';
import VerificationRequired from '../verificationRequired';

import { getRetailInstruments } from '../../misc/ordersWidgetsHelper';
import { formatNumberToLocale, formatOrders, getQuantityForFixedPrice, truncateToDecimals } from '../helper';
import ProcessingButton from '../../misc/processingButton';
import ShiftInstsSelectByProduct from './shift-insts-select-by-products';

class ShiftBuyFixed extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      buy: !props.sell,
      market: true,
      marketBuy: 0,
      marketSell: 0,
      productPairs: [],
      total: 0,
      fee: 0,
      feeProduct: '',
      productPair: '',
      amount: 1,
      price: 0,
      successMsg: '',
      errorMsg: '',
      balances: [],
      OrderType: 2,
      InstrumentId: 0,
      AccountId: ShiftApp.userAccounts.value[0],
      openConfirm: false,
      session: {},
      index: 0,
      decimalPlaces: {},
      OrderId: null,
      status: '',
      bookSells: [],
      bookBuys: [],
      productsArray: [],
    };
  }

  componentDidMount() {
    this.selectedAccount = ShiftApp.selectedAccount.subscribe(AccountId => this.setState({AccountId}));

    this.session = ShiftApp.getUser.subscribe(session => this.setState({session}));

    this.products = ShiftApp.products.subscribe(productsArray => {
      const decimalPlaces = {};
      productsArray.forEach(product => {
        decimalPlaces[product.Product] = product.DecimalPlaces;
      });
      this.setState({decimalPlaces, productsArray}, () => {
        this.productPairs = ShiftApp.instruments
          .filter(instruments => instruments.length)
          .subscribe(productPairs => {

            // Set product pairs
            this.setState({productPairs});

            // Find ShiftApp.prodPair in availablePairs for buy-fixed
            const availablePairs = getRetailInstruments(productPairs, this.state.productsArray);
            const checkForProdPair = !availablePairs.find(pair => pair.Symbol === ShiftApp.prodPair.value);

            // If ShiftApp.prodPair isn't available
            if (checkForProdPair) {
              // Set productPair to first available pair
              this.setState({productPair: availablePairs[0].Symbol});
              // And set Instrument
              this.changePairId(availablePairs[0].InstrumentId);
            } else {
              this.productPair = ShiftApp.prodPair.subscribe(productPair => this.setState({productPair}));
            }
          });
      });
    });

    this.Level1 = Rx.Observable.combineLatest(
      ShiftApp.Level1.filter(data => Object.keys(data).length),
      ShiftApp.instrumentChange,
      (level1, instrument) => level1[instrument] || {},
    ).subscribe(level1 => this.setState({BestBid: level1.BestBid, BestOffer: level1.BestOffer}));

    this.Level2 = ShiftApp.Level2
      .filter(orders => orders.length)
      .map(formatOrders)
      .subscribe(orders => {
        const bookBuys = orders.filter(order => order.Side === 0).sort((a, b) => {
          if (a.Price < b.Price) return 1;
          if (a.Price > b.Price) return -1;
          return 0;
        });
        const bookSells = orders.filter(order => order.Side === 1).sort((a, b) => {
          if (a.Price > b.Price) return 1;
          if (a.Price < b.Price) return -1;
          return 0;
        });

        this.setState({bookBuys, bookSells});
      });

    this.Level2Updates = ShiftApp.Level2Update
      .filter(orders => orders.length)
      .map(formatOrders)
      .subscribe((orders) => {
        const buys = orders.filter(order => order.Side === 0);
        const sells = orders.filter(order => order.Side === 1);

        let { bookBuys, bookSells } = this.state;
        if (buys.length && sells.length) {
          buys.forEach((obj) => {
            const newBuys = this.state.bookBuys.filter(lev => lev.Price !== obj.Price);
            bookBuys = (obj.Quantity ? newBuys.concat(obj) : newBuys).sort((a, b) => b.Price - a.Price);
          });
          sells.forEach((obj) => {
            const newSells = this.state.bookSells.filter(lev => lev.Price !== obj.Price);
            bookSells = (obj.Quantity ? newSells.concat(obj) : newSells).sort((a, b) => a.Price - b.Price);
          });
        }
        this.setState({ bookBuys, bookSells });
      });

    this.accountInformation = ShiftApp.accountPositions
      .map(data => data)
      .subscribe(balances => this.setState({balances}));

    this.orderFee = ShiftApp.orderfee.subscribe(res => {
      const product = this.state.productsArray.find(prod => prod.ProductId === res.ProductId);
      const feeProduct = product ? product.Product : '';
      return this.setState({ fee: res.OrderFee, feeProduct });
    });

    this.senderOrder = ShiftApp.sendorder.subscribe(res => {
      const success = '';
      const reject = '';

      this.setState({
        successMsg: success,
        errorMsg: reject,
        fee: res.fee,
        feeProduct: res.feeProduct,
        OrderId: res.OrderId,
      });
    });

    this.orderStateEvent = ShiftApp.accountBalances
      .filter(order => (
        order.OrderId === this.state.OrderId
        && order.ChangeReason === 'SystemCanceled_NoMoreMarket'
      ))
      .subscribe(() => this.setState({status: 'killed'}));

    this.orderTradeEvent = ShiftApp.accountTrades
      .subscribe(trades => {
        const orderWasFilled = trades.some(trade => trade.OrderId === this.state.OrderId);

        if (orderWasFilled) this.setState({status: 'filled'});
      });
  }

  componentWillUnmount() {
    this.productPairs.dispose();
    this.productPair.dispose();
    this.accountInformation.dispose();
    this.senderOrder.dispose();
    this.orderFee.dispose();
    this.selectedAccount.dispose();
    this.orderStateEvent.dispose();
    this.orderTradeEvent.dispose();
    this.Level2.dispose();
    this.Level2Updates.dispose();
  }

  getOrderFee = (Amount) => {
    const pair = this.state.productPairs.find(prod => this.state.productPair === prod.Symbol) || {};
    const product1 = this.state.balances.find(prod => pair.Product1Symbol === prod.ProductSymbol) || {};
    const product2 = this.state.balances.find(prod => pair.Product2Symbol === prod.ProductSymbol) || {};
    const MakerTaker = 'Taker';

    const payload = {
      OMSId: ShiftApp.oms.value,
      AccountId: this.state.AccountId,
      InstrumentId: pair.InstrumentId || 0,
      ProductId: this.state.buy ? product1.ProductId : product2.ProductId,
      Amount, // If buy, this will be amount of product1. If sell, this will be amount of product2
      OrderType: this.state.OrderType,
      MakerTaker,
      Side: this.state.buy ? 0 : 1,
      Quantity: Amount,
    };

    ShiftApp.getOrderFee(payload);
  };

  changeMode = buy => this.setState({buy}, this.getOrderFee);

  openModal = (indexOfDealPriceToMatch, amountOfCryptoBeingOrdered, price) => {
    let feeSend = 0;
    this.setState({
      openConfirm: true,
      index: indexOfDealPriceToMatch,
      price,
    });

    feeSend = this.state.buy ? amountOfCryptoBeingOrdered : price;

    this.getOrderFee(feeSend);
  };

  closeModal = () => this.setState({openConfirm: false, status: ''});

  fixedOrder = (amountOfCryptoBeingOrdered, amountToPayForABuy, LimitPrice) => {
    const pair = this.state.productPairs.find((prod) => this.state.productPair === prod.Symbol) || {};
    const product1 = this.state.balances.find((prod) => pair.Product1Symbol === prod.ProductSymbol) || {};
    const product2 = this.state.balances.find((prod) => pair.Product2Symbol === prod.ProductSymbol) || {};
    const balance = this.state.buy ? (product2.Amount - product2.Hold || 0) : (product1.Amount - product1.Hold || 0);
    const amountToBuyOrSell = this.state.buy ? amountToPayForABuy : amountOfCryptoBeingOrdered;
    let quantityOfCrypto = +amountOfCryptoBeingOrdered;
    const decimalPlaces = this.state.productsArray.find(prod => prod.ProductId === pair.Product1).DecimalPlaces || 2;
    const insufficientFunds = ShiftApp.translation('BUY_SELL_FIXED.INSUFFICIENT_FUNDS') || 'Insufficient Funds';

    // check if they have enough money
    if (amountToBuyOrSell > balance) {
      $.bootstrapGrowl(
        insufficientFunds,
        {
          type: 'danger',
          allow_dismiss: true,
          align: ShiftApp.config.growlwerPosition,
          delay: ShiftApp.config.growlwerDelay,
          offset: {from: 'top', amount: 30},
          left: '60%',
        },
      );
      return this.setState({ errorMsg: insufficientFunds });
    }

    this.setState({status: 'processing'});

    const payload = {
      AccountId: this.state.AccountId,
      ClientOrderId: 0,
      Side: this.state.buy ? 0 : 1,
      Quantity: truncateToDecimals(quantityOfCrypto, decimalPlaces),
      OrderIdOCO: 0,
      OrderType: 2,
      InstrumentId: pair.InstrumentId,
      TimeInForce: 4,
      OMSId: ShiftApp.oms.value,
      UseDisplayQuantity: false,
      LimitPrice,
    };

    return ShiftApp.sendOrder(payload);
  };

  // changePair = e => ShiftApp.setProductPair(e.target.value);

  changePairId = instrumentId => {
    this.setState({bookBuys: [], bookSells: []});
    window.doSelectIns(instrumentId);
  };

  changeInstrument = e => {
    const myInstrument = this.state.productPairs.filter((pair) => pair.Symbol === e.target.value)[0] || {};
    this.changePairId(myInstrument.InstrumentId);
  };

  getPricesDeal = () => (
    ShiftApp.config.dealPrices[this.state.productPair] || [100, 200, 500, 1000]
  );

  // Create modal with transaction details
  createModal = (indexOfDealPriceToMatch, product1symbol, productFullname, product2symbol) => {
    const prices_deal = this.getPricesDeal();
    const transaction_fee = isNaN(this.state.fee) ? '-' : this.state.fee;
    const action = this.state.buy ? 'Buy' :  'Sell';
    const priceUnformatted = prices_deal.find((deal, index) => indexOfDealPriceToMatch === index) || 0;
    const dealsQuantity = prices_deal
      .map(px => getQuantityForFixedPrice(
        px,
        ShiftApp.config.fixedOrdersMargin,
        this.state.buy ? this.state.bookSells : this.state.bookBuys,
        false,
        this.state.decimalPlaces[product2symbol]
      ));
    const dealObject = dealsQuantity.find(deal => deal.Price === priceUnformatted);
    const cryptoQuantity = dealObject && dealObject.Quantity;
    const netAmountProduct = action === 'Buy' ? product1symbol : product2symbol;
    const { feeProduct } = this.state;
    const net_amount_boughtUnformatted = Math.max(0, cryptoQuantity - transaction_fee);
    const net_amount_soldUnformatted = Math.max(0, priceUnformatted - transaction_fee);
    const net_amount_bought = isNaN(net_amount_boughtUnformatted)
      ? '-'
      : formatNumberToLocale(net_amount_boughtUnformatted, this.state.decimalPlaces[product1symbol]);
    const net_amount_sold = isNaN(net_amount_soldUnformatted)
      ? '-'
      : formatNumberToLocale(net_amount_soldUnformatted, this.state.decimalPlaces[product2symbol]);
    const price = formatNumberToLocale(priceUnformatted, this.state.decimalPlaces[product2symbol]);
    // const amountofCrypto = market
    //   ? formatNumberToLocale(cryptoQuantity, this.state.decimalPlaces[product1symbol])
    //   : '-';

    const confirmModal = (this.state.openConfirm &&
    <Modal idModal="confirm" close={this.closeModal}>
      <div div className="ap-widget">
        <div className="ap-header">
          <div className="ap-title text-center">
              <span style={{borderBottom: '2px solid #c8c8c8'}}>
                {`${action === 'Buy' ?
      (ShiftApp.translation('BUY_SELL_FIXED.TITLE_BUY') || 'Buy') :
      (ShiftApp.translation('BUY_SELL_FIXED.TITLE_SELL') || 'Sell')} ${ShiftApp.translation('BUY_SELL_FIXED.CONFIRMATION') || 'Confirmation'}`}
                </span>
            <div className="sub-title"/>
          </div>
          <div style={{float: 'right'}} className="ap-header-actions">
            <div>
              <div className="ap-header-actions-btn-close" onClick={this.closeModal}>×</div>
            </div>
          </div>
        </div>
        <div className="modal-body" style={{padding: '0px'}}>
          <div className="pad text-center">
            <p className="text-center modal-paragraph"/>
            <div className="row">
              <div className="col-sm-12">
                <p>{this.state.session.UserName}&nbsp;
                  {ShiftApp.translation('BUY_SELL_FIXED.CONFIRM_MSG_A') || 'you are about to'}&nbsp;
                  {action === 'Buy' ?
                    (ShiftApp.translation('BUY_SELL_FIXED.BUY_LOW') || 'Buy') :
                    (ShiftApp.translation('BUY_SELL_FIXED.SELL_LOW') || 'Sell')} {productFullname} &nbsp;
                  {ShiftApp.translation('BUY_SELL_FIXED.CONFIRM_MSG_B') || 'at the price below'}
                </p>
              </div>
              <div className="row">

                {/* ================================== TRANSACTION DETAILS ================================= */}
                <div className="confirm-modal col-xs-4 col-md-offset-2  text-left">
                  <div>{action === 'Buy' ? `${ShiftApp.translation('BUY_SELL_FIXED.TOTAL_COST') || 'Total Cost'} (${product2symbol})` : `${ShiftApp.translation('BUY_SELL_FIXED.TOTAL_SOLD') || 'Total'} ${product1symbol} ${ShiftApp.translation('BUY_SELL_FIXED.TOTAL_SOLD_2') || 'Sold'}`}</div>
                  <div>{ShiftApp.translation('BUY_SELL_FIXED.TOTAL_RECEIVED') || 'Total Received'}
                    ({action === 'Buy' ? product1symbol : product2symbol})
                  </div>
                  <div>{ShiftApp.translation('BUY_SELL_FIXED.TRANSACTION_FEE') || 'Transaction Fee'}
                    ({ feeProduct })
                  </div>
                  { netAmountProduct === feeProduct &&
                    <div>
                      { ShiftApp.translation('BUY_SELL_FIXED.NET_AMOUNT') || 'Net Amount Received' } ({ netAmountProduct })
                    </div>
                  }
                </div>

                  <div className="confirm-modal col-xs-4 text-right">
                    <div style={{ fontWeight: '600' }}>{action === 'Buy' ? price : formatNumberToLocale(cryptoQuantity, this.state.decimalPlaces[product1symbol])}</div>
                    <div style={{ fontWeight: '600' }}>{action === 'Buy' ? formatNumberToLocale(cryptoQuantity, this.state.decimalPlaces[product1symbol]) : price}</div>
                    <div style={{ fontWeight: '600' }}>{formatNumberToLocale(transaction_fee, this.state.decimalPlaces[this.state.buy ? product1symbol : product2symbol])}</div>
                    { netAmountProduct === feeProduct &&
                      <div style={{ fontWeight: '600' }}>
                        {action === 'Buy' ? net_amount_bought : net_amount_sold}
                      </div>
                    }
                  </div>
                  {/* // =============================== TRANSACTION DETAILS ================================= */}

              </div>
            </div>
            {this.state.status === 'killed' &&
            <p style={{color: 'red', marginTop: '2rem'}}>
              {ShiftApp.translation('BUY_SELL_FIXED.CHANGED_MARKET') || ' The market has changed and your order could not be filled. Please review the new market values and resubmit your order.'}
            </p>}
            <ProcessingButton
              className="deposit-button btn btn-action btn-modal"
              processing={this.state.status === 'processing'}
              onClick={() => this.fixedOrder(cryptoQuantity, price, dealObject.LimitPrice)}
            >
              {ShiftApp.translation('BUY_SELL_FIXED.PLACE_ORDER_A') || 'Place'} 
              {action === 'Buy' ?
                (ShiftApp.translation('BUY_SELL_FIXED.BTN_BUY') || 'Buy') :
                (ShiftApp.translation('BUY_SELL_FIXED.BTN_SELL') || 'Sell') } 
              {ShiftApp.translation('BUY_SELL_FIXED.PLACE_ORDER_B') || 'Order'}
            </ProcessingButton>
          </div>
        </div>
      </div>
    </Modal>);

    return confirmModal;
  };

  render() {
    const inputsRadio = [];
    const labelsRadio = [];
    const prices_deal = this.getPricesDeal();
    const availablePairs = getRetailInstruments(this.state.productPairs, this.state.productsArray);

    const options = availablePairs.map(pair => (
      <option
        key={pair.InstrumentId}
        value={pair.Symbol}
        // onChange={() => this.changePairId(pair.InstrumentId)}
      >
        {ShiftApp.config.reversePairs ? (pair.Product2Symbol + pair.Product1Symbol) : pair.Symbol}
      </option>
    ));

    if (availablePairs.length > 0) {
      for (let i = 0; i < availablePairs.length; i++) {
        inputsRadio.push(
          <input
            type="radio"
            value={availablePairs[i].Symbol}
            name="sc-1-1"
            id={`sc-1-1-${i + 1}`}
            readOnly
            onClick={() => this.changePairId(availablePairs[i].InstrumentId)}
            key={i}
            checked={(availablePairs[i].Symbol === this.state.productPair && true) || false}
          />,
        );
        labelsRadio.push(
          <label
            htmlFor={`sc-1-1-${i + 1}`}
            data-value={availablePairs[i].Symbol}
          >{availablePairs[i].Symbol}</label>,
        );
      }
    }
    const pair = this.state.productPairs.find(prod => this.state.productPair === prod.Symbol) || {};
    const product1 = this.state.balances.find(prod => pair.Product1Symbol === prod.ProductSymbol) || {};
    const product2 = this.state.balances.find(prod => pair.Product2Symbol === prod.ProductSymbol) || {};
    // const market = this.state.buy ? this.state.marketBuy : this.state.marketSell;
    const buyTabText = ShiftApp.translation('BUY_SELL_FIXED.TAB_BUY') || 'Buy';
    const sellTabText = ShiftApp.translation('BUY_SELL_FIXED.TAB_SELL') || 'Sell';
    const tabs = (
      <div>
        <span
          className={`tab tab-first ${this.state.buy ? 'active' : ''}`}
          onClick={() => this.changeMode(true)}
        >{buyTabText} {product1.fullName}</span>
        <span
          className={`tab tab-second ${!this.state.buy ? 'active' : ''}`}
          onClick={() => this.changeMode(false)}
        >{sellTabText} {product1.fullName}</span>
        <span className="blue-line"/>
      </div>
    );
    const buyText = ShiftApp.translation('BUY_SELL_FIXED.PRICE_BUY') || 'Buy';
    const sellText = ShiftApp.translation('BUY_SELL_FIXED.PRICE_SELL') || 'Sell';
    const action = this.state.buy ? buyText : sellText;
    const dealsQuantity = prices_deal
      .map(px => getQuantityForFixedPrice(
        px,
        ShiftApp.config.fixedOrdersMargin,
        this.state.buy ? this.state.bookSells : this.state.bookBuys,
        false,
        this.state.decimalPlaces[product2]
      ));
    const deals = prices_deal.map((px, index) => {
      const price = isNaN(px) ? '-' : px;
      const dealQuantity = dealsQuantity.find(deal => deal.Price === +px);
      const amountOfCryptoBeingOrdered = dealQuantity.Quantity === 'Infinity' ? 0 : dealQuantity.Quantity;
      const amountToBuyOrSell = isNaN(dealQuantity.Quantity) || dealQuantity === 'Infinity' ? NaN : dealQuantity.Quantity;
      const noMarketForDeal = !amountOfCryptoBeingOrdered || amountOfCryptoBeingOrdered === '-';

      return (
        <div
          className="col-md-3 col-sm-6 col-xs-12"
          key={`${px}-deal`}
          style={noMarketForDeal ? {opacity: 0.7, pointerEvents: 'none'} : {}}
        >
          <div className="pricing-table pricing-table-popular">
            {index === 2 && <span>{ShiftApp.translation('BUY_SELL_FIXED.POPULAR') || 'Popular'}</span>}
            <h3 className="pricing-table-title">{action} <br />
              {Number.isNaN(amountToBuyOrSell) ?
                '-' :
                `~${formatNumberToLocale(dealQuantity.Quantity, ShiftApp.config.decimalPlacesPrice || 0)}`} <br />
              {product1.ProductSymbol}
            </h3>
            <div className="pricing-table-content">
              <p className="pricing-table-for">{ShiftApp.translation('BUY_SELL_FIXED.FOR') || 'For'}</p>
              <p className="pricing-table-price">
                {product2.ProductSymbol === 'USD' ? `$${price}` : price} {product2.ProductSymbol}
              </p>
              {index === 0 && <div className="pricing-table-separator pricing-table-separator-100"/>}
              {index === 1 && <div className="pricing-table-separator pricing-table-separator-200"/>}
              {index === 2 && <div className="pricing-table-separator pricing-table-separator-500"/>}
              {index === 3 && <div className="pricing-table-separator pricing-table-separator-1000"/>}
              <button
                className="btn btn-orange"
                onClick={() => this.openModal(index, amountOfCryptoBeingOrdered, price)}
                disabled={noMarketForDeal}
              >{ShiftApp.translation('BUY_SELL_FIXED.SELECT') || 'Select'}</button>
            </div>
          </div>
        </div>);
    });

    return (
      <WidgetBase
        {...this.props}
        login
        error={this.state.errorMsg}
        success={this.state.successMsg}
        style={{width: '600px'}}
      >
        <VerificationRequired>
          <div className="clearfix buy-sell-tabs">
            {!this.props.hideSelect && <div className="tabs-main pull-left">{tabs}</div>}

            {!ShiftApp.config.usePairDropdown // eslint-disable-line
              ? (<div className="segmented-control pull-right">
                {inputsRadio}
                {labelsRadio}
              </div>)
              : options.length < 2
                ? null
                : (
                  <div>
                    {ShiftApp.config.simpleUiShowSelectByProduct ? 
                      <div className="select-instruments pull-right">
                        <ShiftInstsSelectByProduct ></ShiftInstsSelectByProduct>
                      </div>
                      : 
                      <div className="pull-right">
                        <select className="form-control pull-left" style={{borderRadius: '10px'}}
                                value={this.state.productPair} onChange={this.changeInstrument}>
                          {options}
                        </select>
                      </div>
                    }
                  </div>
              )}
          </div>
          <div className="row">
            {deals}
          </div>

          {this.createModal(this.state.index, product1.ProductSymbol, product1.fullName, product2.ProductSymbol)}

          <div className="sep"/>
          <div className="row">
            <div className="col-xs-12 pull-left">
              <p className="custom-currency-link">
                <a id="customAmount" href="buy-sell-custom.html">
                  {ShiftApp.translation('BUY_SELL_FIXED.CLICK_HERE') || 'Click here'}
                </a>
                {ShiftApp.translation('BUY_SELL_FIXED.INPUT_MESSAGE') || ' to input a custom amount of digital currency.'}
              </p>
            </div>
          </div>

          {this.state.status === 'filled' &&
          <Modal idModal="filledOrder" close={this.closeModal}>
            <div div className="ap-widget">
              <div className="ap-header">
                <div style={{float: 'right'}} className="ap-header-actions">
                  <div>
                    <div className="ap-header-actions-btn-close" onClick={this.closeModal}>×</div>
                  </div>
                </div>
              </div>
              <div className="modal-body" style={{padding: '0px'}}>
                <div className="pad text-center">
                  <p className="text-center modal-paragraph"/>
                  <div className="row">
                    <div className="col-sm-12">
                      <div style={{textAlign: 'center'}}>
                        <svg
                          fill="#6FC366"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{width: '10rem', height: '10rem'}}
                        >
                          <path d="M0 0h24v24H0z" fill="none"/>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52
                              2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                          />
                        </svg>
                        <p>{ShiftApp.translation('BUY_SELL_FIXED.SUBMIT_SUCCESS') || 'Order submitted and executed successfully'}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal>}
        </VerificationRequired>
      </WidgetBase>);
  }
}

ShiftBuyFixed.defaultProps = {
  hideSelect: false,
  hideCloseLink: true,
  sell: false,
};

ShiftBuyFixed.propTypes = {
  hideSelect: React.PropTypes.bool,
  sell: React.PropTypes.bool,
};

export default ShiftBuyFixed;
