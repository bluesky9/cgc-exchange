import React, { Component } from 'react';

// ShiftApp Components
import ShiftTicker from './shift-ticker';

// Shift Components
import ShiftWidgetWrapper from './shift-widget-wrapper';

export default class ShiftTickerContainer extends Component {
  render() {
    return !ShiftApp.config.dropdownInstrumentSelect && (
      <ShiftWidgetWrapper tabs={[ShiftApp.translation('WIDGET_WRAPPER.TAB.INSTRUMENTS') || 'Instruments']}>
        <ShiftTicker />
      </ShiftWidgetWrapper>
    );
  }
}