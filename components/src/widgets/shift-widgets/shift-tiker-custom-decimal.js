/* global ShiftApp */
import React from 'react';

class ShiftTickerCustomDecimal extends React.Component {
  constructor() {
    super();

    this.state = {
      data: {},
      bookLast: {}
    };
  };

  componentDidMount() {
    this.currentInstrument = Rx.Observable.combineLatest(
      ShiftApp.instrumentChange,
      ShiftApp.instruments,
      (selected, instruments) => {

        if (instruments.length === 0) {
          instruments = ShiftApp.instruments.value;
        }

        const instrument = instruments.find((inst) => inst.InstrumentId === +selected);

        return instrument;
      },
    )
      .filter(instrument => instrument)
      .subscribe((instrument) => this.setState({ currentInstrument: instrument.Symbol }));

    // Getting ticker data to display in ticker
    this.tickerData = ShiftApp.tickerBook
      .filter(data => Object.keys(data).length)
      .subscribe(data => {
        if (data.InstrumentId === +localStorage.getItem('SessionInstrumentId')) {
          this.setState({ data })
        }
        const bookLast = { ...this.state.bookLast };
        bookLast[data.InstrumentId] = data;
        this.setState({ bookLast });
      });

    this.instrumentChangeSub = ShiftApp.instrumentChange.subscribe(ins => {
      if (this.state.bookLast[ins]) {
        const newData = { ...this.state.bookLast[ins] };
        this.setState ({ data: newData });
      }
    });
  };

  componentWillUnmount() {
    this.tickerData.dispose();
    this.instrumentChangeSub.dispose();
    this.currentInstrument.dispose();
  };

  render() {
    const {
      LastTradedPx = 0,
      Rolling24HrPxChange = 0,
      BestBid = 0,
      BestOffer = 0,
      Rolling24HrVolume = 0,
      SessionHigh = 0,
      SessionLow = 0,
    } = this.state.data;
    if (!ShiftApp.config.DecimalPerInstrument) {
      ShiftApp.config.DecimalPerInstrument = {};
    }

    const decimalLength = ShiftApp.config.DecimalPerInstrument[this.state.currentInstrument] 
                          || ShiftApp.config.advancedUITickerDecimalPlaces 
                          || 2;

    return (
      <div className="ticker-wrapper">
        <div className="last-price">
          {ShiftApp.translation('TICKERS.LAST_PRICE') || 'Last Price'}
          <span>{LastTradedPx.toFixed(decimalLength)}</span>
        </div>
        <div className="last-price">
          {ShiftApp.translation('TICKERS.T24_HOUR_CHANGE') || '24 Hour Change'}
          <span>{Rolling24HrPxChange.toFixed(2)}%</span>
        </div>
        <div className="last-price">
          {ShiftApp.translation('TICKERS.BID') || 'Bid'}
          <span>{BestBid.toFixed(decimalLength)}</span>
        </div>
        <div className="last-price">
          {ShiftApp.translation('TICKERS.ASK') || 'Ask'}
          <span>{BestOffer.toFixed(decimalLength)}</span>
        </div>
        <div className="day-stat">
          {ShiftApp.translation('TICKERS.T24_HOUR_VOLUME') || '24 Hour Volume'}
          <span>{Rolling24HrVolume.toFixed(2)}</span>
        </div>
        <div className="day-stat">
          {ShiftApp.translation('TICKERS.T24_HOUR_LOW') || '24 Hour Low'}
          <span>{SessionLow.toFixed(decimalLength)}</span>
        </div>
        <div className="day-stat">
          {ShiftApp.translation('TICKERS.T24_HOUR_HIGH') || '24 Hour High'}
          <span>{SessionHigh.toFixed(decimalLength)}</span>
        </div>
      </div>
    );
  }
}

export default ShiftTickerCustomDecimal;
