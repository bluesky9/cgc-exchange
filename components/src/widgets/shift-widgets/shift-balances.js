/* global ShiftApp */
/* eslint-disable react/no-multi-comp */
import React from 'react';
import {
  formatNumberToLocale,
  sortProducts,
  allowDeposit,
  allowWithdraw
} from '../helper';
import WidgetBase from '../base';

import Modal from '../modal';
import WithdrawDigital from '../withdrawDigital';
import DepositDigital from '../depositDigital';
// import WithdrawFIAT2 from '../withdrawFIAT2'; // Replaced by ShiftWithdrawFIAT
// import DepositFIAT from '../depositFIAT'; // Replaced by ShiftDepositFiat
import VerificationRequired from '../verificationRequired';

import ShiftDepositFiat from './shift-deposit-fiat';
import ShiftWithdrawFIAT from './shift-withdraw-fiat';

// DIFFERENCE FROM DEFAULT BALANCES
// adds semantic classes for styling
// changes bootstrap classes for mobile views

class BalanceRow extends React.Component {
  constructor() {
    super();

    this.state = {
      rowState: 'success',
      isDeposit: false,
      isWithdraw: false,
      deposit: false,
      withdraw: false,
      name: false,
      balance: false
    };
  }

  openDeposit = () => this.setState({ isDeposit: true });

  openWithdraw = () => this.setState({ isWithdraw: true });

  closeDeposit = () => this.setState({ isDeposit: false });

  closeWithdraw = () => this.setState({ isWithdraw: false });

  mouseEnter = str => this.setState({ hover: str });

  mouseLeave = () => this.setState({ hover: '' });

  render() {
    let name =
      this.state.hover === 'name'
        ? this.props.fullName
        : this.props.ProductSymbol;
    if (ShiftApp.config.ReverseBalanceHover) {
      name =
        this.state.hover === 'name'
          ? this.props.ProductSymbol
          : this.props.fullName;
    }
    const availableBalance = this.props.Amount - this.props.Hold;
    const showWithdrawButton = allowWithdraw(this.props.ProductSymbol);
    const showDepositButton = allowDeposit(this.props.ProductSymbol);
    return (
      <div className="col-xs-12 currency-row">
        <div className="row">
          <div
            className="col-sm-2 col-xs-6 name"
            style={{ textAlign: 'center' }}
            onMouseEnter={() => this.mouseEnter('name')}
            onMouseLeave={this.mouseLeave}
          >
            <span className="currency-name">
              {ShiftApp.config.BalanceHover ? name : this.props.fullName}
              <span className="hidden-sm hidden-md hidden-lg">
                {formatNumberToLocale(
                  this.props.Amount,
                  this.props.DecimalPlaces
                )}
              </span>
            </span>
          </div>

          <div
            className="col-sm-2 col-xs-10 hidden-xs balance"
            style={{ textAlign: 'center' }}
            onMouseEnter={() => this.mouseEnter('balance')}
            onMouseLeave={this.mouseLeave}
          >
            <div>
              <small>
                {ShiftApp.translation('BALANCES.BALANCE') || 'Balance'}
              </small>
            </div>
            <div>
              {formatNumberToLocale(
                this.props.Amount,
                this.props.DecimalPlaces
              )}
            </div>
          </div>

          <div
            className="col-sm-2 col-xs-3 hold"
            style={{ textAlign: 'center' }}
            onMouseEnter={() => this.mouseEnter('hold')}
            onMouseLeave={this.mouseLeave}
          >
            <div>
              <small>{ShiftApp.translation('BALANCES.HOLD') || 'Hold'}</small>
            </div>
            <div style={{ position: 'relative' }}>
              {formatNumberToLocale(this.props.Hold, this.props.DecimalPlaces)}
            </div>
          </div>

          {/* COMMENTED OUT UNTIL PENDING WITHDRAW IS WORKING
          <div className="col-sm-2  col-xs-4" style={{textAlign:'right'}}
              onMouseEnter={this.mouseEnter.bind(this,'withdraw')} onMouseLeave={this.mouseLeave}>
          <div>
            <small>{ShiftApp.translation('BALANCES.PENDING_WITHDRAWS') || "Pending withdraws"}</small>
          </div>

          <div style={{position:'relative'}}> */}
          {/* this.state.hover==='withdraw' ?
            this.props.unconfirmedWithdraw : this.props.unconfirmedWithdraw.toFixed(this.props.decimalPlaces) */}
          {/* this.props.unconfirmedWithdraw ? <div style={{
             position:'absolute', backgroundColor:'#61bae2',
             height:7, width:7, borderRadius:'100%',
             top: 6, left: -13
             }}></div> : null } */}

          <div
            className="col-sm-2  col-xs-3 pending-deposits"
            style={{ textAlign: 'center' }}
            onMouseEnter={() => this.mouseEnter('deposit')}
            onMouseLeave={this.mouseLeave}
          >
            <div>
              <small>
                {ShiftApp.translation('BALANCES.PENDING_DEPOSITS') ||
                  'Pending deposits'}
              </small>
            </div>
            <div style={{ position: 'relative' }}>
              {formatNumberToLocale(
                this.props.PendingDeposits,
                this.props.DecimalPlaces
              )}
              {this.props.unconfirmed ? <div /> : null}
            </div>
          </div>

          {(showWithdrawButton || showDepositButton) && (
            <div
              className="col-sm-4  col-xs-12 action-buttons"
              style={{ textAlign: 'center' }}
            >
              {showWithdrawButton && (
                <button
                  className="deposit-button btn btn-action"
                  onClick={this.openDeposit}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_DEPOSIT') || 'Deposit'}
                </button>
              )}
              {showDepositButton && (
                <button
                  className="withdraw-button btn btn-action"
                  onClick={this.openWithdraw}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_WITHDRAW') ||
                    'Withdraw'}
                </button>
              )}
            </div>
          )}

          {this.props.ProductType === 'CryptoCurrency' &&
            this.state.isDeposit && (
              <Modal close={this.closeDeposit}>
                <VerificationRequired>
                  <DepositDigital
                    Product={this.props.ProductSymbol}
                    close={this.closeDeposit}
                  />
                </VerificationRequired>
              </Modal>
            )}

          {this.props.ProductType === 'CryptoCurrency' &&
            this.state.isWithdraw && (
              <Modal close={this.closeWithdraw}>
                <VerificationRequired>
                  <WithdrawDigital
                    Product={this.props.ProductSymbol}
                    balance={availableBalance}
                    fullName={this.props.fullName}
                    close={this.closeWithdraw}
                  />
                </VerificationRequired>
              </Modal>
            )}

          {this.props.ProductType === 'NationalCurrency' &&
            this.state.isDeposit && (
              <Modal close={this.closeDeposit}>
                <VerificationRequired>
                  <ShiftDepositFiat
                    Product={this.props.ProductSymbol}
                    close={this.closeDeposit}
                  />
                </VerificationRequired>
              </Modal>
            )}

          {this.props.ProductType === 'NationalCurrency' &&
            this.state.isWithdraw && (
              <Modal close={this.closeWithdraw}>
                <VerificationRequired>
                  <ShiftWithdrawFIAT
                    balance={this.props.Amount}
                    Product={this.props.ProductSymbol}
                    close={this.closeWithdraw}
                  />
                </VerificationRequired>
              </Modal>
            )}
        </div>
      </div>
    );
  }
}

BalanceRow.defaultProps = {
  fullName: '',
  ProductSymbol: '',
  Amount: null,
  Hold: null,
  ProductType: '',
  PendingDeposits: null,
  DecimalPlaces: null,
  unconfirmed: false
};

BalanceRow.propTypes = {
  fullName: React.PropTypes.string,
  ProductSymbol: React.PropTypes.string,
  Amount: React.PropTypes.number,
  Hold: React.PropTypes.number,
  ProductType: React.PropTypes.string,
  PendingDeposits: React.PropTypes.number,
  DecimalPlaces: React.PropTypes.number,
  unconfirmed: React.PropTypes.bool
};

class ShiftBalances extends React.Component {
  constructor() {
    super();

    this.state = {
      accountInformation: [],
      session: {},
      filter: 'all',
      showLogin: false,
      accountInformationError: '',
      sessionError: '',
      products: [],
      selectedAccount: null
    };
  }

  componentDidMount() {
    this.accountChangedEvent = ShiftApp.selectedAccount.subscribe(
      selectedAccount => {
        this.setState({ selectedAccount });
      }
    );
    this.products = ShiftApp.products.subscribe(data =>
      this.setState({ products: data.length && data })
    );
    this.accountInformation = ShiftApp.accountPositions.subscribe(
      accountInformation => {
        if (ShiftApp.config.sortProducts) {
          accountInformation.sort(sortProducts);
        }
        this.setState({ accountInformation });
      }
    );
  }

  componentWillUnmount() {
    this.accountChangedEvent.dispose();
    this.accountInformation.dispose();
    this.products.dispose();
  }

  switchFilter = filter => this.setState({ filter });

  loginToggle = () => this.setState({ showLogin: !this.state.showLogin });

  render() {
    this.state.accountInformation.forEach(balance => {
      const product =
        this.state.products.find(
          prod => prod.Product === balance.ProductSymbol
        ) || {};
      const name =
        this.state.products.find(
          prod => prod.Product === balance.ProductSymbol
        ) || {};

      /* eslint-disable no-param-reassign */
      balance.DecimalPlaces = product.DecimalPlaces;
      balance.ProductType = product.ProductType;
      balance.fullName = name.ProductFullName;
      /* eslint-enable no-param-reassign */
    });

    const rows = this.state.accountInformation
      .filter(currency => {
        if (currency.AccountId !== this.state.selectedAccount) return false;
        if (!currency.fullName || currency.fullName === undefined) return false;
        if (this.state.filter === 'all') return true;
        if (
          this.state.filter === 'crypto' &&
          currency.ProductType === 'CryptoCurrency'
        )
          return true;
        if (
          this.state.filter === 'fiat' &&
          currency.ProductType === 'NationalCurrency'
        )
          return true;
        return false;
      })
      .map((currency, idx) => <BalanceRow key={idx} {...currency} />);

    let filter = (
      <div>
        <span
          className={`tab ${this.state.filter === 'all' ? 'active' : ''}`}
          onClick={() => this.switchFilter('all')}
        >
          {ShiftApp.translation('BALANCES.TAB_ALL') || 'All'}
        </span>
        <span
          className={`tab ${this.state.filter === 'crypto' ? 'active' : ''}`}
          onClick={() => this.switchFilter('crypto')}
        >
          {ShiftApp.translation('BALANCES.TAB_CRYPTO') || 'Crypto'}
        </span>
        <span
          className={`tab ${this.state.filter === 'fiat' ? 'active' : ''}`}
          onClick={() => this.switchFilter('fiat')}
        >
          {ShiftApp.translation('BALANCES.TAB_FIAT') || 'Fiat'}
        </span>
      </div>
    );

    if (ShiftApp.config.noBalanceFilter) filter = <div />;

    return (
      // wrap all content in widget base
      <WidgetBase
        login
        {...this.props}
        left={filter}
        error={this.state.accountInformation.rejectReason}
        headerTitle={
          ShiftApp.translation('BALANCES.TITLE_TEXT') || 'Balances'
        }
        hideCloseLink
      >
        {ShiftApp.config.displayBalancesHeaders && (
          <div className="table-div-headings">
            <div className="row">
              <div
                className="col-sm-2 col-xs-6"
                style={{ textAlign: 'center', fontWeight: '500' }}
              >
                {ShiftApp.translation('BALANCES.CURRENCY') || 'Currency'}
              </div>
              <div
                className="col-sm-2 col-xs-10 hidden-xs"
                style={{ textAlign: 'center', fontWeight: '500' }}
              >
                {ShiftApp.translation('BALANCES.BALANCE') || 'Balance'}
              </div>
              <div
                className="col-sm-2 col-xs-3"
                style={{ textAlign: 'center', fontWeight: '500' }}
              >
                {ShiftApp.translation('BALANCES.HOLD') || 'Hold'}
              </div>
              <div
                className="col-sm-2  col-xs-3"
                style={{ textAlign: 'center', fontWeight: '500' }}
              >
                {ShiftApp.translation('BALANCES.PENDING_DEPOSITS') ||
                  'Pending deposits'}
              </div>
              <div
                className="col-sm-4  col-xs-12 hidden-xs"
                style={{ textAlign: 'center', fontWeight: '500' }}
              >
                {ShiftApp.translation('BALANCES.ACCOUNT_ACTIONS') ||
                  'Account Actions'}
              </div>
            </div>
          </div>
        )}
        {rows}
      </WidgetBase>
    );
  }
}

// Balances.defaultProps = {}

export default ShiftBalances;
