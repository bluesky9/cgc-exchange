/* global ShiftApp, $, window, alert, document */
/* eslint-disable react/no-multi-comp, no-alert */

/**
 * Config variables
 *
 */
import React from 'react';

import ProcessingButton from '../../misc/processingButton';
import ApInput from '../../misc/form/apInput';
import ShiftInputLabeled from '../../misc/shift-fields/shift-input-labeled';
import ShiftMaskInput from '../../misc/shift-fields/shift-mask-input';
import ApSelect from '../../misc/form/apSelect';
import ApDatepicker from '../../misc/form/apDatepicker';
import { states, countriesCodes } from '../../common';
import MaskedInput from 'react-maskedinput';
import ShiftFileDrop from '../../misc/shift-fields/shift-file-drop';
import Validate from '../../misc/form/validators';
import ShiftSelect from '../../misc/shift-fields/shift-select';

import { entityScopes } from '../../common/index';

const REQUIRED = 'required',
  HIDE = 'hide',
  REQUIRED_FILE = 'requiredFile',
  PHOTO_ID = 'photoId',
  SELFIE = 'selfie',
  PROOF_OF_ADDRESS = 'proofOfAddress';

export default class ShiftManualKyc extends React.Component {
  state = {
    // throwError booleans
    requiredFields: [
      'firstName',
      'lastName',
      'dob',
      'gender',
      'telephone',
      'email',
      'billingAddressLine1',
      'billingCity',
      'billingState',
      'billingZip',
      'billingCountry',
      'citizenship',
      'nationalID',
      'entityScope'
    ],
    userConfig: {
      firstName: '',
      middleName: '',
      lastName: '',
      dob: '',
      gender: '',
      telephone: '',
      email: '',
      billingAddressLine1: '',
      billingAddressLine2: '',
      billingSubdivision: '',
      billingCity: '',
      billingState: '',
      billingZip: '',
      billingCountry: '',
      cnpj: '',
      howHear: '',
      photoIdKey: '',
      selfieKey: '',
      citizenship: '',
      nationalID: '',
      entityScope: '',
      physical_docs: [],
      proofOfIncome: false
    },
    validations: {
      dob: false
    },

    hiddenFields: [],
    processing: false,
    confirmClose: '',
    verificationLevel: 0,
    dropErrors: {
      photoId: '',
      selfie: ''
    },
    dropAccepted: {
      photoId: false,
      selfie: false
    },
    dropRejected: {
      photoId: false,
      selfie: false
    },
    readyToUpload: false,
    displayErrorMessage: false,
    formValid: false,

    encodedFiles: []
  };

  componentDidMount() {
    if (ShiftApp.config.kycFields) {
      const requiredFields = [],
        requiredFiles = [],
        hiddenFields = [];

      for (let prop in ShiftApp.config.kycFields) {
        if (ShiftApp.config.kycFields[prop].indexOf(REQUIRED) !== -1) {
          requiredFields.push(prop);
        }
        if (ShiftApp.config.kycFields[prop].indexOf(REQUIRED_FILE) !== -1) {
          requiredFiles.push(prop);
        }
        if (ShiftApp.config.kycFields[prop].indexOf(HIDE) !== -1) {
          hiddenFields.push(prop);
        }
      }

      this.setState({ requiredFields, requiredFiles, hiddenFields });
    }

    this.userConfiguration = ShiftApp.getUserConfig.subscribe(data => {
      let configs = this.state.userConfig;

      if (data.length > 0) {
        data.reduce((item, i) => {
          return (configs[i.Key] = i.Value); // eslint-disable-line no-param-reassign
        }, {});

        configs.UseNoAuth =
          configs.UseNoAuth && (configs.UseNoAuth.toString() || 'true');
        configs.UseGoogle2FA =
          configs.UseGoogle2FA && (configs.UseGoogle2FA.toString() || 'false');

        this.setState({ userConfig: configs });
      }
    });

    this.accountInfo = ShiftApp.accountInfo.subscribe(data => {
      this.setState({ verificationLevel: data.VerificationLevel });
    });

    this.getUser = ShiftApp.getUser.subscribe(user => {
      let configs = this.state.userConfig;
      configs.email = user.Email;

      this.setState({ userConfig: configs });
    });

    ShiftApp.getUserCon({ UserId: ShiftApp.userData.value.UserId });
    this.checkAllValidations();
  }

  componentWillUnmount() {
    /* eslint-disable no-unused-expressions */
    this.userConfiguration && this.userConfiguration.dispose();
    this.verifyLevel && this.verifyLevel.dispose();
    this.verifcationLevelUpdate && this.verifcationLevelUpdate.dispose();
    this.validatorRes && this.validatorRes.dispose();
    this.getUser && this.getUser.dispose();
    /* eslint-enable no-unused-expressions */
  }

  dateChanged = field => date => {
    const { userConfig } = this.state;
    userConfig[field] = date;
    this.checkAllRequiredFields();
    this.checkDob();
    this.setState({ userConfig });
  };

  genderChanged = e => {
    const userConfig = this.state.userConfig;
    userConfig.gender = e.target.value;
    this.checkAllRequiredFields();
    this.setState({ userConfig });
  };

  handleProofOfIncomeChange = () =>
    this.setState(prevState => {
      const { userConfig } = prevState;
      userConfig.proofOfIncome = !userConfig.proofOfIncome;
      return { userConfig };
    }, this.checkAllRequiredFields);

  // File upload event handlers

  handleFileDrop = (uploadType, dropStatus) => file => {
    this.setState(
      prevState => ({
        [dropStatus]: {
          ...prevState[dropStatus],
          [uploadType]: true
        }
      }),
      this.checkAllRequiredFields
    );

    if (dropStatus === 'dropRejected') {
      if (!Validate.imageFileType(file)) {
        this.setState(prevState => ({
          dropErrors: {
            ...prevState.dropErrors,
            [uploadType]:
              ShiftApp.translation('VERIFY.FILE_TYPE_ERROR') ||
              'the image is not a valid file type'
          }
        }));
      } else if (!Validate.imageFileSize(file)) {
        this.setState(prevState => ({
          dropErrors: {
            ...prevState.dropErrors,
            [uploadType]:
              ShiftApp.translation('VERIFY.FILE_SIZE_ERROR') ||
              'the image file size is too big. Upload files less than 3 MB'
          }
        }));
      }
    }
  };

  uploadPhotosAndSubmit = e => {
    e.preventDefault();
    if (this.state.formValid && !ShiftApp.config.synapseKyc) {
      this.setState({ readyToUpload: true });
    } else if (this.state.formValid && ShiftApp.config.synapseKyc) {
      this.submit();
    } else {
      this.setState({ displayErrorMessage: true });
    }
  };

  changed = e => {
    const userConfig = this.state.userConfig;
    userConfig[e.target.name] = e.target.value;
    this.checkAllRequiredFields();
    this.setState({ userConfig });
  };

  /**
   * @param {String}  fieldName
   * @param {String}  value
   * @param {Integer} messagesCount
   * @param {Boolean} formIsValid
   */
  changedFieldMask = (fieldName, value, messagesCount, formIsValid) => {
    if (messagesCount === 0 && formIsValid) {
      const userConfig = this.state.userConfig;
      userConfig[fieldName] = value;
      this.checkAllRequiredFields();
      this.setState({ userConfig });
    }
  };

  countryChange = (fieldName, e) => {
    const userConfig = this.state.userConfig;
    userConfig[fieldName] = e.target.value;
    this.checkAllRequiredFields();
    this.setState({ userConfig });
  };

  entityScopeChange = e => {
    const userConfig = this.state.userConfig;
    userConfig.entityScope = e.target.value;
    this.checkAllRequiredFields();
    this.setState({ userConfig });
  };

  submit = () => {
    if (this.state.formValid) {
      if (ShiftApp.config.synapseKyc) {
        this.submitSynapse();
      } else {
        this.submitAP();
      }
    } else {
      // If fields specified in isRequired() function are empty,
      return false;
    }
  };

  submitSynapse = () => {
    const { userConfig } = this.state;

    this.setState({ processing: true });

    const newUserRequestBody = {
      logins: [{ email: userConfig.email }],
      phone_numbers: [userConfig.email, userConfig.telephone],
      legal_names: [this.parseFullName()],
      extra: {
        supp_id: ShiftApp.userData.value.UserId,
        cip_tag: 1,
        is_business: false
      }
    };

    const virtualDocs = userConfig.nationalID
      ? [
          {
            document_value: userConfig.nationalID,
            document_type:
              ShiftApp.config.kycNationalIDType || 'PERSONAL_IDENTIFICATION'
          }
        ]
      : [];

    const kycRequestBody = {
      user_id: ShiftApp.userData.value.UserId,
      documents: [
        {
          email: userConfig.email,
          phone_number: userConfig.telephone,
          ip: '::1',
          name: this.parseFullName(),
          alias: this.parseFullName(),
          entity_scope: userConfig.entityScope,
          entity_type: userConfig.gender,
          day: this.parseDate('day'),
          month: this.parseDate('month'),
          year: this.parseDate('year'),
          address_street: userConfig.billingAddressLine1,
          address_city: userConfig.billingCity,
          address_subdivision: userConfig.billingState,
          address_postal_code: userConfig.billingZip,
          address_country_code: userConfig.billingCountry,
          physical_docs: userConfig.physical_docs,
          virtual_docs: virtualDocs,
          social_docs: []
        }
      ]
    };

    console.log(kycRequestBody);
    // debugger;

    fetch('https://synapse.shiftcrypto.com/api/users/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(newUserRequestBody)
    })
      .then(response => response.json())
      .then(user => {
        if (!user.error) {
          fetch('https://synapse.shiftcrypto.com/api/users/docs', {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify(kycRequestBody)
          })
            .then(response => response.json())
            .then(user => {
              if (!user.error_message) {
                this.submitAP();
              } else {
                console.error(user.error_message);
              }
            })
            .catch(error => {
              console.error('Document submission error:', error);
              this.setState({ comfirmClose: 'fail' });
            });
        } else {
          console.error(user.error);
        }
      })
      .catch(error => {
        console.error('Add user error:', error);
        this.setState({ comfirmClose: 'fail' });
      });
  };

  parseFullName = () => {
    let fullName = this.state.userConfig.firstName;
    if (this.state.userConfig.middleName) {
      fullName += ' ' + this.state.userConfig.middleName;
    }
    fullName += ' ' + this.state.userConfig.lastName;
    return fullName;
  };

  parseDate = dateElement => {
    const dateElements = this.state.userConfig.dob.split('/');
    if (dateElement === 'day') {
      return parseInt(dateElements[1]);
    } else if (dateElement === 'month') {
      return parseInt(dateElements[0]);
    } else if (dateElement === 'year') {
      return parseInt(dateElements[2]);
    } else {
      return false;
    }
  };

  submitAP = () => {
    // Checking for required fields; returns submit as false if any kycRequiredFields are empty
    const that = this;
    const configIn = [];
    const growlerOptions = {
      allow_dismiss: ShiftApp.config.growlerDismiss || true,
      align: 'center',
      delay: ShiftApp.config.growlerDelay,
      offset: { from: 'top', amount: 30 },
      left: '60%'
    };
    const growlerOptionsLongDelay = {
      allow_dismiss: true,
      align: 'center',
      delay: 20000,
      offset: { from: 'top', amount: 30 },
      left: '60%'
    };
    let configs = {};
    let server = {};
    let userInfo = {};
    this.setState({ processing: true });

    Object.keys(this.state.userConfig).forEach(key => {
      let entry,
        value = this.state.userConfig[key];

      entry = {
        Key: key,
        Value: value.toString()
      };

      configIn.push(entry);
    });

    let manualKYCFormSubmitted = {
      Key: 'levelIncreaseStatus',
      value: 'underReview'
    };

    configIn.push(manualKYCFormSubmitted);

    configs = {
      UserId: ShiftApp.userData.value.UserId,
      Config: configIn
    };

    // Set userInfo
    userInfo = {
      firstName: this.state.userConfig.firstName,
      middleName: this.state.userConfig.middleName,
      lastName: this.state.userConfig.lastName,
      dob: this.state.userConfig.dob,
      accountName: JSON.stringify(ShiftApp.userData.value.UserId),
      email: this.state.userConfig.email,
      billingStreetAddress: this.state.userConfig.billingStreetAddress,
      billingCountry: this.state.userConfig.billingCountry,
      billingCity: this.state.userConfig.billingCity,
      billingState: this.state.userConfig.billingState,
      billingZip: this.state.userConfig.billingZip,
      cnpj: this.state.userConfig.cnpj,
      howHear: this.state.userConfig.howHear,
      phone: this.state.userConfig.telephone,
      state: this.state.userConfig.billingState,
      billingFlatNumber: '',
      billingStreetNumber: this.state.userConfig.billingStreetNumber, // greenId has it separated from streetName
      billingStreetType: this.state.userConfig.billingStreetType, // greenId specific, shall not be included in streetName
      salutation: ''
      // proofOfIncome: this.state.userConfig.proofOfIncome.toString()
    };

    ShiftApp.setUserConfig.subscribe(data => {
      if (data.result) {
        if (ShiftApp.config.kycType === 'ManualKYC') {
          this.setState({ processing: false, confirmClose: 'success' });
        }
      }
      if (!data.result && data.length > 0) {
        $.bootstrapGrowl(
          ShiftApp.translation('KYC.INFO_DENIED') ||
            'Your information has been denied',
          { ...growlerOptions, type: 'danger' }
        );

        if (ShiftApp.config.kycType === 'ManualKYC') {
          this.setState({ processing: false, confirmClose: 'success' });
        }
      }
      if (data.result) return true;
    });

    const filteredConfigs = configs.Config.filter(
      config => !(config.Key === 'physical_docs')
    );
    configs.Config = filteredConfigs;

    // debugger;

    // Setting user config
    ShiftApp.setUserCon(configs);
  };

  checkAllValidations = () => {
    this.checkAllRequiredFields();
    this.checkDob();
  };

  checkAllRequiredFields = () => {
    const { userConfig, dropAccepted } = this.state;

    if (
      this.state.requiredFields.every(field => {
        return field in userConfig && userConfig[field];
      }) &&
      this.state.requiredFiles.every(file => {
        return file in dropAccepted && dropAccepted[file];
      }) &&
      this.state.validations.dob
    ) {
      this.setState({ formValid: true });
    } else {
      this.setState({ formValid: false });
    }
  };

  checkIfRequired(field) {
    const kycFields = ShiftApp.config.kycFields || [];
    const kycRequiredFields = Object.keys(kycFields).filter(key => {
      return kycFields[key].includes('required');
    });

    return kycRequiredFields.indexOf(field) > -1;
  }

  checkAllUploadsAreComplete = uploadType => fileKey => {
    this.setState(
      prevState => {
        const newConfig = prevState.userConfig;
        newConfig[`${uploadType}Key`] = `${ShiftApp.config.aws.urlPrefix}${
          ShiftApp.config.aws.bucketName
        }/${fileKey[0]}`;
        return { userConfig: newConfig };
      },
      () => {
        const userConfig = this.state.userConfig;
        if (
          (this.state.requiredFields.indexOf(PHOTO_ID) === -1 ||
            userConfig.photoIdKey) &&
          (this.state.requiredFields.indexOf(SELFIE) === -1 ||
            userConfig.selfieKey)
        ) {
          this.setState({ readyToUpload: false }, this.submit);
        }
      }
    );
  };

  checkDob = () => {
    const validations = this.state.validations;
    const dob = Date.parse(this.state.userConfig.dob);
    const dateToday = Date.now();

    // Checks if over 18 years old, or 5.676e11 ms if you're a computer
    if (dateToday - dob > 5.676e11) {
      validations.dob = true;
    } else {
      validations.dob = false;
    }
    this.setState({ validations });
  };

  renderFileUpload = uploadType => {
    if (this.state.dropAccepted[uploadType]) {
      return (
        <div>
          <p>
            {ShiftApp.translation('VERIFY.FILE_SELECT_SUCCESS') ||
              'File ready to be uploaded.'}
          </p>
        </div>
      );
    } else if (this.state.dropRejected[uploadType]) {
      return (
        <div>
          <p>{`${ShiftApp.translation('VERIFY.FILE_SELECT_FAIL') ||
            'Your file cannot be uploaded, '}${
            this.state.dropErrors[uploadType]
          }.`}</p>
          {this.renderFileUploadButton(uploadType)}
        </div>
      );
    } else {
      return this.renderFileUploadButton(uploadType);
    }
  };

  preventDefaultButtonAction = e => e.preventDefault();

  renderFileUploadButton = uploadType => {
    if (uploadType === PHOTO_ID) {
      return (
        <div>
          <button onClick={this.preventDefaultButtonAction}>
            {ShiftApp.translation('VERIFY.UPLOAD_PHOTO_ID') ||
              'Upload Photo ID'}
          </button>
        </div>
      );
    } else if (uploadType === SELFIE) {
      return (
        <div>
          <button onClick={this.preventDefaultButtonAction}>
            {ShiftApp.translation('VERIFY.UPLOAD_SELFIE_BUTTON') ||
              'Upload Selfie'}
          </button>
        </div>
      );
    } else if (uploadType === PROOF_OF_ADDRESS) {
      return (
        <div>
          <button onClick={this.preventDefaultButtonAction}>
            {ShiftApp.translation('VERIFY.UPLOAD_PROOF_OF_ADDRESS') ||
              'Upload Proof of Address'}
          </button>
        </div>
      );
    } else if (uploadType === 'videoAuthorization') {
      return (
        <div>
          <button onClick={this.preventDefaultButtonAction}>
            {ShiftApp.translation('VERIFY.UPLOAD_VIDEO_AUTHORIZATION') ||
              'Upload Video Authorization'}
          </button>
        </div>
      );
    }
  };

  /**
   *
   * @param {String} field - name of field
   *
   * @return {JSX} - element with asterisk
   */
  requiredElement(field) {
    let element = '';
    if (this.state.requiredFields.indexOf(field) !== -1) {
      element = <span className="required">*</span>;
    }

    return element;
  }

  /**
   *
   * @param {String} field - name of field
   *
   * @return {Object} - { display: 'none' }
   */
  hideField(field) {
    let option = {};
    if (this.state.hiddenFields.indexOf(field) !== -1) {
      option = { display: 'none' };
    }

    return option;
  }

  setEncodedFiles = uploadType => encodedFile => {
    this.setState(prevState => {
      const userConfig = prevState.userConfig;
      if (uploadType === PHOTO_ID) {
        userConfig.physical_docs.push({
          document_value: encodedFile,
          document_type: 'GOVT_ID'
        });
        const photoIdAccepted = prevState.dropAccepted;
        photoIdAccepted.photoId = true;

        return { userConfig, dropAccepted: photoIdAccepted };
      } else if (uploadType === SELFIE) {
        userConfig.physical_docs.push({
          document_value: encodedFile,
          document_type: 'SELFIE'
        });
        const selfieAccepted = prevState.dropAccepted;
        selfieAccepted.photoId = true;

        return { userConfig, dropAccepted: selfieAccepted };
      } else if (uploadType === PROOF_OF_ADDRESS) {
        userConfig.physical_docs.push({
          document_value: encodedFile,
          document_type: 'PROOF_OF_ADDRESS'
        });
      } else if (uploadType === 'videoAuthorization') {
        userConfig.physical_docs.push({
          document_value: encodedFile,
          document_type: 'VIDEO_AUTHORIZATION'
        });
        return { userConfig };
      }
    });
  };

  render() {
    const countries = countriesCodes.map(country => (
      <option value={country.code} key={country.code}>
        {country.name}
      </option>
    ));

    if (ShiftApp.config.onlyShowOneCountryKYC) {
      if (ShiftApp.config.kycCountriesList.length > 1) {
        var listSpecificCountries = countriesCodes
          .filter(function(country) {
            return (
              ShiftApp.config.kycCountriesList.indexOf(country.name) > -1
            );
          })
          .map(theCountry => (
            <option value={theCountry.code} key={theCountry.code}>
              {theCountry.name}
            </option>
          ));
      } else {
        var listSpecificCountries = countriesCodes
          .filter(country => {
            return country.name === ShiftApp.config.kycCountriesList;
          })
          .map(theCountry => (
            <option value={theCountry.code} key={theCountry.code}>
              {theCountry.name}
            </option>
          ));
      }
    }

    const countryNow = {
      billingCountry:
        countriesCodes.find(
          country => country.code === this.state.userConfig.billingCountry
        ) || '',
      citizenship:
        countriesCodes.find(
          country => country.code === this.state.userConfig.citizenship
        ) || ''
    };

    const statesOptions = states.map(state => (
      <option value={state.code} key={state.code}>
        {state.name}
      </option>
    ));

    const maxDate = new Date();
    maxDate.setFullYear(new Date().getFullYear() - 18);

    return (
      <div className="manual-kyc">
        {this.state.processing && (
          <div className="loader-container">
            <div className="loader">Loading...</div>
          </div>
        )}

        {this.state.confirmClose === 'success' && (
          <div className="loader-container-confirm">
            <p>
              {ShiftApp.translation('KYC.INFO_ACCEPTED') ||
                'Your information has been accepted'}
            </p>
            <button
              className="confirm-close-btn blue-btn"
              onClick={this.props.close}
            >
              {ShiftApp.translation('COMMON.OKAY') || 'Okay'}
            </button>
          </div>
        )}

        {this.state.confirmClose === 'fail' && (
          <div className="loader-container-confirm">
            <p>
              {ShiftApp.translation('KYC.SUBMIT_ERROR') ||
                'There was an error processing your verification. Please try again.'}
            </p>
            <button
              className="confirm-close-btn blue-btn"
              onClick={this.props.close}
            >
              {ShiftApp.translation('COMMON.OKAY') || 'Okay'}
            </button>
          </div>
        )}

        {!this.state.processing &&
          !this.state.confirmClose && (
            <form
              onSubmit={this.uploadPhotosAndSubmit}
              style={{ overflow: 'hidden' }}
              className="manual-kyc-form"
            >
              <div className="form-row row-1">
                <div className="form-field">
                  <p>
                    {ShiftApp.translation('VERIFY.FIRSTNAME') || 'First Name'}
                    {this.requiredElement('firstName')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.FIRSTNAME') || 'First Name'
                    }
                    name="firstName"
                    ref="firstName"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.firstName}
                  />
                </div>

                <div
                  className="form-field"
                  style={this.hideField('middleName')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.MIDDLENAME') ||
                      'Middle Name'}
                    {this.requiredElement('middleName')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.MIDDLENAME') ||
                      'Middle Name'
                    }
                    name="middleName"
                    ref="middleName"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.middleName}
                  />
                </div>

                <div className="form-field">
                  <p>
                    {ShiftApp.translation('VERIFY.LASTNAME') || 'Last Name'}
                    {this.requiredElement('lastName')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.LASTNAME') || 'Last Name'
                    }
                    name="lastName"
                    ref="lastName"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.lastName}
                  />
                </div>
              </div>

              <div
                className="form-row row-2"
                style={this.hideField('telephone')}
              >
                <div className="form-field">
                  <p>
                    {ShiftApp.translation('VERIFY.PHONE') || 'Phone Number'}
                    {this.requiredElement('telephone')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.PHONE') || 'Phone Number'
                    }
                    name="telephone"
                    ref="telephone"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.telephone}
                  />
                </div>
                <div className="form-field" style={this.hideField('dob')}>
                  <p>
                    {ShiftApp.translation('VERIFY.DATE') || 'DOB'}
                    {this.requiredElement('dob')}
                  </p>
                  <ApDatepicker
                    maxDate={maxDate}
                    name="dob"
                    dob
                    value={this.state.userConfig.dob}
                    onChange={this.dateChanged('dob')}
                    onBlur={this.checkDob}
                    throwError={this.state.requiredFields.dob}
                    errorDescription={
                      ShiftApp.translation('VERIFY.REQUIRED_TEXT') ||
                      'This field is required'
                    }
                    label={ShiftApp.translation('VERIFY.DATE') || 'DOB'}
                  />
                </div>

                <div
                  className="form-field gender-field"
                  style={this.hideField('gender')}
                >
                  <p>{ShiftApp.translation('VERIFY.GENDER') || 'Gender'}</p>
                  <div className="gender-option-group">
                    <div className="gender-option">
                      <input
                        type="radio"
                        name="gender"
                        value="M"
                        checked={this.state.userConfig.gender === 'M'}
                        onChange={this.genderChanged}
                      />
                      <p>
                        {ShiftApp.translation('VERIFY.GENDER_MALE') || 'Male'}
                      </p>
                    </div>
                    <div className="gender-option">
                      <input
                        type="radio"
                        name="gender"
                        value="F"
                        checked={this.state.userConfig.gender === 'F'}
                        onChange={this.genderChanged}
                      />
                      <p>
                        {ShiftApp.translation('VERIFY.GENDER_FEMALE') ||
                          'Female'}
                      </p>
                    </div>
                    <div className="gender-option">
                      <input
                        type="radio"
                        name="gender"
                        value="O"
                        checked={this.state.userConfig.gender === 'O'}
                        onChange={this.genderChanged}
                      />
                      <p>
                        {ShiftApp.translation('VERIFY.GENDER_OTHER') ||
                          'Other'}
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="form-row">
                <div
                  className="form-field"
                  style={this.hideField('billingAddressLine1')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.BILLING_ADDRESS_LINE1') ||
                      'Billing Address Line 1'}
                    {this.requiredElement('billingAddressLine1')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.BILLING_ADDRESS_LINE1') ||
                      'Billing Address Line 1'
                    }
                    name="billingAddressLine1"
                    ref="billingAddressLine1"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.billingAddressLine1}
                  />
                </div>
              </div>

              <div className="form-row row-4">
                <div
                  className="form-field billing-address-line-2-field"
                  style={this.hideField('billingAddressLine2')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.BILLING_ADDRESS_LINE2') ||
                      'Billing Address Line 2'}
                    {this.requiredElement('billingAddressLine2')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.BILLING_ADDRESS_LINE2') ||
                      'Billing Address Line 2'
                    }
                    name="billingAddressLine2"
                    ref="billingAddressLine2"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.billingAddressLine2}
                  />
                </div>
              </div>

              <div className="form-row row-5">
                <div
                  className="form-field city-field"
                  style={this.hideField('billingCity')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.CITY') || 'City'}
                    {this.requiredElement('billingCity')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.CITY') || 'City'
                    }
                    name="billingCity"
                    ref="billingCity"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.billingCity}
                  />
                </div>

                <div
                  className="form-field state-field"
                  style={this.hideField('billingState')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.STATE') || 'State'}
                    {this.requiredElement('billingState')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.STATE') || 'State'
                    }
                    name="billingState"
                    ref="billingState"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.billingState}
                  />
                </div>

                <div
                  className="form-field zip-field"
                  style={this.hideField('billingZip')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.ZIP') ||
                      'ZIP Code/Postal Code'}
                    {this.requiredElement('billingZip')}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('VERIFY.ZIP') ||
                      'ZIP Code/Postal Code'
                    }
                    name="billingZip"
                    ref="billingZip"
                    className="form-control"
                    onChange={this.changed}
                    value={this.state.userConfig.billingZip}
                  />
                </div>

                <div
                  className="form-field country-field"
                  style={this.hideField('billingCountry')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.COUNTRY') || 'Country'}
                    {this.requiredElement('billingCountry')}
                  </p>
                  <ShiftSelect
                    name="billingCountry"
                    onChange={this.countryChange.bind(this, 'billingCountry')}
                    value={this.state.userConfig.billingCountry}
                    errorDescription={
                      ShiftApp.translation('VERIFY.REQUIRED_TEXT') ||
                      'This field is required'
                    }
                    label={
                      ShiftApp.translation('VERIFY.COUNTRY') || 'Country'
                    }
                  >
                    <option>
                      {countryNow.billingCountry.name ||
                        (ShiftApp.translation('VERIFY.COUNTRY') ||
                          'Select Country') +
                          (this.checkIfRequired('billingCountry') ? '*' : '')}
                    </option>
                    {ShiftApp.config.onlyShowOneCountryKYC
                      ? listSpecificCountries
                      : countries}
                  </ShiftSelect>
                </div>
              </div>

              <div className="form-row row-6">
                {ShiftApp.config.synapseKyc && (
                  <div className="form-field industry-field">
                    <p>
                      {ShiftApp.translation('VERIFY.INDUSTRY') || 'Industry'}
                      {this.requiredElement('entityScope')}
                    </p>
                    <ShiftSelect
                      name="entityScope"
                      onChange={this.entityScopeChange}
                      value={this.state.userConfig.entityScope}
                      errorDescription={
                        ShiftApp.translation('VERIFY.REQUIRED_TEXT') ||
                        'This field is required'
                      }
                      label={
                        ShiftApp.translation('VERIFY.INDUSTRY') || 'Industry'
                      }
                    >
                      <option>
                        {(ShiftApp.translation('VERIFY.SELECT_INDUSTRY') ||
                          'Select Industry') +
                          (this.checkIfRequired('entityScope') ? '*' : '')}
                      </option>
                      {entityScopes.map((entityScope, index) => (
                        <option key={index}>{entityScope}</option>
                      ))}
                    </ShiftSelect>
                  </div>
                )}

                {ShiftApp.config.kycShowCNPJField && (
                  <div
                    className="form-field cnpj-field"
                    style={this.hideField('cnpj')}
                  >
                    <p>
                      {ShiftApp.translation('VERIFY.CNPJ') || 'CPF/CNPJ'}
                      {this.requiredElement('cnpj')}
                    </p>
                    <ShiftMaskInput
                      name="cnpj"
                      value={this.state.userConfig.cnpj}
                      validations={
                        ShiftApp.config.kycFields.cnpj &&
                        ShiftApp.config.kycFields.cnpj
                      }
                      onChange={this.changedFieldMask}
                      label={
                        ShiftApp.translation('VERIFY.CNPJ') || 'CPF/CNPJ'
                      }
                      mask="111.111.111-11"
                    />
                  </div>
                )}

                {ShiftApp.config.kycShowHowHearField && (
                  <div
                    className="form-field how-hear-field"
                    style={this.hideField('howHear')}
                  >
                    <p>
                      {ShiftApp.translation('VERIFY.HOW_HEAR') ||
                        'How did you hear about us?'}
                      {this.requiredElement('howHear')}
                    </p>
                    <ShiftInputLabeled
                      name="howHear"
                      value={this.state.userConfig.howHear}
                      onChange={this.changed}
                      label={
                        ShiftApp.translation('VERIFY.HOW_HEAR') ||
                        'How did you hear about us?'
                      }
                      placeholder={
                        ShiftApp.translation('VERIFY.HOW_HEAR') ||
                        'How did you hear about us?'
                      }
                    />
                  </div>
                )}
              </div>

              <div className="form-row row-7">
                <div
                  className="form-field cnpj-field"
                  style={this.hideField('nationalID')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.NATIONAL_ID') ||
                      'National ID Number'}
                    {this.requiredElement('nationalID')}
                  </p>
                  <ShiftInputLabeled
                    name="nationalID"
                    value={this.state.userConfig.nationalID}
                    onChange={this.changed}
                    label={
                      ShiftApp.translation('VERIFY.NATIONAL_ID') ||
                      'National ID Number'
                    }
                    placeholder={
                      ShiftApp.translation('VERIFY.NATIONAL_ID') ||
                      'National ID Number'
                    }
                  />
                </div>

                <div
                  className="form-field citizenship"
                  style={this.hideField('citizenship')}
                >
                  <p>
                    {ShiftApp.translation('VERIFY.CITIZENSHIP') ||
                      'Citizenship'}
                    {this.requiredElement('citizenship')}
                  </p>
                  <ShiftSelect
                    name="citizenship"
                    onChange={this.countryChange.bind(this, 'citizenship')}
                    value={this.state.userConfig.citizenship}
                    label={
                      ShiftApp.translation('VERIFY.CITIZENSHIP') ||
                      'Citizenship'
                    }
                  >
                    <option>
                      {countryNow.citizenship.name ||
                        (ShiftApp.translation('VERIFY.CITIZENSHIP') ||
                          'Select Country') +
                          (this.checkIfRequired('citizenship') ? '*' : '')}
                    </option>
                    {ShiftApp.config.onlyShowOneCountryKYC
                      ? listSpecificCountries
                      : countries}
                  </ShiftSelect>
                </div>
              </div>

              <div className="form-row row-7">
                <div className="form-field" style={this.hideField(PHOTO_ID)}>
                  <p>
                    {ShiftApp.translation('VERIFY.PHOTO_ID') || 'Photo ID:'}
                    {this.requiredElement(PHOTO_ID)}
                  </p>
                  <p>
                    {ShiftApp.translation('VERIFY.PHOTO_ID_DESCRIPTION') ||
                      'Government issued photo ID, such as a driver’s license or passport.'}
                  </p>
                  <ShiftFileDrop
                    onDropAccepted={this.handleFileDrop(
                      PHOTO_ID,
                      'dropAccepted'
                    )}
                    onDropRejected={this.handleFileDrop(
                      PHOTO_ID,
                      'dropRejected'
                    )}
                    onUploadComplete={this.checkAllUploadsAreComplete(PHOTO_ID)}
                    onEncodeComplete={this.setEncodedFiles(PHOTO_ID)}
                    readyToUpload={this.state.readyToUpload}
                    encode
                    multiple={false}
                  >
                    {this.renderFileUpload(PHOTO_ID)}
                  </ShiftFileDrop>
                </div>

                <div className="form-field" style={this.hideField(SELFIE)}>
                  <p>
                    {ShiftApp.translation('VERIFY.UPLOAD_SELFIE') ||
                      'Selfie:'}
                    {this.requiredElement(SELFIE)}
                  </p>
                  <ShiftFileDrop
                    onDropAccepted={this.handleFileDrop(SELFIE, 'dropAccepted')}
                    onDropRejected={this.handleFileDrop(SELFIE, 'dropRejected')}
                    onUploadComplete={this.checkAllUploadsAreComplete(SELFIE)}
                    onEncodeComplete={this.setEncodedFiles(SELFIE)}
                    readyToUpload={this.state.readyToUpload}
                    encode
                    multiple={false}
                  >
                    {this.renderFileUpload(SELFIE)}
                  </ShiftFileDrop>
                </div>

                {ShiftApp.config.kycFields.proofOfAddress &&
                  !ShiftApp.config.kycFields.proofOfAddress.includes(
                    'hide'
                  ) && (
                    <div
                      className="form-field"
                      style={this.hideField(PROOF_OF_ADDRESS)}
                    >
                      <p>
                        {ShiftApp.translation('VERIFY.PROOF_OF_ADDRESS') ||
                          'Proof of Address:'}
                        {this.requiredElement(PROOF_OF_ADDRESS)}
                      </p>
                      <p>
                        {ShiftApp.translation(
                          'VERIFY.PROOF_OF_ADDRESS_DESCRIPTION'
                        ) ||
                          'The document must show your name and address such as a utility bill or bank statement.'}
                      </p>
                      <ShiftFileDrop
                        onDropAccepted={this.handleFileDrop(
                          PROOF_OF_ADDRESS,
                          'dropAccepted'
                        )}
                        onDropRejected={this.handleFileDrop(
                          PROOF_OF_ADDRESS,
                          'dropRejected'
                        )}
                        onUploadComplete={this.checkAllUploadsAreComplete(
                          PROOF_OF_ADDRESS
                        )}
                        onEncodeComplete={this.setEncodedFiles(
                          PROOF_OF_ADDRESS
                        )}
                        readyToUpload={this.state.readyToUpload}
                        encode
                        multiple={false}
                      >
                        {this.renderFileUpload(PROOF_OF_ADDRESS)}
                      </ShiftFileDrop>
                    </div>
                  )}
              </div>

              {ShiftApp.config.kycFields.videoAuthorization && (
                <div className="form-row video-authorization-row">
                  <div
                    className="form-field"
                    style={this.hideField('videoAuthorization')}
                  >
                    <p>
                      {ShiftApp.translation('VERIFY.VIDEO_AUTHORIZATION') ||
                        'Video Authorization:'}
                      {this.requiredElement('videoAuthorization')}
                    </p>
                    <p>
                      {ShiftApp.translation(
                        'VERIFY.VIDEO_AUTHORIZATION_DESCRIPTION1'
                      ) ||
                        'Please record a video of yourself reading the following statement:'}
                    </p>
                    <p>
                      {ShiftApp.translation(
                        'VERIFY.VIDEO_AUTHORIZATION_DESCRIPTION2'
                      ) ||
                        '“I authorize Zion to open a bank account for me to use on Zion’s trading platform.  I legally acquired all assets that I will use to buy/sell/trade on Zion’s trading platform.”'}
                    </p>
                    <ShiftFileDrop
                      onDropAccepted={this.handleFileDrop(
                        'videoAuthorization',
                        'dropAccepted'
                      )}
                      onDropRejected={this.handleFileDrop(
                        'videoAuthorization',
                        'dropRejected'
                      )}
                      onUploadComplete={this.checkAllUploadsAreComplete(
                        'videoAuthorization'
                      )}
                      onEncodeComplete={this.setEncodedFiles(
                        'videoAuthorization'
                      )}
                      readyToUpload={this.state.readyToUpload}
                      encode
                      multiple={false}
                    >
                      {this.renderFileUpload('videoAuthorization')}
                    </ShiftFileDrop>
                  </div>
                </div>
              )}

              {ShiftApp.config.kycFields.proofOfIncome && (
                <div className="form-row proof-of-income-row">
                  <div
                    className="form-field"
                    style={this.hideField('proofOfIncome')}
                  >
                    <p>
                      {ShiftApp.translation('VERIFY.PROOF_OF_INCOME') ||
                        'Proof of Income:'}
                      {this.requiredElement('videoAuthorization')}
                    </p>
                    <div className="proof-of-income-field-container">
                      <div className="proof-of-income-checkbox-container">
                        <input
                          type="checkbox"
                          name="proof-of-income"
                          onChange={this.handleProofOfIncomeChange}
                          checked={this.state.userConfig.proofOfIncome}
                        />
                      </div>
                      <div>
                        <p>
                          {ShiftApp.translation(
                            'VERIFY.PROOF_OF_INCOME_AGREEMENT'
                          ) ||
                            'I verify that I have sufficient assets to engage in trading of digital assets.  I acknowledge that trading digital assets carries risk including total loss.  I represent and warrant if I were to suffer a complete loss in the value of my digital assets, it would not ruin me financially and I could still maintain my current standard of living.'}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="submit-buttons">
                <ProcessingButton
                  type="submit"
                  onClick={this.uploadPhotosAndSubmit}
                  processing={this.state.processing}
                  disabled={!this.state.formValid}
                  className="btn btn-action input-verify"
                >
                  {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') ||
                    'Send Verification Form'}
                </ProcessingButton>

                {/* // Add this error message */}
                {this.state.displayErrorMessage && (
                  <span
                    // Move this to CSS file
                    style={{
                      color: 'lightcoral',
                      fontWeight: '600',
                      fontSize: '13px',
                      display: 'inline-block',
                      marginLeft: '12px'
                    }}
                  >
                    {ShiftApp.translation('VERIFY.FORM_INVALID_MESSAGE') ||
                      'Please check that each field is filled in correctly.'}
                  </span>
                )}
              </div>
            </form>
          )}
      </div>
    );
  }
}

ShiftManualKyc.defaultProps = {
  increaseLevel: () => {},
  setError: () => {},
  hideHeader: true
};

ShiftManualKyc.propTypes = {
  increaseLevel: React.PropTypes.func,
  setError: React.PropTypes.func
};
