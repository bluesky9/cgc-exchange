/* global ShiftApp */
import React from 'react';

import WidgetBase from '../base';
import { getTimeFormatEpoch } from '../../common';
import { formatNumberToLocale } from '../helper';

class ShiftTrades extends React.Component {
  constructor() {
    super();

    this.state = {
      page: 0,
      data: [],
      pairs: [],
      ticker: {},
      products: [],
      decimalPlaces: {},
      selectedAccount: null,
    };
  }

  componentDidMount() {
    this.accountChangedEvent = ShiftApp.selectedAccount
      .subscribe(selectedAccount => {
        this.setState({ selectedAccount });
      });

    this.tradeHistory = ShiftApp.accountTrades
      .filter(data => data)
      .subscribe(data => {
        this.setState({
          data: data.filter(trade => !trade.IsBlockTrade)
            .sort((a, b) => {
              if (a.TradeTime < b.TradeTime) return 1;
              if (a.TradeTime > b.TradeTime) return -1;
              return 0;
            }),
        });
      });

    this.pairs = ShiftApp.instruments.subscribe(pairs => this.setState({ pairs }));

    this.products = ShiftApp.products.subscribe(products => {
      const decimalPlaces = {};
      products.forEach(product => {
        decimalPlaces[product.Product] = product.DecimalPlaces;
      });

      this.setState({ products, decimalPlaces })

    });

    this.userAccounts = ShiftApp.userAccountsInfo.subscribe(accounts => this.setState({ accounts }));
  }

  componentWillUnmount() {
    this.accountChangedEvent.dispose();
    this.tradeHistory.dispose();
    this.pairs.dispose();
    this.products.dispose();
  }

  gotoPage = (page) => this.setState({ page });

  generateGuid = () => {
    function p8(s) {
      const numberStr = `${Math.random().toString(16)}000000000`;
      const p = numberStr.substr(2, 8);

      return s ? `-${p.substr(0, 4)}-${p.substr(4, 4)}` : p;
    }
    return p8() + p8(true) + p8(true) + p8();
  };

  render() {
    const siteName = ShiftApp.config.siteName;
    const pagination = ShiftApp.config.pagination;
    const paginationClass = ShiftApp.config.useBootstrapPagination ? 'pagination' : 'pagi';
    const maxLines = ShiftApp.config.maxLinesWidgets || 15;
    const totalPages = pagination ? Math.ceil(this.state.data.length / maxLines) : 0;
    const rowsSlice = pagination ?
      this.state.data.slice(maxLines * this.state.page, maxLines * (this.state.page + 1))
      :
      this.state.data;
    const rows = rowsSlice
      .map(row => {

        if(row.AccountId !== this.state.selectedAccount) return;

        const accountInfo = this.state.accounts.find(account => account.AccountId === row.AccountId);
        const pairName = this.state.pairs.find(pair => pair.InstrumentId === row.InstrumentId);
        const symbol = this.state.products.find(prod => prod.ProductId === row.FeeProductId);
        const time = <td className="borderless time">{getTimeFormatEpoch(row.TradeTimeMS)}</td>; // add .substring(11,19) to display time only
        const price = row.OrderType === 'TrailingStopMarket' || row.OrderType === 'TrailingStopLimit'
          ? 'TRL'
          : row.Price.toFixed(ShiftApp.config.decimalPlacesTraderUI || ShiftApp.config.decimalPlaces);

        return (
          <tr key={this.generateGuid()}>
            <td className="borderless account-id">{accountInfo.AccountName || row.AccountId || null}</td>
            {siteName !== 'huckleberry' && <td className="borderless trade-id">{row.TradeId}</td>}
            <td className="borderless symbol">{pairName.Symbol}</td>
            {row.Side === 'Buy' ?
              <td className="borderless side buyFont">{ShiftApp.translation('TRADES.BUY') || 'Buy'}</td>
              :
              <td className="borderless side sellFont"> {ShiftApp.translation('TRADES.SELL') || 'Sell'} </td>}
            <td className="borderless quantity" >≈{formatNumberToLocale(row.Quantity, this.state.decimalPlaces[pairName.Product1Symbol])}</td>
            <td className="borderless price" >{formatNumberToLocale(price, this.state.decimalPlaces[pairName.Product2Symbol])}</td>
            {siteName !== 'huckleberry' &&
              <td className="borderless total">
                {formatNumberToLocale(row.Value, (row.Side === 'Buy' ? this.state.decimalPlaces[pairName.Product1Symbol] : this.state.decimalPlaces[pairName.Product2Symbol]))}
              </td>}
            {siteName === 'aztec'
              ? <td className="borderless fee">{(row.Fee).toFixed(2)}</td>
              : <td className="borderless fee">
                {symbol !== undefined
                  ? `${formatNumberToLocale(row.Fee, this.state.decimalPlaces[symbol.Product])}
                    (${symbol.Product})`
                  : row.Fee}
              </td>}
            <td className="borderless execution-id">{row.ExecutionId}</td>
            {time}
            {siteName === 'huckleberry' && <td className="borderless">{ShiftApp.translation('TRADES.EXECUTED') || 'Fully Executed'}</td>}
          </tr>
        );
      });

    const emptyRows = [];
    for (let i = 0; i < maxLines - rows.length; i++) {
      emptyRows.push(<tr key={i}><td className="borderless" colSpan="11">&nbsp;</td></tr>);
    }

    const start = (this.state.page - 2) > 0 ? this.state.page - 2 : 0;
    const end = (this.state.page + 3) <= totalPages ? this.state.page + 3 : totalPages;
    const pages = [];
    
    if (pagination) {
      for (let x = start; x < end; x++) {
        const numButton = (
          <li key={x} className={this.state.page === x ? 'active' : null}>
            <a onClick={() => this.gotoPage(x)}>{x + 1}</a>
          </li>
        );
        pages.push(numButton);
      }
    }

    let displayPagination;

    displayPagination = (
      <ul className="pagi">
        <li><a onClick={() => this.gotoPage(0)}>&laquo;</a></li>
        {pages}
        <li onClick={() => this.gotoPage(totalPages - 1)} ><a>&raquo;</a></li>
      </ul>
    );
    

    const tableClassList = siteName === 'aztec' ?
      'table table--comfy table--hover table--striped table--light'
      :
      'table table-hover minFont';
    return (
      <WidgetBase login hideCloseLink {...this.props} headerTitle={ShiftApp.translation('TRADES.TITLE_TEXT') || 'Trades'}>
        <div className="table-responsive recent-transactions-table">
          <table className={tableClassList}>
            <thead>
              <tr>
                <th className="header nudge" style={{ width: '6rem' }}>{ShiftApp.translation('TRADES.ACCOUNT') || 'Account'}</th>
                {siteName !== 'huckleberry' && <th className="header">{ShiftApp.translation('TRADES.ID_TEXT') || '#'}</th>}
                <th className="header">{ShiftApp.translation('TRADES.INSTRUMENT_TEXT') || 'Instrument'}</th>
                <th className="header">{ShiftApp.translation('TRADES.SIDE_TEXT') || 'Side'}</th>
                <th className="header">{ShiftApp.translation('TRADES.QUANTITY_TEXT') || 'Quantity'}</th>
                <th className="header">{ShiftApp.translation('TRADES.PRICE_TEXT') || 'Price'}</th>
                {siteName !== 'huckleberry' && <th className="header">{ShiftApp.translation('TRADES.TOTAL_TEXT') || 'Total'}</th>}
                <th className="header">{ShiftApp.translation('TRADES.FEE_TEXT') || 'Fee'}</th>
                <th className="header">{ShiftApp.translation('TRADES.EXECUTION_ID_TEXT') || 'Execution ID'}</th>
                <th className="header">{ShiftApp.translation('TRADES.TIME_TEXT') || 'Time'}</th>
                {siteName === 'huckleberry' && <th className="header">{ShiftApp.translation('TRADES.STATUS') || 'Status'}</th>}
              </tr>
            </thead>
            <tbody>
              {rows}
              {emptyRows}
            </tbody>
          </table>
        </div>

        {pagination && pages.length > 1 &&
          <div className="pad">
            <div className="pull-right">
              {displayPagination}
            </div>
          </div>}

      </WidgetBase>
    );
  }
}

export default ShiftTrades;
