/* global FileReader, ShiftApp, location, localStorage, window, AWS */
import React from 'react';
import uuidV4 from 'uuid/v4';

import WidgetBase from '../base';
import InputLabeled from '../../misc/inputLabeled';
import SelectLabeled from '../../misc/selectLabeled';
import TextareaLabeled from '../../misc/textareaLabeled';
import ProcessingButton from '../../misc/processingButton';
import {
  formatNumberToLocale,
  parseNumberToLocale,
  getDecimalPrecision
} from '../helper';
import Trustpay from '../paymentProcessors/Trustpay';
import RazorpayDeposit from '../paymentProcessors/Razorpay';
import Psigate from '../paymentProcessors/Psigate';
import Interswitch from '../paymentProcessors/Interswitch';
import Fennas from '../paymentProcessors/Fennas';
import Validate from '../../misc/form/validators';

// Components
import ShiftSelectLabeled from '../../misc/shift-fields/shift-select-labeled';
import ShiftInputLabeled from '../../misc/shift-fields/shift-input-labeled';
import ShiftFileDrop from '../../misc/shift-fields/shift-file-drop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/free-solid-svg-icons';

const REQUIRED = 'required',
  HIDE = 'hide',
  OPTIONAL = 'optional';

const AMOUNT = 'amount',
  BANK = 'bank',
  AGENCY = 'agency',
  AGENCY_NUMBER = 'agencyNumber',
  ACCOUNT = 'account',
  ACCOUNT_NUMBER = 'accountNumber',
  ROUTING_NUMBER = 'routingNumber',
  BANK_SELECT = 'bankSelect',
  DEPOSIT_TERMS = 'depositTerms';

class ShiftDepositFiat extends React.Component {
  state = {
    data: {},
    processing: false,
    data_uri: null,
    checkoutId: '',
    address: '',
    addressList: [],
    selected: '',
    ProductId: null,
    session: {},
    accountProviders: [],
    decimalPlaces: {},
    bankSelect: { name: '' },
    bank: '',
    agency: '',
    agencyNumber: '',
    account: '',
    accountNumber: '',
    error: '',
    amountString: '',
    amount: '',
    routingNumber: '',
    fullName: '',
    phoneNumber: '',
    AccountId: null,
    depositWorkflowStep: 'formEntry', // 'formEntry' or 'documentUpload'
    acceptedFiles: [],
    rejectedFiles: [],
    dropAccepted: false,
    dropErrors: [],
    maxFileSize: 100,
    uploadedFileKeys: [],
    readyToUpload: false,
    accountNumberSynapse: null,
    routingNumberSynapse: null,
    depositTerms: false
  };

  // Lifecycle methods
  componentWillMount() {
    if (typeof location.origin === 'undefined') {
      location.origin = `${location.protocol}//${location.host}`;
    }
  }

  componentDidMount() {
    this.session = ShiftApp.accountInfo.subscribe(session =>
      this.setState({ session })
    );
    

    let ProductId;
    if (this.props.ProductId) {
      ProductId = this.props.ProductId;
      this.setState({ ProductId: this.props.ProductId });
    } else if (!this.props.ProductId && this.props.Product) {
      ProductId = ShiftApp.products.value.find(
        product => product.Product === this.props.Product
      ).ProductId;
      this.setState({ ProductId });
    }

    this.products = ShiftApp.products
      .filter(data => data.length)
      .subscribe(products => {
        const decimalPlaces = {};
        products.forEach(product => {
          decimalPlaces[product.Product] = product.DecimalPlaces;
        });
        this.setState({ decimalPlaces });
      });

    const accountId =
      ShiftApp.selectedAccount.value || ShiftApp.userAccounts.value[0];

    const payload = {
      OMSId: ShiftApp.oms.value,
      ProductId,
      AccountId: accountId
    };

    this.createDeposit = ShiftApp.createDeposit.subscribe(res => {
      if (res && !res.success) {
        if (res.detail && res.detail.indexOf('invalid amount') > -1) {
          res.errormsg =
            ShiftApp.translation('ERRORS.DEPOSIT.FIAT.GENERAL_ERROR') ||
            `There was an error: ${res.errormsg}, ${res.detail}`;
          return this.setState({
            data: res,
            error: res.errormsg,
            processing: false
          });
        }
        if (res.result === false) {
          this.setState({ processing: false });
          if (res.detail === 'Exceeds_Daily_Deposit_Limit') {
            this.setState({
              data: res,
              error:
                ShiftApp.translation(
                  'ERRORS.DEPOSIT.FIAT.EXCEEDS_DAILY_LIMIT'
                ) ||
                `Deposit Failed. ${formatNumberToLocale(
                  this.state.amount,
                  this.state.decimalPlaces[this.props.Product]
                )} ${this.props.Product} exceeds your daily deposit limit.`
            });
          }
          if (res.detail === 'Exceeds_Monthly_Deposit_Limit') {
            this.setState({
              data: res,
              error:
                ShiftApp.translation(
                  'ERRORS.DEPOSIT.FIAT.EXCEEDS_MONTHLY_LIMIT'
                ) ||
                `Deposit Failed. ${formatNumberToLocale(
                  this.state.amount,
                  this.state.decimalPlaces[this.props.Product]
                )} ${this.props.Product} exceeds your monthly deposit limit.`
            });
          }
          if (res.detail === 'Exceeds_Yearly_Deposit_Limit') {
            this.setState({
              data: res,
              error:
                ShiftApp.translation(
                  'ERRORS.DEPOSIT.FIAT.EXCEEDS_YEARLY_LIMIT'
                ) ||
                `Deposit Failed. ${formatNumberToLocale(
                  this.state.amount,
                  this.state.decimalPlaces[this.props.Product]
                )} ${this.props.Product} exceeds your yearly deposit limit.`
            });
          }
          if (
            res.detail.indexOf('must be greater than zero and less than') > -1
          )
            this.setState({
              data: res,
              error:
                ShiftApp.translation('ERRORS.DEPOSIT.FIAT.AMOUNT_RANGE') ||
                res.detail
            });
        }
      } else if (res && res.success) {
        this.setState({ data: res, processing: false });
      } else if (res.length > 0) {
        this.setState({ data: res, processing: false });
      }
      return false;
    });

    this.deposits = ShiftApp.deposits.subscribe(res => {
      const keys = res.DepositInfo ? JSON.parse(res.DepositInfo) : '';
      const depositKey = keys !== '' ? keys.reverse() : '';

      // need to add something to pick the newest address, not sure if it's max or min of array
      this.setState({
        address: depositKey[0],
        addressList: depositKey,
        selected: depositKey[0]
      });
    });

    this.setState({
      depositWorkflow: ShiftApp.config.depositFiat.workflow
    });
  }

  componentWillUnmount() {
    this.createDeposit.dispose();
    this.deposits.dispose();
    ShiftApp.createDeposit.onNext({});
    ShiftApp.deposits.onNext({});
    this.products.dispose();
  }

  // Event handlers

  deposit = uploadedFileKeys => {
    const data = {};

    this.setState({
      processing: true,
      data: {},
      AccountProviderId: null,
      readyToUpload: false
    });

    // removing non alphanumeric characters in amount field
    const amount = +this.state.amount;

    data.productId = this.state.ProductId;
    data.accountId = ShiftApp.selectedAccount.value;
    data.status = 'New';
    data.currencyCode = this.props.Product;
    data.amount = amount;
    data.comment = this.refs.comment
      ? `Comments: ${this.refs.comment.value()}`
      : null;
    data.imageB64 = '';
    data.phoneNumber = this.state.phoneNumber;
    data.bank = this.state.bankSelect.name;

    if (this.state.data_uri) data.imageB64 = this.state.data_uri;

    // setup the depositInfo
    data.language =
      localStorage && localStorage.lang
        ? localStorage.lang
        : ShiftApp.config.defaultLanguage;

    // data.fullName = this.refs.fullName.value();
    data.fullName = this.state.fullName;

    let fileKeys = [];
    if (uploadedFileKeys && Array.isArray(uploadedFileKeys)) {
      fileKeys = uploadedFileKeys.map(
        key =>
          `${ShiftApp.config.aws.urlPrefix}${
            ShiftApp.config.aws.bucketName
          }/${key}`
      );
    }

    data.depositInfo = {
      'Full Name': data.fullName,
      language: data.language,
      Comments: data.comment,
      phoneNumber: data.phoneNumber,
      bankSelect: data.bank,
      fileKeys: fileKeys,
      bank: this.state.bank,
      agency: this.state.agency,
      agencyNumber: this.state.agencyNumber,
      account: this.state.account,
      accountNumber: this.state.accountNumber,
      amount: this.state.amountString,
      depositTerms: this.state.depositTerms
    };

    console.log(data.depositInfo);

    const newUserRequestBody = {
      logins: [{ email: ShiftApp.getUser.value.Email }],
      extra: {
        supp_id: ShiftApp.userData.value.UserId,
        cip_tag: 1,
        is_business: false
      }
    };

    if (ShiftApp.config.depositFiat.synapseDeposit) {
      fetch('https://synapse.shiftcrypto.com/api/users/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(newUserRequestBody)
      })
        .then(response => response.json())
        .then(user => {
          fetch(
            `https://synapse.shiftcrypto.com/api/transactions/deposit?user_id=${
              ShiftApp.userData.value.UserId
            }`,
            {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              }
            }
          )
            .then(response => response.json())
            .then(deposit => {
              if (!deposit.error_message) {
                data.depositInfo = JSON.stringify(
                  Object.assign(data.depositInfo, {
                    accountNumberSynapse: deposit.account_num,
                    rounting_num: deposit.routing_num.wire
                  })
                );
                this.setState(
                  {
                    accountNumberSynapse: deposit.account_num,
                    routingNumberSynapse: deposit.routing_num.wire
                  },
                  () => ShiftApp.createDepositTicket(data)
                );
              } else {
                console.error(deposit.error_message);
              }
            })
            .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
    } else {
      data.depositInfo = JSON.stringify(data.depositInfo);
      ShiftApp.createDepositTicket(data);
    }
  };

  addressChanged = e => this.setState({ selected: e.target.value });

  handleChangeAmount = e => {
    this.setState({ error: '', amountString: e.target.value });

    const amount = parseNumberToLocale(e.target.value);
    const decimals = getDecimalPrecision(amount);
    const decimalsAllowed = this.state.decimalPlaces[this.props.Product];
    const msgDecimals = `Only ${decimalsAllowed} decimal places are allowed for ${
      this.props.Product
    }.`;

    if (decimals > decimalsAllowed) {
      this.setState({
        amount,
        error: msgDecimals
      });
    } else if (!isNaN(amount)) {
      this.setState({
        amount
      });
    } else {
      this.setState({
        error:
          ShiftApp.translation('DEPOSIT.ERRORS.DEPOSIT_AMOUNT') ||
          'Deposit amount must only include numbers.'
      });
    }
  };

  handleBankSelectChange = e => {
    this.setState({
      bankSelect: ShiftApp.config.depositFiat.bankList.find(
        bank => bank.name === e.target.value
      )
    });
  };

  handleNameChange = e => this.setState({ fullName: e.target.value });

  handlePhoneNumberChange = e => {
    this.setState({ phoneNumber: e.target.value });
    if (!/^\d+$/.test(e.target.value)) {
      this.setState({
        error:
          ShiftApp.translation('DEPOSIT.ERRORS.PHONE_NUMBER') ||
          'Phone number must only contain numbers.'
      });
    }
  };

  handleBankInputChange = e => this.setState({ bank: e.target.value });

  handleAgencyChange = e => this.setState({ agency: e.target.value });

  handleAgencyNumberChange = e =>
    this.setState({ agencyNumber: e.target.value });

  handleAccountChange = e => this.setState({ account: e.target.value });

  handleAccountNumberChange = e =>
    this.setState({ accountNumber: e.target.value });

  handleRoutingNumberChange = e =>
    this.setState({ routingNumber: e.target.value });

  handleDepositTermsChange = () =>
    this.setState(prevState => ({ depositTerms: !prevState.depositTerms }));

  selectAccountProvider = AccountProviderId => {
    const payload = {
      OMSId: ShiftApp.oms.value,
      ProductId: this.state.ProductId,
      AccountId: ShiftApp.selectedAccount.value,
      AccountProviderId
    };
    this.setState({ AccountProviderId });

    return ShiftApp.getDepositRequestInfoTemplate(payload); // If data is literally nothing, which may happen, we have to continue workflow
  };

  checkAllRequiredFields = () => {
    const { fields } = ShiftApp.config.depositFiat;

    if (this.state.depositWorkflow === 'bankSelect') {
      if (this.state.depositWorkflowStep === 'formEntry') {
        if (fields) {
          const checkedFields = [];
          for (const field in fields) {
            if (fields[field].includes(REQUIRED)) {
              checkedFields.push(this.state[field]);
            }
          }
          return (
            !this.state.error &&
            checkedFields.length &&
            checkedFields.every(field => field)
          );
        } else {
          return (
            this.state.amountString &&
            this.state.bankSelect.name &&
            this.state.bank &&
            this.state.agency &&
            this.state.account &&
            !this.state.error
          );
        }
      } else if (this.state.depositWorkflowStep === 'documentUpload') {
        return this.state.acceptedFiles.length;
      }
    } else {
      return this.state.amountString && !this.state.error;
    }
  };

  checkValidDepositAmount = () => {
    if (
      ShiftApp.config.depositFiat.minimumDepositAmount &&
      parseInt(this.state.amountString) <
        ShiftApp.config.depositFiat.minimumDepositAmount
    ) {
      this.setState({
        error:
          ShiftApp.translation('DEPOSIT.ERRORS.MINIMUM_DEPOSIT_AMOUNT') ||
          'Deposit amount is too small, please make a deposit greater than $100.'
      });
    }
  };

  checkValidPhoneNumber = () => {};

  gotoDocumentUpload = () =>
    this.setState({ depositWorkflowStep: 'documentUpload' });

  onDrop = (acceptedFiles, rejectedFiles) => {
    this.setState({ acceptedFiles, rejectedFiles });
  };

  onDropAccepted = () => {
    this.setState({ dropAccepted: true });
  };

  onDropRejected = rejectedFiles => {
    const acceptedFileTypes = [
      'image/jpeg',
      'image/png',
      'image/gif',
      'application/pdf'
    ];

    const largeFileSizeError =
      ShiftApp.translation('DEPOSIT.ERRORS.FILE_SIZE') ||
      'File size is too large, images cannot exceed 3MB.';
    const rejectedFileTypeError =
      ShiftApp.translation('DEPOSIT.ERRORS.FILE_TYPE') ||
      'File was of the incorrect type.';
    const generalErrorMessage =
      ShiftApp.translation('DEPOSIT.ERRORS.FILE_GENERAL') ||
      'Some file(s) could not be uploaded.';

    const dropErrors = [];

    rejectedFiles.map(rejectedFile => {
      if (parseInt(rejectedFile.size) > this.state.maxFileSize) {
        if (!dropErrors.includes(largeFileSizeError)) {
          dropErrors.push(largeFileSizeError);
        }
      } else if (acceptedFileTypes.includes(rejectedFile.type)) {
        if (!dropErrors.includes(rejectedFileTypeError)) {
          dropErrors.push(rejectedFileTypeError);
        }
      }
    });

    this.setState({ error: generalErrorMessage, dropErrors });
  };

  addPhotosAndSubmitDeposit = () => {
    this.setState({ readyToUpload: true });
  };

  // Render Methods

  renderBankInfo = () => {
    return (
      this.state.bankSelect.bankInfo &&
      this.state.bankSelect.bankInfo.map((line, index) => (
        <p key={index}>
          <span>{`${line.key}: `}</span>
          {line.value}
        </p>
      ))
    );
  };

  renderBankSelectWorkflow = () => {
    const banks =
      ShiftApp.config.depositFiat &&
      ShiftApp.config.depositFiat.bankList &&
      ShiftApp.config.depositFiat.bankList.map((bank, index) => (
        <option value={bank.name} key={index}>
          {bank.name}
        </option>
      ));

    if (this.state.depositWorkflowStep === 'formEntry') {
      let fields = {};
      if (ShiftApp.config.depositFiat.fields) {
        fields = ShiftApp.config.depositFiat.fields;
      }

      return (
        <div className="deposit-modal pad">
          <div style={{ marginTop: '1.5rem', marginBottom: '1.5rem' }}>
            {fields[AMOUNT] &&
              !fields[AMOUNT].includes(HIDE) && (
                <div>
                  <p>{ShiftApp.translation('DEPOSIT.AMOUNT') || 'Amount'}</p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.AMOUNT') || 'Amount'
                    }
                    ref="amount"
                    className="form-control"
                    type="text"
                    onChange={this.handleChangeAmount}
                    onBlur={this.checkValidDepositAmount}
                    value={this.state.amountString}
                  />
                </div>
              )}
            {fields[BANK] &&
              !fields[BANK].includes(HIDE) && (
                <div>
                  <p>
                    {ShiftApp.translation('DEPOSIT.BANK_INPUT') || 'Bank'}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.BANK_INPUT') || 'Bank'
                    }
                    ref="bank"
                    className="form-control"
                    type="text"
                    onChange={this.handleBankInputChange}
                    value={this.state.bank}
                  />
                </div>
              )}

            <div className="form-pair">
              {(!fields ||
                fields[AGENCY].includes(REQUIRED) ||
                fields[AGENCY].includes(OPTIONAL)) && (
                <div className="form-field-label">
                  <p>{ShiftApp.translation('DEPOSIT.AGENCY') || 'Agency'}</p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.AGENCY') || 'Agency'
                    }
                    ref="agency"
                    className="form-control"
                    type="text"
                    onChange={this.handleAgencyChange}
                    value={this.state.agency}
                  />
                </div>
              )}
              {(!fields ||
                fields[AGENCY_NUMBER].includes(REQUIRED) ||
                fields[AGENCY_NUMBER].includes(OPTIONAL)) && (
                <div className="form-field-label">
                  <p>
                    {ShiftApp.translation('DEPOSIT.AGENCY_NUMBER') ||
                      'Number'}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.AGENCY_NUMBER') ||
                      'Number'
                    }
                    ref="agencyNumber"
                    className="form-control"
                    type="text"
                    onChange={this.handleAgencyNumberChange}
                    value={this.state.agencyNumber}
                  />
                </div>
              )}
            </div>

            <div className="form-pair">
              {(!fields ||
                fields[ACCOUNT].includes(REQUIRED) ||
                fields[ACCOUNT].includes(OPTIONAL)) && (
                <div className="form-field-label">
                  <p>
                    {ShiftApp.translation('DEPOSIT.ACCOUNT') || 'Account'}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.ACCOUNT') || 'Account'
                    }
                    ref="account"
                    className="form-control"
                    type="text"
                    onChange={this.handleAccountChange}
                    value={this.state.account}
                  />
                </div>
              )}
              {(!fields ||
                fields[ACCOUNT_NUMBER].includes(REQUIRED) ||
                fields[ACCOUNT_NUMBER].includes(OPTIONAL)) && (
                <div className="form-field-label">
                  <p>
                    {ShiftApp.translation('DEPOSIT.ACCOUNT_NUMBER') ||
                      'Number'}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.ACCOUNT_NUMBER') ||
                      'Number'
                    }
                    ref="accountNumber"
                    className="form-control"
                    type="text"
                    onChange={this.handleAccountNumberChange}
                    value={this.state.accountNumber}
                  />
                </div>
              )}

              {(fields[ROUTING_NUMBER].includes(REQUIRED) ||
                fields[ROUTING_NUMBER].includes(OPTIONAL)) && (
                <div className="form-field-label">
                  <p>
                    {ShiftApp.translation('DEPOSIT.ROUTING_NUMBER') ||
                      'Routing Number'}
                  </p>
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.ROUTING_NUMBER') ||
                      'Routing Number'
                    }
                    ref="routingNumber"
                    className="form-control"
                    type="text"
                    onChange={this.handleRoutingNumberChange}
                    value={this.state.routingNumber}
                  />
                </div>
              )}
            </div>
            {fields[BANK_SELECT] &&
              !fields[BANK_SELECT].includes(HIDE) && (
                <div>
                  <p>
                    {ShiftApp.translation('DEPOSIT.BANK_SELECT') ||
                      'Select a bank'}
                  </p>
                  <ShiftSelectLabeled
                    ref="bankSelect"
                    placeholder={
                      ShiftApp.translation('DEPOSIT.BANK_SELECT') ||
                      'Select a bank'
                    }
                    className="form-control"
                    onChange={this.handleBankSelectChange}
                    value={this.state.bankSelect.name}
                  >
                    {banks}
                  </ShiftSelectLabeled>
                </div>
              )}
            <div className="bank-info">
              {this.state.bankSelect ? this.renderBankInfo() : null}
            </div>

            {fields[DEPOSIT_TERMS] &&
              !fields[DEPOSIT_TERMS].includes(HIDE) && (
                <div className="deposit-terms-field">
                  <div className="deposit-terms-checkbox-container">
                    <input
                      name="depositTerms"
                      type="checkbox"
                      checked={this.state.depositTerms}
                      onChange={this.handleDepositTermsChange}
                    />
                  </div>
                  <div>
                    <p
                      dangerouslySetInnerHTML={{
                        __html:
                          ShiftApp.translation('DEPOSIT.DEPOSIT_TERMS') ||
                          'I have read and agree to the terms in the Deposit Agreement'
                      }}
                    />
                  </div>
                </div>
              )}

            <div className="clearfix">
              <div className="deposit-buttons pull-right">
                {this.props.close && (
                  <button
                    className="btn btn-action btn-inverse"
                    onClick={this.props.close}
                  >
                    {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                  </button>
                )}{' '}
                <ProcessingButton
                  className="btn btn-action"
                  processing={this.state.processing}
                  onClick={
                    ShiftApp.config.depositFiat.fileUpload
                      ? this.gotoDocumentUpload
                      : this.deposit
                  }
                  disabled={
                    !this.checkAllRequiredFields() && !this.state.errors
                  }
                >
                  {ShiftApp.config.depositFiat.fileUpload
                    ? ShiftApp.translation('BUTTONS.TEXT_NEXT') || 'Next'
                    : ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Submit'}
                </ProcessingButton>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (this.state.depositWorkflowStep === 'documentUpload') {
      return (
        <div className="deposit-modal clearfix">
          <ShiftFileDrop
            onDrop={this.onDrop}
            onDropAccepted={this.onDropAccepted}
            onDropRejected={this.onDropRejected}
            onUploadComplete={this.deposit}
            dropAccepted={this.state.dropAccepted}
            acceptedFiles={this.state.acceptedFiles}
            rejectedFiles={this.state.rejectedFiles}
            dropErrors={this.state.dropErrors}
            readyToUpload={this.state.readyToUpload}
            multiple={true}
          >
            <div className="upload-icon">
              {/* <i className="fa fa-cloud-upload" aria-hidden="true" /> */}
              <FontAwesomeIcon icon={faUpload} size="lg" />
            </div>
            <div className="upload-text">
              <h2>
                {ShiftApp.translation('FILE_DROP.UPLOAD_TEXT_HEADING') ||
                  'Drag & Drop'}
              </h2>
              <h3
                dangerouslySetInnerHTML={{
                  __html:
                    ShiftApp.translation(
                      'FILE_DROP.UPLOAD_TEXT_SUBHEADING'
                    ) || 'your files here, or <span>browse</span>.'
                }}
              />
              <p>
                {ShiftApp.translation('FILE_DROP.UPLOAD_TEXT_SMALL') ||
                  'image and pdf files accepted'}
              </p>
            </div>
          </ShiftFileDrop>
          <div className="deposit-buttons pull-right">
            {this.props.close && (
              <button
                className="btn btn-action btn-inverse"
                onClick={this.props.close}
              >
                {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
              </button>
            )}{' '}
            <ProcessingButton
              className="btn btn-action"
              processing={this.state.processing}
              onClick={this.addPhotosAndSubmitDeposit}
              disabled={!this.checkAllRequiredFields() || this.state.processing}
            >
              {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Submit'}
            </ProcessingButton>
          </div>
        </div>
      );
    }
  };

  renderDepositSuccess = () => {
    if (ShiftApp.config.depositFiat.synapseDeposit) {
      return (
        <div className="deposit-modal pad">
          <h3 className="text-center">
            {ShiftApp.translation('DEPOSIT.CONFIRM') ||
              'Deposit request successfully sent.'}
          </h3>
          <p
            dangerouslySetInnerHTML={{
              __html:
                ShiftApp.translation('DEPOSIT.WIRE_INSTRUCTIONS') ||
                'Wire the deposit amount to:'
            }}
          />
          <p>
            {(ShiftApp.translation('DEPOSIT.AMOUNT') || 'Amount') +
              ': ' +
              this.state.amountString}
          </p>
          <p>
            {(ShiftApp.translation('DEPOSIT.ACCOUNT_NUMBER') ||
              'Account Number') +
              ': ' +
              this.state.accountNumberSynapse}
          </p>
          <p>
            {(ShiftApp.translation('DEPOSIT.ROUTING_NUMBER') ||
              'Routing Number') +
              ': ' +
              this.state.routingNumberSynapse}
          </p>
          <div className="clearfix">
            <div className="pull-right">
              {this.props.close && (
                <button className="btn btn-action" onClick={this.props.close}>
                  Close
                </button>
              )}
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="deposit-modal pad">
          <h3 className="text-center">
            {ShiftApp.translation('DEPOSIT.CONFIRM') ||
              'Deposit request successfully sent.'}
          </h3>
          <div className="clearfix">
            <div className="pull-right">
              {this.props.close && (
                <button className="btn btn-action" onClick={this.props.close}>
                  Close
                </button>
              )}
            </div>
          </div>
        </div>
      );
    }
  };

  render() {
    const headerTitle = `${ShiftApp.translation('DEPOSIT.DEPOSIT') ||
      'Deposit'} ${this.props.Product ? `(${this.props.Product})` : ''}`;

    if (this.state.addressList.length) {
      const addresses = this.state.addressList.map(address => (
        <option value={address} key={address}>
          {address}
        </option>
      ));
      const depositAddress = this.state.addressList.find(
        address => address === this.state.selected
      );
      const depositType = this.props.Product;

      return (
        <WidgetBase {...this.props} login headerTitle={headerTitle}>
          <div className="deposit-modal pad">
            <div style={{ marginBottom: '1.5rem' }}>
              <h3>
                {ShiftApp.translation('DEPOSIT.NB') ||
                  'Please read the instructions below'}
              </h3>
              <p>
                {ShiftApp.translation('DEPOSIT.FIAT_INSTRUCTION1') ||
                  'Depositing fiat into the exchange is safe and easy.'}
              </p>
              <p>
                {ShiftApp.translation('DEPOSIT.FIAT_INSTRUCTION2') ||
                  'The address below can always be used to deposit fiat into your account.'}
              </p>
              <p>
                {ShiftApp.translation('DEPOSIT.FIAT_INSTRUCTION3') ||
                  'Use your fiat client or wallet service to send the fiat to the address below.'}
              </p>
            </div>

            <SelectLabeled
              placeholder={
                ShiftApp.translation('DEPOSIT.ADDRESS_LIST') || 'Address List'
              }
              onChange={this.addressChanged}
              readOnly
            >
              {addresses}
            </SelectLabeled>
            <p>
              {ShiftApp.translation(
                'DEPOSIT.FIAT_QUICKTELLER_INSTRUCTIONS'
              ) || ''}
            </p>
            <span>
              <InputLabeled
                value={depositAddress}
                label={ShiftApp.translation('DEPOSIT.ADDRESS')}
              />
            </span>

            <div
              className="clearfix"
              style={{ display: 'flex', marginBottom: '1rem' }}
            >
              {this.props.Product !== 'NGN' && (
                <div>
                  <img
                    alt="QR Code"
                    width="200"
                    height="200"
                    style={{ margin: 10, float: 'left' }}
                    src={
                      depositAddress
                        ? `https://api.qrserver.com/v1/create-qr-code/?data='${depositType}:${depositAddress}&size=128x128`
                        : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
                    }
                  />
                </div>
              )}
              <div>
                <p
                  dangerouslySetInnerHTML={{
                    __html:
                      ShiftApp.translation(
                        'DEPOSIT.FIAT_WITH_ADDRESS_INSTRUCTION4'
                      ) ||
                      'Your account will automatically update after the fiat network confirms your transaction. The confirmation may take up to 1 hour.'
                  }}
                />
                <p>
                  {ShiftApp.translation('DEPOSIT.FIAT_WITH_ADDRESS_STEP1') ||
                    '1) Send fiat to this address.'}
                </p>
                <p>
                  {ShiftApp.translation('DEPOSIT.FIAT_WITH_ADDRESS_STEP2') ||
                    '2) Your deposit will automatically be processed.'}
                </p>
              </div>
            </div>

            {this.props.close && (
              <div className="clearfix">
                <div className="pull-right">
                  <button className="btn btn-action" onClick={this.props.close}>
                    {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                  </button>
                </div>
              </div>
            )}
          </div>
        </WidgetBase>
      );
    }

    if (this.state.depositWorkflow === 'bankSelect') {
      return (
        // wrap all content in widget base
        <WidgetBase
          {...this.props}
          login
          headerTitle={headerTitle}
          error={this.state.error}
        >
          {!this.state.data.success
            ? this.renderBankSelectWorkflow()
            : this.renderDepositSuccess()}
        </WidgetBase>
      );
    } else {
      return (
        // wrap all content in widget base
        <WidgetBase
          {...this.props}
          login
          headerTitle={headerTitle}
          error={this.state.error}
        >
          {!this.state.data.success ? (
            <div className="deposit-modal pad">
              <p>
                {ShiftApp.translation('DEPOSIT.TITLE_TEXT') || 'Deposit Form'}
              </p>
              {!ShiftApp.config.showDepositBankDetails ? ( // eslint-disable-line no-nested-ternary
                this.props.Product === 'NZD' ? (
                  <p>
                    {ShiftApp.translation('DEPOSIT.FIAT_STEP1') ||
                      'Step 1: Create a new deposit ticket for each deposit. One ticket per deposit.'}
                    <br />
                    {ShiftApp.translation('DEPOSIT.FIAT_STEP2') ||
                      'Step 2: Check your email for the deposit instructions.'}
                    <br />
                    {ShiftApp.translation('DEPOSIT.FIAT_STEP3') ||
                      'Step 3: Send funds from your New Zealand bank account.'}
                  </p>
                ) : (
                  <p>
                    {ShiftApp.translation('DEPOSIT.FIAT_STEP1') ||
                      'Step 1: Create the deposit ticket.'}
                    <br />
                    {ShiftApp.translation('DEPOSIT.FIAT_STEP2') ||
                      'Step 2: Process deposit instructions on the deposit receipt.'}
                  </p>
                )
              ) : (
                <div className="deposit-modal">
                  {this.props.Product === 'USD' && (
                    <p>
                      <strong>
                        {ShiftApp.translation('DEPOSIT.SEND_FUNDS') ||
                          'Send funds to:'}
                      </strong>
                      <br />

                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation('DEPOSIT.BANK_USD', 
                            { accoutnId: this.state.session.AccountId }) ||
                            'Bank: '
                        }}
                      />

                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation('DEPOSIT.ACCOUNT_NAME', 
                            { accoutnId: this.state.session.AccountId }) ||
                            'Beneficiary: '
                        }}
                      />

                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation(
                              'DEPOSIT.ACCOUNT_#', 
                              { accoutnId: this.state.session.AccountId }
                            ) ||
                            'Account Number: '
                        }}
                      />
                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation('DEPOSIT.SWIFT', 
                            { accoutnId: this.state.session.AccountId }) ||
                            'Swift Code: '
                        }}
                      />

                      <br />
                    </p>
                  )}

                  {this.props.Product !== 'USD' && (
                    <p>
                      <strong>
                        {ShiftApp.translation('DEPOSIT.SEND_FUNDS') ||
                          'Send funds to:'}
                      </strong>
                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation('DEPOSIT.BANK_CNY') ||
                            'Bank: '
                        }}
                      />
                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation('DEPOSIT.ACCOUNT_CNY') ||
                            'Account Holder:  '
                        }}
                      />

                      <br />
                      <span
                        dangerouslySetInnerHTML={{
                          __html:
                            ShiftApp.translation(
                              'DEPOSIT.ACCOUNT_NUMBER_CNY'
                            ) || 'Account Number: '
                        }}
                      />
                      <br />
                    </p>
                  )}
                  <p>
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          ShiftApp.translation('DEPOSIT.FIAT_STEP3') ||
                          'Please fill out the form below and report your remittance to us.'
                      }}
                    />
                  </p>
                </div>
              )}
              {
                <div style={{ marginTop: '1.5rem', marginBottom: '1.5rem' }}>
                  <InputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.FULLNAME') || 'Full Name'
                    }
                    ref="fullName"
                    className="form-control"
                  />
                  <ShiftInputLabeled
                    placeholder={
                      ShiftApp.translation('DEPOSIT.AMOUNT') || 'Amount'
                    }
                    ref="amount"
                    className="form-control"
                    type="text"
                    onChange={this.handleChangeAmount}
                    onBlur={this.checkValidDepositAmount}
                    value={this.state.amountString}
                  />
                  <p>
                    {ShiftApp.translation('DEPOSIT.COMMENT') ||
                      'The comment field is optional. Please use it for special instructions.'}
                  </p>
                  <TextareaLabeled
                    rows="6"
                    placeholder={
                      ShiftApp.translation('DEPOSIT.COMMENT_LABEL') ||
                      'Comment'
                    }
                    ref="comment"
                    className="form-control"
                  />
                </div>
              }

              <div className="clearfix">
                <div className="pull-right">
                  {this.props.close && (
                    <button
                      className="btn btn-action btn-inverse"
                      onClick={this.props.close}
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                    </button>
                  )}{' '}
                  <ProcessingButton
                    className="btn btn-action"
                    processing={this.state.processing}
                    onClick={this.deposit}
                    disabled={
                      !this.checkAllRequiredFields() || this.state.processing
                    }
                  >
                    {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Submit'}
                  </ProcessingButton>
                </div>
              </div>
            </div>
          ) : (
            this.renderDepositSuccess()
          )}
        </WidgetBase>
      );
    }
  }
}

ShiftDepositFiat.defaultProps = {
  close: () => {},
  Product: '',
  ProductId: null
};

ShiftDepositFiat.propTypes = {
  close: React.PropTypes.func,
  Product: React.PropTypes.string,
  ProductId: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ])
};

export default ShiftDepositFiat;
