import Deposit from '../deposit';

class ShiftDeposit extends Deposit {

  prepareProducts(product) {
    const currencies = ShiftApp.config.balances.currenciesWithActionBtns;
    return currencies.indexOf(product.Product) !== -1;
  }

  componentDidMount() {
    super.componentDidMount();
    this.products = ShiftApp.products
      .filter(prods => prods.length)
      .subscribe((products) => {
        const preparedProducts = products.filter(this.prepareProducts);
        super.setState({ products: preparedProducts });
      });
  }
}

export default ShiftDeposit;
