/**
 * Config variables
 * 
 * @param blackList           {Object}  - Object for widget
 * @param blackList.enable    {Boolean} - Enable or disable this modal
 * @param blackList.token     {Boolean} - Should modal appear each time
 * @param blackList.text      {String}  - Text or translation path
 * @param blackList.countries {Array}   - List of countries (country code)
 * 
 */

import React from 'react';
import Modal from '../modal';
import WidgetBase from '../base';
import GeoLookup from '../../misc/utilities/geo-lookup';

const BODY_CLASS = 'modal-blacklist-open';

class ShiftBlackList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      settings: ShiftApp.config.blackList || { countries: [] },
      showModal: false
    };

    if (this.state.settings.enable) {
      this.checkCountry();
    }
    
  }

  checkCountry = () => {
    const geoLookup = new GeoLookup();
    
    geoLookup.getCountry(this.state.settings.token).then(data => {
      const countries = this.state.settings.countries;
      const countryCode = data.data.country_short;

      if (data.result && countries.indexOf(countryCode) !== -1) {
        this.setState({ showModal: true });
        this.toggleBodyClass(true);
      }
    });
  }

  /**
   * @param {Boolean} showModal
   */
  toggleBodyClass = showModal => 
                      document.body.classList.toggle(BODY_CLASS, showModal);

  render() {
    return (
      <div className="modal-blacklist">
        {this.state.showModal &&
          <Modal hideCloseLink>
            <WidgetBase hideCloseLink={true}>
              <div 
                className="modal-blacklist-content" 
                dangerouslySetInnerHTML={{ 
                  __html: ShiftApp.translation(this.state.settings.text) || this.state.settings.text 
                }}>
              </div>
            </WidgetBase>
          </Modal>
        }
      </div>
    );
  }
}

export default ShiftBlackList;
