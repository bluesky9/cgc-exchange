/* global ShiftApp, localStorage, document */
import React from 'react';
import {
  formatNumberToLocale
} from '../helper';

import ShiftTicker from './shift-ticker';

// DIFFERENCE FROM AP InstrumentSelect:
// ADDS PAIR SEARCH FUNCTIONALITY A LA ShiftTicker
// CONFIG 'dropdownInstrumentSelect' SWITCHES BETWEEN RENDERING DROPDOWN TOGGLE BUTTON OR ONLY CURRENT PAIR 

class ShiftInstrumentSelect extends React.Component {
  
  state = {
    instruments: [],
    instrumentTicks: {},
    selectedInstrument: null,
    filterIndices: [], 
    filter: '',
    pair: ShiftApp.prodPair.value
  }
  
  componentDidMount() {
    if (ShiftApp.config.dropdownInstrumentSelect) {
      this.instruments = ShiftApp.instruments
        .filter(instruments => instruments.length)
        .take(1)
        .subscribe((instruments) => {
          this.setState({ instruments }, () => this.selectInstrument(localStorage.getItem('SessionInstrumentId') || 1));
        });

      if (ShiftApp.config.instrumentSelectTicker) {
        this.tickers = ShiftApp.instruments.subscribe(productPairs => {
          productPairs.forEach(pair => ShiftApp.subscribeLvl1(pair.InstrumentId));
        });

        this.level1 = ShiftApp.Level1.subscribe(instrumentTicks => {
          this.setState({ instrumentTicks });
        });

        this.products = ShiftApp.products.filter(data => data.length).subscribe(prods => {
          const decimalPlaces = {};
          prods.forEach(product => {
            decimalPlaces[product.Product] = product.DecimalPlaces;
          });

          this.setState({ decimalPlaces });
        });
      }
      this.instrumentCheck = ShiftApp.instrumentChange.subscribe(i => this.setState({ selectedInstrument: i }));
    } else {
      this.instrumentUpdate = ShiftApp.prodPair.subscribe(pair => this.setState({ pair }));
    }
  }

  componentWillUnmount() {
    if (ShiftApp.config.dropdownInstrumentSelect) {
      this.instruments.dispose();
      if (ShiftApp.config.instrumentSelectTicker) {
        this.level1.dispose();
        this.tickers.dispose();
        this.products.dispose();
      }
      this.instrumentCheck.dispose();
    } else {
      this.instrumentUpdate.dispose();
    }
  }

  onFilterChange = (e) => {
    let filter = e.target.value;
    let filterIndices = [];
    if (filter) {
      filter = filter.toUpperCase();
      this.state.instruments.forEach(instrument => {
        if (instrument.Symbol && instrument.Symbol.indexOf(filter) !== -1) {
            filterIndices.push(instrument.InstrumentId);
        }
      })
    } else {
      // Deactivate filter and show all instruments:
      filter = '';
      this.state.instruments.forEach(instrument => {
        filterIndices.push(instrument.InstrumentId);
      })
    }
    this.setState({ filterIndices, filter });
  }

  onKeyUp = (e) => {
    if (e.keyCode !== 13) {
      // do nothing if the key is not 'ENTER'
      return;
    } else if (this.state.filterIndices.length === 1) {
      // Only trigger instrument change on 'ENTER' if current filter matches exactly one instrument:
      this.selectInstrument(this.state.filterIndices[0]);
    }
  }

  selectInstrument = (instrumentId) => {
    const selectedInstrument = +instrumentId;
    const instrument = this.state.instruments.find((inst) => inst.InstrumentId === selectedInstrument);

    localStorage.setItem('SessionInstrumentId', selectedInstrument);
    document.APAPI.Session.SelectedInstrumentId = selectedInstrument;
    localStorage.setItem('SessionPair', instrument.Symbol);
    ShiftApp.setProductPair(instrument.Symbol);
    ShiftApp.instrumentChange.onNext(selectedInstrument);
    if (this.state.selectedInstrument) {
      this.unsubscribeInstrument(this.state.selectedInstrument);
    }
    this.subscribeInstrument(selectedInstrument);
    this.setState({ 
      selectedInstrument, 
      filter: '', 
      filterIndices: [] 
    });
  };

  subscribeInstrument = (InstrumentId) => {
    ShiftApp.subscribeTrades(InstrumentId, 100);
    ShiftApp.subscribeLvl2(InstrumentId);
  };

  unsubscribeInstrument = (InstrumentId) => {
    ShiftApp.unsubscribeTradesCall(InstrumentId);
    ShiftApp.unsubscribeLvl2(InstrumentId);
  };

  render() {
    if (ShiftApp.config.dropdownInstrumentSelect) {
      const { instrumentTicks, decimalPlaces, filterIndices, instruments } = this.state;
      const instrument = instruments.find((inst) => inst.InstrumentId === +this.state.selectedInstrument);
      const instrumentsList = instruments
        .filter((inst) => (inst.InstrumentId !== (instrument && instrument.InstrumentId)) && (filterIndices.length ? filterIndices.includes(inst.InstrumentId) : true))
        .map((inst, idx) => {
          const pxChange = instrumentTicks[inst.InstrumentId] && instrumentTicks[inst.InstrumentId].Rolling24HrPxChange;
          const priceDecimal = ShiftApp.config.DecimalPerInstrument[inst.Symbol] 
                              || ShiftApp.config.decimalPlaces 
                              || (symbol.slice(-3) === 'USD' ? 2 : 8);
          const pxClass = !pxChange ? '' : pxChange > 0 ? 'up' : 'down';
          return (
            <li key={idx} className={`instrument-${inst.Symbol}`}
              onClick={(e) => {
                  e.preventDefault();
                  this.selectInstrument(inst.InstrumentId);
                }}
            >
              <a
                className={ShiftApp.config.instrumentSelectTicker && "instrument-symbol"}
              >{ShiftApp.config.reversePairs ? (inst.Product2Symbol + inst.Product1Symbol) : inst.Symbol}</a>
              { ShiftApp.config.instrumentSelectTicker &&
                <div className="instrument-row--detail">
                  <div className="instrument-row price">
                    { instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].BestBid, priceDecimal) }
                  </div>
                  <div className={`instrument-row change ${pxClass}`}>
                    { instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].Rolling24HrPxChange, 2 || ShiftApp.config.decimalPlaces) }%
                  </div>
                  { ShiftApp.config.instrumentSelectShowVolume &&
                    <div className="instrument-row volume">
                      { instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].Rolling24HrVolume, priceDecimal) }
                    </div>
                  }
                </div>
              }
            </li>
          )
        });

      return (
        <div className="dropdown instrument-dropdown">
          <button id="instrument-select" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {instrument ? instrument.Symbol : this.state.pair}
            {instrumentsList.length ? <span className="caret" style={{ marginLeft: '8px' }} /> : null}
          </button>
          {instrumentsList.length ?         
            <ul className={`dropdown-menu ${ShiftApp.config.instrumentSelectTicker && 'ticker'}`} aria-labelledby="dropdownMenu2">
              { ShiftApp.config.instrumentSelectTicker &&
                <li key='-2' className='ticker-input'>
                  <p><i className='material-icons'>search</i> {ShiftApp.translation('INSTRUMENT_SELECT.SEARCH_PAIRS') || 'Search Pairs'}</p>
                  <input 
                    onChange={this.onFilterChange} 
                    onKeyUp={this.onKeyUp} 
                    placeholder={ShiftApp.translation('INSTRUMENT_SELECT.EX_BTC') || 'ex. BTC'} 
                    value={this.state.filter} 
                    maxLength='8'
                    size='10'
                  />
                </li>
              }
              { ShiftApp.config.instrumentSelectTicker &&
                <li key={'-1'} className="instrument-header">
                  <div>{ShiftApp.translation('INSTRUMENT_SELECT.INSTRUMENT') || 'Pair'}</div>
                  <div>{ShiftApp.translation('INSTRUMENT_SELECT.LAST_PRICE') || 'Price'}</div>
                  <div>{ShiftApp.translation('INSTRUMENT_SELECT.T24_HOUR_CHANGE') || '24hr Chg'}</div>
                  { ShiftApp.config.instrumentSelectShowVolume &&
                    <div>{ShiftApp.translation('INSTRUMENT_SELECT.VOLUME') || 'Volume'}</div>
                  }
                </li>
              }
              {instrumentsList}
            </ul> : null}
        </div>
      );
    } else {
      return (
        <div className="current-instrument dropdown instrument-dropdown">
          <h3>{this.state.pair}</h3>
        </div>
      );
    }
  }
}

export default ShiftInstrumentSelect;
