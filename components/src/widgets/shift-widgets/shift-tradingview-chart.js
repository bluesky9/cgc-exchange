/* global ShiftApp, localStorage, ChartAPI, TradingView */
import React from 'react';
import Rx from 'rx-lite';

import TradingViewWidget, { Themes } from 'react-tradingview-widget';

const CONTAINER_ID = 'ap-trading-view-chart';
const TIMEZONE     = 'exchange'; 

class ShiftTradingViewChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPair: null,
      currentInstrumentObject: null,
      wsUri: ShiftApp.config.TickerDataWS 
        || localStorage.getItem('tradingServer') 
        || ShiftApp.config.API_V2_URL
    };
  }

  componentDidMount() {
    // TODO: still seems to be race condition for which is set
    this.instrumentSubscription = Rx.Observable.combineLatest(
      ShiftApp.prodPair,
      ShiftApp.instruments,
      (pair, instruments) => {
        const instrument = instruments.find(inst => inst.Symbol === pair);

        this.setState({
          currentPair: pair
        });

        return instrument;
      }
    ).subscribe(instrument => {
      this.setState({ currentInstrumentObject: instrument });
    });
  }

  componentWillUnmount() {
    this.instrumentSubscription.dispose();
  }

  render() {
    const chartOptions = {
      datafeed: new ChartAPI.UDFCompatibleDatafeed(this.state.wsUri, null, null),
      symbol: this.state.currentPair || '',
      locale: ShiftApp.config.defaultLanguage,
      width: '100%',
      height: '400px',
      container_id: CONTAINER_ID,
      theme: Themes.DARK,
      timezone: TIMEZONE,
      ...ShiftApp.config.chart
    };

    return (
      <div className="tradingview-chart">
        <TradingViewWidget
         {...chartOptions}
        />
      </div>
    );
  }
}

export default ShiftTradingViewChart;
