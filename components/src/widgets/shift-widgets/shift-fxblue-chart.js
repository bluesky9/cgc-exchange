/* global ShiftApp, localStorage, ChartAPI, TradingView */
import React from 'react';
import Rx from 'rx-lite';

import TradingViewWidget, { Themes } from 'react-tradingview-widget';
import ShiftIframe from '../../misc/shift-iframe';

class ShiftFXBlueChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productPairs: [],
      currentPair: null,
      currentInstrumentObject: null,
      urlSource: 'https://shiftforex.fxbluelabs.com/#',
      currentPairUrl: ''
    };
  }

  componentDidMount() {
    this.wsUri =
      ShiftApp.config.TickerDataWS ||
      localStorage.getItem('tradingServer') ||
      ShiftApp.config.API_V2_URL;

    this.instrumentSubscription = Rx.Observable.combineLatest(
      ShiftApp.prodPair,
      ShiftApp.instruments,
      (pair, instruments) => {
        const instrument = instruments.find(inst => inst.Symbol === pair);

        this.setState(
          {
            productPairs: instruments,
            currentPair: pair,
            currentPairUrl: this.chartUrlParam(pair)
          },
          this.changeSymbol
        );

        return instrument;
      }
    ).subscribe(instrument => {
      this.setState({ currentInstrumentObject: instrument });
    });

    if (fxblueChartConfig) {
      this.setChartWorkspace();
    }
  }

  componentWillUnmount() {
    this.instrumentSubscription.dispose();
  }

  chartUrlParam = pair => 'bitfinex@' + pair;

  changeSymbol = () => {
    const chart = document.getElementById('fxblue-chart');

    chart.contentWindow.postMessage(
      {
        command: 'setChartSymbol',
        symbol: this.state.currentPairUrl
      },
      '*'
    );
  };

  setChartWorkspace = () => {
    const chart = document.getElementById('fxblue-chart');

    chart.onload = () => {
      chart.contentWindow.postMessage(
        {
          command: 'setWorkspace',
          workspace: fxblueChartConfig
        },
        '*'
      );
      this.changeSymbol();
    };
  };

  render() {
    return (
      <div className="fxblue-chart">
        <ShiftIframe
          src={this.state.urlSource + this.state.currentPairUrl}
          height="400px"
          id="fxblue-chart"
        >
          <p>
            {ShiftApp.translation('CHART.IFRAME_ERROR') ||
              'This feature requires iframe-support to display.'}
          </p>
        </ShiftIframe>
      </div>
    );
  }
}

export default ShiftFXBlueChart;
