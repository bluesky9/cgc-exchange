/* global $ */
/* eslint-disable react/no-multi-comp */
import React from 'react';
import {getURLParameter} from '../helper';
import InputLabeled from '../../misc/inputLabeled';
import InputNoLabel from '../../misc/inputNoLabel';
// import SelectLabeled from '../../misc/selectLabeled';
import WidgetBase from '../base';
import Modal from '../modal';
import ReCAPTCHAv2 from '../../misc/shift-fields/shift-recaptcha-v2';

const ShiftApp = global.ShiftApp;
const document = global.document;

// DIFFERENCE FROM AP WIDGET:
// ADDS RECAPTCHA
// ADDS TRANSLATIONS
// REMOVE INLINE STYLES

// DIFFERENCE FROM Shift WIDGET:
// country select dropdown and alternative terms and conditions


class ClefRegisterButton extends React.Component {
  componentWillMount() {
    !ShiftApp.config.clefFix && // eslint-disable-line no-unused-expressions
    ShiftApp.clef_load();
  }

  render() {
    return (
      <button
        type="button"
        className="clef-button btn btn-action login-buttons"
        data-app-id={ShiftApp.config.clefLogin}
        data-color="blue"
        data-style="flat"
        data-redirect-url={
          `${ShiftApp.config.clefRedirectURL}?type=registerwithclef`
        }
        data-custom="true"
      >
        {ShiftApp.translation('SIGNUP_MODAL.REGISTER_CLEF') || 'Register with Clef'}
      </button>
    );
  }
}

function TermsAndConditions(props) {
  return (
    <WidgetBase
      {...props}
      headerTitle={ShiftApp.translation("SIGNUP_MODAL.TERMS") || "Terms and Conditions"}
    >
      <div className="pad terms-and-conditions">{ShiftApp.translation("SIGNUP_MODAL.TERMS") || "Terms and Conditions"}</div>
    </WidgetBase>);
}

function PrivacyPolicy(props) {
  return (
    <WidgetBase
      {...props}
      headerTitle={ShiftApp.translation("SIGNUP_MODAL.PRIVACY") || "Privacy Policy"}
    >
      <div className="pad privacy-policy">{ShiftApp.translation("SIGNUP_MODAL.PRIVACY") || "Privacy Policy"}</div>
    </WidgetBase>);
}

class ShiftRegisterFormInnerAltV2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      authyRequired: false,
      googleRequired: false,
      smsRequired: false,
      registration: false,
      registered: false,
      passwordReset: false,
      useClef: ShiftApp.config.useClef,
      password: '',
      password2: '',
      username: '',
      email: '',
      termsAccepted: true,
      termsAndConditions: false,
      altTermsAndConditions: ShiftApp.config.registerForm.altTermsAndConditions,
      riskOfCrypto: false,
      notEUResident: false,
      showPrivacyModal: false,
      showTermsModal: false,
      inputsFilled: {email: "", username: "", password: "", password2: "", country: ""}
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {

    this.registerUser = ShiftApp.registerUser.subscribe((res) => {
      if (res.UserId) {
        this.setState({ registered: true });
        return this.props.setBanner({ information: '', error: '' });
      }

      if (res.errormsg) {
        // $.bootstrapGrowl(res.errormsg, {
        //   type: 'danger',
        //   allow_dismiss: true,
        //   align: ShiftApp.config.growlwerPosition,
        //   delay: ShiftApp.config.growlwerDelay,
        //   offset: { from: 'top', amount: 30 },
        //   left: '60%',
        // });
        if (res.errormsg === "Username already exists." && ShiftApp.config.useEmailAsUsername) {
          res.errormsg = ShiftApp.translation("SIGNUP_MODAL.EMAIL_EXISTS") || "Email address already exists.";
        }

        if (res.errormsg === "User Email adready exists." || res.errormsg === "User Email already exists.") {
          res.errormsg = ShiftApp.translation("SIGNUP_MODAL.EMAIL_EXISTS") || "Email address already exists.";
        }

        return this.props.setBanner({
          information: '', 
          error: res.errormsg
        });
      }

      return false;
    });
  }

  componentWillUnmount() {
    this.registerUser.dispose();
  }

  selectTerms = (e) => this.setState({ termsAccepted: e.target.checked });

  register = (e) => {
    // const strongRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$', 'g');
    // const emptyRegex = new RegExp('^(w+S+)$', 'g');
    const mediumRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[0-9]).*$', 'g');
    const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const enoughRegex = new RegExp('(?=.{8,}).*', 'g');
    const whiteSpaceRegex = /\s/;

    const email = (this.state.email).trim();
    const username = (this.state.username).trim();

    const growlerOpts = {
      type: 'danger',
      allow_dismiss: true,
      align: ShiftApp.config.growlwerPosition,
      delay: ShiftApp.config.growlwerDelay,
      offset: { from: 'top', amount: 30 },
      left: '60%',
    };
    const data = {
      UserInfo: {
        UserName: ShiftApp.config.useEmailAsUsername ? email : username,
        passwordHash: this.state.password,
        Email: email,
      },
      UserConfig: [],
      AffiliateTag: getURLParameter('aff') || '',
      OperatorId: ShiftApp.config.OperatorId,
    };

    e.preventDefault();

    if (!this.state.termsAccepted) {
      return this.props.setBanner({
        information: ShiftApp.translation('SIGNUP_MODAL.ACCEPT_TERMS_MSG') || 'Do you accept the terms and conditions?',
        error: '',
      });
    }

    if (!enoughRegex.test(this.state.password)) {
      // $.bootstrapGrowl('Password must contain at least 8 characters', growlerOpts);

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.PASSWORD_ENOUGH_REGEX_MSG') || 'Password must contain at least 8 characters',
      });
    }

    if (!mediumRegex.test(this.state.password)) {
      // $.bootstrapGrowl(
      //   'Password must contain at least 8 characters, one number, and at least one capital letter',
      //   growlerOpts);

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.PASSWORD_MEDIUM_REGEX_MSG') || 'Password must contain at least 8 characters, one number, and at least one capital letter',
      });
    }

    if (!this.state.password || !this.state.password2) {

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.PASSWORD_BLANK_MSG') || 'Please enter a password',
      });
    }

    if (!this.state.country) {

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.COUNTRY_BLANK_MSG') || 'Please Choose A Country',
      });
    }

    

    if (this.state.password !== this.state.password2) {
      // $.bootstrapGrowl(
      //   'Passwords do not match. Password must contain at least 8 characters, one number, and at least one capital letter',
      //   growlerOpts);

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.PASSWORDS_DONT_MATCH_MSG') || 'Passwords do not match.',
      });
    }

    if (!ShiftApp.config.useEmailAsUsername) {
      if (!username) {

        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.USERNAME_BLANK_MSG') || 'Please enter a username',
        });
      }

      // Validation: No spaces allowed in Username field
      if (whiteSpaceRegex.test(username)) {

        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.USERNAME_NO_SPACES_MSG') || 'Username cannot include spaces',
        });
      }
    }

    if (!email) {

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.EMAIL_BLANK_MSG') || 'Please enter your email address',
      });
    }

    if (!emailRegex.test(email)) {
      // Validation: No spaces allowed in Email field
      if (whiteSpaceRegex.test(email)) {

        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.EMAIL_NO_SPACES_MSG') || 'Email address cannot include spaces.',
        });
      }

      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.EMAIL_INVALID_MSG') || 'Please enter a valid email address',
      });
    }

    if (ShiftApp.config.registerForm.checkboxNotEUResident) {
      if (!this.state.notEUResident) {
        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.EU_RESIDENT_RESTRICTION_MSG') || `${ShiftApp.config.siteTitle} restricts users that are EU residents`,
        });
      }
    }
    if (ShiftApp.config.registerForm.checkboxRiskOfCrypto) {
      if (!this.state.riskOfCrypto) {
        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.RISK_ACCEPT_MSG') || 'Please acknowledge the risks associated with crypto-currency trading activities',
        });
      }
    }
    if (ShiftApp.config.registerForm.checkboxTermsAndConditions) {
      if (!this.state.termsAndConditions) {
        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.TERMS_ACCEPT_MSG') || 'Please read and agree to the terms and conditions',
        });
      }
    }
    if (ShiftApp.config.registerForm.altTermsAndConditions) {
      if (!this.state.altTermsAndConditions) {
        return this.props.setBanner({
          information: '',
          error: ShiftApp.translation('SIGNUP_MODAL.ALT_TERMS_ACCEPT_MSG') || 'Alternative Terms and Conditions',
        });
      }
    }    
    // reCAPTCHA check
    if (!grecaptcha.getResponse()) {
      return this.props.setBanner({
        information: '',
        error: ShiftApp.translation('SIGNUP_MODAL.ROBOT_CAPTCHA') || 'Are You a Robot?'
      });
    }

    return ShiftApp.registerNewUser(data);
  }

  resetPassword = () => {
    ShiftApp.resetPassword({
      email: this.state.email,
    }, (res) => this.setState({
      passwordReset: res.isAccepted,
    }));
  }

  showTermsModal = () => this.setState({ showTermsModal: true });

  showPrivacyModal = () => this.setState({ showPrivacyModal: true });

  closeModals = () => this.setState({ showTermsModal: false, showPrivacyModal: false });

  handleInputChange = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const inputsFilled = this.state.inputsFilled;

    this.setState({
      [name]: value
    });

    if (target.type !== 'checkbox') {
      inputsFilled[name] = value;
      this.setState({ inputsFilled })
    }
  }

  defaultView = () => {
    const closeButton = document.getElementById('login') || '';

    if (closeButton) {
      closeButton.addEventListener('click', (e) => {
        if (e.target.className === 'mfp-close') {
          const form = document.getElementById('registerForm');

          form.reset();
          this.props.setBanner({
            information: '',
            error: '',
          });
        }
      });
    }

    return (
      <form id="registerForm" onSubmit={this.register}>
        <div className={ShiftApp.config.v2Widgets ? "pad" : "pad col-xs-12"}>
          {!this.state.passwordReset ?
            <span>
              {!ShiftApp.config.useEmailAsUsername &&
              <span className="input input--custom">
                <InputNoLabel
                  placeholder={ShiftApp.translation('SIGNUP_MODAL.USERNAME') || 'Username'}
                  className="input-field"
                  name="username"
                  onChange={this.handleInputChange}
                />
                <i className={"fa fa-user " + (this.state.inputsFilled.username && "completed")}
                   aria-hidden="true"></i>
              </span>
              }
              <span className="input input--custom">
                <InputNoLabel
                  placeholder={ShiftApp.translation('SIGNUP_MODAL.EMAIL') || 'Email'}
                  className="input-field"
                  name="email"
                  onChange={this.handleInputChange}
                />
                <i className={"fa fa-envelope " + (this.state.inputsFilled.email && "completed")}
                   aria-hidden="true"></i>
              </span>
              <span className="input input--custom">
                <InputNoLabel
                  placeholder={ShiftApp.translation('SIGNUP_MODAL.PASSWORD') || 'Password'}
                  className="input-field"
                  type="password"
                  name="password"
                  onChange={this.handleInputChange}
                />
                <i className={"fa fa-key " + (this.state.inputsFilled.password && "completed")}
                   aria-hidden="true"></i>
              </span>
              <span className="input input--custom">
                <InputNoLabel
                  placeholder={ShiftApp.translation('SIGNUP_MODAL.VERIFYPASSWORD') || 'Confirm Password'}
                  className="input-field"
                  type="password"
                  name="password2"
                  onChange={this.handleInputChange}
                />
                <i className={"fa fa-key " + (this.state.inputsFilled.password2 && "completed")}
                   aria-hidden="true"></i>
              </span>  
              <p className="password-rules col-12">
                {ShiftApp.translation('SIGNUP_MODAL.PASSWORD_RULES') || 'The password needs to be at least 8 characters, including 1 number and 1 capital letter.'}
              </p>              
              <span className="input input--custom">
                    <select name="country" className="form-control" onChange={this.handleInputChange}>
                        <option value="">{ShiftApp.translation('SIGNUP_MODAL.SELECT_COUNTRY_TEXT') || 'SIGNUP_MODAL.SELECT_COUNTRY_TEXT'}</option>
                        <option value="primary">{ShiftApp.translation('SIGNUP_MODAL.PRIMARY_COUNTRY') || 'SIGNUP_MODAL.PRIMARY_COUNTRY'}</option>
                        <option value="other">{ShiftApp.translation('SIGNUP_MODAL.OTHER_COUNTRY') || 'SIGNUP_MODAL.OTHER_COUNTRY'}</option>
                    </select>
                    <i className={"fa fa-key " + (this.state.inputsFilled.country && "completed")}
                   aria-hidden="true"></i>
              </span>


              {ShiftApp.config.registerForm.checkboxNotEUResident &&
              <label className="register-checkbox-label">
                <input
                  name="notEUResident"
                  className="register-checkbox"
                  type="checkbox"
                  checked={this.state.notEUResident}
                  onChange={this.handleInputChange}/>
                {ShiftApp.translation('SIGNUP_MODAL.NOT_EU_RESIDENT') || 'I am not a resident of US.'}
              </label>
              }
              {ShiftApp.config.registerForm.checkboxRiskOfCrypto &&
              <label className="register-checkbox-label">
                <input
                  name="riskOfCrypto"
                  type="checkbox"
                  className="register-checkbox"
                  checked={this.state.riskOfCrypto}
                  onChange={this.handleInputChange}/>
                {ShiftApp.translation('SIGNUP_MODAL.RISK_ASSOCIATED') || 'I understand there are risks associated with crypto-currency trading activities.'}
              </label>
              }

              {(this.state.authyRequired
              || this.state.googleRequired
              || this.state.smsRequired) &&
              <InputLabeled
                placeholder={ShiftApp.translation('SIGNUP_MODAL.AUTH_QUES') || '2FA Verification Code'}
                type="string"
                name="authCode"
              />
              }
              {!ShiftApp.config.registerForm.showTermsandConditions && !ShiftApp.config.registerForm.checkboxTermsAndConditions &&
              <div className="keyPermissions">
                <input
                  type="checkbox"
                  name="terms_accept"
                  onClick={this.selectTerms}
                  value={this.state.termsAccepted}
                /> 
                  <span>{ShiftApp.translation('SIGNUP_MODAL.I_ACCEPT') || 'I accept the' }
                  <a target="_blank" href="terms.html">{ShiftApp.translation('SIGNUP_MODAL.TERMS') || 'Terms and Conditions'}</a> </span>
                &nbsp;{ShiftApp.translation('SIGNUP_MODAL.AND') || 'and'} 
                  <a target="_blank" href="privacy.html">{ShiftApp.translation('SIGNUP_MODAL.PRIVACY') || 'Privacy Policy'} </a>
                <br />
              </div>
              }

              <br />
              {/* RECAPTCHA */}
              <ReCAPTCHAv2 />
              {/* END RECAPTCHA */}
              {this.state.inputsFilled.country === 'other' &&
                <div className="keyPermissions">
                  {ShiftApp.translation('SIGNUP_MODAL.BY_CLICKING') || 'By Clicking '} 
                  <span className="sign-up-text">{ShiftApp.translation('SIGNUP_MODAL.SIGNUP') || 'Sign Up '}</span> 
                  {ShiftApp.translation('SIGNUP_MODAL.YOU_ACCEPT_OUR') || 'you accept our '}&nbsp;
                  <a target="_blank" className="keyPermissions-link" href={ShiftApp.translation('SIGNUP_MODAL.OTHER_TERMS_URL') || 'terms.html'}>
                    {ShiftApp.translation('SIGNUP_MODAL.OTHER_TERMS') || 'OTHER_TERMS Terms and Conditions'}
                  </a>
                </div>
              }
              {this.state.inputsFilled.country === 'primary' &&
                  <div className="keyPermissions">
                    {ShiftApp.translation('SIGNUP_MODAL.BY_CLICKING') || 'By Clicking '} 
                    <span className="sign-up-text">{ShiftApp.translation('SIGNUP_MODAL.SIGNUP') || 'Sign Up '}</span>
                    {ShiftApp.translation('SIGNUP_MODAL.YOU_ACCEPT_OUR') || 'you accept our '}&nbsp;
                    <a target="_blank" className="keyPermissions-link" href={ShiftApp.translation('SIGNUP_MODAL.OTHER_TERMS_URL') || 'terms.html'}>
                      {ShiftApp.translation('SIGNUP_MODAL.OTHER_TERMS') || 'Terms and Conditions of the Subsidiary'}
                    </a>
                    <span> {ShiftApp.translation('SIGNUP_MODAL.AND_OUR') || ' and our '}</span>
                    
                    <a target="_blank" className="keyPermissions-link" href={ShiftApp.translation('SIGNUP_MODAL.PRIMARY_TERMS_URL') || 'terms.html'}>
                      {ShiftApp.translation('SIGNUP_MODAL.PRIMARY_TERMS') || 'Primary: Terms and Conditions'}
                    </a>
                    
                  </div>
                }
              <div className="clearfix">
                <div className={ShiftApp.config.templateStyle === 'retail' ? '' : 'pull-right'}>
                  <br />
                  {ShiftApp.config.templateStyle !== 'retail' &&
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.props.close}
                  >
                    {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                  </button>}

                  {this.state.useClef && <ClefRegisterButton />}
                  {' '}
                  {ShiftApp.config.templateStyle === 'retail' ?
                    <button
                      type="submit"
                      style={ShiftApp.config.v2Widgets ? null : {width: '100%', margin: '0 auto'}}
                      onClick={this.register}
                      className="btn btn-lg submit underline"
                    >
                      {ShiftApp.translation('SIGNUP_MODAL.SIGNUP') || 'Sign Up'}
                    </button>
                    :
                    <button
                      type="submit"
                      className="btn btn-action"
                    >
                      {ShiftApp.translation('BUTTONS.TEXT_SIGNUP') || 'Create Account'}
                    </button>
                  }
                </div>

              </div>
              <br />
            </span>
            :
            <h2 className="text-center">
              {ShiftApp.translation('SIGNIN_MODAL.PASSWORD_SENT') || 'Check email for password reset link'}
            </h2>
          }
        </div>

        {this.state.showTermsModal && <Modal close={this.closeModals}><TermsAndConditions /></Modal>}
        {this.state.showPrivacyModal && <Modal close={this.closeModals}><PrivacyPolicy /></Modal>}
      </form>
    );
  };

  render() {
    if (!this.state.registered) return this.defaultView();

    return (
      <span>
        <h3 className={ShiftApp.config.apexSite ? 'text-center pad' : 'text-center'}>
          {ShiftApp.translation('SIGNUP_MODAL.REGISTERED') || 'Account Created. Check your email for Activation Link.'}
        </h3>
      </span>);
  }
}

ShiftRegisterFormInnerAltV2.defaultProps = {
    setBanner: () => {
    },
    close: () => {
    },
    hideCloseLink: true,
};

ShiftRegisterFormInnerAltV2.propTypes = {
    setBanner: React.PropTypes.func,
    close: React.PropTypes.func,
};

export default ShiftRegisterFormInnerAltV2;