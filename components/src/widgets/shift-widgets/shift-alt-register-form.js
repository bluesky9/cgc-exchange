/* global ShiftApp */
import React from 'react';

import WidgetBase from '../base';
import ShiftRegisterFormInnerAltV2 from './shift-register-form-inner-alt-v2';
// Until I get the css to work on the widget
// var InputLabeled = require('../misc/inputLabeled');


class ShiftAltRegisterForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: '',
      information: '',
    };
  }

  setBanner = (info) => this.setState(info);

  render() {
    return (
      <WidgetBase
        {...this.props}
        headerTitle={ShiftApp.translation('SIGNUP_MODAL.TITLE_TEXT') || 'Signup'}
        information={this.state.information}
        error={this.state.error}
      >
        <ShiftRegisterFormInnerAltV2 {...this.props} setBanner={this.setBanner} /> 
      </WidgetBase>
    );
  }
}

ShiftAltRegisterForm.defaultProps = {
  hideCloseLink: true,
};

export default ShiftAltRegisterForm;