/* global ShiftApp, $, atob, window */
/**
 * Config variables
 * @param resetPassword        {Object}  - Settings for reset password
 * @param modalWelcome.showAcceptCheckbox {Boolean} - Show checkbox btn
 * @param modalWelcome.showAcceptTEXT {Boolean} - Show terms and condition text
 * 
 */
import React from 'react';
import WidgetBase from '../base';
import InputNoLabel from '../../misc/inputNoLabel';
import { getURLParameter, showGrowlerNotification } from '../helper';

class ShiftResetPasswordV2 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newPassword: '',
            confirmPassword: '',
            inputsFilled: { newPassword: '', confirmPassword: '' },
            error: '',
            success: '',
            information: '',
			hideCloseLink: true,
			termsAccepted: true,
			showPasswordRules: false,
			resetPasswordSettings: ShiftApp.config.resetPassword || {}
        };
	}
	
	componentDidMount() {
		if (this.state.resetPasswordSettings.showAcceptCheckbox) {
			this.setState({ termsAccepted: false })
		}
	}

    colorInIcons = (e) => {
        let value = e.target.value,
        name = e.target.name,
        inputsFilled = this.state.inputsFilled;

        inputsFilled[name] = value;
        this.setState({ inputsFilled })
    };

    setBanner = (info) => {
        this.setState(info);
	};
	
	hidePasswordRules = () => {
		this.setState({ showPasswordRules: false });
	}

	showPasswordRules = (state) => {
		this.setState({ showPasswordRules: state });
	}

    handleSubmit = (e) => {
        e.preventDefault();
        const self = this,
            encodedUrl = getURLParameter('d1'),
            verifyCode = getURLParameter('verifycode'),
            userId = getURLParameter('UserId');
        this.setBanner({
            information: ShiftApp.translation('COMMON.PLEASE_WAIT') || 'Please wait...',
            error: '',
        });
        const mediumRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[0-9]).*$', 'g');
        const enoughRegex = new RegExp('(?=.{8,}).*', 'g');

        if (!this.refs.newPassword.value() || !this.refs.confirmPassword.value()) {
            this.setBanner({
                information: '',
                error: ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_ENTER_FIELDS') || 'Please enter your password in both fields',
            });
        }
        else if (this.refs.newPassword.value() !== this.refs.confirmPassword.value()) {
            this.setBanner({
                information: '',
                error: ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_NOT_MATCH') || 'Your Passwords Do Not Match',
            });
        }
        else if (!enoughRegex.test(this.refs.newPassword.value())) {
          this.setBanner({
            information: '',
            error: ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_RULES') || 'Password must contain at least 8 characters'
          });
        }
        else if (!mediumRegex.test(this.refs.newPassword.value())) {
          this.setBanner({
            information: '',
            error: ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_RULES') || 'Password must contain at least 8 characters, one number, and at least one capital letter'
          });
        }
        else if (!encodedUrl) {
            return
            this.setBanner({
                information: '',
                error: ShiftApp.translation('RESET_PASSWORD.ERROR_D1_NOT_FOUND') || 'Required parameter d1 was not found in url.',
            });
        }
        else if (!verifyCode) {
            return
            this.setBanner({
                information: '',
                error: ShiftApp.translation('RESET_PASSWORD.ERROR_CODE_NOT_FOUND') || 'Required parameter verifycode was not found in url.',
            });
        }
        else if (!userId) {
            return
            this.setBanner({
                information: '',
                error: ShiftApp.translation('RESET_PASSWORD.ERROR_USER_ID_NOT_FOUND') || 'Required parameter UserId was not found in url.',
            });
		} else if (!this.state.termsAccepted) {
			this.setBanner({
				information: '',
				error: ShiftApp.translation('RESET_PASSWORD.ERROR_ACCEPT_TERMS') || 'Do you accept the terms and conditions?'
			  });
		} else {

            const ajaxUrl = atob(encodedUrl);
            const payload = {
                PendingCode: verifyCode,
                Password: this.refs.newPassword.value(),
                UserId: userId,
            };

            return $.ajax({
                type: 'POST',
                url: `${ajaxUrl}ResetPassword2`,
                data: JSON.stringify(payload),
                dataType: 'JSON',
                success: (data) => {
                    if (!data.result) {
                        if (data.rejectReason) {
                            return this.setBanner({information: '', error: `${ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_RESET') || 'Password Reset error'}: ${data.rejectReason}`});
                        }
                        return this.setBanner({information: '', error: ShiftApp.translation('RESET_PASSWORD.ERROR_PASS_RESET') || 'Password Reset error'});
                    }
                    setTimeout(function() {
                        $('.close-m').click();
                    }, 3000);
                    window.localStorage.removeItem('SessionToken');
                    return this.setBanner({
                        success: ShiftApp.translation('RESET_PASSWORD.SUCCESSFULLY') || 'You successfully reset your password.',
                        information: '',
                        error: '',
                    });
                },
            })
        }
	};
	
	selectTerms = (e) => this.setState({ termsAccepted: e.target.checked });

    render() {
        return (
            <WidgetBase 
              {...this.props} 
              {...this.state} 
              headerTitle={ShiftApp.translation('RESET_PASSWORD.TITLE_TEXT') || 'Reset Password'}
            >
                <div className="shift-reset-password">
                    <form>
                        <div className="pad">
							{this.state.showPasswordRules && 
								<p className="password-rules">
									{ShiftApp.translation('SIGNUP_MODAL.PASSWORD_RULES') || 'The password needs to be at least 8 characters, including 1 number and 1 capital letter.'}
								</p>
                			}
                            <span className="input input--custom">
								<InputNoLabel 
									placeholder={ ShiftApp.translation('RESET_PASSWORD.PASS_LABEL') || 'Reset Password' }
									type="password" 
									ref="newPassword" 
									name="newPassword" 
									className="input-field" 
									onClick={() => this.showPasswordRules(true)}
									onBlur={() => this.showPasswordRules(false)}
									colorChange={this.colorInIcons} />
                                <i className={"fa fa-key " + (this.state.inputsFilled.newPassword && "completed")} aria-hidden="true"></i>
                            </span>
                            <span className="input input--custom">
								<InputNoLabel 
									placeholder={ShiftApp.translation('RESET_PASSWORD.PASS_CONFIRM_LABEL') || 'New password' }
									type="password" 
									ref="confirmPassword" 
									name="confirmPassword" 
									className="input-field" 
									colorChange={this.colorInIcons} />
                                <i className={"fa fa-key " + (this.state.inputsFilled.confirmPassword && "completed")} aria-hidden="true"></i>
                            </span>
							<p className="accept-box">
								<span>
									{this.state.resetPasswordSettings.showAcceptCheckbox && (
										<input
											type="checkbox"
											name="terms_accept"
											onClick={this.selectTerms}
										/> 
									)}
									{this.state.resetPasswordSettings.showAcceptText && (
										<span dangerouslySetInnerHTML={{__html: ShiftApp.translation('RESET_PASSWORD.ACCEPT_TEXT') || 'By Clicking <span class="accept-box--signup-text">Sign up</span> you accept our <a target="_blank" href="/terms.html">Terms and Conditions</a> and <a target="_blank" href="/privacy.html">Privacy Policy</a>'}}>
										</span>
									)}
								</span>
							</p>

                            <div className="text-center row around-xs">
                                <button
                                    type="submit"
                                    onClick={this.handleSubmit}
                                    className="btn btn-lg submit underline"
								>{ ShiftApp.translation('RESET_PASSWORD.RESET_BTN') || 'Reset Password' }</button>
                            </div>
                        </div>
                    </form>
                </div>
            </WidgetBase>
        );
    }
}

export default ShiftResetPasswordV2;
