import React from 'react';
import WidgetBase from '../base';
import InputNoLabel from '../../misc/inputNoLabel';
import Modal from '../modal';
import Kyc from '../kyc';
import TwoFACodeInput from '../twoFACodeInput';

var LimitRow = React.createClass({

  render: function () {

    var style
    var showhr //  okqneo
    var products = this.props.products;
    var level = this.props.level
    var daily = <div className="col-xs-3 text-center mt10"> 0 </div>
    var monthly = <div className="col-xs-3 text-center mt10"> 0 </div>

    if (products.length > 0) {
      var header = products.map(function (product) {
        return <div className="col-xs-3"> {product.Product} </div>
      })

      daily = products.map(function (product) {
        var currency = ShiftApp.config.currencyLimits.filter(function (currencyIn) {
            return (currencyIn.name === product.Product)
          }.bind(this))[0] || {};

        if (Object.keys(currency).length > 0) {
          if (level === 0) {
            return <div className="col-xs-3 text-center mt10"> {currency.level0.daily} </div>
          } else if (level === 1) {
            return <div className="col-xs-3 text-center mt10"> {currency.level1.daily} </div>
          } else if (level === 2) {
            return <div className="col-xs-3 text-center mt10"> {currency.level2.daily} </div>
          }
        }
      })
      monthly = products.map(function (product) {
        var currency = ShiftApp.config.currencyLimits.filter(function (currency) {
            return (currency.name === product.Product)
          }.bind(this))[0] || {};
        if (Object.keys(currency).length > 0) {
          if (level === 0) {
            return <div className="col-xs-3 text-center mt10"> {currency.level0.monthly} </div>
          } else if (level === 1) {
            return <div className="col-xs-3 text-center mt10"> {currency.level1.monthly} </div>
          } else if (level === 2) {
            return <div className="col-xs-3 text-center mt10"> {currency.level2.monthly} </div>
          }
        }
      })
    }

    // console.log('header',header);

    (level > 0) ? style = {visibility: "hidden"} : style = {display: "block"}
    // (this.props.level > 0 )?  showhr = {visibility:"hidden"} : showhr  = {display:"block"}
    return (
      <div className="col-xs-12">
        <div className="row ">
          <div className="col-xs-4 limit-header" style={{marginTop: '32px'}}>
            <div className="row">{ShiftApp.translation('USER_SETTINGS.LEVEL') || 'Level'} {level}</div>
            <div className="row mt10">{ShiftApp.translation('USER_SETTINGS.DAILY_LIMIT') || 'Daily Limit'}</div>
            <div className="row mt10">{ShiftApp.translation('USER_SETTINGS.MONTHLY_LIMIT') || 'Month Limit'}</div>
          </div>

          <div className="col-xs-8">
            <div className="row limit-header" style={style}>
              {header}
            </div>
            <hr className="limit-hr" style={style}/>
            <div className="row">
              {daily}
            </div>
            <div className="row">
              {monthly}
            </div>
          </div>
        </div>
      </div>
    )
  }
})

var ShiftSettings = React.createClass({
  getDefaultProps: function () {
    return {
      hideCloseLink: true
    }
  },

  getInitialState: function () {
    return {
      data: {
        UseNoAuth: true,
        UseGoogle2FA: false
      },
      accountInfo: [],
      username: '',
      userId: ShiftApp.userData.value.UserId,
      twoFA: {},
      passwordReset: false,
      waiting2FA: false,
      showVerifyWindow: false,
      stateDataCache: '',
      prerun: false,
      rerun: 0,
      useFA: false,
      hold2FA: false,
      useFaState: false,
      lastChanged: false,
      products: [],
      levelIncreaseStatus: '',
      isFirstNameDisabled: false,
      isLastNameDisabled: false,
    };
  },

  componentWillUnmount: function () {
    this.userInformation.dispose();
    this.userConfiguration.dispose();
    this.auth2FAuthentication.dispose();
    this.accountInformation.dispose();
    this.verifyInfoUpdate.dispose();
  },

  componentDidMount: function () {

    ShiftApp.redirect(window.location.pathname)
    this.userInformation = ShiftApp.getUser.subscribe(function (data) {
      // console.log("Chimney.getUserInfo",data);
      this.setState({username: data, useFA: data.Use2FA, hold2FA: data.Use2FA});
    }.bind(this));

    this.products = ShiftApp.products.subscribe(function (data) {
      this.setState({products: data})
    }.bind(this))

    this.accountInformation = ShiftApp.accountInfo.subscribe(function (data) {
      this.setState({accountInfo: data})
    }.bind(this))

    this.verifyInfoUpdate = ShiftApp.verificationLevelUpdate.subscribe(function (data) {
      this.setState({accountInfo: data})
    }.bind(this));

    this.userConfiguration = ShiftApp.getUserConfig.subscribe(function (data) {
      let configs = this.state.data;
      let that = this;
      if (data.length > 0) {

        configs = data.reduce(function (item, i) {
          item[i.Key] = i.Value;

          if (i.Key === "levelIncreaseStatus") {
            that.setState({levelIncreaseStatus: i.Value});
          }

          return item;
        }, {});

        if (configs.UseNoAuth && configs.UseGoogle2FA) {
          configs.UseNoAuth = JSON.parse(configs.UseNoAuth)
          configs.UseGoogle2FA = JSON.parse(configs.UseGoogle2FA)
        }
      }

      if (configs.firstName) {
        this.setState({ isFirstNameDisabled: true });
      }
      if (configs.lastName) {
        this.setState({ isLastNameDisabled: true });
      }
      this.setState({data: configs, stateDataCache: configs}); //cache to have a copy of configuration data from DidMount
    }.bind(this));

    ShiftApp.getUserCon({UserId: ShiftApp.userData.value.UserId})

    var server = {};
    this.userConfiguration = ShiftApp.getUserConfig.subscribe(function (data) {

      var configs = [];

      if (data.length > 0) {
        configs = data.reduce(function (item, i) {
          item[i.Key] = i.Value;
          return item;
        }, {});

        configs.UseNoAuth = !this.state.username.Use2FA;
        configs.UseGoogle2FA = this.state.username.Use2FA;

      }
      server = configs;
    }.bind(this));

    this.userConfigurationSubs = ShiftApp.setUserConfig.subscribe(function (data) {
      console.log("HERE IS THE DATA", data, data.length);

      // if (this.state.prerun) { return ; }
      var live = this.state.data;
      if (data.length == 0) {
        return;
      }


      if (this.state.hold2FA) {
        var temp2FA = {
          requireGoogle2FA: true,
          useFaState: this.state.hold2FA
        }
        this.setState({twoFA: temp2FA});
      }
      if (!this.state.hold2FA && live.UseGoogle2FA) {
        var temp2FA = {
          requireGoogle2FA: true,
          useFaState: this.state.hold2FA
        }
        this.setState({twoFA: temp2FA});
      }

      //call disable2FA
      if (this.state.hold2FA && !live.UseGoogle2FA) {
        console.log("got to disable");

        ShiftApp.Disable2FA.subscribe(function (res) {
          //then call authenticate 2FA

          var temp2FA = {
            requireGoogle2FA: true,
            useFaState: this.state.hold2FA
          }
          this.setState({twoFA: temp2FA});
        }.bind(this));

        ShiftApp.disable2FA({});
      }

      if (!server.UseGoogle2FA && !live.UseGoogle2FA && data.result) {

        // if (this.state.rerun > 0) {
        //   return ;
        // }
        if (!this.state.showVerifyWindow) {
          $.bootstrapGrowl(
            ShiftApp.translation('USER_SETTINGS.ACCT_INFO_ACCEPTED') || 'Account Information Accepted',
            {
              type: 'info',
              allow_dismiss: true,
              align: ShiftApp.config.growlwerPosition,
              delay: ShiftApp.config.growlwerDelay,
              offset: {from: 'top', amount: 30}, left: '60%'
            }
          );
        }
        //     this.setState({rerun:this.state.rerun++})
      }

    }.bind(this))
  },

  changed: function (e) {

    var data = {};
    for (var key in this.refs) {

      if (this.refs[key].type === 'checkbox' || this.refs[key].type === 'radio') {
        data[key] = this.refs[key].checked;

      } else {
        data[key] = this.refs[key].value();
      }
    }
    this.setState({data: data});
  },

  changed2FA: function (state) {
    this.setState({useFA: state});
  },

  do2FAVerification: function (code) {
    console.log('Authentication Code', code);

    var data = {};
    var server = []

    var data = {
      Code: code
    }
    var payloadData = this.configurePayload()

    this.auth2FAuthentication = ShiftApp.auth2FA.subscribe(function (res) {

      // console.log("Authentication Response",res);
      // console.log("2FA State From Server",this.state.username.Use2FA);
      // console.log("2FA State From hold2FA",this.state.hold2FA);
      // console.log("2FA State From Live",this.state.useFA);

      if (res.length == 0) return

      var reset = this.state.stateDataCache

      if (res.Authenticated == false) {

        $.bootstrapGrowl(
          ShiftApp.translation('USER_SETTINGS.CODE_REJECTED') || 'Code Rejected',
          {
            type: 'danger',
            allow_dismiss: true,
            align: ShiftApp.config.growlwerPosition,
            delay: ShiftApp.config.growlwerDelay,
            offset: {from: 'top', amount: 30},
            left: '60%'
          }
        );

        // this.setState({twoFA:{},waiting2FA:false});
        this.setState({twoFA: {}, data: reset, useFA: this.state.username.Use2FA, waiting2FA: false});

        // var payloadData = this.configurePayload()
        //  okqneo
        // ShiftApp.setUserConfig.subscribe(function(data){
        //   if (data.length==0) return
        //   this.setState({prerun:true}) //to prevent subcribe from the update or update2FA from happening
        // }.bind(this));

        //  ShiftApp.setUserCon(payloadData)
      } // end Authenticated == false

      if (res.Authenticated == true && this.state.waiting2FA) {
        this.setState({waiting2FA: false, passwordReset: true});
      }

      if (res.Authenticated == true) {

        ShiftApp.setUserConfig.subscribe(function (data) {
          if (data.result) {
            // $.bootstrapGrowl(ShiftApp.translation('PROFILE.SAVED') || 'Account Information Updated Please Wait',{ type: 'info',allow_dismiss:true, align:ShiftApp.config.growlwerPosition ,delay:ShiftApp.config.growlwerDelay,offset: {from: 'top', amount: 30}, left:'60%'});
            this.setState({twoFA: {}, hold2FA: !this.state.hold2FA})
            localStorage.setItem('userSettingsUpdate', true)
            setTimeout(function () {
              document.location.reload()
            }, 5000)
          }
        }.bind(this));
        // this.setState({hold2FA:!this.state.hold2FA})
        ShiftApp.setUserCon(payloadData)
      }

    }.bind(this));

    ShiftApp.authenticate2FA(data);
  },

  configurePayload: function () {

    var configurationPairs = [];
    var identification = ShiftApp.userData.value.UserId
    // In the for in below the data has to be sent in an array containing an object wit key value as the props

    for (var key in this.refs) {
      if (this.refs[key].type === 'checkbox' || this.refs[key].type === 'radio') {
        var userConfigs = {
          Key: key,
          Value: JSON.stringify(this.refs[key].checked)
        }
      } else {

        var myVal = this.refs[key].value();

        var userConfigs = {
          Key: key,
          Value: myVal
        };
      }

      configurationPairs.push(userConfigs);
    }

    var payloadData = {
      UserId: identification,
      UserId: this.state.username.UserId,
      Config: configurationPairs
    };

    return payloadData;
  },

  update: function (e) {

    var data = {};
    var server = {};
    var configRes;

    var payloadData = this.configurePayload()

    // console.log("SERVER",server); // this is the set got from the server

    var live = this.state.data;

    live.UseNoAuth = !this.state.useFA
    live.UseGoogle2FA = this.state.useFA

    // console.log("LIVE",live); // this the current set up on the page


    this.setState({prerun: false})
    // this.setState({rerun:false})

    if (this.state.rerun > 0) {
      this.setState({rerun: 0})
      return;
    }

    ShiftApp.setUserCon(payloadData);
  },

  update2FA: function () {
    var live = this.state.data;
    var payloadData = this.configurePayload();

    live.UseGoogle2FA = this.state.useFA // Will be the state of useFA...manipulated by changed2FA()
    live.UseNoAuth = !this.state.useFA // Should obviously therefore be the opposite


    if (!this.state.hold2FA && live.UseGoogle2FA) {
      var temp2FA = {
        requireGoogle2FA: true,
        useFaState: this.state.hold2FA
      }
      this.setState({twoFA: temp2FA});
    }

    //call disable2FA
    if (this.state.hold2FA && !live.UseGoogle2FA) {

      ShiftApp.Disable2FA.subscribe(function (res) {
        //then call authenticate 2FA

        var temp2FA = {
          requireGoogle2FA: true,
          useFaState: this.state.hold2FA
        }
        this.setState({twoFA: temp2FA});
      }.bind(this));

      ShiftApp.disable2FA({});
    }

    ShiftApp.setUserCon(payloadData);
  },

  closeModal: function () {
    this.setState({twoFA: {}, waiting2FA: false});
    document.location.reload()
  },

  verficationWindow: function () {
    if (ShiftApp.config.internalKYCRedirect) {
      $.bootstrapGrowl(
        ShiftApp.translation('USER_SETTINGS.REDIRECT_VERIFICATION') || 'Redirecting to Verification...',
        {
        type: 'info',
        allow_dismiss: true,
        align: ShiftApp.config.growlwerPosition,
        delay: ShiftApp.config.growlwerDelay,
        offset: {from: 'top', amount: 30},
        left: '60%'
      });
      if (ShiftApp.config.openKYCRedirectInNewWindow) {
        window.open(ShiftApp.config.internalKYCRedirectURL, '_blank')
      } else {
        document.location = ShiftApp.config.internalKYCRedirectURL
      }
    } else {
      this.setState({showVerifyWindow: true});
    }

  },

  closeModalVerify: function () {
    this.setState({showVerifyWindow: false});
  },


  resetPassword: function () {
    this.resetUserPassword = ShiftApp.resetPass.subscribe(function (res) {
      if (res.result) {
        this.setState({passwordReset: res.result});
      }
      if (!res.result && res.errormsg === "Waiting for 2FA.") {
        const temp2FA = {
          passwordReset2FA: true,
        };
        this.setState({twoFA: temp2FA, waiting2FA: true});
      }
    }.bind(this));


    ShiftApp.resetPassword({UserName: this.state.username.UserName});
    $.bootstrapGrowl(
      ShiftApp.translation('USER_SETTINGS.RESET_LINK_TO_INBOX') || 'An email with a reset password link has been sent to your inbox',
      {
      ...ShiftApp.config.growlerDefaultOptions,
      type: 'success'
    });
    setTimeout(() => {
      ShiftApp.logout();
      document.location = ShiftApp.config.logoutRedirect;
    }, 5000);
  },

  render: function () {
    let accountText;

    // TODO: Make switch case with fall-through for a more speedy conditional render
    if (ShiftApp.config.kycType === 'greenId') { // GreenId

      if (this.state.accountInfo.VerificationLevel === 0) {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ACCT') || "Verify Account";
      } else if (this.state.accountInfo.VerificationLevel === ShiftApp.config.UnderManualReviewLevel) {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      } else if (this.state.accountInfo.VerificationLevel == ShiftApp.config.VerifiedLevel) {
        accountText = ShiftApp.translation('USER_SETTINGS.ACCT_VERIFIED') || "Fully Verified";
      }

    }
    if (ShiftApp.config.kycType === 'IM' || ShiftApp.config.kycType === 'IDV') { // Identity Mind or IDV

      if (this.state.accountInfo.VerificationLevel === 0) {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ACCT') || "Verify Account";
      }
      if (this.state.accountInfo.VerificationLevel === 0 && this.state.levelIncreaseStatus === 'fail') {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ACCT') || "Verify Account";
      }
      if (this.state.accountInfo.VerificationLevel === 0 && this.state.levelIncreaseStatus === 'underReview') {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      }
      if (this.state.accountInfo.VerificationLevel === 1 && this.state.levelIncreaseStatus === "underReview") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      }
      if (this.state.accountInfo.VerificationLevel === 1 && this.state.levelIncreaseStatus === "fail") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ACCT') || "Verify Account";
      }

      if (this.state.accountInfo.VerificationLevel === 1 && this.state.levelIncreaseStatus === "pass") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ID') || "Verify ID";
      }
      if (this.state.accountInfo.VerificationLevel === 1 && this.state.levelIncreaseStatus === "fail") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ID') || "Verify ID";
      }
      if (this.state.accountInfo.VerificationLevel === 2 && this.state.levelIncreaseStatus === "pass") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ID') || "Verify ID";
      }
      if (this.state.accountInfo.VerificationLevel === 2 && ShiftApp.config.kycType === 'IDV') {
        accountText = ShiftApp.translation('USER_SETTINGS.ACCT_VERIFIED') || "Fully Verified";
      }
      if (this.state.accountInfo.VerificationLevel === 2 && this.state.levelIncreaseStatus === "fail") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ID') || "Verify ID";
      }
      if (this.state.accountInfo.VerificationLevel === 2 && this.state.levelIncreaseStatus === "underReview") {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      }

      if (this.state.accountInfo.VerificationLevel === 3) {
        accountText = ShiftApp.translation('USER_SETTINGS.ACCT_VERIFIED') || "Fully Verified";
      }

    } else if (ShiftApp.config.kycType === 'ManualKYC') { // ManualKYC

      if (this.state.accountInfo.VerificationLevel === 0 && this.state.levelIncreaseStatus === '') {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ACCT') || "Verify Account";
      }
      if (this.state.accountInfo.VerificationLevel === 0 && this.state.levelIncreaseStatus === 'underReview') {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      }
      if (this.state.accountInfo.VerificationLevel === 1 && !ShiftApp.config.sendDocsToEmail) {
        accountText = ShiftApp.translation('USER_SETTINGS.ACCT_VERIFIED') || "Fully Verified";
      }
      if (this.state.accountInfo.VerificationLevel === 1 && ShiftApp.config.sendDocsToEmail) {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFY_ID') || "Verify ID";
      }
      if (this.state.accountInfo.VerificationLevel === 2 && ShiftApp.config.sendDocsToEmail) {
        accountText = ShiftApp.translation('USER_SETTINGS.VERIFICATION_STATUS') || "Check Verification Status";
      }
      if (this.state.accountInfo.VerificationLevel === 3) {
        accountText = ShiftApp.translation('USER_SETTINGS.ACCT_VERIFIED') || "Fully Verified";
      }
    }

    return (
      <WidgetBase {...this.props} HeaderTitle={ShiftApp.translation('USER_SETTINGS.TITLE_TEXT') || 'Settings'}>

        <div className="container-fluid">
          <div className="row">
            <div className="col-md-9 col-xs-12">
              <div className="content">
                <h2
                  className="section-title">{ShiftApp.translation('USER_SETTINGS.UPDATE_TITLE') || "Update Account Information"}</h2>
                <p>{ShiftApp.translation('USER_SETTINGS.UPDATE_MESSAGE') || "By keeping your account information up to date you allow us to provide the prompt and high quality service you deserve. Your changes will be effective immediately."}</p>
                <div className="send-request-page inner">
                  <div className="clearfix"></div>
                  <div className="row mt30">
                    <div className="col-sm-6">
                      <h3
                        className="title-blue-bg">{ShiftApp.translation('USER_SETTINGS.ACCOUNT_INFORMATION') || "Account Information"}</h3>
                      <InputNoLabel 
                        ref='firstName' 
                        placeholder={ShiftApp.translation('USER_SETTINGS.FIRST_NAME') || "First Name"} 
                        value={this.state.data.firstName} 
                        className="mb20" 
                        onChange={this.changed}
                        disabled={this.state.isFirstNameDisabled}
                        />
                      <InputNoLabel 
                        ref='lastName' 
                        placeholder={ShiftApp.translation('USER_SETTINGS.LAST_NAME') || "Last Name"} 
                        value={this.state.data.lastName} 
                        className="mb20" 
                        onChange={this.changed}
                        disabled={this.state.isLastNameDisabled}
                        />
                      <div className="row mb20">
                        <div className="col-sm-5 pr5">
                          <InputNoLabel ref='ccode' placeholder={ShiftApp.translation('USER_SETTINGS.COUNTRY_CODE') || "Country Code"}
                                        value={this.state.data.ccode} onChange={this.changed}/>
                        </div>
                        <div className="col-sm-7 pl5">
                          <InputNoLabel ref='telephone' placeholder={ShiftApp.translation('USER_SETTINGS.PHONE_NUMBER') || "Phone number"}  
                                        value={this.state.data.telephone} onChange={this.changed}/>
                        </div>
                      </div>
                      <a className="btn btn-blue btn-shadow pull-right"
                         onClick={this.update}>{ShiftApp.translation('USER_SETTINGS.UPDATE_TEXT') || "Update"}</a>
                    </div>
                    <div className="col-sm-6">
                      <h3
                        className="title-blue-bg">{ShiftApp.translation('USER_SETTINGS.SECURITY_TEXT') || "Security Settings"}</h3>
                      <p
                        className="p-bottom-border">{ShiftApp.translation('USER_SETTINGS.2FA_AUTH') || "2-Factor Authentication"}</p>
                      <div className="mt15 clearfix">
                        <p
                          className="pull-left fs13 pt10 m0">{ShiftApp.translation('USER_SETTINGS.GOOGLE_AUTH') || "Google Authenticator"}</p>
                        <div className="on-off-switcher segmented-control pull-right">
                          <input type="radio" name="switcher" id="switcher-on" ref="UseGoogle2FA"
                                 checked={this.state.useFA} onChange={this.changed2FA.bind(this, true)}/>
                          <input type="radio" name="switcher" id="switcher-off" ref='UseNoAuth'
                                 checked={!this.state.useFA} onChange={this.changed2FA.bind(this, false)}/>
                          <label htmlFor="switcher-on"
                                 data-value="ON">{ShiftApp.translation('USER_SETTINGS.ON') || "ON"}</label>
                          <label htmlFor="switcher-off"
                                 data-value="OFF">{ShiftApp.translation('USER_SETTINGS.OFF') || "OFF"}</label>
                        </div>
                      </div>
                      <a className="btn btn-blue btn-shadow update-2FA-button" 
                        onClick={this.update2FA}> {ShiftApp.translation('USER_SETTINGS.UPDATE_2FA') || "Update 2FA"}</a>
                      <p
                        className="mt15 p-bottom-border">{ShiftApp.translation('USER_SETTINGS.RESET_PASS') || "Password Reset"}</p>
                      {!this.state.passwordReset ?
                        <p><a className="orange-link" href="#"
                              onClick={this.resetPassword}>{ShiftApp.translation('USER_SETTINGS.CLICK_HERE') || "Click here"}</a> {ShiftApp.translation('USER_SETTINGS.INPUT_MESSAGE') || "to reset your password"}
                        </p> :
                        <p>{ShiftApp.translation('USER_SETTINGS.RESET_PASS_SENT') || "Check your email for password reset link"}</p>
                      }
                    </div>
                  </div>
                  <div className="row mt50">
                    {/* <div className="col-sm-6">
                     <h3 className="title-bottom-border">{ShiftApp.translation('USER_SETTINGS.LIMITS_TITLE')||"Account Limits"}</h3>
                     <LimitRow level={0} products={this.state.products}/>
                     <LimitRow level={1} products={this.state.products}/>
                     <LimitRow level={2} products={this.state.products}/>
                     </div> */}
                    <div className="col-sm-6">
                      <h3
                        className="title-bottom-border">{ShiftApp.translation('USER_SETTINGS.VERIFY_LEVEL') || "Verification Level"}</h3>
                      <p
                        className="fs13 mb30">{ShiftApp.translation('USER_SETTINGS.VERIFY_MESSAGE') || "For your security, some of our services require for you to provide basic or additional levels of verification. To verify your account or to check what your current verification level is, please click below."}</p>
                      <div className="text-center help">
                        <a className="btn btn-orange btn-small" href="#"
                           onClick={this.verficationWindow}>{accountText}</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* /.content */}
            </div>
            {/* /.col-md-9 */}
          </div>
        </div>
        {this.state.showVerifyWindow
        && <Modal close={this.closeModalVerify}><Kyc close={this.closeModalVerify}/></Modal>}

        {(this.state.twoFA.requireGoogle2FA || this.state.twoFA.passwordReset2FA || this.state.twoFA.requireAuthy2FA || this.state.twoFA.requireSMS2FA) &&
        <Modal close={this.closeModal}><TwoFACodeInput {...this.state.twoFA} submit={this.do2FAVerification}/></Modal>}
      </WidgetBase>

    );
  }
});

module.exports = ShiftSettings;
