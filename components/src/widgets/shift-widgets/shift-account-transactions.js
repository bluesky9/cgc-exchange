/* global ShiftApp, APConfig */
import React from 'react';

import WidgetBase from '../base';
import { getTimeFormatEpoch } from '../../common';
import { formatNumberToLocale } from '../helper';

class ShiftAccountTransactions extends React.Component {
  constructor() {
    super();

    this.state = {
      page: 0,
      data: [],
      products: [],
      accountsInfo: [],
      decimalPlaces: {},
      selectedAccount: null,
    };
  }

  componentDidMount() {
    this.accountChangedEvent = ShiftApp.selectedAccount
      .subscribe(selectedAccount => {
        this.setState({ selectedAccount });
      });

    this.accountTransactions = ShiftApp.accountTransactions.subscribe((data) => {
      const actions = Object.values(data)
        .map(account => account)
        .reduce((a, b) => a.concat(b), [])
        .filter((transaction) => transaction.ReferenceType === 'Deposit' || transaction.ReferenceType === 'Withdraw')
        .sort((a, b) => {
          if (a.TimeStamp < b.TimeStamp) return 1;
          if (a.TimeStamp > b.TimeStamp) return -1;
          return 0;
        });

      this.setState({ data: actions });
    });

    this.products = ShiftApp.products
      .filter((data) => data.length)
      .subscribe((products) => {

        const decimalPlaces = {};
        products.forEach(product => {
          decimalPlaces[product.Product] = product.DecimalPlaces;
        });

        this.setState({ products, decimalPlaces })
      }
    );

    this.accountsInfo = ShiftApp.userAccountsInfo.subscribe(accountsInfo => this.setState({ accountsInfo }));
  }

  componentWillUnmount() {
    this.accountChangedEvent.dispose();
    this.accountTransactions.dispose();
    this.products.dispose();
    this.accountsInfo.dispose();
  }

  gotoPage = (page) => this.setState({ page });

  refresh = () => ShiftApp.getAccountTransactions(ShiftApp.selectedAccount.value);

  render() {
    const pagination = ShiftApp.config.pagination;
    const paginationClass = ShiftApp.config.useBootstrapPagination ? 'pagination' : 'pagi';
    const maxLines = ShiftApp.config.maxLinesWidgets || 10;
    const totalPages = Math.ceil(this.state.data.length / maxLines);
    const thStyle = { textAlign: 'center', fontWeight: '500' };
    const rowsSlice = pagination ?
      this.state.data.slice(maxLines * this.state.page, maxLines * (this.state.page + 1))
      :
      this.state.data;
    const rows = rowsSlice.map((row) => {
      if(row.AccountId !== this.state.selectedAccount) return;
      const prodName = this.state.products.find((product) => product.ProductId === row.ProductId) || {};
      const accountInfo = this.state.accountsInfo.find(account => account.AccountId === row.AccountId);

      return (
        <tr style={{ textAlign: 'center' }} key={`${row.TransactionId}-${row.AccountId}-${row.ReferenceId}`}>
          <td>{row.TransactionId}</td>
          <td>{row.ReferenceType === 'Withdraw' ? ShiftApp.translation('ACCOUNT_TRANSACTIONS.WITHDRAW') || 'Withdraw' : ShiftApp.translation('ACCOUNT_TRANSACTIONS.DEPOSIT') || 'Deposit'}</td>
          <td>{prodName.Product}</td>
          <td>
            {row.CR === 0
              ?
              formatNumberToLocale(row.DR, this.state.decimalPlaces[prodName.Product])
              :
              formatNumberToLocale(row.CR, this.state.decimalPlaces[prodName.Product])}
          </td>
          {row.ReceiveTime ?
            <td>{getTimeFormatEpoch(row.ReceiveTime) || '-'}</td> :
            <td>{getTimeFormatEpoch(row.TimeStamp) || '-'}</td>}
        </tr>
      );
    });
    const emptyRows = [];
    for (let i = 0; i < maxLines - rows.length; i++) {
      emptyRows.push(<tr key={i}><td colSpan="6">&nbsp;</td></tr>);
    }

    const pages = [];
    if (pagination) {
      const start = (this.state.page - 2) > 0 ? this.state.page - 2 : 0;
      const end = (this.state.page + 3) <= totalPages ? this.state.page + 3 : totalPages;

      for (let x = start; x < end; x++) {
        const numButton = (
          <li key={x} className={this.state.page === x ? 'active' : null}>
            <a onClick={() => this.gotoPage(x)}>{x + 1}</a>
          </li>
        );
        pages.push(numButton);
      }
    }

    let displayPagination = (<ul className={paginationClass}>
        <li><a onClick={() => this.gotoPage(0)}>&laquo;</a></li>
        {pages}
        <li onClick={() => this.gotoPage(totalPages - 1)}><a>&raquo;</a></li>
      </ul>);

    const refreshIconStyle = {
      marginTop: '-4px',
      marginLeft: '10px',
      cursor: 'pointer',
    };

    return (
      <WidgetBase hideCloseLink {...this.props} headerTitle={ShiftApp.translation('ACCOUNT_TRANSACTIONS.TITLE_TEXT') || 'Deposits and Withdraws'}>
        <div>
          <table className="table table-hover">
            <thead>
              <tr>
                <th className="header" style={thStyle}>{ShiftApp.translation('ACCOUNT_TRANSACTIONS.ID_TEXT') || 'ID'}</th>
                <th className="header" style={thStyle}>{ShiftApp.translation('ACCOUNT_TRANSACTIONS.TYPE') || 'Type'}</th>
                <th className="header" style={thStyle}>{ShiftApp.translation('ACCOUNT_TRANSACTIONS.PRODUCT') || 'Product'}</th>
                <th className="header" style={thStyle}>{ShiftApp.translation('ACCOUNT_TRANSACTIONS.TOTAL') || 'Total'}</th>
                <th className="header" style={thStyle}>{ShiftApp.translation('ACCOUNT_TRANSACTIONS.TIME') || 'Time'}</th>
              </tr>
            </thead>
            <tbody>
              {rows}
              {emptyRows}
            </tbody>
          </table>

          <div className="pad">
            <div className="pull-right">
              {pagination && pages.length > 1 ? displayPagination : null}
                <i 
                  title={ShiftApp.translation('ACCOUNT_TRANSACTIONS.REFRESH') || 'Refresh'}
                  className="material-icons" 
                  onClick={this.refresh} 
                  style={refreshIconStyle}>refresh</i>
            </div>
          </div>
        </div>
      </WidgetBase>
    );
  }
}

export default ShiftAccountTransactions;
