/* global ShiftApp, localStorage, document */
import React from 'react';
import uuidV4 from 'uuid/v4';

class ShiftInstsSelectByProduct extends React.Component {
  constructor() {
    super();

    this.state = {
      instruments: [],
      instrumentTicks: {},
      selectedInstrument: null,
      productsFullName: {},
      stopReRender: false
    };
  }

  shouldComponentUpdate() {
    if (this.state.stopReRender) 
      return false;

    return true;
  }

  onMouseEnter() {
    this.setState({
      stopReRender: true
    });
  } 

  onMouseLeave() {
    this.setState({
      stopReRender: false
    });
  }

  componentDidMount() {
    this.instruments = ShiftApp.instruments
      .filter(instruments => instruments.length)
      .take(1)
      .subscribe((instruments) => {
        this.setState({ instruments }, () => this.selectInstrument(localStorage.getItem('SessionInstrumentId') || 1));
      });

    if (ShiftApp.config.instrumentSelectTicker) {
      this.tickers = ShiftApp.instruments.subscribe(productPairs => {
        productPairs.forEach(pair => ShiftApp.subscribeLvl1(pair.InstrumentId));
      });

      this.level1 = ShiftApp.Level1.subscribe(instrumentTicks => {
        this.setState({ instrumentTicks });
      });

      this.products = ShiftApp.products.filter(data => data.length).subscribe(prods => {
        const decimalPlaces = {},
          productsFullName = {};
        
        prods.forEach(product => {
          decimalPlaces[product.Product] = product.DecimalPlaces;
          productsFullName[product.Product] = {
            fullName: product.ProductFullName,
            items: []
          };
        });
        this.setState({ decimalPlaces, productsFullName });
      });
    }
    this.instrumentCheck = ShiftApp.instrumentChange.subscribe(i => this.setState({ selectedInstrument: i }));
  }

  componentWillUnmount() {
    this.instruments.dispose();
    if (ShiftApp.config.instrumentSelectTicker) {
      this.level1.dispose();
      this.tickers.dispose();
      this.products.dispose();
    }
    this.instrumentCheck.dispose();
  }

  selectInstrument = (instrumentId) => {
    const selectedInstrument = +instrumentId;
    const instrument = this.state.instruments.find((inst) => inst.InstrumentId === selectedInstrument);

    localStorage.setItem('SessionInstrumentId', selectedInstrument);
    document.APAPI.Session.SelectedInstrumentId = selectedInstrument;
    localStorage.setItem('SessionPair', instrument.Symbol);
    ShiftApp.setProductPair(instrument.Symbol);
    ShiftApp.instrumentChange.onNext(selectedInstrument);
    if (this.state.selectedInstrument) {
      this.unsubscribeInstrument(this.state.selectedInstrument);
    }
    this.subscribeInstrument(selectedInstrument);
    this.setState({ selectedInstrument });
  };

  subscribeInstrument = (InstrumentId) => {
    ShiftApp.subscribeTrades(InstrumentId, 100);
    ShiftApp.subscribeLvl2(InstrumentId);
  };

  unsubscribeInstrument = (InstrumentId) => {
    ShiftApp.unsubscribeTradesCall(InstrumentId);
    ShiftApp.unsubscribeLvl2(InstrumentId);
  };

  diffArray = (arr1, arr2) => {
    return arr2.filter(function (i) {
      return arr1.indexOf(i) === -1;
    });
  }

  render() {
    const instrument = this.state.instruments.find((inst) => inst.InstrumentId === +this.state.selectedInstrument);
    const products = JSON.parse(JSON.stringify(this.state.productsFullName)); ;
    let instrumentsList = [],
      productsValue = [],
      productsName = [];

    this.state.instruments
      .filter((inst) => inst.InstrumentId !== (instrument && instrument.InstrumentId))
      .forEach((inst) => {
        products[inst.Product1Symbol].items.push(inst);
        if (products[inst.Product2Symbol]) {
          products[inst.Product2Symbol].items.push(inst);
        }
      });

      productsName = [
        ...ShiftApp.config.selectByProductSortProduct,
        ...this.diffArray(
          ShiftApp.config.selectByProductSortProduct, 
          Object.keys(products)
        )
      ];
      
      // console.log(this.state.productsFullName);
      productsName.forEach(product => {
        if (products[product]) {
          productsValue.push(products[product]);
        }
      });
      

      instrumentsList = productsValue
        .filter((product) => product.items.length)
        .map((product) => (
          <li key={uuidV4()} className={`product-${product.fullName} dropdown-submenu`}>
            <a
              tabIndex="-1"
              className={ShiftApp.config.instrumentSelectTicker && "instrument-symbol"}
              >{product.fullName}
            </a>
            <ul className="dropdown-menu">
              {product.items.map((inst, index) => {
                return (                
                <li 
                  key={ index }
                  onClick={(e) => {
                    e.preventDefault();
                    this.selectInstrument(inst.InstrumentId);
                  }}
                  >
                  <a
                    tabIndex="-1"
                    className={ShiftApp.config.instrumentSelectTicker && "instrument-symbol"}
                    >{ShiftApp.config.reversePairs ? (inst.Product2Symbol + inst.Product1Symbol) : inst.Symbol}
                  </a>
                </li>);
              })}
          </ul>
        </li>
      ));

   
    return (
      <div 
        className="productsByInstrument dropdown instrument-dropdown"
        onMouseEnter={(e) => {
          e.preventDefault();
          this.onMouseEnter();
        }}
        onMouseLeave={(e) => {
          e.preventDefault();
          this.onMouseLeave();
        }}
        >
        <button id="instrument-select" 
                className="dropdown-toggle" 
                data-toggle="dropdown" 
                aria-haspopup="true" 
                aria-expanded="false">
          {instrument && instrument.Symbol}
          {instrumentsList.length 
            ? <span className="caret" style={{ marginLeft: '8px' }} /> 
            : null
          }
        </button>
        {instrumentsList.length 
          ? <ul className="dropdown-menu" aria-labelledby="dropdownMenu2">
              {instrumentsList}
            </ul> 
          : null
        }
      </div>
    );
  }
}

export default ShiftInstsSelectByProduct;