/* global ShiftApp */
import React from 'react';
// import QRCode from 'qrcode.react';

import WidgetBase from './base';
import InputLabeled from '../misc/inputLabeled';

class TwoFACodeInput extends React.Component {
  constructor() {
    super();

    this.state = {
      data: {},
      dataHide: {},
    };
  }

  componentDidMount() {
    this.twoFAEnable = ShiftApp.EnableGoogle2FA.subscribe(res => this.setState({ dataHide: res }));

    ShiftApp.enableGoogle2FA({});
  }

  componentWillUnmount() {
    this.twoFAEnable.dispose();
  }

  googleQRCode = () => this.setState({ data: this.state.dataHide });

  submit = () => this.props.submit(this.refs.code.value());

  render() {
    let authName = '';

    if (this.props.requireGoogle2FA) authName = ShiftApp.translation('2FA.GOOGLE_CODE') || 'Google';
    if (this.props.requireAuthy2FA) authName = ShiftApp.translation('2FA.AUTHY_CODE') || 'Authy';
    if (this.props.requireSMS2FA) authName = ShiftApp.translation('2FA.SMS_CODE') || 'SMS';

    return (
      <WidgetBase
        {...this.props}
        headerTitle={`${authName} ${ShiftApp.translation('2FA.VERIFICATION_CODE') || 'Verification Code for'} ${ShiftApp.config.siteTitle}`}
        error={this.state.data.rejectReason}
      >

        <div className="pad">
          <InputLabeled placeholder={ShiftApp.translation('2FA.VERIFICATION_CODE') || 'Verification Code'} ref="code" />
          <div className="clearfix">
            <div className="pull-right">
              {this.props.close && <button className="btn btn-action" onClick={this.props.close}>{ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}</button>}
              {' '}
              <button className="btn btn-action" onClick={this.submit}>{this.props.authProcessing ? <i className="fa fa-spinner fa-spin" /> : ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Submit'}</button>
            </div>
            {!this.props.doNotShowQRCode &&
              this.props.requireGoogle2FA && this.state.dataHide.GoogleQRCode && !this.props.useFaState &&
              (!this.state.data.GoogleQRCode ?
                <button className="btn btn-default" onClick={this.googleQRCode}>{ShiftApp.translation('2FA.SHOW_QR') || 'Show QRCode'}</button>
                :
                <div>
                  {ShiftApp.translation('2FA.SCAN_GOOGLE') || 'Scan with Google Authenticator'}
                  <br />
                  <img alt="" src={`data:image/jpg;base64,${this.state.data.GoogleQRCode}`} />
                </div>
              )

            }
          </div>
        </div>
      </WidgetBase>
    );
  }
}

TwoFACodeInput.defaultProps = {
  close: () => { },
  submit: () => { },
  requireAuthy2FA: false,
  requireGoogle2FA: false,
  requireSMS2FA: false,
  useFaState: false,
  doNotShowQRCode: false,
  authProcessing: false,
};
TwoFACodeInput.propTypes = {
  close: React.PropTypes.func,
  submit: React.PropTypes.func,
  requireAuthy2FA: React.PropTypes.bool,
  requireGoogle2FA: React.PropTypes.bool,
  requireSMS2FA: React.PropTypes.bool,
  useFaState: React.PropTypes.bool,
  doNotShowQRCode: React.PropTypes.bool,
  authProcessing: React.PropTypes.bool,
};

export default TwoFACodeInput;
