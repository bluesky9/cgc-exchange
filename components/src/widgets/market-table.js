/* global ShiftApp */
import React from 'react';
import ReactTable from 'react-table';

import TradingViewWidget from 'react-tradingview-widget';

import { coindata } from '../common/coindatas';

class MarketTable extends React.Component {
  constructor() {
    super();
    this.state = {
      marketData: [],
      btcSymbol: ['ETHBTC', 'XRPBTC', 'BCHBTC', 'EOSBTC', 'XLMBTC', 'XLMBTC', 'LTCBTC', 'USDTBTC', 'ADABTC', 'XMRBTC', 'IOTABTC'],
      ethSymbol: ['BTCETH', 'XRPETH', 'BCHETH', 'EOSETH', 'XLMETH', 'LTCETH', 'USDTETH', 'ADAETH', 'XMRETH', 'IOTAETH'],
      usdtSymbol: ['BTCUSDT', 'ETHUSDT', 'XRPUSDT', 'BCHUSDT', 'EOSUSDT', 'XLMUSDT', 'LTCUSDT', 'ADAUSDT', 'XMRUSDT', 'IOTAUSDT'],
      btcData: [],
      ethData: [],
      usdtData: [],
      tableData: [],
    };
  }

  componentDidMount() {
    // fetch(`https://api.binance.com/api/v1/ticker/24hr`, {
    //   method: 'GET',
    // }).then(response => response.json())
    //   .then((responseJson) => {
    //     this.setState({ marketData: responseJson });
    //     this.getTableData();
    //   });
    // this.getTableData();
    this.setState({ btcData: coindata.BTC });
    this.setState({ ethData: coindata.ETH });
    this.setState({ usdtData: coindata.USDT });
    this.setState({ tableData: coindata.BTC });
  }

  onPress = (type) => {
    switch (type) {
      case 'BTC':
        this.setState({ tableData: this.state.btcData });
        break;
      case 'ETH':
        this.setState({ tableData: this.state.ethData });
        break;
      case 'USDT':
        this.setState({ tableData: this.state.usdtData });
        break;
      default:
        break;
    }
  }

  getTableData = () => {
    let btcTemp = [];
    let ethTemp = [];
    let usdtTemp = [];
    this.state.btcSymbol.map(data => {
      fetch(`https://api.cryptowat.ch/markets/gdax/${data}/summary`, {
        method: 'GET',
      }).then(response => response.json())
        .then((responseJson) => {
          const d = {
            pair: data.substr(0, data.length - 3) + '/BTC',
            last_price: responseJson.result.price.last,
            change: responseJson.result.price.change.percentage,
            high: responseJson.result.price.high,
            low: responseJson.result.price.low,
            volumn: responseJson.result.volume,
          };
          btcTemp.push(d);
        });
    });
    this.state.ethSymbol.map(data => {
      fetch(`https://api.cryptowat.ch/markets/gdax/${data}/summary`, {
        method: 'GET',
      }).then(response => response.json())
        .then((responseJson) => {
          const d = {
            pair: data.substr(0, data.length - 3) + '/ETH',
            last_price: responseJson.result.price.last,
            change: responseJson.result.price.change.percentage,
            high: responseJson.result.price.high,
            low: responseJson.result.price.low,
            volumn: responseJson.result.volume,
          };
          ethTemp.push(d);
        });
    });
    this.state.usdtSymbol.map(data => {
      fetch(`https://api.cryptowat.ch/markets/gdax/${data}/summary`, {
        method: 'GET',
      }).then(response => response.json())
        .then((responseJson) => {
          const d = {
            pair: data.substr(0, data.length - 4) + '/USDT',
            last_price: responseJson.result.price.last,
            change: responseJson.result.price.change.percentage,
            high: responseJson.result.price.high,
            low: responseJson.result.price.low,
            volumn: responseJson.result.volume,
          };
          usdtTemp.push(d);
        });
    });

    //   if (data.symbol.substr(data.symbol.length - 3) === 'ETH') {
    //     const d = {
    //       pair: data.symbol.substr(0, data.symbol.length - 3) + '/ETH',
    //       last_price: data.lastPrice,
    //       change: data.priceChangePercent,
    //       high: data.highPrice,
    //       low: data.lowPrice,
    //       volumn: data.volume,
    //     };
    //     ethTemp.push(d);
    //   }
    //   if (data.symbol.substr(data.symbol.length - 4) === 'USDT') {
    //     const d = {
    //       pair: data.symbol.substr(0, data.symbol.length - 4) + '/USDT',
    //       last_price: data.lastPrice,
    //       change: data.priceChangePercent,
    //       high: data.highPrice,
    //       low: data.lowPrice,
    //       volumn: data.volume,
    //     };
    //     usdtTemp.push(d);
    //   }
    // });

    this.setState({ btcData: btcTemp });
    this.setState({ ethData: ethTemp });
    this.setState({ usdtData: usdtTemp });
    this.setState({ tableData: btcTemp });
  }

  render() {

    const columns = [
      {
        Header: 'Pair',
        accessor: 'pair',
        Cell: ({ value }) => (value.substr(value.length - 4) === 'USDT' ? <span>{value.substr(0, value.length - 4) + '/USDT'}</span> : <span>{value.substr(0, value.length - 3) + '/' + value.substr(value.length - 3)}</span>),
      }, {
        Header: 'Last Price',
        accessor: 'last_price',
        Cell: ({ value }) => (<span>{parseFloat(value).toFixed(8)}</span>),
      }, {
        Header: '24h Change',
        accessor: 'change',
        Cell: ({ value }) => (value >= 0 ? <span className="increase">{parseFloat(value).toFixed(4)}%</span> : <span className="decrease">{parseFloat(value).toFixed(4)}%</span>),
      }, {
        Header: '24h High',
        accessor: 'high',
        Cell: ({ value }) => (<span>{parseFloat(value).toFixed(4)}</span>),
      }, {
        Header: '24h Low',
        accessor: 'low',
        Cell: ({ value }) => (<span>{parseFloat(value).toFixed(4)}</span>),
      }, {
        Header: '24h Volume',
        accessor: 'volume',
        Cell: ({ value }) => (<span>{parseFloat(value).toFixed(2)}</span>),
      }, {
        expander: true,
        Header: 'Chart',
        width: 65,
        Expander: ({ isExpanded, ...rest }) => 
          <div>
            { isExpanded ? <span>&#x2299;</span> : <span>&#x2295;</span> }
          </div>,
        style: {
          cursor: 'pointer',
          fontSize: 25,
          padding: '0',
          textAlign: 'center',
          userSelect: 'none',
        },
      },
    ];
    return (
      <div className="market-view">
        <div className="search-view min-search">
          <span className="icon"><i className="fa fa-search"></i></span>
          <input type="text" placeholder="Search" name="search"></input>
        </div>
        <div className="market-view-header">
          <div className="scroll-tabs">
            <span className="market-tab active" onClick={() => this.onPress('BTC')}>BTC Market</span>
            <span className="market-tab" onClick={() => this.onPress('ETH')}>ETH Market</span>
            <span className="market-tab" onClick={() => this.onPress('USDT')}>USDT Market</span>
            <span className="market-tab" onClick={() => this.onPress('CGC')}>CGC Market</span>
          </div>
          <div className="search-view">
            <span className="icon"><i className="fa fa-search"></i></span>
            <input type="text" placeholder="Search" name="search"></input>
          </div>
        </div>
        <ReactTable
          data={this.state.tableData}
          columns={columns}
          defaultPageSize={10}
          SubComponent={row => {
            return (
              <div style={{ padding: '20px' }}>
                <TradingViewWidget symbol={row.row.pair} className="trading-view-chart" />
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default MarketTable;
