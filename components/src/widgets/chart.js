/* global ShiftApp, localStorage, ChartAPI, TradingView */
import React from 'react';
import Rx from 'rx-lite';

import WidgetBase from './base';
import moment from 'moment-timezone';

class Chart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productPairs: [],
      currentPair: null,
      currentInstrumentObject: null,
    };

    this.supportedTimeZones = [
      'UTC',
      'America/New_York',
      'America/Los_Angeles',
      'America/Chicago',
      'America/Toronto',
      'America/Vancouver',
      'America/Argentina/Buenos_Aires',
      'America/Bogota',
      'America/Sao_Paulo',
      'Europe/Moscow',
      'Europe/Athens',
      'Europe/Berlin',
      'Europe/London',
      'Europe/Madrid',
      'Europe/Paris',
      'Europe/Warsaw',
      'Australia/Sydney',
      'Asia/Bangkok',
      'Asia/Tokyo',
      'Asia/Taipei',
      'Asia/Singapore',
      'Asia/Shanghai',
      'Asia/Seoul',
      'Asia/Kolkata',
      'Asia/Hong_Kong'
    ]
  }

  componentDidMount() {
    this.wsUri = ShiftApp.config.TickerDataWS || localStorage.getItem('tradingServer') || ShiftApp.config.API_V2_URL;

    // TODO: still seems to be race condition for which is set
    this.instrumentSubscription = Rx.Observable.combineLatest(
      ShiftApp.prodPair,
      ShiftApp.instruments,
      (pair, instruments) => {
        const instrument = instruments.find(inst => inst.Symbol === pair);

        this.setState({
          productPairs: instruments,
          currentPair: pair,
        });

        return instrument;
      },
    )
      .subscribe(instrument => {
        this.setState({ currentInstrumentObject: instrument }, () => {
          if (this.state.currentInstrumentObject) {
            if (!this.chart) {
              const dataFeed = new ChartAPI.UDFCompatibleDatafeed(this.wsUri, null, null);

              this.createChart(dataFeed);
            } else {
              this.chart.setSymbol(this.state.currentPair, '15');
            }
          }
        });
      });
  }

  componentWillUnmount() {
    this.instrumentSubscription.dispose();
    // this.chart.remove();
  }

  setTimezone = () => {
    const localTimezone = moment.tz.guess();
    let closestTimezone, closestUtcOffsetDiff;

    // For each of the supported time zones...
    // if time zone's UTC offset matches user's UTC offset, returns that time zone
    // otherwise, sets to closest supported timezone.
    for (let timezone of this.supportedTimeZones) {
      let utcOffset = moment().tz(timezone).utcOffset() / 60;
      let localUtcOffset = moment().tz(localTimezone).utcOffset() / 60;

      if (utcOffset === localUtcOffset) { 
        return timezone;
      } else {
        let utcOffsetDiff = Math.abs(utcOffset - localUtcOffset);

        if (!closestTimezone || utcOffsetDiff < closestUtcOffsetDiff) {
          closestUtcOffsetDiff = utcOffsetDiff;
          closestTimezone = timezone;
        }
      }
    }

    return closestTimezone;
  }

  createChart = (dataFeed) => {
    
    if (ShiftApp.config.chartAutoTimezone) { ShiftApp.config.chart.timezone = this.setTimezone() };

    const chartOptions = {
      symbol: this.state.currentPair || '',
      datafeed: dataFeed,
      library_path: ShiftApp.config.charting_library || 'libs/charting_library_new/',
      width: '100%',
      height: '400px',
      autosize: true,
      interval: '15',
      container_id: 'ap-trading-view-chart',
      timezone: 'exchange',
      locale: ShiftApp.config.defaultLanguage,
      disabled_features: [
        'header_widget',
        'timeframes_toolbar',
        'control_bar',
        'edit_buttons_in_legend',
        'context_menus',
        'left_toolbar',
      ],
      overrides: {
        'mainSeriesProperties.style': 3,
        'mainSeriesProperties.areaStyle.linewidth': 2,
        'scalesProperties.lineColor': '#e3e3e3',
        'scalesProperties.textColor': '#414040',
        'symbolWatermarkProperties.transparency': 100,
      },
      ...ShiftApp.config.chart,
    };

    this.chart = new TradingView.widget(chartOptions); // eslint-disable-line new-cap
  }

  render() {
    return (
      <WidgetBase {...this.props} headerTitle={ShiftApp.translation('CHART.TITLE_TEXT') || 'Chart'}>
        <div id="ap-trading-view-chart" style={{ height: '400px' }}>
          {!global.jQuery &&
            <h1 style={{ textAlign: 'center', lineHeight: '400px' }}>
              {ShiftApp.translation('CHART.JQUERY_ERROR') || 'jQuery needed for this widget.'}
            </h1>
          }
        </div>
      </WidgetBase>
    );
  }
}

export default Chart;
