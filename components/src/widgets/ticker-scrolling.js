/* global ShiftApp */
import React from 'react';
import uuidV4 from 'uuid/v4';
import Rx from 'rx-lite';
import TickerBlock from '../misc/ticker/ticker-block'
import TickerBlockCCUSD from '../misc/ticker/ticker-block-cc-usd';
import TickerBlockPxChange from '../misc/ticker/ticker-block-pxchange';

class TickerScrolling extends React.Component {
    constructor() {
        super();
        this.state = {
            instruments: [],
            tickerData: [],
            tickerBlocks: {
                TickerBlock: TickerBlock,
                TickerBlockCCUSD: TickerBlockCCUSD,
                TickerBlockPxChange: TickerBlockPxChange,
            }
        };
    }

    componentDidMount() {
        this.productPairs = ShiftApp.instruments.subscribe(instruments => {
            this.setState({ instruments });
            instruments.map(ins => ins.InstrumentId).forEach(instrument => {
                ShiftApp.subscribeLvl1(instrument);
            });

            ShiftApp.Level1.subscribe(tickerData => this.setState({ tickerData: Object.values(tickerData) }));
        });
    }

    render() {
        const TickerBlock = ShiftApp.config.tickerBlock ? this.state.tickerBlocks[ShiftApp.config.tickerBlock] : this.state.tickerBlocks['TickerBlock'];
                
        return (
            <div className="ticker" id={this.state.instruments.length}>
                {this.state.instruments
                    .map(ins => {
                            return (
                                <TickerBlock
                                    {...this.props}
                                    key={uuidV4()}
                                    id={ins.InstrumentId}
                                    symbol={ins.Symbol}
                                    cryptoFontSuffix={(ins.Product1Symbol).toLowerCase()}
                                    product1Symbol={ins.Product1Symbol}
                                    product2Symbol={ins.Product2Symbol}
                                    lastTradedPx={this.state.tickerData.filter(obj => obj.InstrumentId === ins.InstrumentId).map(data => data.LastTradedPx)}
                                    sessionHigh={this.state.tickerData.filter(obj => obj.InstrumentId === ins.InstrumentId).map(data => data.SessionHigh)}
                                    rolling24HrPxChange={this.state.tickerData.filter(obj => obj.InstrumentId == ins.InstrumentId).map(data => data.Rolling24HrPxChange)}
                                />
                            );
                    })}
            </div>
        );
    };
};

TickerScrolling.defaultProps = {
    showHighSymbol: false,
};
    
TickerScrolling.propTypes = {
    showHighSymbol: React.PropTypes.bool,
};

export default TickerScrolling;