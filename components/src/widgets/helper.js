/* global atob, Blob, location, $, ShiftApp, localStorage, matchMedia */
import parseDecimalNumber from 'parse-decimal-number';
import numeral from 'numeral';

require('numeral/locales');

export const toCamelCase = str => str.replace(/-([a-z])/g, g => g[1].toUpperCase());

export const type = variable =>
  Object.prototype.toString
    .call(variable)
    .replace('[object ', '')
    .replace(']', '')
    .toLowerCase();

export function b64toBlob(b64Data, contentType = '', sliceSize = 512) {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);

    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

export function getURLParameter(name) {
  return (
    decodeURIComponent(
      (new RegExp(`[?|&]${name}=([^&;]+?)(&|#|;|$)`).exec(location.search) || [null, ''])[1],
    ) || null
  );
}

export function showGrowlerNotification(growlerType, text) {
  $.bootstrapGrowl(text, { ...ShiftApp.config.growlerDefaultOptions, type: growlerType });
}

export const formatOrders = (orders = []) =>
  orders.map(order => ({
    UpdateId: order[0],
    Account: order[1],
    TimeStamp: order[2],
    ActionType: order[3],
    LastTradePrice: order[4],
    Orders: order[5],
    Price: +order[6],
    ProductPairCode: order[7],
    Quantity: +order[8],
    Side: order[9],
  }));

export function formatNumberToLocale(value, decimalPlaces) {
  if (isNaN(value)) return '';
  const multi = 10 ** decimalPlaces;
  return (Math.floor(value * multi) / multi).toLocaleString(ShiftApp.config.locale, {
    minimumFractionDigits: decimalPlaces,
    maximumFractionDigits: decimalPlaces,
  });
}

export function parseNumberToLocale(value, negativeIsNaN = true) {
  const { delimiters } = numeral.localeData(ShiftApp.config.locale);
  if (value === '' || value === delimiters.decimal) return 0;
  const parsed = parseDecimalNumber(value, numeral.localeData(ShiftApp.config.locale).delimiters);
  return negativeIsNaN && parsed < 0 ? NaN : parsed;
}

// https://stackoverflow.com/a/27865285
export function getDecimalPrecision(a) {
  if (!isFinite(a) || isNaN(a)) return 0;
  let e = 1;
  let p = 0;
  while (Math.round(a * e) / e !== a) {
    e *= 10;
    p++;
  }
  return p;
}

export function formatDateToString(date, date_format = '') {
  const format = !date_format ? ShiftApp.config.dateFormat || 'MM/DD/YYYY' : date_format;
  const day = (`0${date.getDate()}`).slice(-2);
  const month = (`0${date.getMonth() + 1}`).slice(-2);
  const year = date.getFullYear();

  return format === 'DD/MM/YYYY' ? `${day}/${month}/${year}` : `${month}/${day}/${year}`;
}

export function truncateToDecimals(value, decimalPlaces) {
  if (isNaN(value)) return false;
  const multi = 10 ** decimalPlaces;
  return Math.floor(value * multi) / multi;
}

// Used to sort products on balances widgets
export const sortProducts = (a, b) => {
  const aPos = ShiftApp.config.sortProducts.indexOf(a.ProductSymbol);
  const bPos = ShiftApp.config.sortProducts.indexOf(b.ProductSymbol);
  if (aPos > bPos) {
    return 1;
  } else if (aPos < bPos) {
    return -1;
  }
  return 0;
};

export function getQuantityForFixedPrice(price, padding = 0, orders = [], fill = false, places = 2) {
  const paddedPrice = price + (padding * price);
  let sum = 0;
  let quantity = 0;
  let lastPrice;
  let remainingPrice = paddedPrice;
  let lastOrder = { Price: 0 };

  if (orders.length) {
    orders.some(order => {
      const orderTotal = order.Quantity * order.Price;
      sum += orderTotal;
      if (sum < paddedPrice) {
        quantity += order.Quantity;
      } else {
        const remainder = paddedPrice - (sum - orderTotal);
        quantity += remainder / order.Price;
        lastPrice = order.Price;
      }
      remainingPrice -= orderTotal;
      lastOrder = order;
      return sum >= paddedPrice;
    });
  } else {
    return { Price: price, Quantity: quantity, LimitPrice: 0 };
  }
  if (fill && remainingPrice > 0) {
    quantity += remainingPrice / lastOrder.Price;
    sum = paddedPrice;
  }
  if (sum < paddedPrice) return { Price: price, Quantity: '-' };
  const LimitPrice = parseFloat(Number(lastPrice || lastOrder.Price).toFixed(places));
  return { Price: price, Quantity: quantity, LimitPrice };
}

export function getPriceForFixedQuantity(quantity, padding = 0, orders = [], fill = false, places = 2) {
  const paddedQty = quantity + (padding * quantity);
  let price = 0;
  let remainingQty = paddedQty;
  let lastOrderPrice = 0;

  orders.some(order => {
    if (remainingQty > order.Quantity) {
      price += order.Quantity * order.Price;
    } else {
      price += remainingQty * order.Price;
    }
    remainingQty -= order.Quantity;
    lastOrderPrice = order.Price;
    return remainingQty <= 0;
  });

  if (fill && remainingQty > 0) {
    price += remainingQty * lastOrderPrice;
    remainingQty = 0;
  }
  if (remainingQty > 0) return '-';
  return { Price: price, LimitPrice: parseFloat(Number(lastOrderPrice).toFixed(places)) };
}

export function changeAccount(accountId) {
  localStorage.setItem('accountId', accountId);
  ShiftApp.selectedAccount.onNext(accountId);
}

export function path(objectPath, obj) {
  return objectPath.split('.').reduce((o, prop) => o && o[prop], obj);
}

export function isMobile() {
  return matchMedia('only screen and (max-width: 760px)').matches;
}

export function allowDeposit(symbol) {
  // Allow for exclusions
  const { excludeDeposit } = ShiftApp.config;
  return !(excludeDeposit && excludeDeposit.indexOf(symbol) !== -1);
}

export function allowWithdraw(symbol) {
  // Allow for exclusions
  const { excludeWithdraw } = ShiftApp.config;
  return !(excludeWithdraw && excludeWithdraw.indexOf(symbol) !== -1);
}

export default {
  toCamelCase,
  type,
  b64toBlob,
  getURLParameter,
  showGrowlerNotification,
  formatNumberToLocale,
  truncateToDecimals,
  sortProducts,
  getQuantityForFixedPrice,
  getPriceForFixedQuantity,
  changeAccount,
  path,
  isMobile,
  allowDeposit,
  allowWithdraw,
};
