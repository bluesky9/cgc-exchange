/* global ShiftApp, document */
import React from 'react';
import { changeAccount } from './helper';

class AccountSelect extends React.Component {
  constructor() {
    super();

    this.state = {
      selectedAccount: null,
      accounts: [],
      session: {},
    };
  }

  componentDidMount() {
    this.accounts = ShiftApp.userAccountsInfo.subscribe(accounts => this.setState({accounts}));
    this.session = ShiftApp.accountInfo.subscribe(session => this.setState({session}));
    this.selectedAccount = ShiftApp.selectedAccount.subscribe(
      (selectedAccount) => this.setState({selectedAccount}),
    );
  }

  componentWillUnmount() {
    this.selectedAccount.dispose();
    this.userAccountsInfo.dispose();
    this.session.dispose();
    this.accounts.dispose();
  }

  logout = (e) => {
    e.preventDefault();
    ShiftApp.logout();
    document.location = ShiftApp.config.logoutRedirect;
  }

  render() {
    const { accounts, session } = this.state;
    const showLogoutLink = ShiftApp.config.siteName !== 'aztec';
    const currentAccountInfo = this.state.accounts.find(account => (
      account.AccountId === this.state.selectedAccount
    )) || {};
    return (
      <div className="user-menu-container start-xs">
        <div className="dropdown">
          <button
            className="dropdown-toggle"
            type="button"
            id="dropdownMenu2"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {currentAccountInfo.AccountName}
            {showLogoutLink || accounts.length > 1 ?
              <span className="caret" style={{ marginLeft: '1rem' }} /> : null}
          </button>
          {!accounts.length && !showLogoutLink ?
            null
            :
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu2">
              {
                <li>
                  <a>
                    <i
                      className="fa fa-user-circle"/> {ShiftApp.translation('USERNAME.ACCOUNT_ID') || 'Account ID'} {this.state.selectedAccount}
                  </a>
                </li>
              }
              {
                accounts.map((account) => {
                  if (Number(account.AccountId) !== Number(this.state.selectedAccount)) {
                    return (
                      [
                        <li role="separator" className="divider"/>,
                        <li>
                          <a onClick={() => changeAccount(account.AccountId)}>
                            {ShiftApp.translation('USERNAME.ACCOUNT_ID') || 'Account ID'} {account.AccountId}
                          </a>
                        </li>,
                      ]
                    );
                  }
                })
              }
              {ShiftApp.config.siteName !== 'aztec' ?
                <li id="logoutLinkContainer">
                  <a onClick={this.logout}>{ShiftApp.translation('ACCOUNT_SELECT.SIGNOUT') || 'Sign Out'}</a>
                </li>
                :
                null}
            </ul>}
        </div>
      </div>
    );
  }
}

export default AccountSelect;
