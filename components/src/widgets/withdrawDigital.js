/* global $, ShiftApp, APConfig */
import React from 'react';
import Rx from 'rx-lite';
import uuidV4 from 'uuid/v4';
import {
  truncateToDecimals,
  formatNumberToLocale,
  parseNumberToLocale,
} from './helper';

import WidgetBase from './base';
import Modal from './modal';
import InputLabeled from '../misc/inputLabeled';
import ProcessingButton from '../misc/processingButton';
import TwoFACodeInput from './twoFACodeInput';

class WithdrawDigital extends React.Component {
  constructor() {
    super();

    this.state = {
      showTemplateTypeSelect: false,
      showTemplate: false,
      showDefaultForm: false,
      templateTypes: [],
      template: {},
      data: {},
      prodName: '',
      error: '',
      success: '',
      setAmount: null,
      setAmountString: '',
      accountId: 0,
      productId: 0,
      twoFA: {},
      twoFACancel: false,
      processing: false,
      withdraw2FA: false,
    };
  }

  componentDidMount() {
    let payload = {};
    const instrument = this.props.Product || APConfig.WithdrawCryptoProduct;

    this.productsAndAccount = Rx.Observable.combineLatest(
      ShiftApp.selectedAccount,
      ShiftApp.products,
      (accountId, products) => {
        if (accountId && products.length) {
          const data = {
            accountId,
            OMSId: ShiftApp.oms.value,
            productId: products.find((product) => product.Product === instrument).ProductId,
          };

          payload = data;
          return data;
        }
        return false;
      },
    )
      .filter(data => data)
      .take(1)
      .subscribe((data) => {
        if (data) {
          this.setState(data);
          ShiftApp.getWithdrawTemplateTypes(data);
        }
      });

    this.withdrawTemplateTypes = ShiftApp.withdrawTemplateTypes
      .filter((res) => Object.keys(res).length)
      .subscribe((res) => {
        if (res.result) {
          if (res.TemplateTypes.length > 1) {
            return this.setState({
              showTemplateTypeSelect: true,
              selectedTemplate: res.TemplateTypes[0],
              templateTypes: res.TemplateTypes,
            });
          }

          const templateData = {
            accountId: payload.accountId,
            productId: payload.productId,
            templateType: res.TemplateTypes[0],
          };

          return ShiftApp.getWithdrawTemplate(templateData);
        }

        return this.setState({ showDefaultForm: true });
      });

    this.withdrawTemplate = ShiftApp.withdrawTemplate.subscribe((res) => {
      if (res.result) {
        if (!res.Template || res.Template === '[]') {
          return this.setState({ showDefaultForm: true });
        }
        const response = JSON.parse(res.Template);

        if (response.isSuccessful) {
          return this.setState({
            template: response.Template,
            showTemplate: true,
            showTemplateTypeSelect: false,
          });
        }
        return this.setState({
          template: response,
          showTemplate: true,
          showTemplateTypeSelect: false,
        });
      }
      return false;
    });

    this.submitWithdraw = ShiftApp.submitWithdraw.subscribe((res) => {
      if (Object.keys(res).length) {
        if (res.requireGoogle2FA || res.requireAuthy2FA || res.requireSMS2FA) {
          return this.setState({ data: res, twoFA: res, processing: false });
        }

        if (res.result) {
          const successMessage = ShiftApp.translation('WITHDRAW.CRYPTO_RECEIVED') || 'Request Received';
          const successMessage2 = ShiftApp.translation('WITHDRAW.CRYPTO_CONFIRM') || 'Please Check your email';
          $.bootstrapGrowl(successMessage, {
            type: 'success',
            allow_dismiss: true,
            align: ShiftApp.config.growlwerPosition,
            delay: ShiftApp.config.growlwerDelay,
            offset: { from: 'top', amount: 30 },
            left: '60%',
          });
          if (ShiftApp.config.confirmWithEMail) {
            $.bootstrapGrowl(successMessage2, {
              type: 'info',
              allow_dismiss: true,
              align: ShiftApp.config.growlwerPosition,
              delay: ShiftApp.config.growlwerDelay,
              offset: { from: 'top', amount: 30 },
              left: '60%',
            });
          }
          this.setState({
            data: res,
            withdraw2FA: res.result,
            success: '',
            error: '',
            processing: false,
            showDefaultForm: false,
            showTemplate: false,
          }, () => {
            ShiftApp.getWithdrawTickets({
              OMSId: ShiftApp.oms.value,
              AccountId: ShiftApp.selectedAccount.value,
            });
          });
        } else {
          if (res.errormsg === 'Waiting for 2FA.') { // eslint-disable-line no-lonely-if
            this.setState({ data: res, twoFA: { withdraw2FA_Request: true }, processing: false });
          } else if (res.errormsg === 'Server Error' && !res.detail) {
            this.setState({
              data: res,
              success: '',
              error: ShiftApp.translation('WITHDRAW.INVALID_VALUES') || 'Check fields for invalid values.',
              processing: false
            });

            $.bootstrapGrowl(
              ShiftApp.translation('WITHDRAW.WITHDRAW_FAILED') || 'Withdraw Failed',
              {
                type: 'danger',
                allow_dismiss: true,
                align: ShiftApp.config.growlwerPosition,
                delay: ShiftApp.config.growlwerDelay,
                offset: { from: 'top', amount: 30 },
                left: '60%',
              });
          } else {
            this.setState({ data: res, success: '', error: `${res.errormsg}.`, processing: false });
            $.bootstrapGrowl(res.detail, {
              type: 'danger',
              allow_dismiss: true,
              align: ShiftApp.config.growlwerPosition,
              delay: ShiftApp.config.growlwerDelay,
              offset: { from: 'top', amount: 30 },
              left: '60%',
            });
          }
        }

        return ShiftApp.submitWithdraw.onNext([]);
      }
      return false;
    });

    if (ShiftApp.config.useWithdrawFees) {
      this.withdrawFee = ShiftApp.withdrawFee.subscribe(fullFee => {
        let { setAmount } = this.state;
        const { balance } = this.props;
        const fee = this.truncate(fullFee);
        if (setAmount === this.truncate(balance)) {
          setAmount -= fee;
          const setAmountString = this.format(setAmount);
          this.setState({ fee, setAmount, setAmountString });
        }
        this.setState({ fee });
      });
    }
  }

  componentWillUnmount() {
    ShiftApp.withdrawTemplateTypes.onNext([]);
    ShiftApp.withdrawTemplate.onNext([]);
    ShiftApp.submitWithdraw.onNext([]);
    this.productsAndAccount.dispose();
    this.submitWithdraw.dispose();
    this.withdrawTemplateTypes.dispose();
    this.withdrawTemplate.dispose();
    this.auth2FAuthentication && this.auth2FAuthentication.dispose(); // eslint-disable-line no-unused-expressions
    this.withdrawFee && this.withdrawFee.dispose(); // eslint-disable-line no-unused-expressions
  }

  onChangeTemplateField = (e) => {
    const template = { ...this.state.template };

    template[e.target.name] = e.target.value;
    this.setState({ template });
  };

  getWithdrawTemplate = () => {
    const payload = {
      accountId: this.state.accountId,
      productId: this.state.productId,
      templateType: this.state.selectedTemplate,
    };

    ShiftApp.getWithdrawTemplate(payload);
  };

  getProductDecimalPlaces = () => {
    const currentProduct = ShiftApp.products.value.find(item =>
      item.Product === this.props.Product) || '';
    return currentProduct ? currentProduct.DecimalPlaces : ShiftApp.config.decimalPlaces;
  }

  format = num => formatNumberToLocale(num, this.getProductDecimalPlaces());

  truncate = num => truncateToDecimals(num, this.getProductDecimalPlaces());

  withdraw = () => {
    const balance = this.props.balance;
    const { fee, setAmount, template: templateFields } = this.state;
    const payload = {
      OMSId: ShiftApp.oms.value,
      accountId: this.state.accountId,
      productId: this.state.productId,
      amount: setAmount,
      templateForm: JSON.stringify(this.state.template),
      TemplateType: this.state.template.TemplateType || 'ToExternalBitcoinAddress',
    };

    if (templateFields.ExternalAddress === '') {
      return this.setState({
        error: ShiftApp.translation('WITHDRAW.ENTER_ADDRESS') || 'Please enter the external address you\'d like to withdraw to',
        processing: false,
      });
    }

    if (setAmount === '' || setAmount <= 0) {
      return this.setState({
        error: ShiftApp.translation('WITHDRAW.VALID_AMOUNT') || 'Please enter a valid amount to withdraw',
        processing: false,
      });
    }

    this.setState({ processing: true });

    if (this.refs.address) payload.sendToAddress = this.refs.address.value();
    if (this.props.Product === 'MON') payload.PaymentId = this.refs.paymentId.value();

    if (setAmount > balance) {
      return this.setState({ error: 'Insufficient Funds', success: '', processing: false });
    }

    return ShiftApp.withdraw(payload);
  }

  do2FAVerification = (Code) => {
    const data = { Code };

    this.auth2FAuthentication = ShiftApp.auth2FA.subscribe((res) => {
      if (!res.Authenticated) {
        return this.setState({
          twoFA: {},
          success: '',
          error: ShiftApp.translation('WITHDRAW.CODE_REJECTED') || 'Code Rejected',
        });
      }
      return this.setState({
        twoFA: {},
        success: ShiftApp.translation('PROFILE.SAVED') || 'Saved!',
        error: '',
      });
    });

    return ShiftApp.authenticate2FA(data);
  }

  sendAmount = (amount) => this.setState({ setAmount: amount });

  // onKeyPress = (e) => {
  //   if (e.key === "-" || (e.key >= "a" && e.key <= "z") || (e.key >= "A" && e.key <= "Z")) {
  //     e.preventDefault();
  //   }
  // }

  changeAmount = stringValue => {
    const value = parseNumberToLocale(stringValue);

    if (!isNaN(value)) {
      if (ShiftApp.config.useWithdrawFees && value) {
        ShiftApp.getWithdrawFee({
          OMSId: ShiftApp.oms.value,
          ProductId: this.state.productId,
          AccountId: this.state.accountId,
          Amount: value,
        });
      }
      this.setState({
        setAmount: this.truncate(value),
        setAmountString: stringValue
      });
    }
    return true;
  }

  closeModal = () => this.setState({ twoFA: {}, twoFACancel: true });

  selectWithdrawTemplate = (e) => this.setState({ selectedTemplate: e.target.value });

  render() {
    const headerTitle = `${ShiftApp.translation('WITHDRAW.WITHDRAW') || 'Withdraw'} ${this.props.Product ? `(${this.props.Product})` : ''}`;
    if (this.state.showTemplateTypeSelect) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          <div className="pad">
            <h3>{ShiftApp.translation('WITHDRAW.SELECT_TEMPLATE') || 'Select a withdraw template'}</h3>
            <select className="form-control" onChange={this.selectWithdrawTemplate} value={this.state.selectedTemplate}>
              {this.state.templateTypes.map((type) => <option key={uuidV4()}>{type}</option>)}
            </select>
          </div>
          <div className="pad">
            <div className="clearfix">
              <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
                <button
                  className="btn btn-action btn-inverse"
                  onClick={this.props.close}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                </button>
                <button
                  className="btn btn-action"
                  onClick={this.getWithdrawTemplate}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_NEXT') || 'Next'}
                </button>
              </div>
            </div>
          </div>
        </WidgetBase>
      );
    }

    if (this.state.showTemplate) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          <div className="pad">
            <h3>{ShiftApp.translation('WITHDRAW.INSTRUCTION3') || 'Please read the instructions below'}</h3>
            <p>{ShiftApp.translation('WITHDRAW.CONFIRM') || 'A confirmation email will be sent.'}</p>
            <p>
              {ShiftApp.translation('WITHDRAW.YOU_HAVE') || 'You have'}:&nbsp;
              <span
                style={{ cursor: 'pointer' }}
                onClick={() => this.sendAmount(truncateToDecimals(this.props.balance, ShiftApp.config.decimalPlaces))}
              >
                <strong>
                  {formatNumberToLocale(this.props.balance, ShiftApp.config.decimalPlaces)} {this.props.Product}
                </strong>
              </span>&nbsp;
              {ShiftApp.translation('WITHDRAW.AVAILABLE_WITHDRAW') || 'available for withdraw'}.
            </p>
            <p>{ShiftApp.translation('WITHDRAW.TOTAL_AVAILABLE_NOTE') || 'Note: The total available to withdraw is the result of "Balance" minus "Fee"'}</p>
            <p>{ShiftApp.translation('WITHDRAW.INSTRUCTION6') || ''}</p>
            <p>{ShiftApp.translation('WITHDRAW.INSTRUCTION7') || ''}</p>

          </div>
          <div className="pad">
            <div style={{ display: 'inline-block', width: '83%' }}>
              <InputLabeled
                placeholder={ShiftApp.translation('WITHDRAW.AMOUNT') || 'Amount'}
                value={this.state.setAmountString}
                onChange={e => this.changeAmount(e.target.value)}
                onBlur={() => this.setState({ error: '' })}
                ref="amount"
              />
            </div>
            <button
              className="btn btn-action btn-inverse close-withdraw"
              onClick={() => this.changeAmount(this.props.balance + '')}
            >
              {ShiftApp.translation('WITHDRAW.MAX') || 'Max'}
            </button>
            {Object.keys(this.state.template)
              .filter((key) => { return key !== 'TemplateType' && key !== 'Comment' })
              .map((field, idx) => (
                <InputLabeled
                  key={idx}
                  name={field}
                  placeholder={ShiftApp.translation('WITHDRAW.' + field.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase()) || field.replace(/([a-z])([A-Z])/g, '$1 $2')}
                  onChange={this.onChangeTemplateField}
                  value={this.state.template[field]}
                  wrapperClass={'withraw-' + field.toLowerCase() + '-input'}
                  onBlur={() => this.setState({ error: '' })}
                />
              ))}
            <p>
              {ShiftApp.translation('WITHDRAW.DOUBLE_CHECK') || 'Please double check the address before starting the withdraw process.'}
            </p>
            {ShiftApp.config.useWithdrawFees && this.state.fee
              ?
              <p>{ShiftApp.translation('WITHDRAW.FEE_MESSAGE') || 'You\'ll be charged a fee of'} {this.state.fee}{this.props.Product}</p>
              : null}

            <p>
              {ShiftApp.translation('WITHDRAW.INSTRUCTION4') || `The ${this.props.Product} transaction will be processed immediately. ${this.props.Product} network confirmations may take up to 1 hour.`}
            </p>

            <div className="clearfix">
              <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
                <button
                  className="btn btn-action btn-inverse close-withdraw"
                  onClick={this.props.close}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                </button>
                <ProcessingButton
                  className="btn btn-action process-withdraw"
                  processing={this.state.processing}
                  onClick={this.withdraw}
                  disabled={this.state.processing}
                >
                  {ShiftApp.translation('WITHDRAW.PROCESS_WITHDRAW') || 'Process Withdraw'}
                </ProcessingButton>
              </div>
            </div>
          </div>
        </WidgetBase>
      );
    }

    if (this.state.showDefaultForm) {
      return (
        // wrap all content in widget base
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          <div className="pad">
            <h3>{ShiftApp.translation('WITHDRAW.INSTRUCTION3') || 'Please read the instructions below'}</h3>
            <p>{ShiftApp.translation('WITHDRAW.CONFIRM') || 'A confirmation email will be sent.'}</p>
            <p>
              {ShiftApp.translation('WITHDRAW.YOU_HAVE') || 'You have'}: &nbsp;
              <span
                style={{ cursor: 'pointer' }}
                onClick={() => this.sendAmount(formatNumberToLocale(this.props.balance, ShiftApp.config.decimalPlaces))}
              >
                <strong>
                  {formatNumberToLocale(this.props.balance, ShiftApp.config.decimalPlaces)} {this.props.Product}
                </strong>
              </span> &nbsp;
              {ShiftApp.translation('WITHDRAW.AVAILABLE_WITHDRAW') || 'available for withdraw'}.
            </p>
            <p>{ShiftApp.translation('WITHDRAW.TOTAL_AVAILABLE_NOTE') || 'Note: The total available to withdraw is the result of "Balance" minus "Fee"'}</p>
            <p>{ShiftApp.translation('WITHDRAW.INSTRUCTION6') || ''}</p>
            <p>{ShiftApp.translation('WITHDRAW.INSTRUCTION7') || ''}</p>

            {ShiftApp.config.withdrawFee &&
              ShiftApp.config.withdrawFee[this.props.fullName] &&
              <p>
                A mining fee of &nbsp;
              <strong>
                  {ShiftApp.config.withdrawFee[this.props.fullName][this.props.Product]} {this.props.Product}
                </strong>
                &nbsp; will be charged
            </p>}

            <InputLabeled
              placeholder={ShiftApp.translation('WITHDRAW.CRYPTO_ADDRESS') || 'Address'}
              ref="address"
              onBlur={() => this.setState({ error: '' })}
            />

            <InputLabeled
              placeholder={ShiftApp.translation('WITHDRAW.AMOUNT') || 'Amount'}
              value={this.state.setAmountString}
              onChange={this.changeAmount}
              onBlur={() => this.setState({ error: '' })}
              ref="amount"
              type="number"
              min="0"
            />

            {this.props.Product === 'MON' &&
              <span>
                <InputLabeled
                  placeholder={ShiftApp.translation('WITHDRAW.PAYMENT_ID') || 'Payment Id'}
                  ref="paymentId"
                />
              </span>}

            {ShiftApp.config.useWithdrawFees && this.state.fee
              ?
              <p>{ShiftApp.translation('WITHDRAW.FEE_MESSAGE') || 'You\'ll be charged a fee of'} {this.state.fee}{this.props.Product}</p>
              : null}

            <p>
              {ShiftApp.translation('WITHDRAW.INSTRUCTION4') || `The ${this.props.Product} transaction will be processed immediately. ${this.props.Product} network confirmations may take up to 1 hour.`}
            </p>

            <div className="clearfix">
              <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
                <button
                  className="btn btn-action btn-inverse"
                  onClick={this.props.close}
                >
                  {ShiftApp.translation('BUTTONS.TEXT_CANCEL') || 'Cancel'}
                </button>
                {' '}
                <ProcessingButton
                  className="btn btn-action"
                  processing={this.state.processing}
                  onClick={this.withdraw}
                  disabled={this.state.processing}
                >
                  {ShiftApp.translation('WITHDRAW.PROCESS_WITHDRAW') || 'Process Withdraw'}
                </ProcessingButton>
              </div>
            </div>
          </div>

          {(this.state.twoFA.requireGoogle2FA ||
            this.state.twoFA.withdraw2FA_Request ||
            this.state.twoFA.requireAuthy2FA ||
            this.state.twoFA.requireSMS2FA) &&
            <Modal close={this.closeModal}>
              <TwoFACodeInput {...this.state.twoFA} submit={this.do2FAVerification} />
            </Modal>}
        </WidgetBase>
      );
    }

    if (this.state.data.result) {
      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
          error={this.state.error}
          success={this.state.success}
        >
          {!this.state.twoFACancel ?
            <div className="pad">
              <h3 className="text-center">{ShiftApp.translation('WITHDRAW.CONFIRM') || 'Withdraw request sent.'}</h3>
              {ShiftApp.config.siteName === 'worldpayex.com' && <h2
                className="text-center">{ShiftApp.translation('WITHDRAW.REVIEW') || 'Your request is under review'}</h2>}
              <div className="clearfix">
                <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
                  <button className="btn btn-action"
                    onClick={this.props.close}>{ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}</button>
                </div>
              </div>
            </div>
            :
            <div className="pad">
              <h3
                className="text-center">{ShiftApp.translation('WITHDRAW.CANCEL') || 'Withdraw request cancelled.'}</h3>
              <div className="clearfix">
                <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
                  <button
                    className="btn btn-action"
                    onClick={this.props.close}
                  >
                    {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
                  </button>
                </div>
              </div>
            </div>}
        </WidgetBase>
      );
    }

    return (
      <WidgetBase
        {...this.props}
        headerTitle={headerTitle}
        error={this.state.error}
        success={this.state.success}
      >
        <div className="pad">
          <div className="clearfix">
            <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? { width: '100%' } : null}>
              <button
                className="btn btn-action"
                onClick={this.props.close}
              >
                {ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}
              </button>
            </div>
          </div>
        </div>
      </WidgetBase>
    );
  }
}

WithdrawDigital.defaultProps = {
  balance: null,
  Product: '',
  close: () => {
  },
  fullName: '',
};

WithdrawDigital.propTypes = {
  balance: React.PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.string,
  ]),
  Product: React.PropTypes.string,
  close: React.PropTypes.func,
  fullName: React.PropTypes.string,
};

export default WithdrawDigital;
