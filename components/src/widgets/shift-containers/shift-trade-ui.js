import React, { Component } from 'react';

import ShiftTickerContainer from '../shift-widgets/shift-ticker-container';
import ShiftOrderBookTradesContainer from './shift-order-book-trades-container';
import ShiftChartContainer from './shift-chart-container';
import ShiftOrdersTablesContainer from './shift-orders-tables-container';
import ShiftOrderEntryContainer from './shift-order-entry-container';
import ShiftAccountOverviewContainer from './shift-account-overview-container';
import ShiftMobileAdvancedUIContainer from './shift-mobile-advanced-ui-container';

class ShiftTradeUI extends Component {
  state = {
    currentUI: ''
  };

  checkResolution = () => {
    if (window.innerWidth < 601) {
      this.setState({ currentUI: 'mobile' });
    } else {
      this.setState({ currentUI: 'desktop' });
    }
  };

  componentDidMount() {
    this.checkResolution();
    window.addEventListener('resize', this.checkResolution);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.checkResolution);
  }

  render() {
    if (this.state.currentUI === 'desktop') {
      return (
        <div className="container ui-container desktop-ui">
          <div className="row">
            <div className="col-md-3 col-sm-6 col-xs-12">
              <ShiftTickerContainer />
              <ShiftOrderBookTradesContainer />
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12">
              <ShiftChartContainer />
              <ShiftOrdersTablesContainer />
            </div>
            <div className="col-md-3 col-sm-6 col-xs-12">
              <ShiftOrderEntryContainer />
              <ShiftAccountOverviewContainer />
            </div>
          </div>
        </div>
      );
    } else {
      return <ShiftMobileAdvancedUIContainer />;
    }
  }
}

export default ShiftTradeUI;
