import React, { Component } from 'react';

// ShiftApp Components
import Chart from '../chart';

// Shift Components
import ShiftDepthChart from '../shift-widgets/shift-depth-chart';
import ShiftWidgetWrapper from '../shift-widgets/shift-widget-wrapper';
import ShiftTradingViewChart from '../shift-widgets/shift-tradingview-chart';
import ShiftFXBlueChart from '../shift-widgets/shift-fxblue-chart';

const TRADING_VIEW = 'TradingView';
const FXBLUE = 'FXBlue';

export default class ShiftChartContainer extends Component {
  shiftChart = () => {
    const { chartOption } = ShiftApp.config.chart;

    switch (chartOption) {
      case TRADING_VIEW:
        return <ShiftTradingViewChart />;
      case FXBLUE:
        return <ShiftFXBlueChart />;
      default:
        return (
          <div>
            <Chart />
            <ShiftDepthChart />
          </div>
        );
    }
  };

  render() {
    return (
      <ShiftWidgetWrapper
        tabs={[
          ShiftApp.translation('WIDGET_WRAPPER.TAB.PRICE_CHART') ||
            'Price Chart'
        ]}
      >
        {this.shiftChart()}
      </ShiftWidgetWrapper>
    );
  }
}
