import React, { Component } from 'react';

// ShiftApp Components
import ShiftOrderEntry from '../shift-widgets/shift-order-entry';

// Shift Components
import ShiftWidgetWrapper from '../shift-widgets/shift-widget-wrapper';

export default class ShiftOrderEntryContainer extends Component {
  render() {
    return (
      <ShiftWidgetWrapper
        tabs={[
          ShiftApp.translation('WIDGET_WRAPPER.TAB.ORDER_ENTRY') || 'Order Entry'
        ]}
      >
        <ShiftOrderEntry />
      </ShiftWidgetWrapper>
    );
  }
}
