/**
 * Config variables
 * @param dropdownInstrumentSelect {Boolean} - toggles "Pairs" section or dropdown instrument select
 * @param defaultMobileAdvancedUITab {String} - determines which tab the mobile advanced UI initially opens on
 *
 */
import React, { Component } from 'react';

// AP Components
// import OrderEntry from '../order-entry';
import AccountBalances from '../accountBalances';

// Shift Components
import ShiftWidgetWrapper from '../shift-widgets/shift-widget-wrapper';
import Charts from './shift-chart-container';
import Book from './shift-order-book-trades-container';
import Orders from './shift-orders-tables-container';
import Instruments from '../shift-widgets/shift-ticker-container';
import ShiftOrderEntry from '../shift-widgets/shift-order-entry'

// Makeshift component putting OrderEntry and AccountBalances on the same tab
const Trade = () => {
	return (
		<ShiftWidgetWrapper tabs={[
			(ShiftApp.translation('WIDGET_WRAPPER.TAB.ORDER_ENTRY') || 'Order Entry'),
			(ShiftApp.translation('WIDGET_WRAPPER.TAB.BALANCES') || 'Balances')
		]}>
			<ShiftOrderEntry />
			<AccountBalances />
		</ShiftWidgetWrapper>
	)
}

export default class ShiftMobileAdvancedUIContainer extends Component {
	constructor() {
		super();

		this.state = {
			widgets: {
				'Charts': <Charts />,
				'Book': <Book />,
				'Trade': <Trade />,
				'Orders': <Orders />
			}
		}
	}

	componentDidMount() {
		this.setState({
			currentWidget: ShiftApp.config.defaultMobileAdvancedUITab || 'Trade'
		})

		if (!ShiftApp.config.dropdownInstrumentSelect) {
			this.setState({
				widgets: {
					...this.state.widgets,
					'Pairs': <Instruments />
				}
			});

			document.querySelector('.current-instrument').addEventListener('click', () => {
				this.setState({
					currentWidget: 'Pairs'
				});
			});
		}
	}

	setCurrentWidget = (widget) => { this.setState({ currentWidget: widget }) }

	renderNav = () => {
		return Object.keys(this.state.widgets).map((widget, index) => {
			return (
				<span
					key={index}
					className={this.state.currentWidget === widget ? 'active' : ''}
					onClick={() => this.setCurrentWidget(widget)}
				>
					{widget}
				</span>
			)
		})
	}

	renderCurrentWidget = () => {
		return this.state.widgets[this.state.currentWidget];
	}

	render() {
		return (
			<div className='container ui-container mobile-ui'>
				<div className='mobile-nav'>
					{this.renderNav()}
				</div>
				<div className='mobile-content'>
					{this.renderCurrentWidget()}
				</div>
			</div>
		)
	}
}
