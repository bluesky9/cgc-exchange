/**
 * Config variables
 * @param showBlockTradeUI {Boolean} - Show/hide Trade Reports section
 * 
 */
import React, { Component } from 'react';

// ShiftApp Components
import OpenOrders2 from '../openOrders-2';
import OrderHistoryCancelled from '../order-history-cancelled';
import TradeReports from '../trade-reports';
import Trades from '../trades';
import WithdrawStatus from '../withdrawStatus';

// Shift Components
import ShiftWidgetWrapper from '../shift-widgets/shift-widget-wrapper';

export default class ShiftOrdersTablesContainer extends Component {
  constructor() {
    super();
  }

  render() {
    this.tabs = [
      ShiftApp.translation('WIDGET_WRAPPER.TAB.OPEN_ORDERS') || 'Open Orders',
      ShiftApp.translation('WIDGET_WRAPPER.TAB.FILLED_ORDERS') || 'Filled Orders',
      ShiftApp.translation('WIDGET_WRAPPER.TAB.INACTIVE_ORDERS') || 'Inactive Orders',
      ShiftApp.translation('WIDGET_WRAPPER.TAB.WITHDRAW_STATUS') || 'Withdraw Status'
    ];

    if (ShiftApp.config.showBlockTradeUI) {
      this.tabs.splice(3, 0, ShiftApp.translation('WIDGET_WRAPPER.TAB.TRADE_REPORTS') || 'Trade Reports');
    }

    return (
      <ShiftWidgetWrapper
        tabs={this.tabs}
      >
        <OpenOrders2 />
        <Trades />
        <OrderHistoryCancelled />
        <WithdrawStatus />
      </ShiftWidgetWrapper>
    );
  }
}
