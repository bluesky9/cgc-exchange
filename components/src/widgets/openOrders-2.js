/* global ShiftApp */
import React from 'react';
import uuidV4 from 'uuid/v4';
import ReactTooltip from 'react-tooltip';
import { getTimeFormatEpoch } from '../common';
import { formatNumberToLocale } from './helper';

import Popup from './popup';
import WidgetBase from './base';

class OpenOrders2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      data: [],
      pairs: [],
      accounts: [],
      showLogin: false,
      currentOrder: null,
      decimalPlaces: {},
    };
  }

  componentDidMount() {
    this.OpenOrders = ShiftApp.openorders.subscribe((data) => {
      this.setState({
        data: data.sort((a, b) => {
          if (a.ReceiveTime < b.ReceiveTime) return 1;
          if (a.ReceiveTime > b.ReceiveTime) return -1;
          return 0;
        }),
      });
    });
    this.pairs = ShiftApp.instruments.subscribe((pairs) => this.setState({ pairs }));
    this.userAccounts = ShiftApp.userAccountsInfo.subscribe((accounts) => this.setState({ accounts }));

    this.products = ShiftApp.products.filter(data => data.length).subscribe(products => {
      const decimalPlaces = {};
      products.forEach(product => {
        decimalPlaces[product.Product] = product.DecimalPlaces;
      });
      this.setState({ decimalPlaces });
    });
  }

  componentWillUnmount() {
    this.OpenOrders.dispose();
    this.pairs.dispose();
  }

  gotoPage = (page) => this.setState({ page });

  loginToggle = () => this.setState({ showLogin: !this.state.showLogin });

  sort = (a, b) => {
    if (a.OrderId < b.OrderId) { return 1; }
    if (a.OrderId > b.OrderId) { return -1; }
    return 0;
  };

  mapOrderTypeToPriceDisplay = (order, product2Symbol) => {
    const priceDisplay = {
      StopMarket: formatNumberToLocale(order.Price, this.state.decimalPlaces[product2Symbol]),
      TrailingStopLimit: 'TRL',
      TrailingStopMarket: 'TRL',
      Limit: formatNumberToLocale(order.Price, this.state.decimalPlaces[product2Symbol]),
      StopLimit: formatNumberToLocale(order.Price, this.state.decimalPlaces[product2Symbol]),
      Market: 'MKT'
    };

    return priceDisplay[order.OrderType];
  }

  confirmCancel = order => {
    this.popup.create({
      message: `Are you sure you want to cancel order ${order.OrderId}?`,
      actions: [
        {
          text: 'Yes',
          className: 'btn btn-action',
          onClick: () => {
            ShiftApp.cancelOrder({
              OMSId: ShiftApp.oms.value,
              OrderId: order.OrderId,
              AccountId: order.Account,
            });
            this.popup.close();
          },
        },
        {
          text: 'No',
          className: 'btn btn-action',
          onClick: () => this.popup.close(),
        },
      ],
    });
  }

  cancel = order => ShiftApp.cancelOrder({
    OMSId: ShiftApp.oms.value,
    OrderId: order.OrderId,
    AccountId: order.Account,
  });

  render() {
    const pagination = ShiftApp.config.pagination;
    const maxLines = ShiftApp.config.maxLinesWidgets || 15;
    const totalPages = pagination ? Math.ceil(this.state.data.length / maxLines) : 0;
    const rowsSlice = pagination ?
      this.state.data.slice(maxLines * this.state.page, maxLines * (this.state.page + 1))
      :
      this.state.data;
    const rows = rowsSlice
      .sort(this.sort)
      .map((order) => {
        const pairName = this.state.pairs.find((inst) => inst.InstrumentId === order.Instrument);
        const accountInfo = this.state.accounts.find((account) => account.AccountId === order.Account);

        return (
          <tr
            key={uuidV4()}
            data-tip
            data-for={`orderInfo-${order.OrderId}`}
            onMouseEnter={() => {
              // Prevent setTimeout firing multiple times (each pixel the mouse moves) thus allowing other events to come through -> firefox bug #468
              if(this.state.currentOrder === order) return;
              setTimeout(() => this.setState({ currentOrder: order }), 100);
            }}
            onMouseLeave={() => this.setState({ currentOrder: null })}
          >
            {!ShiftApp.config.hideInBuyCustomWidget && <td className="borderless">{accountInfo.AccountName || order.Account || null}</td>}
            <td className="borderless">{pairName.Symbol}</td>
            <td className={`borderless ${order.Side === 'Buy' ? 'buyFont' : 'sellFont'}`}>
              {order.Side === 'Buy' ?
                ShiftApp.translation('OPEN_ORDERS.BUY') || 'Buy'
                :
                ShiftApp.translation('OPEN_ORDERS.SELL') || 'Sell'}
            </td>
            {!ShiftApp.config.hideInBuyCustomWidget && <td className="borderless">{order.IsQuote ? 'Quote' : order.OrderType}</td>}
            <td className="borderless txt-right">≈{formatNumberToLocale(order.Quantity, this.state.decimalPlaces[pairName.Product1Symbol])}</td>
            <td className="borderless txt-right">{this.mapOrderTypeToPriceDisplay(order, pairName.Product2Symbol)}</td>
            <td className="borderless">0.00</td>
            <td className="borderless">{getTimeFormatEpoch(order.ReceiveTime)}</td>
            {!ShiftApp.config.hideInBuyCustomWidget &&  <td className="borderless">{order.OrderState === 'Working' ? 'Open' : order.OrderState}</td>}
            <td className="borderless">
              <div
                className="ordersActions"
                onClick={() => {
                  if (ShiftApp.config.confirmOrderCancellation) return this.confirmCancel(order);
                  return this.cancel(order);
                }}
              >
                {ShiftApp.config.siteName === 'aztec' ?
                  <span><i className="fa fa-ban" style={{ color: '#d60101' }} />Cancel</span> :
                  <span><i className="material-icons">cancel</i>Cancel</span>}
              </div>
            </td>
          </tr>
        );
      });

    const emptyRows = [];
    for (let i = 0; i < maxLines - rows.length; i++) {
      emptyRows.push(
        <tr key={i} style={ {background: 'transparent'} }>
          <td className="borderless" colSpan="10">&nbsp;</td>
        </tr>,
      );
    }

    const pages = [];
    if (pagination) {
      const start = (this.state.page - 2) > 0 ? this.state.page - 2 : 0;
      const end = (this.state.page + 3) <= totalPages ? this.state.page + 3 : totalPages;

      for (let x = start; x < end; x++) {
        const numButton = (
          <li key={x} className={this.state.page === x ? 'active' : null}>
            <a onClick={() => this.gotoPage(x)}>{x + 1}</a>
          </li>
        );
        pages.push(numButton);
      }
    }

    const tableClassList = ShiftApp.config.siteName === 'aztec' ?
      'table table--comfy table--hover table--striped table--light'
      :
      'table table-hover minFont';

    return (
      <WidgetBase login {...this.props} headerTitle={ShiftApp.translation('OPEN_ORDERS.TITLE_TEXT') || 'Open Orders'}>
        <div>
          <table className={tableClassList} id="OrdersTable">
            <thead>
              <tr>
                {!ShiftApp.config.hideInBuyCustomWidget && <th className="header nudge" style={{ width: '6rem' }}>{ShiftApp.translation('OPEN_ORDERS.ACCOUNT') || 'Account'}</th>}
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.PAIR_TEXT') || 'Instrument'}</th>
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.SIDE_TEXT') || 'Side'}</th>
                {!ShiftApp.config.hideInBuyCustomWidget && <th className="header">{ShiftApp.translation('OPEN_ORDERS.TYPE_TEXT') || 'Type'}</th>}
                <th className="header">{ShiftApp.translation('TRADES.QUANTITY_TEXT') || 'Quantity'}</th>
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.PRICE_TEXT') || 'Price'}</th>
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.FEE_TEXT') || 'Fee'}</th>
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.TIME_TEXT') || 'Time'}</th>
                {!ShiftApp.config.hideInBuyCustomWidget && <th className="header">{ShiftApp.translation('OPEN_ORDERS.STATUS_TEXT') || 'Status'}</th>}
                <th className="header">{ShiftApp.translation('OPEN_ORDERS.ACTIONS_TEXT') || 'Actions'}</th>
              </tr>
            </thead>
            <tbody>
              {rows}
              {emptyRows}
            </tbody>
          </table>

          {this.state.currentOrder &&
            <ReactTooltip id={`orderInfo-${this.state.currentOrder.OrderId}`} class="order-tooltip" delayShow={500}>
              <div>
                <h4>Additional Order Information</h4>
                <table>
                  <tbody>
                    <tr>
                      <td>Order Id</td>
                      <td>{this.state.currentOrder.OrderId}</td>
                    </tr>
                    <tr>
                      <td>Original Quantity</td>
                      <td>{this.state.currentOrder.OrigQuantity}</td>
                    </tr>
                    <tr>
                      <td>Display Quantity</td>
                      <td>{this.state.currentOrder.DisplayQuantity}</td>
                    </tr>
                    <tr>
                      <td>Quantity Executed</td>
                      <td>{this.state.currentOrder.QuantityExecuted}</td>
                    </tr>
                    <tr>
                      <td>Remaining Quantity</td>
                      <td>{this.state.currentOrder.Quantity}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </ReactTooltip>}

          {pagination && pages.length > 1 &&
            <div className="clearfix pad-x">
              <ul className="pagi pull-right">
                <li><a onClick={() => this.gotoPage(0)}>&laquo;</a></li>
                {pages}
                <li onClick={() => this.gotoPage(totalPages - 1)} ><a>&raquo;</a></li>
              </ul>
            </div>}
        </div>

        {ShiftApp.config.confirmOrderCancellation && <Popup ref={popup => { this.popup = popup; }} />}
      </WidgetBase>
    );
  }
}

OpenOrders2.defaultProps = {
  hideCloseLink: true,
};

export default OpenOrders2;
