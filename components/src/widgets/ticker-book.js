/* global ShiftApp */
import React from 'react';
import WidgetBase from './base';

class TickerBook extends React.Component {
  constructor() {
    super();

    this.state = {
      data: {},
      pairs: [],
      productPairs: [],
    };
  };

  componentDidMount() {
    this.productPairs = ShiftApp.instruments.subscribe(pairs => this.setState({ pairs }));
    this.bookTickers = ShiftApp.instruments.subscribe(productPairs => {
        productPairs.forEach(pair => ShiftApp.subscribeLvl1(pair.InstrumentId));
    });

    this.level1 = ShiftApp.Level1.subscribe(data => {
        this.setState({ data });
    });
    
  };

  componentWillUnmount() {
    this.productPairs.dispose();
    this.bookTickers.dispose();
  };

  render() {
    const { data } = this.state;
    
    const tickerBook = Object.keys(data).map(key=>{
      const pairName = this.state.pairs.find(inst => inst.InstrumentId === data[key].InstrumentId).Symbol || '';
      return(<tr>
        <th>{pairName}</th>
        <th>{data[key].LastTradedPx.toFixed(ShiftApp.config.advancedUITickerDecimalPlaces || 2)}</th>
        <th>{data[key].Rolling24HrPxChange.toFixed(2)}%</th>
        <th>{data[key].BestBid.toFixed(ShiftApp.config.advancedUITickerDecimalPlaces || 2)}</th>
        <th>{data[key].BestOffer.toFixed(ShiftApp.config.advancedUITickerDecimalPlaces || 2)}</th>
        <th>{data[key].Rolling24HrVolume.toFixed(2)}</th>
        <th>{data[key].SessionLow.toFixed(ShiftApp.config.advancedUITickerDecimalPlaces || 2)}</th>
        <th>{data[key].SessionHigh.toFixed(ShiftApp.config.advancedUITickerDecimalPlaces || 2)}</th>
      </tr>)
    })

    return (
      <WidgetBase
        {...this.props}
        headerTitle={ShiftApp.translation('HEADER_TICKER.TITLE_TEXT') || 'Tickers'}
      >
        <table className="ticker-wrapper">
          <thead>
            <tr>
              <th className="last-price">
                {ShiftApp.translation('PRODUCTS.TITLE_TEXT') || 'Products'}
              </th>
              <th className="last-price">
                {ShiftApp.translation('TICKERS.LAST_PRICE') || 'Last Price'}
              </th>
              <th className="last-price">
                {ShiftApp.translation('TICKERS.T24_HOUR_CHANGE') || '24 Hour Change'}
              </th>
              <th className="last-price">
                {ShiftApp.translation('TICKERS.BID') || 'Bid'}
              </th>
              <th className="last-price">
                {ShiftApp.translation('TICKERS.ASK') || 'Ask'}
              </th>
              <th className="day-stat">
                {ShiftApp.translation('TICKERS.T24_HOUR_VOLUME') || '24 Hour Volume'}
              </th>
              <th className="day-stat">
                {ShiftApp.translation('TICKERS.T24_HOUR_LOW') || '24 Hour Low'}
              </th>
              <th className="day-stat">
                {ShiftApp.translation('TICKERS.T24_HOUR_HIGH') || '24 Hour High'}
              </th>
            </tr>
            { tickerBook }
          </thead>
        </table>
      </WidgetBase>
    );
  }
}

export default TickerBook;
