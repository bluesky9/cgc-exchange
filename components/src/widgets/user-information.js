/* global ShiftApp, $, document, localStorage, location */
import React from 'react';
import WidgetBase from './base';
import Modal from './modal';
import InputLabeled from '../misc/inputLabeled';
import SelectLabeled from '../misc/selectLabeled';
import CountryCodePicker from '../misc/countryCodePicker';
import TwoFACodeInput from './twoFACodeInput';
import { formatNumberToLocale } from './helper';

class UserInformation extends React.Component {
  constructor() {
    super();

    this.state = {
      data: {},
      userName: '',
      error: '',
      success: '',
      twoFA: {},
      passwordReset: false,
      userConfig: {},
      waiting2FA: false,
      level: 0,
      loyaltyOMSProducts: [],
      loyaltyOMSEnabled: false,
      loyaltyProductId: 0,
    };
  }

  componentDidMount() {
    this.userInformation = ShiftApp.getUser.subscribe(data => this.setState({
      data: {
        UserId: data.UserId,
        UserName: data.UserName,
        Password: data.Password,
        Email: data.Email,
        EmailVerified: data.EmailVerified,
        AccountId: data.AccountId,
        Use2FA: data.Use2FA,
      },
      Use2FA: data.Use2FA,
    }));

    this.userConfiguration = ShiftApp.getUserConfig.subscribe(data => {
      const configs = ((Array.isArray(data) && data) || []).reduce((item, i) => {
        item[i.Key] = i.Value;
        return item;
      }, {});

      this.setState({ userConfig: configs });
    });

    this.accountInfo = ShiftApp.accountInfo.subscribe(data => this.setState({
      level: data.VerificationLevel,
      loyaltyProductId: data.LoyaltyEnabled ? data.LoyaltyProductId : 0,
    }));

    if (ShiftApp.config.loyaltyToken) {
      this.loyaltyFeeConfigs = ShiftApp.loyaltyFeeConfigs.subscribe(result => {
        const loyaltyOMSProducts = [];
        const loyaltyOMSEnabled = true;
        result.forEach(token => {
          if (token.IsEnabled) {
            const product = ShiftApp.accountPositions.value.find(x => x.ProductId === token.LoyaltyProductId);
            const discount = token.LoyaltyDiscount * 100;
            const decimals = (Math.floor(discount) !== discount) ? discount.toString().split('.')[1].length || 0 : 0;
            product.LoyaltyDiscount = formatNumberToLocale(discount, decimals);
            loyaltyOMSProducts.push(product);
          }
        });
        this.setState({ loyaltyOMSEnabled, loyaltyOMSProducts });
      });
    }

    this.disable2FA = ShiftApp.Disable2FA.subscribe(response => {
      // then call authenticate 2FA
      if (response.result) {
        this.setState({
          twoFA: { requireGoogle2FA: true },
        });
      }
    });

    this.showError = () => {
      $.bootstrapGrowl(
        ShiftApp.translation('PROFILE.USER_INFO_NOT_SAVED') || 'User information not saved',
        {
          type: 'danger',
          allow_dismiss: true,
          align: ShiftApp.config.growlwerPosition,
          delay: ShiftApp.config.growlwerDelay,
          offset: { from: 'top', amount: 30 },
          left: '60%',
        });
    };

    this.setUserInformation = ShiftApp.setUser
      .filter(data => Object.keys(data).length)
      .subscribe(res => {
        if (res.length && !res.UserId) {
          this.showError();
          return this.setState({ success: '', error: 'Error' });
        }

        if (res.Use2FA === this.state.Use2FA) {
          if (res.UserId) {
            this.setState({
              success: ShiftApp.translation('PROFILE.SAVED') || 'Saved!',
              error: '',
            });

            $.bootstrapGrowl(
              ShiftApp.translation('PROFILE.USER_INFO_SAVED') || 'User information saved',
              {
              type: 'success',
              allow_dismiss: true,
              align: ShiftApp.config.growlwerPosition,
              delay: ShiftApp.config.growlwerDelay,
              offset: { from: 'top', amount: 30 },
              left: '60%',
            });
            setTimeout(() => location.reload(), 300);
          }
        }

        // check if we need to activate any 2FAs
        // if (!res.Use2FA && this.state.data.UseGoogle2FA) {
        //   this.setState({ twoFA: { requireGoogle2FA: true } });
        // }

        // call disable2FA
        // if (res.Use2FA && !this.state.data.UseGoogle2FA) {
        //   this.disable2FA = ShiftApp.Disable2FA.subscribe(response => {
        //     // then call authenticate 2FA
        //     if (response.result) this.setState({ twoFA: { requireGoogle2FA: true } });
        //   });
        //   ShiftApp.disable2FA({});
        // }
        return true;
      });

    this.auth2FAuthentication = ShiftApp.auth2FA.subscribe(res => {
      if (!res.Authenticated) {
        return this.setState({
          twoFA: {},
          success: '',
          error: ShiftApp.translation('PROFILE.CODE_REJECTED') || 'Code Rejected',
          Use2FA: this.state.data.Use2FA,
        });
      }

      if (res.Authenticated && this.state.waiting2FA) {
        return this.setState({
          waiting2FA: false,
          passwordReset: true,
        });
      }

      if (res.Authenticated) {
        return this.setState({
          twoFA: {},
          data: { ...this.state.data, Use2FA: this.state.Use2FA },
          success: ShiftApp.translation('PROFILE.SAVED') || 'Saved!',
          error: '',
        });
      }
      return false;
    });

    ShiftApp.getUserCon({ UserId: +localStorage.UserId || ShiftApp.getUser.value.UserId });
    ShiftApp.getUserInfo({ UserId: +localStorage.UserId || ShiftApp.getUser.value.UserId });

    this.setState({
      username: document.APAPI.Session.UserObj.UserName,
      email: document.APAPI.Session.UserObj.Email,
    }, () => this.userInformation.dispose());
  }

  componentWillUnmount() {
    this.userInformation.dispose();
    this.userConfiguration.dispose();
    this.accountInfo.dispose();
    this.setUserInformation.dispose();
    this.auth2FAuthentication.dispose();
    this.disable2FA.dispose();
    if (this.resetUserPassword) {
      this.resetUserPassword.dispose();
    }
    if (ShiftApp.config.loyaltyToken) {
      this.loyaltyFeeConfigs.dispose();
    }
  }

  changed = e => {
    const data = this.state.data;

    Object.keys(this.refs).forEach(key => {
      if (this.refs[key].type === 'checkbox' || this.refs[key].type === 'radio') {
        data[key] = this.refs[key].checked;
      } else {
        data[key] = this.refs[key].value();
      }
    });

    data.Use2FA = data.UseGoogle2FA;
    this.setState({ data, success: '', error: '' });

    if (e.target.name === 'language') {
      const { userConfig } = this.state;
      localStorage.lang = e.target.value;
      userConfig.language = e.target.value;
      this.setState({ userConfig });
    }
  };

  change2FASettings = e => {
    const { value } = e.target;

    if (value === 'UseGoogle2FA') {
      return this.setState({
        Use2FA: true,
        twoFA: { requireGoogle2FA: true },
      });
    }
    ShiftApp.disable2FA({});
    return this.setState({ Use2FA: false });
  };

  do2FAVerification = code => {
    const data = { Code: code };

    ShiftApp.authenticate2FA(data);
  };

  submit = () => {
    // This is what the backend expects
    const data = {
      UserId: this.state.data.UserId,
      UserName: ShiftApp.config.useEmailAsUsername ? this.state.data.Email : this.state.data.UserName,
      Email: this.state.data.Email,
      EmailVerified: this.state.data.EmailVerified,
      AccountId: this.state.data.AccountId,
      // Use2FA: this.state.Use2FA,
    };

    ShiftApp.setUserInfo(data);

    // Name sets the userconfig but it sets the key to "null" instead of "language"
    // TODO: Fix with David Hyatt
    const payload = {
      UserId: this.state.data.UserId,
      Config: [{
        Key: 'language',
        Value: this.state.userConfig.language,
      }],
    };
    ShiftApp.setUserCon(payload);
  };

  closeModal = () => this.setState({
    twoFA: {},
    waiting2FA: false,
    Use2FA: this.state.data.Use2FA,
  });

  resetPassword = () => {
    this.resetUserPassword = ShiftApp.resetPass.subscribe(res => {
      // console.log('RESET RESPONSE', res);
      if (res.result) {
        this.setState({ passwordReset: res.result });
      }
      if (!res.result && res.errormsg === 'Waiting for 2FA.') {
        this.setState({
          twoFA: { passwordReset2FA: true },
          waiting2FA: true,
        });
      }
    });

    ShiftApp.resetPassword({ UserName: this.state.username });
    setTimeout(() => {
      ShiftApp.logout();
      document.location = ShiftApp.config.logoutRedirect;
    }, 5000);
  };

  changeLoyalty = e => {
    const loyaltyIndex = e.target.value;
    const payload = {
      ...ShiftApp.accountInfo.value,
      LoyaltyProductId: 0,
      LoyaltyEnabled: false,
    };
    if (loyaltyIndex > -1) {
      const { loyaltyOMSProducts } = this.state;
      const loyaltyProduct = loyaltyOMSProducts[loyaltyIndex];
      const { Amount, ProductId } = loyaltyProduct;
      payload.LoyaltyProductId = ProductId;

      // Never enable the loyalty token functionality if the balance is 0.
      if (Amount > 0) {
        payload.LoyaltyEnabled = true;
      }
    }
    return document.APAPI.RPCCall('UpdateAccount', payload, (result) => {
      if (result) {
        this.setState({
          loyaltyProductId: payload.LoyaltyEnabled ? payload.LoyaltyProductId : 0,
        });
      } else this.showError();
    });
  }

  loyaltyDisabled = index => !this.state.loyaltyOMSEnabled || this.state.loyaltyOMSProducts[index].Amount <= 0;

  render() {
    const languageList = ((ShiftApp.config.languages && ShiftApp.config.languages.items) || [])
      .map(item => (<option value={item.value} key={item.name}>{item.name}</option>));

    return (
      <WidgetBase
        {...this.props}
        login
        headerTitle={ShiftApp.translation('PROFILE.TITLE_TEXT') || 'Information'}
        error={this.state.error}
        success={this.state.success}
      >
        <div className="pad-y pad">
          {ShiftApp.config.siteName !== 'yap.cx' && ShiftApp.config.useEmailAsUsername &&
            <div className="row">
              <InputLabeled
                ref="UserName"
                type="hidden"
                value={this.state.data.Email}
                onChange={this.changed}
                wrapperClass="col-xs-12"
                disabled={ShiftApp.config.siteName === 'aztec' || this.state.level > 0}
              />
            </div>}
          {ShiftApp.config.siteName !== 'yap.cx' && !ShiftApp.config.useEmailAsUsername &&
            <div className="row">
              <InputLabeled
                ref="UserName"
                placeholder={ShiftApp.translation('PROFILE.USERNAME') || 'User Name'}
                value={this.state.data.UserName}
                onChange={this.changed}
                wrapperClass="col-xs-12"
                disabled={ShiftApp.config.siteName === 'aztec' || this.state.level > 0}
              />
            </div>}

          {ShiftApp.config.siteName !== 'yap.cx' && ShiftApp.config.siteName !== 'aztec' &&
            <div className="row">
              <InputLabeled
                ref="Email"
                placeholder={ShiftApp.translation('PROFILE.EMAIL') || 'Email'}
                value={this.state.data.Email}
                onChange={this.changed}
                disabled={ShiftApp.config.disableEditEmail || this.state.level > 0}
                wrapperClass="col-xs-12"
              />
            </div>}

          {ShiftApp.config.siteName !== 'yap.cx' && ShiftApp.config.siteName !== 'aztec' && ShiftApp.config.showCCode &&
            <div className="row">
              <CountryCodePicker
                ref="Cell2FACountryCode"
                placeholder={ShiftApp.translation('PROFILE.PHONE_CODE') || 'Country Code'}
                value={this.state.data.Cell2FACountryCode}
                onChange={this.changed}
                wrapperClass="col-xs-6"
              />
              <InputLabeled
                ref="Cell2FAValue"
                placeholder={ShiftApp.translation('PROFILE.PHONE') || 'Cell Number'}
                value={this.state.data.Cell2FAValue}
                onChange={this.changed}
                wrapperClass="col-xs-6"
              />
            </div>}
          {ShiftApp.config.disableLangUserInformation ? null :
            ShiftApp.config.siteName !== 'yap.cx' && ShiftApp.config.siteName !== 'aztec' &&
            <div className="row">
              <SelectLabeled
                name="language"
                ref="Language"
                placeholder={ShiftApp.translation('PROFILE.LANGUAGE') || 'Language'}
                onChange={this.changed}
                value={this.state.userConfig.language}
                wrapperClass="col-xs-12"
              >
                {languageList}
              </SelectLabeled>
            </div>
          }
          {ShiftApp.config.siteName !== 'aztec' &&
            <div className="row">
              <div className="col-xs-12 form-group">
                <label htmlFor="resetPass">{ShiftApp.translation('PASSWORD_MODAL.TITLE_TEXT') || 'Reset Password'}</label>
                <div id="resetPass">
                  {!this.state.passwordReset ?
                    <a onClick={this.resetPassword} style={{ textDecoration: 'underline' }}>
                      {ShiftApp.translation('PROFILE.RESET_PASSWORD') || 'Click here to reset your password'}
                    </a>
                    :
                    <span>{ShiftApp.translation('PROFILE.RESET_PASS_SENT') || 'Check your email for password reset link'}</span>}
                </div>
              </div>
            </div>}

          {ShiftApp.config.siteName !== 'aztec' &&
            <div className="row">
              <div className="col-xs-12">
                <p>{ShiftApp.translation('2FA.TITLE_TEXT') || 'For added security, enable one of the following'}:</p>
                {ShiftApp.config.authy2FA &&
                  <div className="radio">
                    <label htmlFor="auth">
                      <input
                        type="radio"
                        name="auth"
                        id="auth"
                        ref="UseAuthy2FA"
                        disabled={
                          ShiftApp.userInformation.value.UseGoogle2FA
                          || ShiftApp.userInformation.value.UseSMS2FA
                        }
                        checked={this.state.data.UseAuthy2FA}
                        onChange={this.changed}
                      />
                      {ShiftApp.translation('PROFILE.AUTHY') || 'UseAuthy2FA'}
                    </label>
                  </div>}

                {ShiftApp.config.authSMS &&
                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        name="auth"
                        ref="UseSMS2FA"
                        disabled={
                          ShiftApp.userInformation.value.UseAuthy2FA ||
                          ShiftApp.userInformation.value.UseGoogle2FA
                        }
                        checked={this.state.data.UseSMS2FA}
                        onChange={this.changed}
                      />
                      {ShiftApp.translation('PROFILE.SMS') || 'UseSMS2FA'}
                    </label>
                  </div>}

                {ShiftApp.config.authGoogle &&
                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        name="auth"
                        ref="UseGoogle2FA"
                        value="UseGoogle2FA"
                        checked={this.state.Use2FA}
                        onChange={this.change2FASettings}
                      />
                      {ShiftApp.translation('PROFILE.GOOGLE') || 'UseGoogle2FA'}
                    </label>
                  </div>}

                <div className="radio">
                  <label>
                    <input
                      type="radio"
                      name="auth"
                      ref="UseNoAuth"
                      value="UseNoAuth"
                      checked={!this.state.Use2FA}
                      onChange={this.change2FASettings}
                    />
                    {ShiftApp.translation('PROFILE.NOAUTH') || 'None'}
                  </label>
                </div>
              </div>
            </div>}
          {this.state.loyaltyOMSEnabled &&
            <div className="row loyalty">
              <div className="col-xs-12">
                <p>{ShiftApp.translation('PROFILE.FEES') || 'Fees'}</p>
                { this.state.loyaltyOMSProducts.map((product, index) => {
                  const id = productId => `loyaltyToken${productId}`;
                  return (
                    <div className="radio">
                      <label htmlFor={id(product.ProductId)}>
                        <input
                          type="radio"
                          name="loyaltyToken"
                          id={id(product.ProductId)}
                          value={index}
                          checked={this.state.loyaltyProductId === product.ProductId}
                          onChange={this.changeLoyalty}
                          disabled={this.loyaltyDisabled(index)}
                        />
                        { this.loyaltyDisabled(index) ?
                          ShiftApp.translation('PROFILE.LOYALTY_FEES_DISABLED', { TOKEN: product.ProductSymbol }) ||
                          `You must have a balance to use ${product.ProductSymbol} to pay for fees` :
                          ShiftApp.translation('PROFILE.LOYALTY_FEES', { TOKEN: product.ProductSymbol }) ||
                          `Use ${product.ProductSymbol} to pay for fees`
                        }
                        <span>&nbsp;
                          {
                            ShiftApp.translation('PROFILE.LOYALTY_DISCOUNT', { DISCOUNT: product.LoyaltyDiscount }) ||
                            `(${product.LoyaltyDiscount}% discount)`
                          }
                        </span>
                      </label>
                    </div>
                  );
                })}
                <div className="radio">
                  <label htmlFor="loyaltyToken0">
                    <input
                      type="radio"
                      name="loyaltyToken"
                      id="loyaltyToken0"
                      value={-1}
                      checked={this.state.loyaltyProductId === 0}
                      onChange={this.changeLoyalty}
                    />
                    { ShiftApp.translation('PROFILE.LOYALTY_DEFAULT') || 'Use default fees' }
                  </label>
                </div>
              </div>
            </div>
          }

          <div className={ShiftApp.config.siteName === 'aztec' ? 'clearfix pad' : 'clearfix pad-x pad col-xs-12'}>
            <div className="pull-right">
              <button
                type="submit"
                className="btn btn-action"
                onClick={this.submit}
              >
                {ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Submit'}
              </button>
            </div>
          </div>
        </div>

        {(this.state.twoFA.requireGoogle2FA ||
          this.state.twoFA.passwordReset2FA ||
          this.state.twoFA.requireAuthy2FA ||
          this.state.twoFA.requireSMS2FA) &&
          <Modal close={this.closeModal}>
            <TwoFACodeInput
              {...this.state.twoFA}
              doNotShowQRCode={this.state.data.Use2FA}
              submit={this.do2FAVerification}
            />
          </Modal>}
      </WidgetBase>
    );
  }
}

export default UserInformation;
