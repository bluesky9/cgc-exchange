/* global ShiftApp, localStorage, document */
import React from 'react';
import uuidV4 from 'uuid/v4';
import {
  formatNumberToLocale
} from './helper';

class InstrumentSelect extends React.Component {
  constructor() {
    super();

    this.state = {
      instruments: [],
      instrumentTicks: {},
      selectedInstrument: null,
    };
  }

  componentDidMount() {
    this.instruments = ShiftApp.instruments
      .filter(instruments => instruments.length)
      .take(1)
      .subscribe((instruments) => {
        this.setState({ instruments }, () => this.selectInstrument(localStorage.getItem('SessionInstrumentId') || 1));
      });

    if (ShiftApp.config.instrumentSelectTicker) {
      this.tickers = ShiftApp.instruments.subscribe(productPairs => {
        productPairs.forEach(pair => ShiftApp.subscribeLvl1(pair.InstrumentId));
      });

      this.level1 = ShiftApp.Level1.subscribe(instrumentTicks => {
        this.setState({ instrumentTicks });
      });

      this.products = ShiftApp.products.filter(data => data.length).subscribe(prods => {
        const decimalPlaces = {};
        prods.forEach(product => {
          decimalPlaces[product.Product] = product.DecimalPlaces;
        });
        this.setState({ decimalPlaces });
      });
    }
    this.instrumentCheck = ShiftApp.instrumentChange.subscribe(i => this.setState({ selectedInstrument: i }));
  }

  componentWillUnmount() {
    this.instruments.dispose();
    if (ShiftApp.config.instrumentSelectTicker) {
      this.level1.dispose();
      this.tickers.dispose();
      this.products.dispose();
    }
    this.instrumentCheck.dispose();
  }

  selectInstrument = (instrumentId) => {
    const selectedInstrument = +instrumentId;
    const instrument = this.state.instruments.find((inst) => inst.InstrumentId === selectedInstrument);

    localStorage.setItem('SessionInstrumentId', selectedInstrument);
    document.APAPI.Session.SelectedInstrumentId = selectedInstrument;
    localStorage.setItem('SessionPair', instrument.Symbol);
    ShiftApp.setProductPair(instrument.Symbol);
    ShiftApp.instrumentChange.onNext(selectedInstrument);
    if (this.state.selectedInstrument) {
      this.unsubscribeInstrument(this.state.selectedInstrument);
    }
    this.subscribeInstrument(selectedInstrument);
    this.setState({ selectedInstrument });
  };

  subscribeInstrument = (InstrumentId) => {
    ShiftApp.subscribeTrades(InstrumentId, 100);
    ShiftApp.subscribeLvl2(InstrumentId);
  };

  unsubscribeInstrument = (InstrumentId) => {
    ShiftApp.unsubscribeTradesCall(InstrumentId);
    ShiftApp.unsubscribeLvl2(InstrumentId);
  };

  render() {
    const instrumentTicks = this.state.instrumentTicks;
    const instrument = this.state.instruments.find((inst) => inst.InstrumentId === +this.state.selectedInstrument);
    const decimalPlaces = this.state.decimalPlaces;
    const instrumentsList = this.state.instruments
      .filter((inst) => inst.InstrumentId !== (instrument && instrument.InstrumentId))
      .map((inst) => (
        <li key={uuidV4()} className={`instrument-${inst.Symbol}`}
          onClick={(e) => {
              e.preventDefault();
              this.selectInstrument(inst.InstrumentId);
            }}
        >
          <a
            className={ShiftApp.config.instrumentSelectTicker && "instrument-symbol"}
          >{ShiftApp.config.reversePairs ? (inst.Product2Symbol + inst.Product1Symbol) : inst.Symbol}</a>
          { ShiftApp.config.instrumentSelectTicker &&
            <div className="instrument-row--detail">
              <div className="instrument-row__detail-price" data-value={ instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].LastTradedPx, decimalPlaces[inst.Product2Symbol]) }>
              </div>
              <div className="instrument-row__detail-change" data-value={ instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].Rolling24HrPxChange, decimalPlaces[inst.Product2Symbol] || ShiftApp.config.decimalPlaces) }>
              </div>
              { ShiftApp.config.instrumentSelectShowVolume &&
                <div className="instrument-row__detail-volume" data-value={ instrumentTicks[inst.InstrumentId] && formatNumberToLocale(instrumentTicks[inst.InstrumentId].Rolling24HrVolume, decimalPlaces[inst.Product2Symbol] || ShiftApp.config.decimalPlaces) }>
                </div>
              }
            </div>
          }
        </li>
      ));
    const dropdownStyle = ShiftApp.config.instrumentSelectTicker ? { minWidth: "320px", right: 'unset' } : { right: 'unset' };
    return (
      <div className="dropdown instrument-dropdown">
        <button id="instrument-select" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {instrument && instrument.Symbol}
          {instrumentsList.length ? <span className="caret" style={{ marginLeft: '8px' }} /> : null}
        </button>
        {instrumentsList.length ?
          <ul className="dropdown-menu" style={dropdownStyle} aria-labelledby="dropdownMenu2">
            { ShiftApp.config.instrumentSelectTicker &&
              <li key={uuidV4()} className="instrument-header">
                <div>{ShiftApp.translation('INSTRUMENT_SELECT.INSTRUMENT') || 'Pair'}</div>
                <div>{ShiftApp.translation('INSTRUMENT_SELECT.LAST_PRICE') || 'Price'}</div>
                <div>{ShiftApp.translation('INSTRUMENT_SELECT.T24_HOUR_CHANGE') || '24hr Chg'}</div>
                { ShiftApp.config.instrumentSelectShowVolume &&
                  <div>{ShiftApp.translation('INSTRUMENT_SELECT.VOLUME') || 'Volume'}</div>
                }
              </li>
            }
            {instrumentsList}
          </ul> : null}
      </div>
    );
  }
}

export default InstrumentSelect;
