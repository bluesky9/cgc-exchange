/* global ShiftApp, $, atob, window */
import React from 'react';

import WidgetBase from './base';
import InputLabeled from '../misc/inputLabeled';
import { getURLParameter, showGrowlerNotification } from './helper';

class ResetPassword extends React.Component {
  constructor() {
    super();

    this.state = {
      newPassword: '',
      passwordConfirm: '',
      error: '',
      information: ''
    };
  }

  handleOnChange = (e) => {
    const { name, value } = e.target;
    const newState = {
      ...this.state,
      [name]: value,
    };

    this.setState(newState);
  };

  setBanner = (info) => {
    this.setState(info);
  }

  handleSubmit = () => {
    const self = this;
    const encodedUrl = getURLParameter('d1');
    const verifyCode = getURLParameter('verifycode');
    const userId = getURLParameter('UserId');
    const mediumRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[0-9]).*$', 'g');
    const enoughRegex = new RegExp('(?=.{8,}).*', 'g');

    if (!enoughRegex.test(this.refs.password.value())) {
      return this.setBanner({
        information: '',
        error: 'Password must contain at least 8 characters'
      });
    }
    if (!mediumRegex.test(this.refs.password.value())) {
      return this.setBanner({
        information: '',
        error: 'Password must contain at least 8 characters, one number, and at least one capital letter'
      });
    }
    if (!encodedUrl) {
      return this.setBanner({
        information: '',
        error: 'Required parameter d1 was not found in url'
      });

      // return showGrowlerNotification('error', 'Required parameter d1 was not found in url');
    }
    if (!verifyCode) {
      return this.setBanner({
        information: '',
        error: 'Required parameter verifycode was not found in url'
      });
      // return showGrowlerNotification('error', 'Required parameter verifycode was not found in url');
    }
    if (!userId) {
      return this.setBanner({
        information: '',
        error: 'Required parameter UserId was not found in url'
      });
      // return showGrowlerNotification('error', 'Required parameter UserId was not found in url');
    }
    if (!this.state.newPassword || !this.state.passwordConfirm) {
      return this.setBanner({
        information: '',
        error: 'Please enter your password in both inputs'
      });
      // return showGrowlerNotification('error', 'Please enter your password in both inputs');
    }
    if (this.state.newPassword !== this.state.passwordConfirm) {
      return this.setBanner({
        information: '',
        error: 'Passwords don\'t match, please type them again'
      });
      // return showGrowlerNotification('error', 'Passwords don\'t match, please type them again');
    }


    const ajaxUrl = atob(encodedUrl);
    const payload = {
      PendingCode: verifyCode,
      Password: this.state.newPassword,
      UserId: userId,
    };

    return $.ajax({
      type: 'POST',
      url: `${ajaxUrl}ResetPassword2`,
      data: JSON.stringify(payload),
      dataType: 'JSON',
      success: (data) => {
        if (!data.result) {
          if (data.rejectReason) {
            return this.setBanner({
              information: '',
              error: `Password Reset Error: ${data.rejectReason}`
            });
          }
          return this.setBanner({
            information: '',
            error: `Password Reset Error`
          });
        }
        window.localStorage.removeItem('SessionToken');
        showGrowlerNotification('success', 'Your password was updated successfully!');
        return self.props.close();
      },
    });
  };

  render() {
    return (
      <WidgetBase
        {...this.props}
        headerTitle={ShiftApp.translation('PASSWORD_MODAL.TITLE_TEXT') || 'Reset Password'}
        style={{ width: '600px' }}
        information={this.state.information}
        error={this.state.error}
      >
        <form>
          <div className="pad">
            <InputLabeled label="New password" type="password" ref="password" name="newPassword" onChange={this.handleOnChange} />
            <InputLabeled label="Confirm password" type="password" ref="confirm" name="passwordConfirm" onChange={this.handleOnChange} />
            <div className="text-center row around-xs">
              <button type="button" className="btn btn-default" onClick={this.props.close}>Cancel</button>
              <button type="button" className="btn btn-default" onClick={this.handleSubmit}>Reset Password</button>
            </div>
          </div>
        </form>
      </WidgetBase>
    );
  }
}

ResetPassword.defaultProps = {
  close: () => {},
};

ResetPassword.propTypes = {
  close: React.PropTypes.func,
};

export default ResetPassword;
