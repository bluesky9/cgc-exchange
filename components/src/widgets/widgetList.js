import AccountBalances from './accountBalances';
import OrderBook from './bookview';
import Deposit from './deposit';
import Reports from './reports';
import WithdrawStatus from './withdrawStatus';
import OpenOrders2 from './openOrders-2';
import InstrumentSelect from './instrumentSelect';
import DepositFIAT from './depositFIAT';
import AccountTransactions from './accountTransactions';
import Buy_Fixed from './buy-fixed';
import AccountActions from './accountActions';
import BuyCustom from './buy-custom';
import UserTable from './user-table';
import APIKeys from './APIKeys';
import SendRequest from './send-request';
import SendRequestModal from './send-request-modal';
import Balances from './balances';
import Withdraw from './withdraw';
import Trades from './trades';
import BlockTrade from './block-trade';
import ProductPairs from './product-pairs';
import BuyOrders from './buy-orders';
import SellOrders from './sell-orders';
import TradesNarrow from './tradesNarrow';
import Products from './products';
import Login from './login';
import Logout from './logout';
import LogInOut from './logInOut';
import Chart from './chart';
import Tickers from './tickers';
import TickersNarrow from './tickersNarrow';
import UserInfo from './user-information';
import KYC from './kyc';
import BuySell from './buy-sell';
import QuoteEntry from './quote-entry';
import Rates from './rates';
import Referral from './referral';
import DepositDigital from './depositDigital';
import WithdrawDigital from './withdrawDigital';
import WithdrawFIAT from './withdrawFIAT';
import WithdrawFIAT2 from './withdrawFIAT2';
import OpenOrders from './openOrders';
import Username from './username';
import VerificationLevels from './verificationLevels';
import LoginRequired from './LoginRequired';
import SaveLocalstorage from './saveLocalstorage';
import LoginForm from './login-form';
import RegisterForm from './register-form';
import MarketValue from './market-value';
import MarketTable from './market-table';
import Instruments from './instruments';
import BUY_SELL from './buy-btc';
import BuyAdvanced from './buy-advanced';
import Buttons from './buttons';
import OrderHistory from './order-history';
import OrderHistoryCancelled from './order-history-cancelled';
import Alerts from './alerts';
import Public_Trades from './public-trades';
import Public_Trades_Narrow from './public-trades-narrow';
import HeaderTicker from './header-ticker';
import RegisterDirect from './register-new';
import LoginDirect from './login-new';
import OrdersBook from './orders-book';
import OrdersBook_Orders from './orders-book-orders';
import TradeReports from './trade-reports';
import OrderEntry from './order-entry';
import OrderEntryAztec from './order-entry-aztec';
import Settings from './user-settings';
import ConfirmReject from './confirmReject';
import KycLaunch from './kyc_launch';
import HomeCoindirect from './home-coindirect';
import HomeUltraExchange from './home-ultraexchange';
import ResetPassword from './resetPassword';
import VerifyEmail from './verifyEmail';
import ConfirmWithdraw from './confirmWithdraw';
import SellNarrow from './sell-narrow';
import BuyNarrow from './buy-narrow';
import StopLimit from './stop-limit';
import TickerAdvanced from './ticker-advanced';
import TickerVolume from './ticker-volume';
import Ticker from './ticker';
import AccountSelect from './accountSelect';
import WithdrawsAndDeposits from './withdrawsAndDeposits';
import MerchantDepositConfirmation from './merchantDepositConfirmation';
import AffiliateProgram from './affiliateProgram';
import TickerScrolling from './ticker-scrolling';
import ConfirmWithdrawV2 from './confirmWithdraw-v2';
import VerifyEmailV2 from './verifyEmail-v2';
import ResetPasswordV2 from './reset-password-v2';
import BuyCustomLimit from './buy-custom-limit';
import MobileWarningPopup from './mobileWarningPopup';
import LoginLogoutBtns from './loginLogoutBtns';
import OrderEntryStandard from './order-entry-standard';

// Shift Widgets
import ShiftBalances from './shift-widgets/shift-balances';
import ShiftOrderBook from './shift-widgets/shift-bookview';
import ShiftBuyFixed from './shift-widgets/shift-buy-fixed';
import ShiftDeposit from './shift-widgets/shift-deposit';
import ShiftDepthChart from './shift-widgets/shift-depth-chart';
import ShiftHomeCoindirect from './shift-widgets/shift-home-coindirect';
import ShiftMainNav from './shift-widgets/shift-main-nav';
import ShiftModalWelcome from './shift-widgets/shift-modal-welcome';
import ShiftPublicTradesNarrow from './shift-widgets/shift-public-trades-narrow';
import ShiftResetPassword from './shift-widgets/shift-reset-password';
import ShiftSelectLanguage from './shift-widgets/shift-select-language';
import ShiftTickerContainer from './shift-widgets/shift-ticker-container';
import ShiftTicker from './shift-widgets/shift-ticker';
import ShiftTickerScrolling from './shift-widgets/shift-ticker-scrolling';
import ShiftWidgetWrapper from './shift-widgets/shift-widget-wrapper';
import ShiftWithdraw from './shift-widgets/shift-withdraw';
import ShiftWithdrawFIAT from './shift-widgets/shift-withdraw-fiat';
import ShiftInstrumentSelect from './shift-widgets/shift-instrument-select';
import ShiftRegisterForm from './shift-widgets/shift-register-form';
import ShiftTrades from './shift-widgets/shift-trades';
import ShiftTickerCustomDecimal from './shift-widgets/shift-tiker-custom-decimal';
import ShiftDepositFiat from './shift-widgets/shift-deposit-fiat';
import ShiftResetPasswordV2 from './shift-widgets/shift-reset-password-v2';
import ShiftOrderEntry from './shift-widgets/shift-order-entry';
import ShiftRegisterFormInnerAltV2 from './shift-widgets/shift-register-form-inner-alt-v2';
import ShiftAltRegisterForm from './shift-widgets/shift-alt-register-form';
import ShiftTradingViewChart from './shift-widgets/shift-tradingview-chart';
import ShiftAccountTransactions from './shift-widgets/shift-account-transactions';
import ShiftSettings from './shift-widgets/shift-user-settings';
import ShiftBlackList from './shift-widgets/shift-blacklist-countries';
import ShiftInstsSelectByProduct from './shift-widgets/shift-insts-select-by-products';

// Shift Containers
import ShiftAccountOverviewContainer from './shift-containers/shift-account-overview-container';
import ShiftChartContainer from './shift-containers/shift-chart-container';
import ShiftOrderBookTradesContainer from './shift-containers/shift-order-book-trades-container';
import ShiftOrderEntryContainer from './shift-containers/shift-order-entry-container';
import ShiftOrdersTablesContainer from './shift-containers/shift-orders-tables-container';
import ShiftMobileAdvancedUIContainer from './shift-containers/shift-mobile-advanced-ui-container';
import ShiftTradeUI from './shift-containers/shift-trade-ui';

export default {
  AccountBalances,
  OrderBook,
  Deposit,
  Reports,
  WithdrawStatus,
  OpenOrders2,
  InstrumentSelect,
  DepositFIAT,
  AccountTransactions,
  Buy_Fixed,
  AccountActions,
  BuyCustom,
  UserTable,
  APIKeys,
  SendRequest,
  SendRequestModal,
  Balances,
  Withdraw,
  Trades,
  BlockTrade,
  ProductPairs,
  BuyOrders,
  SellOrders,
  TradesNarrow,
  Products,
  Login,
  Logout,
  LogInOut,
  LoginLogoutBtns,
  Chart,
  Tickers,
  TickersNarrow,
  UserInfo,
  KYC,
  BuySell,
  QuoteEntry,
  Rates,
  Referral,
  DepositDigital,
  WithdrawDigital,
  WithdrawFIAT,
  WithdrawFIAT2,
  OpenOrders,
  Username,
  VerificationLevels,
  LoginRequired,
  SaveLocalstorage,
  LoginForm,
  RegisterForm,
  MarketValue,
  MarketTable,
  Instruments,
  BUY_SELL,
  BuyAdvanced,
  Buttons,
  OrderHistory,
  OrderHistoryCancelled,
  Alerts,
  Public_Trades,
  Public_Trades_Narrow,
  HeaderTicker,
  RegisterDirect,
  LoginDirect,
  OrdersBook,
  OrdersBook_Orders,
  TradeReports,
  OrderEntry,
  OrderEntryAztec,
  Settings,
  ConfirmReject,
  KycLaunch,
  HomeCoindirect,
  HomeUltraExchange,
  ResetPassword,
  ConfirmWithdraw,
  VerifyEmail,
  SellNarrow,
  BuyNarrow,
  StopLimit,
  TickerAdvanced,
  TickerVolume,
  Ticker,
  AccountSelect,
  WithdrawsAndDeposits,
  MerchantDepositConfirmation,
  AffiliateProgram,
  TickerScrolling,
  ConfirmWithdrawV2,
  VerifyEmailV2,
  ResetPasswordV2,
  BuyCustomLimit,
  MobileWarningPopup,
  OrderEntryStandard,

  // Shift Widgets
  ShiftBalances,
  ShiftOrderBook,
  ShiftBuyFixed,
  ShiftDeposit,
  ShiftDepthChart,
  ShiftHomeCoindirect,
  ShiftMainNav,
  ShiftModalWelcome,
  ShiftPublicTradesNarrow,
  ShiftResetPassword,
  ShiftSelectLanguage,
  ShiftTickerContainer,
  ShiftTicker,
  ShiftTickerScrolling,
  ShiftWidgetWrapper,
  ShiftWithdraw,
  ShiftWithdrawFIAT,
  ShiftInstrumentSelect,
  ShiftRegisterForm,
  ShiftTrades,
  ShiftTickerCustomDecimal,
  ShiftDepositFiat,
  ShiftResetPasswordV2,
  ShiftOrderEntry,
  ShiftRegisterFormInnerAltV2,
  ShiftAltRegisterForm,
  ShiftTradingViewChart,
  ShiftAccountTransactions,
  ShiftSettings,
  ShiftBlackList,
  ShiftInstsSelectByProduct,
  
  // Shift Containers
  ShiftAccountOverviewContainer,
  ShiftChartContainer,
  ShiftOrderBookTradesContainer,
  ShiftOrderEntryContainer,
  ShiftOrdersTablesContainer,
  ShiftMobileAdvancedUIContainer,
  ShiftTradeUI,
};
