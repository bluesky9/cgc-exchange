/* global ShiftApp, $, alert, window */
/* eslint-disable react/no-multi-comp, no-alert */
import React from 'react';

import ProcessingButton from '../misc/processingButton';
import ApInput from '../misc/form/apInput';
import ApDatepicker from '../misc/form/apDatepicker';
import ApSelect from '../misc/form/apSelect';
import { states, countriesCodes } from '../common';

export default class GreenId extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        // throwError booleans
        requiredFields: {
          dob: false,
          billingCountry: false,
          passportExpiration: false,
        },
        userConfig: {
          firstName: '',
          middleName: '',
          lastName: '',
          dob: '',
          telephone: '',
          billingFlatNumber: '',
          billingStreetNumber: '',
          billingStreetAddress: '',
          billingStreetType: '',
          billingCountry: '',
          billingCity: '',
          billingSuburb: '',
          billingZip: '',
          driverLicenseNumber: '',
          driverLicenseVersion: '',
          passportNumber: '',
          passportExpiration: '',
        },

        processing: false,
        // regexMatch: true,
        confirmClose: false,
        verificationLevel: 0,
        formIsValid: true,
      };
      // this.passportNumberValidation = this.passportNumberValidation.bind(this);
      this.isRequired = this.isRequired.bind(this);
    }

    componentDidMount() {

      this.userConfiguration = ShiftApp.getUserConfig.subscribe(data => {
        let configs = this.state.userConfig;

        if (data.length > 0) {

          data.reduce((item, i) => {

            return configs[i.Key] = i.Value; // eslint-disable-line no-param-reassign

          }, {});

          configs.UseNoAuth = configs.UseNoAuth && (configs.UseNoAuth.toString() || "true");
          configs.UseGoogle2FA = configs.UseGoogle2FA && (configs.UseGoogle2FA.toString() || "false");

          this.setState({ userConfig: configs });
        }

      });

      this.accountInfo = ShiftApp.accountInfo.subscribe(data => {
        this.setState({ verificationLevel: data.VerificationLevel })
      })

      ShiftApp.getUserCon({ UserId: ShiftApp.userData.value.UserId });
    }

    componentWillUnmount() {
      /* eslint-disable no-unused-expressions */
      this.userConfiguration && this.userConfiguration.dispose();
      this.verifyLevel && this.verifyLevel.dispose();
      this.verifcationLevelUpdate && this.verifcationLevelUpdate.dispose();
      this.validatorRes && this.validatorRes.dispose();
      /* eslint-enable no-unused-expressions */
    }

    dateChanged = field => date => {
      const { userConfig } = this.state;
      userConfig[field] = date;
      this.setState({ userConfig });
    }

    changed = (name, value, validationMessagesLength, formIsValid) => {
      const userConfig = this.state.userConfig;
      userConfig[name] = value;
      this.setState({ userConfig, formIsValid });
    }

    submit = (e) => {
      if (this.isRequired()) { // Checking for required fields; returns submit as false if any kycRequiredFields are empty
        const that = this;
        const configIn = [];
        const growlerOptions = {
          allow_dismiss: true,
          align: "center",
          delay: ShiftApp.config.growlerDelay,
          offset: { from: 'top', amount: 30 },
          left: '60%',
        };
        const growlerOptionsLongDelay = {
          allow_dismiss: true,
          align: "center",
          delay: 20000,
          offset: { from: 'top', amount: 30 },
          left: '60%',
        };
        let configs = {};
        let server = {};
        let userInfo = {};

        e.preventDefault();
        this.setState({ processing: true });

        Object.keys(this.state.userConfig).forEach(key => {
          let entry,
              value = this.state.userConfig[key];

            entry = {
              Key: key,
              Value: value,
            }
            configIn.push(entry);

        });
        // console.log("configIn", configIn);

        configs = {
          UserId: ShiftApp.userData.value.UserId,
          Config: configIn,
        };

        this.props.setError('');

        // Set userInfo
        userInfo = {
          firstName: this.state.userConfig.firstName,
          middleName: this.state.userConfig.middleName,
          lastName: this.state.userConfig.lastName,
          dob: this.state.userConfig.dob,
          accountName: JSON.stringify(ShiftApp.userData.value.UserId),
          billingStreetAddress: this.state.userConfig.billingStreetAddress,
          billingCountry: this.state.userConfig.billingCountry,
          billingCity: ShiftApp.config.siteName === "lexexchange" ? "" : this.state.userConfig.billingCity, // "OR" suburb just in case city field is not being used on UI
          billingZip: this.state.userConfig.billingZip,
          phone: this.state.userConfig.telephone,
          billingFlatNumber: this.state.userConfig.billingFlatNumber,
          billingStreetNumber: this.state.userConfig.billingStreetNumber,
          billingStreetType: this.state.userConfig.billingStreetType,
          billingSuburb: this.state.userConfig.billingSuburb, // Australia specific
          salutation: '',
          // Temporary key names
          MerchAccountName: this.state.userConfig.passportNumber,
          MerchAccountTaxID: new Date(this.state.userConfig.passportExpiration),
          MerchPhone: this.state.userConfig.driverLicenseNumber,
          MerchFirstName: ShiftApp.config.noDlVersionForGreenId ? "" : this.state.userConfig.driverLicenseVersion
        };

        // ==============================================
        // ShiftApp.getUserConfig LOGIC
        // ==============================================
        // this.userConfiguration = ShiftApp.getUserConfig.subscribe(data => {
        //   let configurations = [];

        //   if (data.length > 0) {
        //     configurations = data.reduce((item, i) => {
        //       item[i.Key] = i.Value; // eslint-disable-line no-param-reassign
        //       return item;
        //     }, {});

        //     configurations.UseNoAuth = JSON.parse(configurations.UseNoAuth || "true");
        //     configurations.UseGoogle2FA = JSON.parse(configurations.UseGoogle2FA || "false");
        //   }
        //   server = configurations;
        // });

        ShiftApp.setUserConfig.subscribe(data => {

          if (data.result) {

            $.bootstrapGrowl(
              ShiftApp.translation('KYC.INFO_ACCEPTED') || 'Your information has been accepted',
              { ...growlerOptions, type: 'success' },
            );
          }
          if (!data.result && data.length > 0) {

            $.bootstrapGrowl(
              ShiftApp.translation('KYC.INFO_DENIED') || 'Your information has been denied',
              { ...growlerOptions, type: 'danger' },
            );
          }
          if (data.result)

            return true;
        });

        // Setting user config
        ShiftApp.setUserCon(configs);


        // ==============================================
        // KYC VALIDATOR RESPONSE LOGIC
        // ==============================================
        const clientInfo = {
          alphaPointSessiontoken: ShiftApp.session.value.SessionToken, // session token of the user to be validated, not used yet
          alphaPointUserID: JSON.stringify(ShiftApp.userData.value.UserId),
          validationStage: 1, // validation stage from identityMind controlPanel
          validator: ShiftApp.config.kycType,
        };

        const params = {
          requestIdentifier: ShiftApp.config.kycClientId,
          clientInfo,
          userInfo,
        };

        this.verifcationLevelUpdate = ShiftApp.verificationLevelUpdate.subscribe(res => {
          // console.log("level update event");
          if (res.VerificationStatus === 'Approved') {
            this.props.increaseLevel(res.VerificationLevel);
          }
        });

        this.validatorRes = ShiftApp.validatorResponse.subscribe(res => {
          if (res.result === 'Unknown Validator Request') {
            // this.setState({ processing: false });
            $.bootstrapGrowl(
              ShiftApp.translation('KYC.UNKNOWN_VALIDATOR_REQUEST') || 'Unknown Validator Request',
              { ...growlerOptions, type: 'danger' },
            );

            if (res.ValidationAnswerData) {
              if (res.ValidationAnswerData.isAccepted) {
                // this.setState({ processing: false });
                $.bootstrapGrowl(
                  ShiftApp.translation('KYC.INFO_ACCEPTED') || 'Your information has been accepted',
                  { ...growlerOptions, type: 'success' },
                );
              } if ((!res.ValidationAnswerData.isAccepted && res.NeedsManualReview)) { // eslint-disable-line max-len
                // this.setState({ processing: false });
                $.bootstrapGrowl(
                  ShiftApp.translation('KYC.VERIFICATION_DENIED') || 'Verification Denied: Not Accepted',
                  { ...growlerOptions, type: 'danger' },
                );
              }
            }
            this.setState({ processing: false, confirmClose: true });
          }
        });

        this.verifyLevel = ShiftApp.verifylevel.subscribe(res => {
          // TODO:::::: Add setState for validatorRespondedMesssage
          // CATCHING VALIDATOR ERRORS
          if (res === "Validator Not Connected") {
            this.setState({ processing: false, confirmClose: true });
            $.bootstrapGrowl(
              res,
              { ...growlerOptionsLongDelay, type: 'danger' },
            );
          } else if (res === "Unable to validate") {
            this.setState({ processing: false, confirmClose: true });
            $.bootstrapGrowl(
              res,
              { ...growlerOptionsLongDelay, type: 'danger' },
            );
          } else if (res === "Validator Call failed: The remote server returned an error: (500) Internal Server Error.") {
            this.setState({ processing: false, confirmClose: true });
            $.bootstrapGrowl(
              res,
              { ...growlerOptionsLongDelay, type: 'danger' },
            );
          } else if (res.ErrorMessage) {
            this.setState({ processing: false, confirmClose: true });
            $.bootstrapGrowl(
              ShiftApp.translation('KYC.VALIDATION_ERROR') || 'Validator Error',
              { ...growlerOptionsLongDelay, type: 'danger' },
            );
            if (res.ErrorMessage === 'SYSTEM: The remote server returned an error: (400) Bad Request.') {
              $.bootstrapGrowl(
                ShiftApp.translation('KYC.TRY_AGAIN') || 'Please try again',
                { ...growlerOptions, type: 'info' },
              );
              $.bootstrapGrowl(
                ShiftApp.translation('KYC.MISSING_BILLING_COUNTRY') || 'Check that you have entered Billing Country',
                { ...growlerOptions, type: 'info' },
              );
              this.setState({ processing: false, confirmClose: true });
            }
          } else if (res === false) {
            this.setState({ processing: false, confirmClose: true });
            $.bootstrapGrowl(
              ShiftApp.translation('KYC.VALIDATION_ERROR') || 'Validator Error',
              { ...growlerOptionsLongDelay, type: 'danger' },
            );
          }

          // IF VALIDATOR HAS A VALID RESPONSE WITH VALIDATION ANSWER DATA
          if (res.ValidationAnswerData) {
            // TODO:::::: Add setState for validatorRespondedMesssage
            if (res.ValidationAnswerData.isAccepted) {
              this.setState({ processing: false, confirmClose: true });
              $.bootstrapGrowl(
                ShiftApp.translation('KYC.INFO_ACCEPTED') || 'Your information has been accepted',
                { ...growlerOptions, type: 'success' },
              );
            } else if ((!res.ValidationAnswerData.isAccepted && res.ValidationAnswerData.NeedsManualReview)) { // eslint-disable-line max-len
              this.setState({ processing: false, confirmClose: true });
              $.bootstrapGrowl(
                ShiftApp.translation('KYC.INFO_MANUAL_REVIEW') || 'Your information requires manual review',
                { ...growlerOptions, type: 'info' },
              );
            } else if (!res.ValidationAnswerData.isAccepted && res.ValidationAnswerData.ApiErrorDescription) {
              this.setState({ processing: false, confirmClose: true });
              $.bootstrapGrowl(
                res.ValidationAnswerData.ApiErrorDescription,
                { ...growlerOptionsLongDelay, type: 'danger' },
              );
            } else if (!res.ValidationAnswerData.isAccepted && res.ValidationAnswerData.ApiError) {
              this.setState({ processing: false, confirmClose: true });
              $.bootstrapGrowl(
                res.ValidationAnswerData.ApiErrorDescription,
                { ...growlerOptionsLongDelay, type: 'danger' },
              );
            } else {
              this.setState({ processing: false, confirmClose: true });
              $.bootstrapGrowl(
                ShiftApp.translation('KYC.RESPONSE', {answerData: res.ValidationAnswerData}) || `The response was ${res.ValidationAnswerData}`,
                { ...growlerOptionsLongDelay, type: 'info' },
              );
            }
          }
        });

        ShiftApp.validateUserRegistration(params);

      } else { // If fields specified in isRequired() function are empty,
        e.preventDefault();
        return false;
      }
    }

    // Required Fields validation
    isRequired() {
      const userConfig = this.state.userConfig;
      let formIsValid = true;

      const kycFields = ShiftApp.config.kycFields || [];

      const kycRequiredFields = Object.keys(kycFields).filter(key => {
        return kycFields[key].includes('required')
      });

      for (const key in userConfig) {

        if (!userConfig[key]) {
          if (kycRequiredFields.indexOf(key) > -1) {
            formIsValid = false;
            this.setState({ formIsValid, [key]: true });
          }
        }
      }
      return formIsValid;
    }

    render() {
      const countries = countriesCodes.map(country => (
        <option value={country.code} key={country.code} >{country.name}</option>
      ));

      if (ShiftApp.config.onlyShowOneCountryKYC) {
        if (ShiftApp.config.kycCountriesList.length > 1) {
          var listSpecificCountries = countriesCodes.filter(function (country) {
            return ShiftApp.config.kycCountriesList.indexOf(country.name) > -1;
          }).map(theCountry => (
            <option value={theCountry.code} key={theCountry.code} >{theCountry.name}</option>
          ));
        } else {
          var listSpecificCountries = countriesCodes.filter(country => { return country.name === ShiftApp.config.kycCountriesList }).map(theCountry => (
            <option value={theCountry.code} key={theCountry.code} >{theCountry.name}</option>
          ));
        }
      }

      const countryNow = countriesCodes.find(country => country.code === this.state.userConfig.billingCountry) || '';
      const statesOptions = states.map(state => <option value={state.code} key={state.code} >{state.name}</option>);

      return (
        <div>
          {this.state.processing && <div className="loader-container">
            <div className="loader">{ShiftApp.translation('COMMON.LOADING') || 'Loading...'}</div>
          </div>}

          {this.state.confirmClose && <div className="loader-container-confirm">
            <span>{this.state.validatorRespondedMesssage}</span>
            <button className="confirm-close-btn blue-btn" onClick={() => window.location.reload()}>{ShiftApp.translation('COMMON.OKAY') || 'Okay'}</button>
          </div>}

          <form onSubmit={this.submit} style={{ overflow: 'hidden' }}>
            <div className="pad-y" style={{ marginTop: '15px' }}>

              <ApInput
                name="firstName"
                value={this.state.userConfig.firstName}
                validations={ShiftApp.config.kycFields.firstName && ShiftApp.config.kycFields.firstName}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.FIRST_NAME') || 'First Name'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-6 col-sm-4 input-firstName"}
              />

              <ApInput
                name="middleName"
                value={this.state.userConfig.middleName}
                validations={ShiftApp.config.kycFields.middleName && ShiftApp.config.kycFields.middleName}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.MIDDLE_NAME') || 'Middle Name'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-6 col-sm-4 input-middleName"}
              />

              <ApInput
                name="lastName"
                value={this.state.userConfig.lastName}
                validations={ShiftApp.config.kycFields.lastName && ShiftApp.config.kycFields.lastName}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.LAST_NAME') || 'Last Name'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-6 col-sm-4 input-lastName"}
              />
              <ApDatepicker
                name="dob"
                dob
                onChange={this.dateChanged('dob')}
                value={this.state.userConfig.dob}
                throwError={this.state.requiredFields.dob}
                errorDescription={ShiftApp.translation('VERIFY.REQUIRED_TEXT') || 'This field is required'}
                label={ShiftApp.translation('VERIFY.DATE') || 'DOB'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-4 input-dob"}
              />

              <ApInput
                name="telephone"
                value={this.state.userConfig.telephone}
                validations={ShiftApp.config.kycFields.telephone && ShiftApp.config.kycFields.telephone}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.TELEPHONE') || 'Telephone'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-4 input-telephone"}
              />

              <ApSelect
                name="billingCountry"
                onChange={this.changed}
                className={this.state.requiredFields.billingCountry && "input-error"}
                throwError={this.state.requiredFields.billingCountry}
                value={this.state.userConfig.billingCountry}
                errorDescription={ShiftApp.translation('VERIFY.REQUIRED_TEXT') || 'This field is required'}
                label={ShiftApp.translation('VERIFY.COUNTRY') || 'Select Country'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-4 input-billingCountry"}
              >
                <option value={this.state.userConfig.billingCountry}>
                  {countryNow.name || ShiftApp.translation('VERIFY.COUNTRY') || 'Select Country'}
                </option>
                {ShiftApp.config.onlyShowOneCountryKYC ? listSpecificCountries : countries}
              </ApSelect>

              <ApInput
                name="billingFlatNumber"
                validations={ShiftApp.config.kycFields.billingFlatNumber && ShiftApp.config.kycFields.billingFlatNumber}
                value={this.state.userConfig.billingFlatNumber}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.FLAT_NUMBER') || 'Flat Number'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-2 kyc-input-inline" : "col-xs-6 col-sm-2 input-billingFlatNumber"}
              />

              <ApInput
                name="billingStreetNumber"
                validations={ShiftApp.config.kycFields.billingStreetNumber && ShiftApp.config.kycFields.billingStreetNumber}
                value={this.state.userConfig.billingStreetNumber}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.STREET_NUMBER') || 'Street Number'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-2 col-sm-2 kyc-input-inline" : "col-xs-6 col-sm-2 input-billingStreetNumber"}
              />

              <ApInput
                name="billingStreetAddress"
                validations={ShiftApp.config.kycFields.billingStreetAddress && ShiftApp.config.kycFields.billingStreetAddress}
                value={this.state.userConfig.billingStreetAddress}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.ADDRESS') || 'Street Name'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-2 kyc-input-inline" : "col-xs-6 col-sm-4 input-billingStreetAddress"}
              />

              <ApInput
                name="billingStreetType"
                validations={ShiftApp.config.kycFields.billingStreetType && ShiftApp.config.kycFields.billingStreetType}
                value={this.state.userConfig.billingStreetType}
                onChange={this.changed}
                label={ShiftApp.translation('VERIFY.STREET_TYPE') || 'Street Type (Ave., St.)'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-2 kyc-input-inline" : "col-xs-6 col-sm-4 input-billingStreetType"}
              />

              {ShiftApp.config.siteName === 'lexexchange' ? null :
              <ApInput
                  name="billingCity"
                  validations={ShiftApp.config.kycFields.billingCity && ShiftApp.config.kycFields.billingCity}
                  value={this.state.userConfig.billingCity}
                  onChange={this.changed}
                  label={ShiftApp.translation('VERIFY.CITY') || 'City'}
                  wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-4 input-billingCity"}
              />}

              <ApInput
              name="billingSuburb"
              validations={ShiftApp.config.kycFields.billingSuburb && ShiftApp.config.kycFields.billingSuburb}
              value={this.state.userConfig.billingSuburb}
              onChange={this.changed}
              label={ShiftApp.translation('VERIFY.SUBURB') || 'Suburb'}
              wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-3 kyc-input-inline" : "col-xs-4 input-billingSuburb"}
              />

              <ApInput
              name="billingZip"
              validations={ShiftApp.config.kycFields.billingZip && ShiftApp.config.kycFields.billingZip}
              value={this.state.userConfig.billingZip}
              onChange={this.changed}
              label={ShiftApp.translation('VERIFY.ZIP') || 'Zipcode/Postcode'}
              wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-4 input-billingZip"}
              />

              <ApInput
              name="driverLicenseNumber"
              validations={ShiftApp.config.kycFields.driverLicenseNumber && ShiftApp.config.kycFields.driverLicenseNumber}
              value={this.state.userConfig.driverLicenseNumber}
              onChange={this.changed}
              label={ShiftApp.translation('VERIFY.DRIVER_LICENSE_NUMBER') || 'DL Number'}
              wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-3 kyc-input-inline" : (ShiftApp.config.siteName === "lexexchange" ? "col-xs-4" : "col-xs-3")}
              />

              {ShiftApp.config.siteName === 'lexexchange' ? null :
              <ApInput
                  name="driverLicenseVersion"
                  validations={ShiftApp.config.kycFields.driverLicenseVersion && ShiftApp.config.kycFields.driverLicenseVersion}
                  value={this.state.userConfig.driverLicenseVersion}
                  onChange={this.changed}
                  label={ShiftApp.translation('VERIFY.DRIVER_LICENSE_VERSION') || 'DL Version'}
                  wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-3"}
              />}

              <ApInput
              name="passportNumber"
              validations={ShiftApp.config.kycFields.passportNumber && ShiftApp.config.kycFields.passportNumber}
              value={this.state.userConfig.passportNumber}
              onChange={this.changed}
              label={ShiftApp.translation('VERIFY.PASSPORT_NUMBER') || 'Passport Number'}
              wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-3"}
              />

              <ApDatepicker
                name="passportExpiration"
                value={this.state.userConfig.passportExpiration}
                onChange={this.dateChanged('passportExpiration')}
                throwError={this.state.requiredFields.passportExpiration}
                errorDescription={ShiftApp.translation('VERIFY.REQUIRED_TEXT') || 'This field is required'}
                label={ShiftApp.translation('VERIFY.PASSPORT_EXPIRATION') || 'Pspt Exp'}
                wrapperClass={ShiftApp.config.advancedUIKYC ? "col-xs-4 kyc-input-inline" : "col-xs-3"}
              />
              {ShiftApp.config.advancedUIKYC && <div style={{height:"50px"}} className="form-group col-xs-4 kyc-input-inline space-filler"></div>}

            </div>

            <div className="pad" style={{ paddingBottom: '10px' }}>
            <div className="col-xs-12 container-kyc-submit" style={{ margin: "10px 0 5px 0" }}>
                {false &&
                  <ProcessingButton
                    type="submit"
                    onClick={this.submit}
                    processing={this.state.processing}
                    className="btn btn-action input-verify"
                  >{ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Verify'}</ProcessingButton>}

                <ProcessingButton
                  type="submit"
                  onClick={this.submit}
                  processing={this.state.processing}
                  disabled={!this.state.formIsValid}
                  className="btn btn-action input-verify"
                >{ShiftApp.translation('BUTTONS.TEXT_SUBMIT') || 'Verify'}</ProcessingButton>

                {!this.state.formIsValid &&
                <span style={{ color: 'lightcoral', fontWeight: '600', fontSize: '13px', display: 'inline-block', marginLeft: '12px'}}>
                  {ShiftApp.translation('VERIFY.FORM_INVALID_MESSAGE') ||
                  'Please check that each field is filled in correctly.'}
                </span>}
              </div>
            </div>
          </form>
        </div>
      );
    }
  }

  GreenId.defaultProps = {
    increaseLevel: () => { },
    setError: () => { },
    hideHeader: true,
  };

  GreenId.propTypes = {
    increaseLevel: React.PropTypes.func,
    setError: React.PropTypes.func,
  };
