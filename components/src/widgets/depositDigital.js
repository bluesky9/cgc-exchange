/* global ShiftApp, document, location $ */
import React from 'react';

import QRCode from 'qrcode.react';
import WidgetBase from './base';
import SelectLabeled from '../misc/selectLabeled';
import InputLabeled from '../misc/inputLabeled';
import InputNoLabel from '../misc/inputNoLabel';
import CustomDropdown from '../misc/customDropdown';
import Spinner from '../misc/spinner';

class DepositDigital extends React.Component {
  constructor(props) {
    super(props);

    this.getDeposits = this.getDeposits.bind(this);
    this.state = {
      address: [],
      addressList: [],
      dasAccount: '',
      dasExchangeAccount: '',
      selected: props.Product || '',
      productId: 0,
      selectedId: '',
      newkey: true,
      paymentIds: [],
      useImage: false,
      copied: 0,
      accountId: 0,
      products: [],
      showTemplate: false,
      template: null,
      templateFields: {},
      getNewDepositKeyClicked: false,
      nextBtnClicked: false,
      selectedAddress: '',
      depositRequestTimedOut: false,
      depositRequestTimeOutId: 0,
      loadingDeposits: false,
      showMemoQRCode: false, 
      showExchangeQRCode: false,
    };
  }

  componentDidMount() {
    const payload = {
      OMSId: ShiftApp.oms.value,
      AccountId: ShiftApp.selectedAccount.value || ShiftApp.userAccounts.value[0],
    };
    const product = this.props.Product;
    const currentProd = this.state.selected || product;

    if (ShiftApp.config.siteName === 'bitcoindirect') this.setState({siteName: true});

    this.products = ShiftApp.products.subscribe((products) => {
      const myProd = products.find((prod) => prod.Product === currentProd);

      this.setState({productId: myProd.ProductId});
      payload.ProductId = myProd.ProductId;
    });

    if (ShiftApp.config.useDepositTemplates) {
      this.depositTemplate = ShiftApp.depositTemplate.subscribe((data) => {
        if (data.Template) {
          const fields = JSON.parse(data.Template.Template);

          if (!fields.length && data.Template.UseGetDepositWorkflow) {
            ShiftApp.getDepositInfo({
              ...payload,
              GenerateNewKey: !ShiftApp.config.optionalNewDepositKeys,
            });
          }
          if (!fields.length && !data.Template.UseGetDepositWorkflow && (data.Template.DepositWorkflow === 'CryptoWallet')) {
            ShiftApp.getDepositInfo({
              ...payload,
              GenerateNewKey: !ShiftApp.config.optionalNewDepositKeys,
            });
          }

          this.setState({
            showTemplate: !!Object.keys(fields).length,
            template: data.Template,
            templateFields: fields,
          });
        } else {
          this.setState({
            showTemplate: false,
            template: null,
            templateFields: {},
          });
        }
      });
    }

    this.deposits = ShiftApp.deposits.subscribe((res) => {
      const depositInfo = res.DepositInfo ? JSON.parse(res.DepositInfo) : '';
      let depositKey;
      let selectedKey;

      this.endDepositsRequestTimeout();

      if (this.props.Product === 'XRP') {
        for (let i = 0; i < depositInfo.length; i++) {
          depositInfo[i] = depositInfo[i].split('?dt=')[0];
        }
      }

      if (product === 'DAS' || this.props.Product === 'DASC') {
        let dasExchangeAccount;
        let dasAccount;
        for (let i = 0; i < depositInfo.length; i++) {
          // depositInfo[i] = depositInfo[i].split('?dt=')[0];
          // this.setState({ dasAccount: depositInfo[i].split('?dt=')[1] });
          dasExchangeAccount = depositInfo[i].split('?dt=')[0];
          dasAccount = depositInfo[i].split('?dt=')[1];
        } 
        return this.setState({ 
          dasExchangeAccount,
          dasAccount,
          showTemplate: false,
          loadingDeposits: false,
        });
      }

      const keys = depositInfo;

      if (product === 'MON') {
        depositKey = keys.WalletAddress ? JSON.parse(keys.WalletAddress) : '';
        selectedKey = keys.PaymentIds ? keys.PaymentIds[0] : '';
        this.setState({ paymentIds: keys.PaymentIds, selectedId: selectedKey });
      } else {
        depositKey = keys !== '' ? keys.reverse() : '';
      }

      // if (this.props.Product === 'XRP') {
      //   for (let i = 0; i < depositKey.length; i++) {
      //     depositKey[i] += '?dt='+ ShiftApp.userAccounts.value[0];
      //   }
      // }

      // need to add something to pick the newest address, not sure if it's max or min of array
      this.setState({
        showTemplate: !res.result,
        address: depositKey[0],
        addressList: depositKey,
        selected: depositKey[0],
        selectedAddress: depositKey[0],
        loadingDeposits: false,
      });

      if (this.state.getNewDepositKeyClicked) {
        this.setState({ selectedAddress: depositKey[0], getNewDepositKeyClicked: false });
      }
    });

    this.getDeposits(payload);
  }

  componentWillUnmount() {
    this.products.dispose();
    this.deposits.dispose();
    ShiftApp.deposits.onNext([]);
    if (ShiftApp.config.useDepositTemplates) {
      this.depositTemplate.dispose();
      ShiftApp.depositTemplate.onNext({});
    }
  }

  getDeposits(payload) {
    this.startDepositsRequestTimeout();
    this.setState({ loadingDeposits: true });
    if (ShiftApp.config.useDepositTemplates) {
      ShiftApp.getDepositRequestInfoTemplate(payload);
    } else {
      ShiftApp.getDepositInfo({...payload, GenerateNewKey: !ShiftApp.config.optionalNewDepositKeys});
    }
  }

  getNewDepositKey = () => {
    ShiftApp.getDepositInfo({
      OMSId: ShiftApp.oms.value,
      AccountId: ShiftApp.selectedAccount.value || ShiftApp.userAccounts.value[0],
      ProductId: this.state.productId,
      GenerateNewKey: true,
    });
    // generate and display new QRcode
    this.setState({getNewDepositKeyClicked: true});
  };

  endDepositsRequestTimeout() {
    if(this.state.depositRequestTimeOutId) {
      clearTimeout(this.state.depositRequestTimeOutId);
    }
  }

  startDepositsRequestTimeout() {
    const timeoutId = setTimeout(() => {
      this.setState({ depositRequestTimedOut: true });
    }, 5000);

    this.setState({ depositRequestTimeOutId: timeoutId });
  }

  addressChanged = (e) => this.setState({selected: e.target.value});

  paymentIdChanged = (e) => this.setState({selectedId: e.target.value});

  copyMeFunction = (e) => {
    e.preventDefault();
    const self = this;

    function copyAddress(text) {
      self.setState({selectedAddress: text, selected: text});
      const textarea = document.createElement('textarea');
      textarea.id = 'copy-this-address';
      textarea.innerText = text;
      document.body.appendChild(textarea);
      textarea.select();

      try {
        if (document.execCommand('copy')) {
          $.bootstrapGrowl(
            ShiftApp.translation('DEPOSIT.COPIED_TO_CLIPBOARD') || 'Address Copied to Clipboard',
            {...ShiftApp.config.growlerDefaultOptions, type: 'info'},
          );
          document.querySelector('#fillMe').textContent = text;
          textarea.remove();
          return true;
        }
      } catch (error) {
        throw error;
      }

      textarea.remove();
      return false;
    }

    $('li').unbind().on('click', (event) => {
      event.preventDefault();
      if (!copyAddress(event.target.textContent)) console.log('Copy failed'); // Do something
    });
  };

  deselect = () => {
    document.getSelection().removeAllRanges();
    this.setState({useImage: false});
  };

  handleOnChangeTemplate = (e) => {
    const templateFields = {...this.state.templateFields};

    templateFields[e.target.name] = e.target.value;
    this.setState({templateFields});
  }

  handleSubmitTemplate = () => {
    ShiftApp.getDepositInfo({
      OMSId: ShiftApp.oms.value,
      ProductId: this.props.ProductId || this.state.productId,
      DepositInfo: JSON.stringify(this.state.templateFields),
      AccountId: ShiftApp.selectedAccount.value,
      GenerateNewKey: !ShiftApp.config.optionalNewDepositKeys,
    });
    this.setState({ nextBtnClicked: true, loadingDeposits: true })
  }

  showMemoQRCode = () => {
    this.setState({ showMemoQRCode: true, showExchangeQRCode: false });
  }

  showExchangeQRCode = () => {
    this.setState({ showExchangeQRCode: true, showMemoQRCode: false });
  }

  render() {
    const headerTitle = `${ShiftApp.translation('DEPOSIT.DEPOSIT') || 'Deposit'} ${this.props.Product ? `(${this.props.Product})` : ''}`;

    function DropDown(el) {
      this.dd = el;
      this.placeholder = this.dd.children('span');
      this.opts = this.dd.find('ul.dropdown > li');
      this.val = '';
      this.index = -1;
      this.initEvents();
    }

    DropDown.prototype = {
      initEvents: () => {
        const obj = this;

        $(obj.dd).on('click', (e) => {
          e.preventDefault();
          $(this).toggleClass('active');
          return false;
        });

        $(obj.opts).on('click', (e) => {
          e.preventDefault();
          const opt = $(this);

          obj.val = opt.text();
          obj.index = opt.index();
          obj.placeholder.text(`Gender: ${obj.val}`);
        });
      },
      getValue() {
        return this.val;
      },
      getIndex() {
        return this.index;
      },
    };

    const addresses = [];
    const paymentIds = [];
    let depositAddress = '',
      selectedPaymentId = '',
      customDepositProduct = '',
      customDepositProductAddress = '';

    if (ShiftApp.config.customDepositAddress) {
      let product = this.props.Product;
      let addresses = Object.keys(ShiftApp.config.depositAddresses)
        .filter(function(key){ return key === product});

        addresses.map(key => {
          customDepositProductAddress = ShiftApp.config.depositAddresses[key];
          customDepositProduct = key
        });
    }

    if (ShiftApp.config.templateStyle === 'retail') {
      if (customDepositProduct) {
        depositAddress = customDepositProductAddress;
      } else {
        for (let i = 0; i < this.state.addressList.length; i++) {
          addresses.push(
            <li
              onMouseOver={this.copyMeFunction}
              onMouseLeave={this.deselect}
              value={this.state.addressList[i]}
              key={this.state.addressList[i]}
            >
              <a className="copy" href=""><i className="icon-user"/>{this.state.addressList[i]}</a>
            </li>,
          );
          if (this.state.addressList[i] === this.state.selected) {
            depositAddress = this.state.addressList[i];
          }
        }
      }
    } else if (customDepositProduct) {
      depositAddress = customDepositProductAddress;
    } else {
      for (let i = 0; i < this.state.addressList.length; i++) {
        addresses.push(
          <option value={this.state.addressList[i]} key={this.state.addressList[i]}>
            {this.state.addressList[i]}
          </option>,
        );
        if (this.state.addressList[i] === this.state.selected) {
          depositAddress = this.state.addressList[i];
        }
      }
    }

    if (this.state.paymentIds) {
      for (let i = 0; i < this.state.paymentIds.length; i++) {
        paymentIds.push(
          <option
            value={this.state.paymentIds[i]}
            key={this.state.paymentIds[i]}
          >{this.state.paymentIds[i]}</option>,
        );
        if (this.state.paymentIds[i] === this.state.selectedId) {
          selectedPaymentId = this.state.paymentIds[i];
        }
      }
    }

    let depositType = 'bitcoin';
    if (this.props.Product === 'EGD') depositType = 'egdcoin';
    if (this.props.Product === 'XFC') depositType = 'forevercoin';
    if (this.props.Product === 'ETH') depositType = 'ethereum';
    if (this.props.Product === 'QNT') depositType = 'qnt';

    const LoadingBlock = () => {
      const loadingWrapperStyle = {
        position: 'absolute',
        top: '0',
        right: '0',
        bottom: '0',
        left: '0',
        background: 'rgba(255, 255, 255, .7)',
        zIndex: 1,
      };
      const loadingContentStyle = {
        textAlign: 'center',
        position: 'relative',
        top: '50%',
        transform: 'translateY(-50%)',
      };

      return (
        <div style={loadingWrapperStyle}>
          <div style={loadingContentStyle}>
            {
              !this.state.depositRequestTimedOut ?
                <Spinner /> :
                <div>
                  <p>
                    {ShiftApp.translation('DEPOSIT.REQUEST_TIMEDOUT') || 'The server is taking too long to find an address. Please try again.'}
                  </p>
                  <button className="btn btn-action" onClick={() => location.reload(true)}>Reload page</button>
                </div>
            }
          </div>
        </div>
      );
    };

    if (ShiftApp.config.useDepositTemplates && this.state.showTemplate) {
      const fields = Object.keys(this.state.templateFields).map((fieldName) => (
        <InputLabeled
          onChange={this.handleOnChangeTemplate}
          name={fieldName}
          label={ShiftApp.translation(`DEPOSIT.TEMPLATE_FIELD_${fieldName}`) || fieldName}
        />
      ));

      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
        >
          { this.state.loadingDeposits && <LoadingBlock /> }
          <div className="pad">
            <p style={{marginBottom: '1.5rem'}}>{ShiftApp.translation('DEPOSIT.FILL_REQUIRED_FIELDS') || 'Please fill these required fields:'}</p>
            {fields}
            <div className="clearfix">
              <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? {width: '100%'} : null}>
                <button className="btn btn-action" onClick={this.handleSubmitTemplate}>
                  {ShiftApp.translation('BUTTONS.TEXT_NEXT') || 'Next'}
                </button>
              </div>
            </div>
          </div>
        </WidgetBase>
      );
    }

    if (this.props.Product === 'DAS' || this.props.Product === 'DASC') {

      return (
        <WidgetBase
          {...this.props}
          headerTitle={headerTitle}
        >
          { this.state.loadingDeposits && <LoadingBlock /> }  
          <div className="pad">
            <p style={{ marginBottom: '.5rem' }}>{ShiftApp.translation('DEPOSIT.FIELD_EXCHANGE_ACCOUNT') || 'Exchange Address'} <small>{ShiftApp.translation('DEPOSIT.DAS_EXCHANGE_ACCOUNT_WARNING') || '- Be sure to send to the exchange address indicated here'}</small></p>
            <InputNoLabel
              value={this.state.dasExchangeAccount}
              readOnly
              style={{ background: 'lightgray', maxWidth: '400px' }}
            />
            
            {!this.state.showExchangeQRCode ? <a style={{ marginLeft: '10px' }} onClick={this.showExchangeQRCode}>Show QR Code</a>  :
              <div style={{ minHeight: '130px' }}>
                <span style={{ float: 'left', borderRight: '1px solid #eee', paddingRight: '10px' }}>
                <QRCode
                  value={this.state.dasExchangeAccount}
                  size={128}
                />
                </span>
                <small style={{ float: 'left', marginBottom: '.5rem', marginLeft: '1rem', maxWidth: '250px' }}>
                  {ShiftApp.translation('DEPOSIT.DAS_EXCHANGE_ID_QR_CODE_INSTRUCTIONS') || 'Capture our Exchange ID by scanning this QR Code'}
                </small>
              </div>
            }
            

            <p style={{ marginBottom: '.5rem', clear: 'both', marginTop: '2rem' }}>
            {ShiftApp.translation('DEPOSIT.FIELD_YOUR_DAS_ACCOUNT') || 'Memo'} <small>{ShiftApp.translation('DEPOSIT.DAS_YOUR_ACCOUNT_WARNING') || '- Be sure to include the ID indicated here in your withdraw memo to ensure proper routing'}</small>
            </p>
            <InputNoLabel
              value={this.state.dasAccount}
              readOnly
              style={{ background: 'lightgray', maxWidth: '400px' }}
            />
            
            {!this.state.showMemoQRCode ? <a style={{ marginLeft: '10px' }} onClick={this.showMemoQRCode}>Show QR Code</a>  :
              <div style={{ minHeight: '130px' }}>
                <span style={{ float: 'left', borderRight: '1px solid #eee', paddingRight: '10px' }}>
                <QRCode
                  value={this.state.dasAccount}
                  size={128}
                />
                </span>
                <small style={{ float: 'left', marginBottom: '.5rem', marginLeft: '1rem', maxWidth: '250px' }}>
                  {ShiftApp.translation('DEPOSIT.DAS_MEMO_ID_QR_CODE_INSTRUCTIONS') || 'Capture your Memo ID by scanning this QR Code'}
                </small>
              </div>
            }
            <br />
            <br />
            {ShiftApp.config.templateStyle === "retail" &&
            <div className="clearfix" style={{ clear: 'both' }}>
                <button className="btn btn-action" onClick={this.props.close}>
                  {ShiftApp.translation('BUTTONS.CLOSE') || 'Close'}
                </button>
            </div>
            }
          </div>
        </WidgetBase>
      );
    }

    return (
      // wrap all content in widget base
      <WidgetBase
        {...this.props}
        login
        headerTitle={headerTitle}
      >
        { this.state.loadingDeposits && <LoadingBlock /> }
        <div className="pad">
          <div style={{marginBottom: '1.5rem'}}>
            <h3>{ShiftApp.translation('DEPOSIT.NB') || 'Please read the instructions below'}</h3>
            <p>
              {ShiftApp.translation('DEPOSIT.INSTRUCTION1') || `Depositing ${this.props.Product} into the exchange is safe and easy.`}
            </p>
            <p>
              {ShiftApp.translation('DEPOSIT.INSTRUCTION2') || `The address below can always be used to deposit ${this.props.Product} into your account.`}
            </p>
            <p>
              {ShiftApp.translation('DEPOSIT.INSTRUCTION3') || `Use your ${this.props.Product} client or wallet service to send the ${this.props.Product} to the address below.`}
              {this.props.Product === 'XRP' &&
              <strong>{ ShiftApp.translation('DEPOSIT.XRP_WARNING') || 'Be sure to include your destination tag to ensure proper routing.'}</strong>}
            </p>
          </div>

          {
            ShiftApp.config.siteTitle === 'Dasset' &&
            <p style={{margin: '1rem 0', fontWeight: 'bold'}}>
              Select an address below and enter it as the Recipient Address {this.props.Product === 'XRP' && `and populate the Destination
                Tag with your account ID: ${ShiftApp.selectedAccount.value}`}
            </p>
          }

          {
            ShiftApp.config.templateStyle !== 'retail' && !customDepositProduct &&
            <SelectLabeled
              placeholder={ShiftApp.translation('DEPOSIT.ADDRESS_LIST') || 'Address List'}
              onChange={this.addressChanged}
              readOnly
            >
              {addresses}
            </SelectLabeled>
          }

          {ShiftApp.config.templateStyle === 'retail' && !customDepositProduct &&
          <div>
            <CustomDropdown selectedAddress={this.state.selectedAddress}>
              {addresses}
            </CustomDropdown>
            {this.props.Product === 'XRP' &&
            <strong>
              {ShiftApp.translation('DEPOSIT.DESTINATION_TAG') || 'Destination Tag'}: {ShiftApp.selectedAccount.value}
            </strong>}
          </div>}

          {ShiftApp.config.templateStyle !== 'retail' && !customDepositProduct &&
          <span>
            <InputLabeled value={depositAddress} label={ShiftApp.translation('DEPOSIT.ADDRESS')}/>
          </span>}

          {ShiftApp.config.templateStyle !== 'retail' && !customDepositProduct && this.props.Product === 'XRP' &&
          <span>
            <InputLabeled
              value={ShiftApp.selectedAccount.value}
              label={ShiftApp.translation('DEPOSIT.DESTINATION_TAG') || 'Destination Tag'}
            />
          </span>}


          {customDepositProduct &&
          <span>
            <InputLabeled
              value={depositAddress}
            />
          </span>
          }

          {(this.props.Product === 'MON') &&
          <span>
            <SelectLabeled placeholder="Payment Ids List" onChange={this.paymentIdChanged}>
              {paymentIds}
            </SelectLabeled>
            <InputLabeled
              value={selectedPaymentId}
              label={ShiftApp.translation('DEPOSIT.PAYMENTIDS') || 'Payment Id'}
            />
          </span>}

          <div className="clearfix" style={{display: 'flex', marginBottom: '1rem'}}>
            <div>
              {depositAddress ?
                <div style={{margin: 10, float: 'left'}}>
                  <QRCode
                    value={`${depositType}:${customDepositProductAddress !== '' ? customDepositProductAddress : this.state.selected}`}
                    size={128}
                  />
                </div>
                :
                <img
                  alt="QR Code"
                  width="200px"
                  height="200px"
                  style={{margin: 10, float: 'left'}}
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                />}
            </div>
            <div>
              <p>
                {ShiftApp.translation('DEPOSIT.INSTRUCTION4') || `Your account will automatically update after the ${ShiftApp.config.siteName === 'quantatrading' ? 'Ethereum' : 'cryptocurrency'} network confirms your transaction. The confirmation may take up to 1 hour.`}
              </p>
              <p>
                {ShiftApp.translation('DEPOSIT.CRYPT_STEP1') || `1) Send ${ShiftApp.config.siteName === 'quantatrading' ? this.props.Product : 'cryptocurrency'} to this address.`}
              </p>
              <p>
                {ShiftApp.translation('DEPOSIT.CRYPT_STEP2') || '2) Your deposit will automatically be processed.'}
              </p>
            </div>
          </div>

          {this.props.close &&
          <div className="clearfix">
            <div className="pull-right" style={ShiftApp.config.siteName === 'aztec' ? {width: '100%'} : null}>
              {ShiftApp.config.optionalNewDepositKeys ?

                ShiftApp.config.disableNewDepositKeys.indexOf(this.props.Product) > -1 ? null :
                  <button
                    className="btn btn-action"
                    onClick={this.getNewDepositKey}
                    style={{float: 'left', marginRight: '1rem'}}
                  >Generate New Key</button>

                : null
              }
              <button
                className="btn btn-action"
                onClick={this.props.close}
              >{ShiftApp.translation('BUTTONS.TEXT_CLOSE') || 'Close'}</button>
            </div>
          </div>}
        </div>
      </WidgetBase>
    );
  }
}

DepositDigital.defaultProps = {
  close: () => {
  },
  Product: '',
  ProductId: null,
};

DepositDigital.propTypes = {
  close: React.PropTypes.func,
  Product: React.PropTypes.string,
  ProductId: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
  ]),
};

export default DepositDigital;
