export const coindata = {
    BTC: [
        {
            pair: "ETHBTC",
            priceChange: "0.00157400",
            change: "4.813",
            last_price: "0.03427600",
            high: "0.03483600",
            low: "0.03236000",
            volume: "255774.49200000"
        },
        {
            pair: "XRPBTC",
            priceChange: "0.00001980",
            change: "39.007",
            last_price: "0.00007056",
            high: "0.00007575",
            low: "0.00005036",
            volume: "312130089.00000000"
        },
        {
            pair: "BCCBTC",
            priceChange: "0.00362300",
            change: "5.427",
            last_price: "0.07038400",
            high: "0.07161000",
            low: "0.06615200",
            volume: "38716.89700000"
        },
        {
            pair: "EOSBTC",
            priceChange: "0.00006390",
            change: "7.852",
            last_price: "0.00087770",
            high: "0.00089620",
            low: "0.00081200",
            volume: "4473416.33000000"
        },
        {
            pair: "XLMBTC",
            priceChange: "0.00000439",
            change: "13.587",
            last_price: "0.00003670",
            high: "0.00003787",
            low: "0.00003211",
            volume: "127396884.00000000"
        },
        {
            pair: "LTCBTC",
            priceChange: "0.00029700",
            change: "3.526",
            last_price: "0.00872000",
            high: "0.00880000",
            low: "0.00835700",
            volume: "192341.37000000"
        },
        {
            pair: "NEOBTC",
            priceChange: "0.00011600",
            change: "4.330",
            last_price: "0.00279500",
            high: "0.00284300",
            low: "0.00265800",
            volume: "524202.95000000"
        },
        {
            pair: "ADABTC",
            priceChange: "0.00000149",
            change: "13.244",
            last_price: "0.00001274",
            high: "0.00001326",
            low: "0.00001114",
            volume: "296270525.00000000"
        },
        {
            pair: "XMRBTC",
            priceChange: "0.00037000",
            change: "2.123",
            last_price: "0.01779800",
            high: "0.01794200",
            low: "0.01730800",
            volume: "38810.36800000"
        },
        {
            pair: "IOTABTC",
            priceChange: "0.00000346",
            change: "4.175",
            last_price: "0.00008633",
            high: "0.00008890",
            low: "0.00008252",
            volume: "13092041.00000000"
        }
    ],
    ETH: [
        {
            pair: "XRPETH",
            priceChange: "0.00050702",
            change: "32.606",
            last_price: "0.00206202",
            high: "0.00229873",
            low: "0.00154180",
            volume: "33091957.00000000"
        },
        {
            pair: "BCCETH",
            priceChange: "0.00937000",
            change: "0.459",
            last_price: "2.05128000",
            high: "2.10364000",
            low: "2.02276000",
            volume: "1257.38700000"
        },
        {
            pair: "EOSETH",
            priceChange: "0.00071700",
            change: "2.882",
            last_price: "0.02559700",
            high: "0.02638300",
            low: "0.02481200",
            volume: "396067.14000000"
        },
        {
            pair: "XLMETH",
            priceChange: "0.00007967",
            change: "8.066",
            last_price: "0.00106737",
            high: "0.00113172",
            low: "0.00098375",
            volume: "5934696.00000000"
        },
        {
            pair: "LTCETH",
            priceChange: "-0.00409000",
            change: "-1.585",
            last_price: "0.25397000",
            high: "0.26012000",
            low: "0.24970000",
            volume: "9418.78000000"
        },
        {
            pair: "NEOETH",
            priceChange: "-0.00048900",
            change: "-0.596",
            last_price: "0.08151100",
            high: "0.08407700",
            low: "0.08033900",
            volume: "37296.99000000"
        },
        {
            pair: "ADAETH",
            priceChange: "0.00002659",
            change: "7.728",
            last_price: "0.00037066",
            high: "0.00038447",
            low: "0.00034246",
            volume: "34099444.00000000"
        },
        {
            pair: "XMRETH",
            priceChange: "-0.01653000",
            change: "-3.094",
            last_price: "0.51781000",
            high: "0.54018000",
            low: "0.51179000",
            volume: "1366.88300000"
        },
        {
            pair: "IOTAETH",
            priceChange: "-0.00001265",
            change: "-0.500",
            last_price: "0.00251919",
            high: "0.00264271",
            low: "0.00251000",
            volume: "731276.00000000"
        },
        {
            pair: "DASHETH",
            priceChange: "0.00120000",
            change: "0.132",
            last_price: "0.91350000",
            high: "0.92319000",
            low: "0.88399000",
            volume: "1188.80400000"
        }
    ],
    USDT: [
        {
            pair: "BTCUSDT",
            priceChange: "147.58000000",
            change: "2.309",
            last_price: "6539.01000000",
            high: "6540.00000000",
            low: "6325.00000000",
            volume: "25578.04300200"
        },
        {
            pair: "ETHUSDT",
            priceChange: "15.23000000",
            change: "7.287",
            last_price: "224.23000000",
            high: "226.68000000",
            low: "205.56000000",
            volume: "591103.24032000"
        },
        {
            pair: "XRPUSDT",
            priceChange: "0.13696000",
            change: "42.235",
            last_price: "0.46124000",
            high: "0.49200000",
            low: "0.32103000",
            volume: "313173136.50000000"
        },
        {
            pair: "BCCUSDT",
            priceChange: "33.86000000",
            change: "7.938",
            last_price: "460.42000000",
            high: "466.91000000",
            low: "410.00000000",
            volume: "61711.17424000"
        },
        {
            pair: "EOSUSDT",
            priceChange: "0.53610000",
            change: "10.313",
            last_price: "5.73430000",
            high: "5.85820000",
            low: "5.18380000",
            volume: "12534819.15000000"
        },
        {
            pair: "XLMUSDT",
            priceChange: "0.03316000",
            change: "16.032",
            last_price: "0.23999000",
            high: "0.24550000",
            low: "0.20500000",
            volume: "74836078.40000000"
        },
        {
            pair: "LTCUSDT",
            priceChange: "3.29000000",
            change: "6.112",
            last_price: "57.12000000",
            high: "57.42000000",
            low: "53.28000000",
            volume: "200584.29804000"
        },
        {
            pair: "ADAUSDT",
            priceChange: "0.01149000",
            change: "15.996",
            last_price: "0.08332000",
            high: "0.08659000",
            low: "0.07111000",
            volume: "363036369.00000000"
        },
        {
            pair: "IOTAUSDT",
            priceChange: "0.03630000",
            change: "6.865",
            last_price: "0.56510000",
            high: "0.58000000",
            low: "0.52330000",
            volume: "21758624.28000000"
        },
        {
            pair: "NEOUSDT",
            priceChange: "1.13000000",
            change: "6.594",
            last_price: "18.26800000",
            high: "18.57400000",
            low: "16.96400000",
            volume: "930030.66800000"
        }
    ]
}