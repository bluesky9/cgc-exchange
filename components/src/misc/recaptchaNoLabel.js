import React from 'react';
import Recaptcha from 'react-recaptcha';

class RecaptchaNoLabel extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      sitekey: props.sitekey ? props.sitekey : ( ShiftApp.config.reCaptcha_siteKey ? ShiftApp.config.reCaptcha_siteKey : APConfig.reCaptcha_siteKey)
    };
    this.setSiteKey = this.setSiteKey.bind(this);
  }

  setSiteKey = (sitekey) => {
    this.setState({sitekey: sitekey});
  };

  render() {
    if (!this.state.sitekey || this.state.sitekey === ''){
      return null;
    }
    return (
      <div className="form-group">
        <Recaptcha
          sitekey={this.state.sitekey}
          render={this.props.render}
          verifyCallback={this.props.verifyCallback}
          onloadCallback={this.props.onloadCallback}
        />
      </div>
    );
  }
}

RecaptchaNoLabel.defaultProps = {
  wrapperClass: '',
  placeholder: '',
  append: false,
  className: '',
  label: '',
  render: 'explicit'
};

RecaptchaNoLabel.propTypes = {
  wrapperClass: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  className: React.PropTypes.string,
  append: React.PropTypes.bool,
  label: React.PropTypes.string,
  render: React.PropTypes.string
};

export default RecaptchaNoLabel;
