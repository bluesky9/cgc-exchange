import React from 'react';

class ShiftSelectLabeled extends React.Component {
  render() {
    let props = { ...this.props };
    delete props.wrapperClass;

    return (
      <div className={`form-group ${this.props.wrapperClass}`}>
        <label htmlFor={this._rootNodeID}>
          {this.props.label || this.props.placeholder}
        </label>
        <select
          {...props}
          ref="input"
          className="form-control"
          id={this._rootNodeID}
          value={this.props.value}
          onChange={this.props.onChange}
        >
          {this.props.label || this.props.placeholder ? (
            <option value="" disabled>
              {this.props.label || this.props.placeholder}
            </option>
          ) : null}
          {this.props.children}
        </select>
      </div>
    );
  }
}

ShiftSelectLabeled.defaultProps = {
  wrapperClass: '',
  label: '',
  placeholder: '',
  children: null
};

ShiftSelectLabeled.propTypes = {
  wrapperClass: React.PropTypes.string,
  label: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  children: React.PropTypes.node
};

export default ShiftSelectLabeled;
