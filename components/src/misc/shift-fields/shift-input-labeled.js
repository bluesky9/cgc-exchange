import React from 'react';

class ShiftInputLabeled extends React.Component {
  render() {
    const { wrapperClass, append, ...props } = this.props;
    return (
      <div className={`form-group ${wrapperClass}`}>
        <label htmlFor={this._rootNodeID}>
          {this.props.label || this.props.placeholder}
        </label>
        <div className={append ? 'input-group' : ''}>
          <input
            {...props}
            ref="input"
            placeholder={this.props.placeholder}
            className={`form-control ${this.props.className}`}
            id={this._rootNodeID}
            name={this.props.name}
            onKeyPress={this.props.onKeyPress}
            onChange={this.props.onChange}
            onBlur={this.props.onBlur}
            value={this.props.value}
          />
          <label htmlFor={this._rootNodeID} className="error">
            {this.props.throwError ? this.props.errorDescription : null}
          </label>
          {this.props.append && (
            <span className="input-group-addon">{this.props.append}</span>
          )}
        </div>
      </div>
    );
  }
}

ShiftInputLabeled.defaultProps = {
  wrapperClass: '',
  placeholder: '',
  append: false,
  className: '',
  label: ''
};

ShiftInputLabeled.propTypes = {
  wrapperClass: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  className: React.PropTypes.string,
  append: React.PropTypes.bool,
  label: React.PropTypes.string
};

export default ShiftInputLabeled;
