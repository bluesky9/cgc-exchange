import React from 'react';

class ShiftSelect extends React.Component {
  state = {
    hasFocus: false
  };

  setValue = value => {
    this.refs.input.value = value;
  };

  onFocus = e => {
    this.setState({ hasFocus: true });
  };

  onBlur = e => {
    this.setState({ hasFocus: false });
  };

  onChange = e => this.props.onChange(e);

  render() {
    const props = { ...this.props };
    ['throwError', 'errorDescription', 'wrapperClass'].forEach(
      key => delete props[key]
    );

    return (
      <div
        className={`form-group ${this.props.wrapperClass} ${
          this.state.hasFocus ? 'input-has-focus' : ''
        }`}
      >
        <label htmlFor={this.props.label} />
        <span className="select-label-text">
          {this.props.label || this.props.placeholder}
        </span>
        <select
          {...props}
          name={this.props.label}
          onChange={this.onChange}
          ref="input"
          className="form-control select-input"
          id={this._rootNodeID}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        >
          {this.props.children}
        </select>
        <span className="input-error">
          {this.props.throwError
            ? ShiftApp.translation('DATE.ERROR') ||
              this.props.errorDescription
            : null}
        </span>
      </div>
    );
  }
}

ShiftSelect.defaultProps = {
  wrapperClass: '',
  label: '',
  placeholder: '',
  throwError: false,
  children: null,
  name: '',
  onChange: () => {}
};

ShiftSelect.propTypes = {
  wrapperClass: React.PropTypes.string,
  label: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  throwError: React.PropTypes.bool,
  children: React.PropTypes.node,
  name: React.PropTypes.string,
  onChange: React.PropTypes.func
};

export default ShiftSelect;
