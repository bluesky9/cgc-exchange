/* global ShiftApp */
import React from 'react';
import { formatNumberToLocale } from '../../widgets/helper';

const downTrend = {
    color: 'lightcoral',
    verticalAlign: 'bottom',
    marginLeft: '5px'
};

const upTrend = {
    color: 'lightgreen',
    verticalAlign: 'bottom',
    marginLeft: '5px'
};

const TickerBlockPxChange = props => {
    const pxChangeDirection = props.rolling24HrPxChange < 0 ? 
      (<i style={downTrend} className="material-icons">trending_down</i>) 
      : (<i style={upTrend} className="material-icons">trending_up</i>);
    
    // Getting original Px before the rolling24HrPxChange
    const pxBefore24HrChange = props.rolling24HrPxChange < 0 ? 
      (props.lastTradedPx / (100 - Math.abs(props.rolling24HrPxChange)) * 100)
      : ((props.lastTradedPx / (Math.abs(props.rolling24HrPxChange) + 100)) * 100);
    
    // PxChange in terms of product2 (converted from a percentage)
    const convertedPxChange = props.rolling24HrPxChange < 0 ? 
        formatNumberToLocale(pxBefore24HrChange - props.lastTradedPx, ShiftApp.config.tickerScrollingPxChangeDecimals || 2) || '-'
      : formatNumberToLocale(props.lastTradedPx - pxBefore24HrChange, ShiftApp.config.tickerScrollingPxChangeDecimals || 2) || '-';

    if(props.lastTradedPx > 0) {
        return (
            <div className="ticker-block">
                <i className={`cf cf-${props.cryptoFontSuffix}`} />
                <p className="ticker-symbol">{props.symbol} {pxChangeDirection}</p>
                <p className="ticker-rolling"> {(props.rolling24HrPxChange < 0) && '-'}{convertedPxChange} {props.product2Symbol} | {formatNumberToLocale(props.rolling24HrPxChange, ShiftApp.config.tickerScrollingPxChangeDecimals || 2)}%</p>
                <p className="ticker-last-trade">{props.lastTradedPx + ' '}
                    <span className="currency">
                        {props.product2Symbol}
                    </span></p>
                    <p className="ticker-session-high">High:
                        <span className="high">{' ' + props.sessionHigh} {props.showHighSymbol && props.product2Symbol}</span>
                    </p>
                <p>
                
                </p>
            </div>
        );
    }else{
        return (
            <div className="ticker-block" style={{display: "none"}}>
                <i className={`cf cf-${props.cryptoFontSuffix}`} />
                <p className="ticker-symbol">{props.symbol} {pxChangeDirection}
                <br />
                <span> {(props.rolling24HrPxChange < 0) && '-'}{convertedPxChange} {props.product2Symbol} | {formatNumberToLocale(props.rolling24HrPxChange, ShiftApp.config.tickerScrollingPxChangeDecimals || 2)} %</span></p>
                <p className="ticker-last-trade">{props.lastTradedPx + ' '}
                    <span className="currency">
                        {props.product2Symbol}
                    </span></p>
                    <p className="ticker-session-high">High:
                        <span className="high">{' ' + props.sessionHigh} {props.showHighSymbol && props.product2Symbol}</span>
                    </p>
                <p>
                
                </p>
            </div>
        );
    }
};


TickerBlockPxChange.defaultProps = {
    symbol: '',
    product2Symbol: '',
    lastTradedPx: '-',
    sessionHigh: '-',
    cryptoFontSuffix: '-',
};

TickerBlockPxChange.propTypes = {
    symbol: React.PropTypes.string,
    product2Symbol: React.PropTypes.string,
    lastTradedPx: React.PropTypes.number,
    sessionHigh: React.PropTypes.number,
    rolling24HrPxChange: React.PropTypes.number,
    cryptoFontSuffix: React.PropTypes.string,
};

export default TickerBlockPxChange;