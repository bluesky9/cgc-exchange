const GEO_LOOKUP_URL = 'https://geo.shiftcrypto.com/api/v1/countries';

class GeoLookup {
  /**
   * 
   * @param {String} token 
   */
  getCountry(token) {
    return fetch(GEO_LOOKUP_URL, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authtoken: token
      }
    }).then((response) => {
      return response.json();
    });
  }
}

export default GeoLookup;