/* global ShiftApp, $, window, alert, document */
/* eslint-disable react/no-multi-comp, no-alert */
import React from 'react';

export default class SynapseTestDashboard extends React.Component {
  state = {
    userId: '10',
    login: 'tester@tmail.com',
    phoneNumber: '818.555.5555',
    name: 'Test Man'
  };

  handleGetUserList = () => {
    fetch('https://synapse.shiftcrypto.com/api/users/get_list')
      .then(response => response.json())
      .then(json => console.log(json));
  };

  handleLoginChange = e => this.setState({ login: e.target.value });

  handlePhoneNumberChange = e => this.setState({ phoneNumber: e.target.value });

  handleNameChange = e => this.setState({ name: e.target.value });

  handleCreateUser = e => {
    fetch('https://synapse.shiftcrypto.com/api/users/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        logins: [{ email: this.state.login }],
        phone_numbers: [this.state.phoneNumber, this.state.login],
        legal_names: [this.state.name],
        extra: {
          supp_id: '122eddfgbeafrfvbbb',
          cip_tag: 1,
          is_business: false
        }
      })
    })
      .then(response => response.json())
      .then(json => console.log(json));
  };

  handleUserIdChange = e => this.setState({ userId: e.target.value });

  handleGetUser = () => {
    fetch(
      `https://synapse.shiftcrypto.com/api/users/get?user_id=${
        this.state.userId
      }`
    )
      .then(response => response.json())
      .then(json => console.log(json));
  };

  handleAddDocs = () => {
    fetch('', {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        user_id: this.state.userId,
        documents: [
          {
            email: '12345@test.com',
            phone_number: '901.111.1111',
            ip: '::1',
            name: 'Test User',
            alias: 'Test',
            entity_type: 'M',
            entity_scope: 'Arts & Entertainment',
            day: 2,
            month: 5,
            year: 1989,
            address_street: '1 Market St.',
            address_city: 'SF',
            address_subdivision: 'CA',
            address_postal_code: '94114',
            address_country_code: 'US',
            virtual_docs: [
              {
                document_value: '2222',
                document_type: 'SSN'
              }
            ],
            physical_docs: [
              {
                document_value: 'data:image/gif;base64,SUQs==',
                document_type: 'GOVT_ID'
              }
            ],
            social_docs: [
              {
                document_value: 'https://www.facebook.com/valid',
                document_type: 'FACEBOOK'
              }
            ]
          }
        ]
      })
    })
      .then(response => response.json())
      .then(json => console.log(json));
  };

  handleDeposit = () => {
    fetch(
      `https://synapse.shiftcrypto.com/api/transactions/deposit?user_id=${
        this.state.userId
      }`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      }
    ).then(response => response.json()).then(deposit => {
      console.log(deposit);
    });
  };

  render() {
    return (
      <div>
        <button onClick={this.handleGetUserList}>Get User List</button>
        <div>
          <input
            type="text"
            onChange={this.handleLoginChange}
            value={this.state.login}
          />
          <p>Login (email)</p>
          <input
            type="text"
            onChange={this.handlePhoneNumberChange}
            value={this.state.phoneNumber}
          />
          <p>Phone Number</p>
          <input
            type="text"
            onChange={this.handleNameChange}
            value={this.state.name}
          />
          <p>Name</p>
          <button onClick={this.handleCreateUser}>Create User</button>
        </div>
        <div>
          <div>
            <input
              type="text"
              onChange={this.handleUserIdChange}
              value={this.state.userId}
            />
            <p>User ID</p>
          </div>
          <button onClick={this.handleGetUser}>Get User</button>
          <button onClick={this.handleAddDocs}>Add Docs</button>
        </div>
        <div>
          <button onClick={this.handleDeposit}>Make a Deposit</button>
        </div>
      </div>
    );
  }
}
