import React from 'react';

class InputNoLabel extends React.Component {
  setValue = value => {
    this.refs.input.value = value;
  };

  value = () => this.refs.input.value;

  render() {
    return (
      <div className={`form-group ${this.props.wrapperClass}`}>
        {ShiftApp.config.v2Widgets ?
          <input
            {...this.props}
            ref="input"
            className={`form-control ${this.props.className}`}
            id={this._rootNodeID}
            name={this.props.name}
            onChange={this.props.onChange}
          /> :
          <input
            {...this.props}
            ref="input"
            className={`form-control ${this.props.className}`}
            id={this._rootNodeID}
            name={this.props.name}
          />
        }
        {this.props.append &&
          <span className="input-group-addon">
            {this.props.append}
          </span>}
      </div>
    );
  }
}

InputNoLabel.defaultProps = {
  wrapperClass: '',
  className: '',
  append: false,
};

InputNoLabel.propTypes = {
  wrapperClass: React.PropTypes.string,
  className: React.PropTypes.string,
  append: React.PropTypes.bool,
};

export default InputNoLabel;
