/* global ShiftApp */

const Messages = {
    email: (ShiftApp.translation('VALIDATION_MESSAGES.EMAIL') || 'Please enter a valid email'),
    required: (ShiftApp.translation('VALIDATION_MESSAGES.REQUIRED') || 'This field is required'),
    alphanumeric: (ShiftApp.translation('VALIDATION_MESSAGES.ALPHANUMERIC') || 'Only letters and numbers allowed'),
    integer: (ShiftApp.translation('VALIDATION_MESSAGES.INTEGER') || 'Please enter a valid number'),
};

export default Messages;