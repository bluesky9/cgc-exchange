// Common place for all validating function to be re used across componentns

const Validate = {
  email: value => {
    // some email validation
    const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    return emailRegex.test(value);
  },

  required: value => {
    // some required validation
    return value && value.trim();
  },

  alphanumeric: value => {
    // some alphanumeric validation
    const alphanumericRegex = /^[a-zA-Z0-9]*$/;

    return alphanumericRegex.test(value);
  },

  integer: value => {
    // some interger valdation
    const integerRegex = /^[0-9]*$/;

    return integerRegex.test(value);
  },

  imageFileType: file => {
    return ShiftApp.config.aws.acceptedFileTypes.includes(file.type);
  },

  imageFileSize: file => {
    // max 3 MB
    return parseInt(file.size) < 3000000;
  }
};

export default Validate;
