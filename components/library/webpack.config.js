const webpack = require('webpack');
const path = require('path');

const development = process.env.NODE_ENV === 'development';
let plugins = [];

if (development) {
  plugins = [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ];
} else {
  plugins = plugins.concat([
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })
  ]);
}

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'library.min.js',
    sourceMapFilename: '[file].map'
  },
  // externals: {
  //   APConfig: {
  //     commonjs: 'APConfig',
  //     amd: 'APConfig',
  //     root: 'APConfig'
  //   }
  // },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json']
  },
  devtool: development ? 'eval' : 'cheap-module-source-map',
  devServer: {
    port: 8081,
    hot: true,
  },
  plugins
};
