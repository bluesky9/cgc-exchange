/* global document, ShiftApp */
import logs from './logs';

function transferFunds(requestPayload = {}) {
  logs.socketOpen
    // prettier-ignore
    .filter(open => open)
    .take(1)
    .subscribe(() => {
      document.APAPI.RPCCall('TransferFunds', requestPayload, data => {
        if (data.result) {
          ShiftApp.getTransfers(requestPayload.SenderAccountId);
          ShiftApp.getTransfersReceived(requestPayload.SenderAccountId);
        }
        ShiftApp.transfunds.onNext(data);
      });
    });
}

export default transferFunds;
