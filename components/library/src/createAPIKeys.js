/* global document, ShiftApp */
import logs from './logs';

// prettier-ignore
function createAPIKey(requestPayload = {}) {
  // console.log('ShiftApp.userID', ShiftApp.userData);
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('AddUserAPIKey', requestPayload, (data) => ShiftApp.addApiKey.onNext(data));
  });
}

export default createAPIKey;
