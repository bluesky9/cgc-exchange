import logs from './logs';

function getUserAccounts(AccountId) {
  const requestPayload = {
    AccountId,
    OMSId: ShiftApp.oms.value,
  };

  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetUserAccounts', requestPayload, (data) => {
      const selectedAccount = document.APAPI.Session.selectedAccount;

      ShiftApp.userAccounts.onNext(data);
      data.forEach((accountId) => {
        ShiftApp.getOpenOrders(accountId);
        ShiftApp.getAccountPositions(accountId);
        ShiftApp.getAccountTrades(accountId);
        ShiftApp.getAccountTransactions(accountId);
        ShiftApp.subscribeAccountEvents(accountId);
        ShiftApp.getOrderHistory(accountId);
        ShiftApp.getRequestTransfers(accountId);
      });
      ShiftApp.getAccountInfo({ AccountId: document.APAPI.Session.selectedAccount, OMSId: ShiftApp.oms.value });
      ShiftApp.getAPIKey({ UserId: ShiftApp.userData.value.UserId });
      ShiftApp.getUserCon({ UserId: ShiftApp.userData.value.UserId });
      if (ShiftApp.userAccounts.value) ShiftApp.synched.onNext(true);
    });
  });
};

export default getUserAccounts;
