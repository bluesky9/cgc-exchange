/* global document, ShiftApp, APConfig */
import logs from './logs';

function getProducts() {
  if (!ShiftApp.oms.value) return;
  const requestPayload = { OMSId: ShiftApp.oms.value };

  // prettier-ignore
  logs.socketOpen
  .filter((open) => open)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetProducts', requestPayload, (rawData) => {
      const excluded = APConfig.excludedProducts || [];
      const data = rawData.filter(prod => !excluded.includes(prod.Product));
      ShiftApp.products.onNext(data);
    });
  });
}

export default getProducts;
