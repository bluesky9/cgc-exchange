import logs from './logs';

function setUserInfo(requestPayload = {}) {
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('SetUserInfo', requestPayload, (data) => {
      ShiftApp.setUser.onNext(data);
      ShiftApp.getUser.onNext({ ...ShiftApp.getUser.value, ...data});
    });
  });
};

export default setUserInfo;
