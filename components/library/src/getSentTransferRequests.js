/* global ShiftApp, document */
import logs from './logs';

function getSentTransferRequests(accountId) {
  const requestPayload = {
    OMSId: ShiftApp.oms.value,
    OperatorId: ShiftApp.config.OperatorId,
    RequestorAccountId: accountId
    // Status: 0,
  };

  logs.socketOpen.filter(open => open).take(1).subscribe(() => {
    document.APAPI.RPCCall(
      'GetRequestTransferRequestsRequested',
      requestPayload,
      data => {
        // ShiftApp.sentTransferRequests.onNext([
        //   ...data.filter(req => req.RequestorAccountId === requestPayload.RequestorAccountId),
        // ]);
        ShiftApp.sentTransferRequests.onNext({
          ...ShiftApp.sentTransferRequests.value,
          [accountId]: data.filter(
            req => req.RequestorAccountId === requestPayload.RequestorAccountId
          )
        });
      }
    );
  });
}

export default getSentTransferRequests;
