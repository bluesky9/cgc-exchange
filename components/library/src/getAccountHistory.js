/* global document, ShiftApp */
import logs from './logs';

function getAccountHistory(requestPayload) {
  var requestPayload = {
    AccountId: ShiftApp.userAccounts.value,
    OMSId: ShiftApp.oms.value
  };

  // prettier-ignore
  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetAccountHistory', requestPayload, (data) => {
      ShiftApp.accountHistory.onNext(data);
    });
  });
}

export default getAccountHistory;
