/* global ShiftApp, document */
import logs from './logs';

function getTransfers(accountId) {
  const requestPayload = {
    OMSId: ShiftApp.oms.value,
    OperatorId: ShiftApp.config.OperatorId,
    AccountId: accountId
    // Status: 0,
  };

  logs.socketOpen.filter(open => open).take(1).subscribe(() => {
    document.APAPI.RPCCall('GetTransfers', requestPayload, data => {
      // ShiftApp.sentTransfers.onNext(data);
      ShiftApp.sentTransfers.onNext({
        ...ShiftApp.sentTransfers.value,
        [accountId]: data
      });
    });
  });
}

export default getTransfers;
