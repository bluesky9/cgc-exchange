/* global ShiftApp, document */
import logs from './logs';

function getReceivedTransferRequests(accountId) {
  const requestPayload = {
    OMSId: ShiftApp.oms.value,
    OperatorId: ShiftApp.config.OperatorId,
    PayerAccountId: accountId
    // Status: 0,
  };

  logs.socketOpen.filter(open => open).take(1).subscribe(() => {
    document.APAPI.RPCCall(
      'GetRequestTransferRequestsReceived',
      requestPayload,
      data => {
        // ShiftApp.receivedTransferRequests.onNext([
        //   ...data.filter(req => req.PayerAccountId === requestPayload.PayerAccountId),
        // ]);
        ShiftApp.receivedTransferRequests.onNext({
          ...ShiftApp.receivedTransferRequests.value,
          [accountId]: data.filter(
            req => req.PayerAccountId === requestPayload.PayerAccountId
          )
        });
      }
    );
  });
}

export default getReceivedTransferRequests;
