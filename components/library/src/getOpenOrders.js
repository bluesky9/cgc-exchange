import logs from './logs';

function getOpenOrders(accountId) {
  if (!ShiftApp.oms.value || !ShiftApp.userAccounts.value) return;
  const requestPayload = {
    AccountId: accountId,
    OMSId: ShiftApp.oms.value,
  };

  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetOpenOrders', requestPayload, (data) => {
      ShiftApp.openorders.onNext(ShiftApp.openorders.value.concat(data));
    });
  });
};

export default getOpenOrders;
