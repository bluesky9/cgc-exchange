/* global ShiftApp, document */
import logs from './logs';

function getAccountsInfo(payload = {}) {
  logs.session.filter(open => open.SessionToken).subscribe(() => {
    document.APAPI.RPCCall('GetAccountInfo', payload, data => {
      ShiftApp.config.debug && console.log('GetAccountInfo', data); // eslint-disable-line
      ShiftApp.userAccountsInfo.onNext([
        ...ShiftApp.userAccountsInfo.value,
        data
      ]);
    });
  });
}

export default getAccountsInfo;
