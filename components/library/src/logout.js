import config from './config';
import logs from './logs.js';

function reset() {
  logs.session.onNext({})
}

function logout(data = {}) {
  localStorage.setItem('SessionToken', '');
  if (data.SessionToken) {
    logs.session.onNext({})
    document.location = ShiftApp.config.logoutRedirect;
  } else {
    reset();
    document.location = ShiftApp.config.logoutRedirect;
  }
};

export default logout;
