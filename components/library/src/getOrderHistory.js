/* global document, ShiftApp */
import logs from './logs';

function getOrderHistory(accountId) {
  const requestPayload = {
    AccountId: accountId,
    OMSId: ShiftApp.oms.value,
    Depth: 1000
  };

  // prettier-ignore
  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetOrderHistory', requestPayload, (data) => {
      ShiftApp.orderHistory.onNext(ShiftApp.orderHistory.value.concat(data));
    });
  });
}

export default getOrderHistory;
