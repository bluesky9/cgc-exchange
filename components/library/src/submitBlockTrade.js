import logs from './logs';

function submitBlockTrade(data = {}) {
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('SubmitBlockTrade', data, (data) => ShiftApp.submitBlockTradeEvent.onNext(data));
  });
};

export default submitBlockTrade;
