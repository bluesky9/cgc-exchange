/* global document, ShiftApp, $, window */
import format from './formatOrders';
import { updateArrayByPx, formatNumberToLocale } from './helper';

const level1Update = [];

export default function init() {
  const marketDataWS = ShiftApp.config.MarketDataWS
    ? document.MarketDataWS
    : document.APAPI;

  if (ShiftApp.config.useRedirect) {
    ShiftApp.redirect(window.location.pathname);
  }
  document.APAPI.SubscribeToEvent('Logout', data => {
    ShiftApp.logoutV2.onNext(data);
    if (data.result) ShiftApp.logout();
  });
  marketDataWS.SubscribeToEvent('SubscribeLevel1', data => {
    ShiftApp.tickerBook.onNext(data);
  });
  marketDataWS.SubscribeToEvent('SubscribeLevel2', data => {
    ShiftApp.subscribe2.onNext(data);
    ShiftApp.Level2.onNext(data);
  });

  document.APAPI.SubscribeToEvent('RegisterNewUser', data => {
    ShiftApp.registerUser.onNext(data);
  });

  document.APAPI.SubscribeToEvent('GetUserInfo', data => {
    ShiftApp.getUser.onNext(data);
  });
  document.APAPI.SubscribeToEvent('GetUserConfig', data => {
    ShiftApp.getUserConfig.onNext(data);
  });

  document.APAPI.SubscribeToEvent('GetTickerHistory', data => {
    ShiftApp.getTickerHist.onNext(data);
  });

  document.APAPI.SubscribeToEvent('RequestVerifyEmail', data => {
    ShiftApp.reqVerifyEmail.onNext(data);
  });

  document.APAPI.SubscribeToEvent('ValidateUserRegistration', data => {
    ShiftApp.validatorResponse.onNext(data);
  });

  marketDataWS.SubscribeToEvent('Level1UpdateEvent', data => {
    ShiftApp.Level1.onNext({
      ...ShiftApp.Level1.value,
      [data.InstrumentId]: data
    });
    ShiftApp.tickerBook.onNext(data);

    const orderBook = ShiftApp.orderBook.value;
    level1Update.push(data);
    ShiftApp.subscribe1.onNext(level1Update);
    if (!orderBook[data.InstrumentId]) orderBook[data.InstrumentId] = {};
    orderBook[data.InstrumentId].ticker = data;
    ShiftApp.orderBook.onNext(orderBook);
  });

  marketDataWS.SubscribeToEvent('Level2UpdateEvent', data => {
    const orderBook = ShiftApp.orderBook.value;
    const orders = format.orders(data);
    let buys;
    let sells;

    if (ShiftApp.config.L2UpdateMethod === 2) {
      return ShiftApp.Level2Update.onNext(data);
    }

    if (orders.length > 0) {
      if (!orderBook[orders[0].ProductPairCode]) {
        orderBook[orders[0].ProductPairCode] = {};
      }

      buys = orders.filter(trade => trade.Side === 0);
      sells = orders.filter(trade => trade.Side === 1);
    }

    if (buys.length > 0) {
      if (orderBook[buys[0].ProductPairCode].buys) {
        buys.forEach(buy => {
          orderBook[buys[0].ProductPairCode].buys = updateArrayByPx(
            (orderBook[buys[0].ProductPairCode] || {}).buys,
            buy
          );
        });
      } else {
        orderBook[buys[0].ProductPairCode].buys = buys;
      }

      orderBook[buys[0].ProductPairCode].buys.sort((a, b) => {
        if (a.Price < b.Price) return 1;
        if (a.Price > b.Price) return -1;
        return 0;
      });
    }

    if (sells.length > 0) {
      if (orderBook[sells[0].ProductPairCode].sells) {
        sells.forEach(sell => {
          (orderBook[sells[0].ProductPairCode] || {}).sells = updateArrayByPx(
            (orderBook[sells[0].ProductPairCode] || {}).sells,
            sell
          );
        });
      } else {
        orderBook[sells[0].ProductPairCode].sells = sells;
      }

      orderBook[sells[0].ProductPairCode].sells.sort((a, b) => {
        if (a.Price > b.Price) return 1;
        if (a.Price < b.Price) return -1;
        return 0;
      });
    }

    return ShiftApp.orderBook.onNext(orderBook);
  });

  document.APAPI.SubscribeToEvent('OrderTradeEvent', data => {
    const newTrades = ShiftApp.accountTrades.value;
    const quantity = `${data.Quantity}`.includes('-')
      ? formatNumberToLocale(data.Quantity, 8) // TODO: work in dynamic decimal places for product traded
      : data.Quantity;

    const { TradeTimeMS, TradeTime } = data;
    const trade = data;
    if (TradeTimeMS < 0) {
      trade.TradeTimeMS = TradeTime;
      trade.TradeTime = TradeTimeMS;
    }

    newTrades.push(trade);
    ShiftApp.accountTrades.onNext(newTrades);
    $.bootstrapGrowl(
      `Order ${data.OrderId} ${data.Side} ${quantity} @ ${data.Price}`,
      { ...ShiftApp.config.growlerDefaultOptions, type: 'success' }
    );
  });

  document.APAPI.SubscribeToEvent('OrderStateEvent', data => {
    ShiftApp.accountBalances.onNext(data);

    if (data.OrderType === 'BlockTrade') {
      if (data.OrderState === 'Canceled') {
        const update = {
          ...ShiftApp.tradeReports.value,
          [data.Account]: ShiftApp.tradeReports.value[data.Account].filter(
            order => order.OrderId !== data.OrderId
          )
        };

        return ShiftApp.tradeReports.onNext(update);
      }
      return false;
    }

    if (data.OrderState === 'Canceled') {
      data.CancelReason = data.ChangeReason;
      const newOrders = ShiftApp.openorders.value.filter(
        order => order.OrderId !== data.OrderId
      );

      ShiftApp.orderHistory.onNext(
        ShiftApp.orderHistory.value.concat(data)
      );

      ShiftApp.openorders.onNext(newOrders);
    }

    if (data.OrderState === 'Rejected') {
      ShiftApp.rejectedOrders.onNext(data);
      ShiftApp.orderHistory.onNext(
        ShiftApp.orderHistory.value.concat(data)
      );
    }

    if (data.OrderState === 'Working') {
      const newOrders = ShiftApp.openorders.value.filter(
        order => order.OrderId !== data.OrderId
      );

      newOrders.push(data);
      ShiftApp.openorders.onNext(newOrders);
    }

    if (data.OrderState === 'FullyExecuted') {
      const newOrders = ShiftApp.openorders.value.filter(
        order => order.OrderId !== data.OrderId
      );

      ShiftApp.openorders.onNext(newOrders);
    }
    return true;
  });

  document.APAPI.SubscribeToEvent('AccountPositionEvent', data => {
    const oldPositions = [].concat(ShiftApp.accountPositions.value);
    const newPositions = oldPositions.map((balance, index) => {
      if (
        data.AccountId === balance.AccountId &&
        data.ProductId === balance.ProductId
      ) {
        return data;
      }
      return balance;
    });

    ShiftApp.accountPositions.onNext(newPositions);
  });

  document.APAPI.SubscribeToEvent('NewOrderRejectEvent', data => {
    ShiftApp.rejectedOrders.onNext(data);
  });

  document.APAPI.SubscribeToEvent('MarketStateUpdate', data => {
    const instrument = ShiftApp.instruments.value.find(
      inst => inst.InstrumentId === data.VenueInstrumentId
    );

    if (data.Action === 'ReSync') return false;
    return $.bootstrapGrowl(
      `Market for ${instrument.Symbol} is ${data.NewStatus}`,
      { ...ShiftApp.config.growlerDefaultOptions, type: 'info' }
    );
  });

  marketDataWS.SubscribeToEvent('TradeDataUpdateEvent', data => {
    const trades = format.trades(data) || [];
    const orderBook = ShiftApp.orderBook.value;

    if (trades.length) {
      if (!orderBook[trades[0].ProductPairCode])
        orderBook[trades[0].ProductPairCode] = {};
      trades.forEach(trade => {
        if (orderBook[trades[0].ProductPairCode].trades) {
          return orderBook[trades[0].ProductPairCode].trades.push(trade);
        }
        orderBook[trades[0].ProductPairCode].trades = [].concat(trade);
        return true;
      });
    }
    ShiftApp.orderBook.onNext(orderBook);
  });

  document.APAPI.SubscribeToEvent('LogoutEvent', () => {
    // ShiftApp.retryLogin();
    ShiftApp.logout();
  });

  document.APAPI.SubscribeToEvent('VerificationLevelUpdateEvent', data => {
    const growlerOptions = {
      allow_dismiss: true,
      align: 'center',
      delay: ShiftApp.config.growlerDelay,
      offset: { from: 'top', amount: 30 },
      left: '60%'
    };
    if (data.VerificationStatus === 'Approved') {

      if (data.VerificationLevel === 2 && ShiftApp.config.kycType === 'IM') return;

      $.bootstrapGrowl(
        `Your verification has been ${data.VerificationStatus}`,
        { ...growlerOptions, type: 'success' }
      );
      $.bootstrapGrowl(`You are now level ${data.VerificationLevel} verified`, {
        ...growlerOptions,
        type: 'success'
      });
    }
    if (data.VerificationStatus !== 'Approved') {
      $.bootstrapGrowl(data.VerificationStatus, {
        ...growlerOptions,
        type: 'success'
      });
    }
    ShiftApp.verificationLevelUpdate.onNext(data);
  });
}
