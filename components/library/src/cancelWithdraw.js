/* global document, ShiftApp */
import logs from './logs';

// prettier-ignore
function cancelWithdraw(payload = {}) {
  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe(() => {
    document.APAPI.RPCCall('CancelWithdraw', payload, (data) => {
      ShiftApp.canceledWithdraw.onNext(data);
    });
  });
}

export default cancelWithdraw;
