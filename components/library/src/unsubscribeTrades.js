/* global document, ShiftApp */
import logs from './logs';

function unsubscribeTradesCall(instrumentId, callback) {
  let InstrumentId;

  if (!instrumentId) {
    ShiftApp.instruments.subscribe(data => {
      InstrumentId = data.length && data[0].InstrumentId;
    });
  } else {
    InstrumentId = instrumentId;
  }

  const requestPayload = { OMSId: ShiftApp.oms.value, InstrumentId };

  logs.socketOpen
    // prettier-ignore
    .filter(open => open)
    .take(1)
    .subscribe(open => {
      const marketDataWS = document.MarketDataWS || document.APAPI;

      marketDataWS.RPCCall('UnsubscribeTrades', requestPayload, callback);
    });
}

export default unsubscribeTradesCall;
