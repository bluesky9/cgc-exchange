/* global document, ShiftApp, APConfig, window */
import logs from './logs';

function getInstruments() {
  const requestPayload = { OMSId: ShiftApp.oms.value };

  // prettier-ignore
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe(() => {
    document.APAPI.RPCCall('GetInstruments', requestPayload, (raw) => {
      const excluded = APConfig.excludedInstruments || [];
      const data = raw.filter(ins => !excluded.includes(ins.Symbol)) || raw;
      ShiftApp.instruments.onNext(data);
      data.forEach((ins) => {
        const orderBook = ShiftApp.orderBook.value;
        let tickerBook = ShiftApp.tickerBook.value;

        if (!orderBook[ins.InstrumentId]) orderBook[ins.InstrumentId] = {};
        orderBook[ins.InstrumentId].Symbol = ins.Symbol;
        orderBook[ins.InstrumentId].InstrumentId = ins.InstrumentId;
        ShiftApp.orderBook.onNext(orderBook);

        if (!tickerBook) tickerBook= [];
        ShiftApp.tickerBook.onNext(tickerBook);

      });
    });
  });
}

export default getInstruments;
