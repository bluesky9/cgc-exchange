/* global document */
import logs from './logs';
import ShiftApp from './library';

// prettier-ignore
function getWithdrawFee(requestPayload = {}) {
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe(() => {
    document.APAPI.RPCCall(
      'GetWithdrawFee',
      requestPayload,
      data => ShiftApp.withdrawFee.onNext(data.FeeAmount))
  });
}

export default getWithdrawFee;
