/* global document, ShiftApp */
import logs from './logs';

// prettier-ignore
function disable2FA(requestPayload = {}) {
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe(() => {
    document.APAPI.RPCCall('Disable2FA', requestPayload, (data) => {
      ShiftApp.Disable2FA.onNext(data);
    });
  });
}

export default disable2FA;
