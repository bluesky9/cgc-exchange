/* global ShiftApp, document */
import logs from './logs';

function getTransfersReceived(accountId) {
  const requestPayload = {
    OMSId: ShiftApp.oms.value,
    OperatorId: ShiftApp.config.OperatorId,
    AccountId: accountId
    // Status: 0,
  };

  logs.socketOpen.filter(open => open).take(1).subscribe(() => {
    document.APAPI.RPCCall('GetTransfersReceived', requestPayload, data => {
      // ShiftApp.receivedTransfers.onNext(data);
      ShiftApp.receivedTransfers.onNext({
        ...ShiftApp.receivedTransfers.value,
        [accountId]: data
      });
    });
  });
}

export default getTransfersReceived;
