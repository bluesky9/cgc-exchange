import logs from './logs';

function getAccountInfo(payload = {}) {
  logs.session
  .filter(open => open.SessionToken)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetAccountInfo', payload, (data) => {
      ShiftApp.config.debug && console.log('GetAccountInfo', data);
      ShiftApp.accountInfo.onNext(data);
    });
  });
};

export default getAccountInfo;
