/* global ShiftApp, document */
import logs from './logs';

function getOperatorLoyaltyFeeConfigsForOms(payload = {}) {
  logs.session.filter(open => open.SessionToken).subscribe(() => {
    document.APAPI.RPCCall(
      'GetOperatorLoyaltyFeeConfigsForOms',
      payload,
      data => {
        if (ShiftApp.config.debug) {
          console.log('GetOperatorLoyaltyFeeConfigsForOms', data);
        }
        ShiftApp.loyaltyFeeConfigs.onNext(data);
      }
    );
  });
}

export default getOperatorLoyaltyFeeConfigsForOms;
