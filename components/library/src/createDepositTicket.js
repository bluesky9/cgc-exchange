/* global document, ShiftApp */
import logs from './logs';

function createDepositTicket(data) {
  const requestPayload = {
    OMSId: ShiftApp.oms.value,
    accountId: data.accountId,
    assetId: data.productId,
    assetName: data.currencyCode,
    amount: data.amount,
    RequestUser: data.accountId,
    OperatorId: ShiftApp.config.OperatorId,
    Status: data.status,
    DepositInfo: data.depositInfo
  };

  // prettier-ignore
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe(() => {
    document.APAPI.RPCCall('createDepositTicket', requestPayload, (data) => {
      ShiftApp.createDeposit.onNext(data);
    });
  });
}

export default createDepositTicket;
