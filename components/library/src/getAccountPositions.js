import logs from './logs';

function getAccountPositions(accountId) {
  if (!ShiftApp.oms.value) return;

  const requestPayload = {
    AccountId: accountId,
    OMSId: ShiftApp.oms.value,
  };

  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetAccountPositions', requestPayload, (data) => {
      ShiftApp.accountPositions.onNext(ShiftApp.accountPositions.value.concat(data));
    });
  });
};


export default getAccountPositions;
