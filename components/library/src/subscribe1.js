import logs from './logs';

function subscribeLvl1(payload, callback = () => {}) {
  const InstrumentId = payload;
  const requestPayload = { OMSId: ShiftApp.oms.value, InstrumentId: InstrumentId };

  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe((open) => {
    const marketDataWS = document.MarketDataWS || document.APAPI;

    marketDataWS.RPCCall('SubscribeLevel1', requestPayload, (data) => {
      if (data.InstrumentId) {
        ShiftApp.Level1.onNext({ ...ShiftApp.Level1.value, [data.InstrumentId]: data });
        callback();
      }

      const level1Trades = [];
      let tickerBook = ShiftApp.tickerBook.value;

      level1Trades.push(data);
      ShiftApp.subscribe1.onNext(level1Trades);
      if (!tickerBook) tickerBook = [];
      tickerBook.push(data)
      ShiftApp.tickerBook.onNext(tickerBook);
    });
  });
};

export default subscribeLvl1;
