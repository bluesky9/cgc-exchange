import logs from './logs';

function getDepositInfo(requestPayload = {}) {
  logs.socketOpen
  .filter(open => open)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetDepositInfo', requestPayload, (data) => {
      const keys = data.DepositInfo && JSON.parse(data.DepositInfo);

      ShiftApp.keys.onNext(keys);
      ShiftApp.deposits.onNext(data);
    });
  });
};

export default getDepositInfo;
