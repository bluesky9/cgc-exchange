import logs from './logs';

function getAccountTrades(accountId) {
  const requestPayload = {
    AccountId: accountId,
    OMSId: ShiftApp.oms.value,
    StartIndex: 0,
    Count: 100,
  };

  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetAccountTrades', requestPayload, (data) => {
      ShiftApp.accountTrades.onNext(ShiftApp.accountTrades.value.concat(data));
    });
  });
};

export default getAccountTrades;
