import logs from './logs';

function getUserReportTickets() {
  const requestPayload = {
    UserId: ShiftApp.userData.value.UserId,
  };

  logs.session
  .filter(open => open.SessionToken)
  .take(1)
  .subscribe((open) => {
    document.APAPI.RPCCall('GetUserReportTickets', requestPayload, (data) => {
      ShiftApp.userReportTickets.onNext(data);
    });
  });
};

export default getUserReportTickets;
