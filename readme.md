# Crypto Theme

## Rollout Process

### Windows + Linux Subsytem
```
cd components/library
npm install
cd ../
npm install
cd ../
npm install
```
#### To do a full build

This will setup the root directory to be served via any webserver as a standalone build, no server-side processing required.

```
npm run library
npm run components
```

#### To run a dev serve locally

You'll need 2 terminal windows, one for the components server and another for the theme itself.  Both terminals should browse to the root directory of the project before running the related commands.  This will run a dev server with hot reloading of the theme and components code.

*Components Server*
```
cd components
npm run dev
```

*Theme Server*
```
npm run library
npm run start:components
```

#### Remote Installs and Builds
Do the same as above but do NOT run the `npm run dev:server` command
Please remember to `sudo chown -R ubuntu:www-data` to the parent folder after install

## First time
If this is the first time you initiate the project, you'll have to run either `npm install` or `yarn install` depending on the package manager you have installed in your system.

**Notice** that the project expect **Node v7.7.4** or higher. But its possible that you won't have any problems building from a previous version.

